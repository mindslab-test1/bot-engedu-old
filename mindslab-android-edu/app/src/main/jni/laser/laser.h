/*! \file laser.h
 * \brief 단말내장형 음성인식용 LASER API 헤더파일
 *
 * PowerVoice 음성 인식용 LASER API 헤더 파일이다.
 * 본 API를 이용한 단말 내장 음성 인식은 아래와 같은 절차로 이루어진다.
 * \verbatim
    LASER* pxLASER = CreateLASER( "./LASERDB" );
    PrepareLASER( pxLASER, "vrec", 0 );

    while ( 1 )
    {
        ResetLASER( pxLASER );

        do
        {
            n = getAudioData( sbuf );
            ret = Recognize( pxLASER, sbuf, n, &nEvent, &NBest, aLaserResult );
        } while( ret == LASER_OK && 
                !(nEvent & LASER_EVENT_RESULT) &&
                !(nEvent & LASER_EVENT_REJECTION) &&
                !(nEvent & LASER_EVENT_TIMEOUT) &&
                !(nEvent & LASER_EVENT_BREAK); \endverbatim
    }

    UnprepareLASER( pxLASER );
    FreeLaser( pxLASER );

 * 상세한 내용은 사용자 문서를 참고한다.
 *
 * Written by KIYOUNG PARK (pkyoung@etri.re.kr)
 */

#ifndef	_LASER_H
#define	_LASER_H

#ifdef USE_FRONTEND_V2
#include "frontend_api_v2.h"
#else
#include "frontend_api.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined( _WIN32 ) || defined( _WIN32_CE )
#ifdef	_USRDLL
#define APITYPE __declspec (dllexport)
#else
#define APITYPE __declspec (dllimport)
#endif
#else
#define APITYPE
#endif

#define LASER_OK                    0
#define LASER_FAIL                  -1

#define LASER_OPTION_CREATEUV       0x00000001
#define LASER_OPTION_CREATEMREC     0x00000002
#define LASER_OPTION_CREATEKWS      0x00000004
#define LASER_OPTION_CREATENREC     0x00000008
#define LASER_OPTION_UTFRESULT      0x00001000
#define LASER_OPTION_DOPOSTUV       0x00002000
#define LASER_OPTION_KWSMODE        0x00004000 // only works when mode==5
#define LASER_OPTION_LOADCLSDEFBIN  0x00008000
#define LASER_OPTION_SAVELRECRESULT 0x00010000

#define LASER_EVENT_NOEVENT         0x0000
#define LASER_EVENT_RESULT          0x0001
#define LASER_EVENT_TIMEOUT         0x0002
#define LASER_EVENT_REJECTION       0x0004
#define LASER_EVENT_BREAK           0x0008
#define LASER_EVENT_DO_BACKTRACKING 0x0010
#define LASER_EVENT_NORESULT        0x0020

#define LASER_MAXRESULTCHARS        256

/// 생성된 LASER 객체를 저장하기 위한 타입정의
typedef     void    LASER;

typedef     struct _LASERRESULT LASERRESULT;

/// LASER에서 반환하는 인식결과를 담는 구조체
struct _LASERRESULT
{
	float fScore;       ///< 인식 결과에 대한 likelihood를 저장한다. 
	float fConfidence;  ///< 인식 결과에 대한 confidence measure를 저장한다. (not implemented)
	char szWord[LASER_MAXRESULTCHARS];    ///< 인식 결과를 저장한다.
};

/// LASER객체를 생성하고, 동작에 필요한 메모리를 할당하고,
/// 인식기의 기본 옵션을 세팅한다.
/// @return 생성된 객체의 포인터를 반환한다. 에러가 있으면 NULL을 반환
APITYPE	LASER *	CreateLASER( char *szBaseDir );

/// LASER객체를 생성하고, 동작에 필요한 메모리를 할당하고,
/// 인식기의 기본 옵션을 세팅한다.
/// @param a_flag 인식기에 세팅할 옵션으로 아래값을 논리 OR 하여 전달한다.
///  LASER_OPTION_CREATENREC, LASER_OPTION_CREATEMREC, LASER_OPTION_CREATEKWS, LASER_OPTION_CREATEUV
/// @return 생성된 객체의 포인터를 반환한다. 에러가 있으면 NULL을 반환
APITYPE	LASER *	CreateLASERExt( char *szBaseDir, unsigned int a_flag );

/// 생성된 LASER객체를 해제한다.
APITYPE	void 	FreeLASER( LASER *pxLASER );

/// 인식 도메인을 설정한다.
/// @param pxLASER 생성된 LASER 객체의 포인터
/// @param szLexicon 설정하고자하는 도메인의 이름
/// @param iMode 0으로 고정하여 사용한다.
APITYPE	int	 	PrepareLASER( LASER *pxLASER, char *szLexicon, int iMode );

/// 설정된 인식 도메인을 해제하고 메모리를 반환한다.
APITYPE	int	 	UnprepareLASER( LASER *pxLASER );
//
/// 인식 도메인을 해제하고 메모리를 반환한다.
APITYPE	int	 	UnprepareLASERExt( LASER *pxLASER, int iMode );

/// 발화 단위의 초기화를 수행한다. 
///
/// 새로운 발화에 대해서 인식을 수행하기 전에 호출해준다. 내부적으로 사용되는
/// 변수값을 모두 초기화한다.
/// @param pxLASER 생성된 LASER 객체의 포인터
APITYPE	int	 	ResetLASER( LASER *pxLASER );

/// 발화 단위의 초기화를 수행한다. 추가 옵션을 전달할 수 있다. 
///
/// 새로운 발화에 대해서 인식을 수행하기 전에 호출해준다. 내부적으로 사용되는
/// 변수값을 모두 초기화한다.
/// @param pxLASER 생성된 LASER 객체의 포인터
/// @param a_param 값이 0인 경우에는 기존의 ResetLASER함수와 동일, 1인경우에는 frontend는 초기화하지 않는다.
APITYPE	int	 	ResetLASERExt( LASER *pxLASER, int a_param );

/// 인식 모드를 설정한다.
/// 0: 주소인식
/// 2: 대용량 POI 인식
/// 5: KWS 인식 모드
APITYPE void SetRecModeLASER( LASER* pLASER, int mode );

/// 실제 인식을 수행한다.
///
/// 매 프레임마다 입력된 음성신호로부터 인식을 수행한다.
/// 입력신호의 길이가 너무 긴 경우 오류를 발생시킬 수 있다. 3200 이하를 권장.
/// @param pLASER 생성된 LASER 객체의 포인터
/// @param sbuf 입력된 음성신호의 포인터
/// @param slen 입력된 음성 신호의 길이
/// @param pnEvent LASER의 내부 이벤터를 저장할 변수의 포인터
/// @param nResultSize 인식결과의 개수를 저장할 변수의 포인터
/// @param szResult 인식결과를 저장할 변수의 포인터
/// @return 문제가 없으면 0을 에러가 있으면 -1을 반환한다.
APITYPE int Recognize( LASER* pLASER, short *sbuf, unsigned int slen,
               unsigned int *pnEvent, unsigned int *nResultSize,
               LASERRESULT *szResult );
/// 실제 인식을 수행한다.
///
/// 매 프레임마다 입력된 음성신호로부터 인식을 수행한다.
/// 입력신호의 길이가 160의 배수인 경우에 사용한다.
/// 길이가 긴 입력에대해서도 안정성이 보장된다.
/// @param pLASER 생성된 LASER 객체의 포인터
/// @param sbuf 입력된 음성신호의 포인터
/// @param slen 입력된 음성 신호의 길이
/// @param pnEvent LASER의 내부 이벤터를 저장할 변수의 포인터
/// @param nResultSize 인식결과의 개수를 저장할 변수의 포인터
/// @param szResult 인식결과를 저장할 변수의 포인터
/// @return 문제가 없으면 0을 에러가 있으면 -1을 반환한다.
APITYPE int Recognize160( LASER* pLASER, short *sbuf, unsigned int slen,
               unsigned int *pnEvent, unsigned int *nResultSize,
               LASERRESULT *szResult );

/// 실제 인식을 수행한다.
///
/// 매 프레임마다 추출된 특징벡터를 인가하여 인식을 수행한다.
/// @param pLASER 생성된 LASER 객체의 포인터
/// @param mfccP 추출된 특징벡터
/// @param mfccN 추출된 특징벡터의 길이
/// @param pnEvent LASER의 내부 이벤터를 저장할 변수의 포인터
/// @param nResultSize 인식결과의 개수를 저장할 변수의 포인터
/// @param szResult 인식결과를 저장할 변수의 포인터
/// @return 문제가 없으면 0을 에러가 있으면 -1을 반환한다.
APITYPE int RecognizeExt( LASER* pLASER, float *mfccP, unsigned int mfccN,
               unsigned int *pnEvent, unsigned int *nResultSize,
               LASERRESULT *szResult );

/// LASER내부의 프론트엔드객체의 포인터를 반환한다.
/// 이 포인터를 이용하여 프론트엔드의 옵션을 설정할 수 있다.
APITYPE LFrontEnd* GetFrontEnd( LASER* pLASER );


/// LASER의 옵션을 설정한다.
/// 현재 설정가능한 옵션의 목록은 매뉴얼을 참고한다.
APITYPE int SetOptionLASER( LASER* pLASER, char* key, char* val );

#ifdef __cplusplus
}
#endif

#endif
