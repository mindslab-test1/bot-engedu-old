LOCAL_PATH := $(call my-dir)
LIB_PATH := ~/git/elsa/android/app/src/lib.android
# $(error abi $(LOCAL_PATH))
include $(CLEAR_VARS)

LOCAL_MODULE    := voicecmd_laser 
LOCAL_SRC_FILES := \
	com_etri_voicecmd_LASER.c

LOCAL_LDLIBS := \
	-L$(SYSROOT)/usr/lib -llog \
	$(LIB_PATH)/liblaserapi.a \
	$(LIB_PATH)/libuv.a \
	$(LIB_PATH)/libasearch.a \
	$(LIB_PATH)/libbase.a \
	$(LIB_PATH)/libfrontend.a

# JNI headers
LOCAL_C_INCLUDES += $(JNI_H_INCLUDE) 

LOCAL_CFLAGS    := -g -O0 -fshort-wchar -fno-short-enums -fpic \
	-D__ARM_ARCH_5__ -D__ARM_ARCH_5T__ \
	-D__ARM_ARCH_5E__ -D__ARM_ARCH_5TE__ \
	-DANDROID

LOCAL_ARM_MODE := arm

LOCAL_PRELINK_MODULE := false
LOCAL_DISABLE_FATAL_LINKER_WARNINGS = true

include $(BUILD_SHARED_LIBRARY)

#LOCAL_CFLAGS    := $(LOCAL_CFLAGS) -DBUILD_EXECUTABLE
#include $(BUILD_EXECUTABLE) 
