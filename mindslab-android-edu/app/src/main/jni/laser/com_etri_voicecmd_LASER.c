#include <stdio.h>
#include <stdlib.h>
#include <jni.h>
#include <string.h>

#include "laser.h"
#include "frontend_api.h"

static LASER 		*laserP = NULL;
static LFrontEnd* pFront = NULL;
static LASERRESULT	aLASERResult[10];
static unsigned int NBest;

jbyteArray cstr2jbyteArray( JNIEnv *env, const char *nativeStr)
{
	jbyteArray javaBytes;
	int len = strlen( nativeStr );
	javaBytes = (*env)->NewByteArray(env, len);
	(*env)->SetByteArrayRegion( env, javaBytes, 0, len, (jbyte *) nativeStr);
	return javaBytes;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Create
(JNIEnv *env, jobject thiz, jstring szBaseDir )
{
	const char *p = NULL;
	p = (*env)->GetStringUTFChars( env, szBaseDir, 0 );
	
	if( p != NULL )
	{
		laserP = CreateLASER( (char *)p );
        pFront = GetFrontEnd( laserP );
		(*env)->ReleaseStringUTFChars( env, szBaseDir, p );
	}

	if( laserP == NULL )
		return -1;

	return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_CreateExt
(JNIEnv *env, jobject thiz, jstring szBaseDir, jint flag )
{
	const char *p = NULL;
	p = (*env)->GetStringUTFChars( env, szBaseDir, 0 );
	
	if( p != NULL )
	{
		laserP = CreateLASERExt( (char *)p, flag );
        pFront = GetFrontEnd( laserP );
		(*env)->ReleaseStringUTFChars( env, szBaseDir, p );
	}

	if( laserP == NULL )
		return -1;

	return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_SetRecMode
(JNIEnv *env, jobject thiz, jint mode )
{
    SetRecModeLASER( laserP, mode );
	return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_SetOption
(JNIEnv *env, jobject thiz, jstring key, jstring val )
{
	char *p= NULL;
	char *q= NULL;

	p = (char*)((*env)->GetStringUTFChars( env, key, 0 ));
	q = (char*)((*env)->GetStringUTFChars( env, val, 0 ));
	
	if ( p!= NULL && q!=NULL)
	{
        SetOptionLASER( laserP, p, q );
	}
    if ( p )
		(*env)->ReleaseStringUTFChars( env, key, p );
    if ( q )
		(*env)->ReleaseStringUTFChars( env, val, q );

	return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_SetLogFile
(JNIEnv *env, jobject thiz, jstring fname )
{
	char *p= NULL;

	p= (char*)(*env)->GetStringUTFChars( env, fname, 0 );
	
	if ( p!= NULL )
	{
        SetOptionLASER( laserP, "LASER_LOGFILENAME", p );
		(*env)->ReleaseStringUTFChars( env, fname, p );
	}

	return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_SetFrontEndOption
(JNIEnv *env, jobject thiz, jstring key, jstring val )
{
  char *pkey = NULL;
  char* pval = NULL;

  pkey = (char*)(*env)->GetStringUTFChars( env, key, 0 );
  pval = (char*)(*env)->GetStringUTFChars( env, val, 0 );

  if ( pkey != NULL && pval != NULL )
  {
    setOptionLFrontEnd( pFront, pkey, pval );
    (*env)->ReleaseStringUTFChars( env, key, pkey );
    (*env)->ReleaseStringUTFChars( env, val, pval );
  }

  return 0;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Free
(JNIEnv *env, jobject thiz )
{
	if( laserP != NULL )
		FreeLASER( laserP );

	return 0;		
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Prepare
(JNIEnv *env, jobject thiz, jstring szLexicon, jint iMode )
{
	int ret = -1;
	const char *p;

	if( laserP == NULL )
		return ret;

	p = (*env)->GetStringUTFChars( env, szLexicon, 0 );
	if( p != NULL )
	{
		ret = PrepareLASER( laserP, (char *) p, iMode );
		(*env)->ReleaseStringUTFChars( env, szLexicon, p );
	}
	return ret;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Unprepare
(JNIEnv *env, jobject thiz, jint mode )
{
	int ret = -1;

	if( laserP != NULL )
		ret = UnprepareLASERExt(laserP, mode);

	return ret;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Reset
(JNIEnv *env, jobject thiz )
{
	int ret = -1;

	if( laserP != NULL )
		ret = ResetLASER( laserP );

	aLASERResult[0].szWord[0] = '\0';	

	return ret;
}

JNIEXPORT jint JNICALL Java_com_etri_voicecmd_LASER_Recognize
(JNIEnv *env, jobject thiz, jint a_size, jshortArray arr)
{
    short* sbuf;
	int i, ret;
	unsigned int nEvent; 

	ret = 0;
	NBest = 10;
	nEvent = 0;
    sbuf = (*env)->GetShortArrayElements( env, arr, 0 );
	if( sbuf != NULL )
	{
        ret = Recognize( laserP, sbuf, a_size, &nEvent, &NBest, aLASERResult );
        ret = nEvent;
        /*
        if( (nEvent & LASER_EVENT_RESULT) ||
            (nEvent & LASER_EVENT_REJECTION) ||
            (nEvent & LASER_EVENT_TIMEOUT) ||
            (nEvent & LASER_EVENT_BREAK) )
        {
            ret = 1;
        }
        */
		(*env)->ReleaseShortArrayElements( env, arr, sbuf, 0 );
	}
	else 
        ret = -1;
	return ret;
}

JNIEXPORT jbyteArray JNICALL Java_com_etri_voicecmd_LASER_GetResult
(JNIEnv *env, jobject thiz, jint nBest )
{
    int i;
    char buf[2048];
    buf[0] = '\0';
    for ( i = 0 ; i < NBest ; i++ )
    {
        strcat( buf, aLASERResult[i].szWord );
        strcat( buf, " ; " );
    }
	return cstr2jbyteArray( env, buf );
}

