package ai.mindslab.eduapp.network;

import static android.R.attr.port;
import static android.provider.Contacts.SettingsColumns.KEY;
import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

import android.content.Context;
import android.preference.Preference;
import android.util.Log;

import ai.mindslab.eduapp.preference.PreferenceHelper;

/**
 * Classes for NetConfigInfo
 */
public final class NetConfigInfo {
  //PROFILE 설정
  private final static String profiles = "DEV";
//  private final static String profiles = "STG";
//  private final static String profiles = "PROD";

  /***********************************************************************************************
   * 설정 정보
   **********************************************************************************************/
  //protocal
//  public final static String PROTOCOL = "HTTP";
  public static String PROTOCOL = "HTTPS";

  public static String DEV_API_URL = "beta.mindslab.ai";/*
  private static String STG_API_URL = "beta.mindslab.ai";
  private static String PROD_API_URL = "mindslab.ai";*/

  public static Integer REST_PORT = 443;

  // Grpc connection server info settings.
  public static String DEV_GRPC_HOST_ADDRESS = "beta.mindslab.ai";
  /*  private static String STG_GRPC_HOST_ADDRESS = "beta.mindslab.ai";
    private static String PROD_GRPC_HOST_ADDRESS = "mindslab.ai";*/
  // GRPC Port
  public static Integer GRPC_PORT = 9901;
  // Profile image download url.
  private static final String PROFILE_IMAGE_URL = "/api/v1/SvcUser";
  // Chatbot Image Icon URL.
  public final static String CHATBOT_ICON_URL_PREFIX_128 = "https://cdn.mindslab.ai/icons/128";
  // Chatbot ScreenShot Image URL.(Ex:http://cdn.mindslab.ai/images/chatbots/{{name}}.png)
  public final static String CHATBOT_SCREENSHOT_URL_PREFIX = "https://cdn.mindslab.ai/images/chatbots";

  // API 공통 URI
  public final static String API_COMMON_URI = "/api/v1";

  public final static int TIMEOUT = 30; // 기본 30초
  public final static String CHARSET = "UTF-8";  // 기본 UTF-8

  public final static int RESCODE_EXCEPTION = 999; //Network 오류 시 초기설정 코드(Network Exception)

  //PUT, GET, POST, PATCH, DELETE
  public final static String HTTP_METHOD_PUT = "PUT";
  public final static String HTTP_METHOD_GET = "GET";
  public final static String HTTP_METHOD_POST = "POST";
  public final static String HTTP_METHOD_PATCH = "PATCH";
  public final static String HTTP_METHOD_DELETE = "DELETE";

  /**
   * Net config init.
   * @param context
   */
  public static int initNetConfig(Context context) {
    //Set rest http protocol type.
    boolean isHttps = PreferenceHelper.getBooleanKeyValue(context,
            PreferenceHelper.KEY_NET_REST_IS_HTTPS);
    if (isHttps == false) {
      PROTOCOL = "HTTPS";
    } else {
      PROTOCOL = "HTTP";
    }

    // Rest url & port.
    DEV_API_URL = PreferenceHelper.getStringKeyValue(context,
            PreferenceHelper.KEY_NET_REST_URL);
    if (DEV_API_URL == null || DEV_API_URL.length() == 0) {
      return -1;
    }

    /*
    //Set rest port.
    REST_PORT = PreferenceHelper.getIntKeyValue(context,
        PreferenceHelper.KEY_NET_REST_PORT);
    if (REST_PORT == null || REST_PORT == 0) {
      return -2;
    }*/

    // Grpc url & port.
    DEV_GRPC_HOST_ADDRESS = PreferenceHelper.getStringKeyValue(context,
            PreferenceHelper.KEY_NET_GRPC_URL);
    if (DEV_GRPC_HOST_ADDRESS == null || DEV_GRPC_HOST_ADDRESS.length() == 0) {
      return -3;
    }
    //Set grpc port.
    GRPC_PORT = PreferenceHelper.getIntKeyValue(context,
            PreferenceHelper.KEY_NET_GRPC_PORT);
    if (GRPC_PORT == null || GRPC_PORT == 0) {
      return -4;
    }

    return 0;
  }

  /**
   * Set http protocol.
   * @param isHttps
   */
  public static void setHttpProtocolType(Context context, boolean isHttps) {
    if (isHttps == false) {
      PROTOCOL = "HTTPS";
    } else {
      PROTOCOL = "HTTP";
    }

    //Set rest http protocol type.
    PreferenceHelper.setBooleanKeyValue(context, PreferenceHelper.KEY_NET_REST_IS_HTTPS, isHttps);
  }

  /**
   * REST API Url
   * @return rest api url stirng.
   */
  public static String getApiUrl() {
    String protocol = "";
    protocol += PROTOCOL.toLowerCase() + "://";
    protocol += DEV_API_URL;

    Log.d(TAG, "getApiUrl():" + protocol);
    return protocol;
  }

  /**
   * Set rest api url.
   * @param context
   * @param url
   * @param port
   */
  public static void setApiUrlPort(Context context, String url, int port) {
    DEV_API_URL = url;
    REST_PORT = port;

    //Set rest url.
    PreferenceHelper.setStringKeyValue(context, PreferenceHelper.KEY_NET_REST_URL, url);
    //Set rest port.
    PreferenceHelper.setIntKeyValue(context, PreferenceHelper.KEY_NET_REST_PORT, port);
  }

  /**
   * GRPC URL
   * @return grpc url string.
   */
  public static String getGrpcUrl() {
    Log.d(TAG, "getGrpcUrl():"  + DEV_GRPC_HOST_ADDRESS);
    return DEV_GRPC_HOST_ADDRESS;
  }

  /**
   * Set grpc url & port
   * @param url
   * @param port
   */
  public static void setGrpcUrlPort(Context context, String url, Integer port) {
    DEV_GRPC_HOST_ADDRESS = url;
    GRPC_PORT = port;

    //Set grpc url.
    PreferenceHelper.setStringKeyValue(context, PreferenceHelper.KEY_NET_GRPC_URL, url);
    //Set grpc port.
    PreferenceHelper.setIntKeyValue(context, PreferenceHelper.KEY_NET_GRPC_PORT, port);
  }

  /**
   * Profile Image Url prefix.
   * @return profile image url string.
   */
  public static String getProfileImageUrl() {
    String url = NetConfigInfo.getApiUrl() + PROFILE_IMAGE_URL;
    Log.d(TAG, "getProfileImageUrl() : " + url);
    return url;
  }
}
