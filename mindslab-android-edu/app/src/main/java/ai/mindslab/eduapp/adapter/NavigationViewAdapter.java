package ai.mindslab.eduapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ai.mindslab.eduapp.model.NavigationListItem;
import ai.mindslab.eduapp.view.NavigationListItemView;

/**
 * Classes for NavigationViewAdapter
 */
public class NavigationViewAdapter extends BaseAdapter {
  /* Context */
  private Context context;
  /* Navigatinos Item */
  private ArrayList<NavigationListItem> items;

  /**
   * Constructor
   * @param context base context.
   */
  public NavigationViewAdapter(Context context) {
    this.context = context;
  }

  @Override
  public int getCount() {
    if (this.items != null) {
      return this.items.size();
    }
    return 0;
  }

  @Override
  public Object getItem(int i) {
    return this.items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return 0;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {

    NavigationListItemView itemView;
    if (view != null && view instanceof NavigationListItemView) {
      itemView = (NavigationListItemView) view;
    } else {
      itemView = new NavigationListItemView(context).inflate(viewGroup);
    }
    itemView.setData(getItem(i));
    return itemView;
  }

  /**
   * User custom function.
   * @param items NavigationListItem type ArrayList.
   */
  public void setItems(ArrayList<NavigationListItem> items) {
    this.items = items;
    notifyDataSetChanged();
  }
}
