package ai.mindslab.eduapp.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.model.StoreListItem;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * Classes for StoreListItemView
 */
public class StoreListItemView extends RelativeLayout {
  /* Context */
  private Context context;
  /* UI Controls */
  private ImageView listItemIv;
  /* UI Controls */
  private TextView titleTv;
  /* UI Controls */
  private TextView contentTv;
  /* UI Controls */
  private TextView writerTv;

  /**
   * Constructor
   * @param context base context.
   */
  public StoreListItemView(Context context) {
    this(context, null);
  }

  public StoreListItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public StoreListItemView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    LayoutInflater.from(context).inflate(R.layout.main_list_item_view, this, true);
    initView(context);
  }

  public StoreListItemView inflate(ViewGroup parent) {
    StoreListItemView view =
        (StoreListItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.main_list_item_row, parent, false);
    return view;
  }

  /**
   * Setup view holder.
   */
  private void setupViewHolder() {
    listItemIv = (ImageView) findViewById(R.id.list_item_iv);
    titleTv = (TextView) findViewById(R.id.list_item_title);
    contentTv = (TextView) findViewById(R.id.list_item_content);
    writerTv = (TextView) findViewById(R.id.list_item_writer);
  }

  /**
   * Set layout data.
   * @param data menu item object.
   */
  public void setData(Object data) {
    if (data != null) {
      StoreListItem item = (StoreListItem) data;

      // icon image loader.
      ImageLoader imageLoader = ImageLoader.getInstance();
      imageLoader.loadImage(item.getImageUrl(), new SimpleImageLoadingListener() {
        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
          listItemIv.setImageBitmap(loadedImage);
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
          super.onLoadingFailed(imageUri, view, failReason);
          // get bitmap of the image
          Bitmap origBitmap = BitmapFactory.decodeResource(context.getResources(),
              R.drawable.ic_chatbot_mindslab_128);
          listItemIv.setImageBitmap(origBitmap);
        }
      });

      titleTv.setText(item.getBigName());
      contentTv.setText(item.getDescription());
      writerTv.setText(Html.fromHtml(String.format("<u>%s</u>", item.getSmallName())));

    }
  }

  /**
   * User custom function.
   * @param context base context.
   */
  private void initView(Context context) {
    this.context = context;
    setupViewHolder();
  }

}
