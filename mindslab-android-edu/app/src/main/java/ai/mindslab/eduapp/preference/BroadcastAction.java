package ai.mindslab.eduapp.preference;

/**
 * Broadcast Action define.
 */
public interface BroadcastAction {
  /**
   * 구매 Event 수신
   */
  static final String ACTION_CHATBOT_PURCHASE_RECEIVED = "ai.mindslab.eduapp.intent.action.ACTION_CHATBOT_PURCHASE_RECEIVED";
}
