package ai.mindslab.eduapp.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

public interface IGetServiceGroupCallback {
  void onGetServiceGropuList(java.util.List<elsa.facade.Dialog.ServiceGroup> list);
  void onError(String messsage);
}
