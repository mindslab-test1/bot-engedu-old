package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class HanActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#han");
        super.onCreate(savedInstanceState);

        setTitleBarText("한글 듣고 영어로 답하기");

        loadUrl();
    }

}
