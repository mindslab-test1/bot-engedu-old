package ai.mindslab.eduapp.model;

/**
 * Created by hybell on 2017. 2. 20..
 */

public class PropertiesIntegerItem extends PropertiesItemBase {
  /* Integer type property value */
  private String integerValue = "";

  /**
   * Constructor
   * @param type
   * @param keyValue
   * @param attrTitle
   */
  public PropertiesIntegerItem(int type, String keyValue, String attrTitle) {
    super(type, keyValue, attrTitle);
  }

  /**
   * Integer value gegtter.
   * @return integer value.
   */
  public String getIntegerValue() {
    return integerValue;
  }

  /**
   * Integer value setter.
   * @param integerValue integer value.
   */
  public void setIntegerValue(String integerValue) {
    this.integerValue = integerValue;
  }
}
