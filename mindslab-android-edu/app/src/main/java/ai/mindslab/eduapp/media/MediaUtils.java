package ai.mindslab.eduapp.media;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

/**
 * Classes for MediaUtils
 */
public class MediaUtils {
  private static final String TAG	= MediaUtils.class.getSimpleName();
  private static final int UNCONSTRAINED = -1;

  private MediaUtils() {}

  /**
   * 샘플 사이즈를 계산한다.
   * @param options
   * @param minSideLength
   * @param maxNumOfPixels
   * @return size
   */
  public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
    int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
    return computeRoundedSize(initialSize);
  }

  /**
   * 초기 샘플 사이즈를 계산한다.
   * @param options
   * @param minSideLength
   * @param maxNumOfPixels
   * @return size.
   */
  private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
    double w = options.outWidth;
    double h = options.outHeight;

    int lowerBound = (maxNumOfPixels == UNCONSTRAINED) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
    int upperBound = (minSideLength == UNCONSTRAINED) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));

    if (upperBound < lowerBound) {
      return lowerBound;
    }

    if ((maxNumOfPixels == UNCONSTRAINED) && (minSideLength == UNCONSTRAINED)) {
      return 1;
    } else if (minSideLength == UNCONSTRAINED) {
      return lowerBound;
    } else {
      return upperBound;
    }
  }

  /**
   * Rounded size 를 계산한다.
   * @param initialSize
   * @return size
   */
  private static int computeRoundedSize(int initialSize) {
    int roundedSize;

    if (initialSize <= 8 ) {
      roundedSize = 1;
      while (roundedSize < initialSize) {
        roundedSize <<= 1;
      }
    } else {
      roundedSize = (initialSize + 7) / 8 * 8;
    }

    return roundedSize;
  }

  /**
   * Resize Image
   * @param context
   * @param filePath
   * @param resizeWidth
   * @param resizeHeight
   * @return resize file name.
   */
  public static String resizeImage(Context context, String filePath, int resizeWidth, int resizeHeight) {
    Bitmap bitmap = null;
    BitmapFactory.Options options = new BitmapFactory.Options();
    options.inDither = false;
    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

    try {
      options.inSampleSize = 1;
      options.inJustDecodeBounds = true;

      BitmapFactory.decodeFile(filePath, options);

      if (options.outWidth * options.outHeight > resizeWidth * resizeHeight) {
        options.inSampleSize = computeSampleSize(options, resizeWidth, resizeWidth * resizeHeight);
      } else {
        options.inSampleSize = 1;
      }

      options.inJustDecodeBounds = false;

      bitmap = BitmapFactory.decodeFile(filePath, options);

      /*
      //Exif error disable : Markup 89 Error~~.
      int nOrientation = getExifOrientation(filePath);
      Log.d(TAG, "Image Orientation : " + nOrientation);

      if (nOrientation == 90 || nOrientation == 270) {
        bitmap = rotateImage(bitmap, nOrientation);
      }*/

      String fileName = getFileName(filePath);
      String dateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
      fileName = dateTime + ".jpg";

      if (saveImageToInternal(context, bitmap, fileName)) {
        return fileName;
      } else {
        return null;
      }
    } catch (OutOfMemoryError e) {
      Log.e(TAG, "OutOfMemoryError", e);
      return null;
    } catch (Exception e) {
      Log.e(TAG, "Exception", e);
      return null;
    } finally {
      if (bitmap != null) {
        bitmap.recycle();
        bitmap = null;
      }
    }
  }

  /**
   * Image Rotate
   * @param b
   * @param degrees
   * @return bitmap result image.
   */
  public static Bitmap rotateImage(Bitmap b, int degrees) {
    if (degrees != 0 && b != null) {
      Matrix m = new Matrix();
      m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);

      try {
        Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
        if (b != b2) {
          b.recycle();
          b = b2;
        }
      } catch (OutOfMemoryError e) {
        Log.e(TAG, "OutOfMemoryError", e);
      }
    }
    return b;
  }

  /**
   * Exif Orientation 정보를 가져온다.
   * @param filepath
   * @return degree value.
   */
  public static int getExifOrientation(String filepath) {
    int degree = 0;
    ExifInterface exif = null;

    try {
      exif = new ExifInterface(filepath);
    } catch (IOException e) {
      Log.e(TAG, "cannot read exif", e);
    }

    if (exif != null) {
      int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

      if (orientation != -1) {
        switch (orientation) {
          case ExifInterface.ORIENTATION_ROTATE_90:
            degree = 90;
            break;

          case ExifInterface.ORIENTATION_ROTATE_180:
            degree = 180;
            break;

          case ExifInterface.ORIENTATION_ROTATE_270:
            degree = 270;
            break;
        }
      }
    }

    return degree;
  }

  /**
   * 미디어 File Path 를 가져완다.
   * @param context
   * @param baseUri
   * @return file path.
   */
  public static String getMediaFilePath(Context context, Uri baseUri) {
    Cursor cursor = null;
    String filePath = "";

    try {
      cursor = context.getContentResolver().query(
          baseUri,
          new String[] {MediaStore.Images.ImageColumns.DATA},
          null,
          null,
          null
      );

      if (cursor.moveToFirst() && !cursor.isNull(0)) {
        filePath = cursor.getString(0);
      }
    } catch(Exception e) {
      Log.e(TAG, "Error", e);
    } finally {
      if(cursor != null) {
        cursor.close();
      }
    }

    return filePath;
  }

  public static boolean saveImageToInternal(Context context, Bitmap bitmap, String filename) {
    boolean success = false;
    FileOutputStream fos = null;

    try {
      fos = context.openFileOutput(filename, 0);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
      fos.flush();
      fos.close();
      success = true;
    } catch (IOException e) {
      Log.e(TAG, "IOException", e);
    } catch (Exception e) {
      Log.e(TAG, "Error", e);
    } finally {
      if (bitmap != null) {
        bitmap = null;
      }
    }

    return success;
  }

  /**
   * File name 을 가져온다.
   * @param filePath
   * @return get file name.
   */
  public static String getFileName(String filePath) {
    File file = new File(filePath);
    return file.getName();
  }

  /**
   * 내부 파일을 삭제한다.
   * @param context
   */
  public static void deleteInternalFile(Context context) {
    try {
      File dir = new File(context.getFilesDir().getAbsolutePath() + "/");
      File[] fileList = dir.listFiles();

      for (int i=0; i<fileList.length; i++) {
        fileList[i].delete();
      }
    } catch (Exception e) {
      Log.e(TAG, "Error", e);
    }
  }

  /**
   * Base64 Encoding
   * @param filepath
   * @return base64 string.
   */
  public static String fileToBase64String(String filepath) {
    File file = new File(filepath);
    String fileString = new String();
    FileInputStream inputStream = null;
    ByteArrayOutputStream byteOutStream = null;

    try {
      inputStream = new FileInputStream(file);
      byteOutStream = new ByteArrayOutputStream();

      int len = 0;
      byte[] buf = new byte[1024];

      while ((len = inputStream.read(buf)) != -1) {
        byteOutStream.write(buf, 0, len);
      }

      byte[] fileArray = byteOutStream.toByteArray();
      fileString = new String(Base64.encodeToString(fileArray, Base64.NO_WRAP));
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        inputStream.close();
        byteOutStream.close();
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }
    }

    return fileString;
  }

  /**
   * Round bitmap create.
   * @param bitmap bitmap image.
   * @return Round bitmap image.
   */
  public static Bitmap getRoundedBitmap(Bitmap bitmap)
  {
    if (bitmap == null) {
      return null;
    }

    // 정사각형 이미지 만들기 위한 size check.
    int minSize = Math.min(bitmap.getWidth(), bitmap.getHeight());

    final Bitmap output = Bitmap.createBitmap(minSize, minSize, Bitmap.Config.ARGB_8888);
    final Canvas canvas = new Canvas(output);

    final int color = Color.RED;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, minSize, minSize);
    final RectF rectF = new RectF(rect);

    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    canvas.drawOval(rectF, paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);

    // Very Important.(Memory)
    bitmap.recycle();

    return output;
  }
}

