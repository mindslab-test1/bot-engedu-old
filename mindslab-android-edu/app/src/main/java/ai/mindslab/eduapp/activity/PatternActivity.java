package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class PatternActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#pattern");
        super.onCreate(savedInstanceState);

        setTitleBarText("회화 패턴 연습");

        loadUrl();
    }

}
