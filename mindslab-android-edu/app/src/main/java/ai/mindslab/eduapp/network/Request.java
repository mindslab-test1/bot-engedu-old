package ai.mindslab.eduapp.network;

import org.json.simple.JSONObject;

import java.util.HashMap;

/**
 * 요청 파라메터
 */
public class Request extends HashMap<String, Object> {

  /**
   * Map을 JSON으로 변경한다.
   *
   * @return JSON 값으로 변경한다.
   */
  public JSONObject mapToJson() {
    JSONObject json = new JSONObject();
    json.putAll(this);
    return json;
  }

}
