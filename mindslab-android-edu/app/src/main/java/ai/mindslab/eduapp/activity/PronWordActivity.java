package ai.mindslab.eduapp.activity;
import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class PronWordActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#pronword");
        super.onCreate(savedInstanceState);

        setTitleBarText("발음 평가 (단어)");

        loadUrl();
    }

}
