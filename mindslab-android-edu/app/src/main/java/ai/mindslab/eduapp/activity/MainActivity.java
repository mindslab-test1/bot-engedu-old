package ai.mindslab.eduapp.activity;

import static ai.mindslab.eduapp.network.NetConfigInfo.CHATBOT_ICON_URL_PREFIX_128;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.adapter.StoreListAdapter;
import ai.mindslab.eduapp.chatclient.ServiceGroupFinderClient;
import ai.mindslab.eduapp.chatclient.IGetServiceGroupCallback;
import ai.mindslab.eduapp.chatclient.IMetadataGetter;
import ai.mindslab.eduapp.model.StoreListItem;
import ai.mindslab.eduapp.network.MarketPacket;
import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.network.Response;
import ai.mindslab.eduapp.preference.BroadcastAction;
import ai.mindslab.eduapp.preference.PreferenceHelper;
import ai.mindslab.eduapp.utils.FileUtil;
import ai.mindslab.eduapp.view.SimpleProgressDialog;
import elsa.facade.Dialog;
import im.delight.android.webview.AdvancedWebView;
import io.grpc.Metadata;

/*
 * Classes for Main activity.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    /* For log tag */
    private static final String TAG = MainActivity.class.getSimpleName();
    /* Context */
    private Context context;
    /* Subactivty close result */
    private static final int REQUEST_SUBACTIVITY_CLOSED = 10;
    /* ui cotrols */
    private SimpleProgressDialog progressDialog;
    ServiceGroupFinderClient serviceGroupFinderClient;
    private StoreListAdapter storeListAdapter;
    /* Chatbot getting list */
    private ArrayList<StoreListItem> storeListItems = new ArrayList<StoreListItem>();
    /* Rest packet */
    private MarketPacket packet;
    /* Purchase exist for list refresh */
//    private boolean isPurchased = false;

    private ListView mainListView;
    private AdvancedWebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mainListView = (ListView)findViewById(R.id.main_list_view);
        mainListView.setVisibility(View.INVISIBLE);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        //  웹 프로그램이 모바일 웹뷰인지 인식할 수 있게 하기위해
        String agentModified = mWebView.getSettings().getUserAgentString().concat(" " + getResources().getString(R.string.user_agent_string));
        mWebView.getSettings().setUserAgentString(agentModified);
        mWebView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");
//        mWebView.setListener(this, this);

        mWebView.loadUrl(getResources().getString(R.string.service_http_host) + "/web#main");

        context = MainActivity.this;

        // init net config.
        if (NetConfigInfo.initNetConfig(context) != 0) {
            showNetSettingDialog(false);
            return;
        }

        // Init send handler.
        packet = new MarketPacket(context);
        packet.setListener(onPacket);

        // Init ui controls
        initUiControls();

        checkPermmision();

        // added by kimho.
        // 이게 없으면 디렉토리가 없어서 에러
        copyAssetsToSdcard();

        // webview에 의해 사라지지 않더라도 무조건 7초후에는 사라지게
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    hideIntroView();
                }
            }, 7000);
    }

    /**
     * Copy laser files to sdcard.
     */
    private void copyAssetsToSdcard() {
        String laserDbPath = Environment.getExternalStorageDirectory().toString();
        if (laserDbPath == null || laserDbPath.length() == 0) {
            laserDbPath = getFilesDir().getAbsolutePath().toString();
        }
        Log.d(TAG, "copyAssetsToSdcard()..path:" + laserDbPath);

        AssetManager assetManager = getAssets();
        String[] files = null;
        String mkdir = null;
        try {
            files = assetManager.list("");
            //이미지만 가져올때 files = assetManager.list("image");

        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        for (int i = 0; i < files.length; i++) {
            Log.d("<<<<<<<<<< ", String.valueOf(files[i]));
            Log.d(" >>> file length ", String.valueOf(files.length));
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(files[i]);

                //폴더생성
                String str = Environment.getExternalStorageState();
                if (str.equals(Environment.MEDIA_MOUNTED)) {
                    mkdir = laserDbPath + "/Laser/LASERDB/";
                } else {
                    Environment.getExternalStorageDirectory();
                }
                File mpath = new File(mkdir);
                if (!mpath.isDirectory()) {
                    mpath.mkdirs();
                }
                //
                int lastIndex = files[i].lastIndexOf(".");
                if (files[i].substring(lastIndex + 1, files[i].length()).equals("bin")) {
                    Log.d(">>> file name ", files[i].replace("_", "."));
                    out = new FileOutputStream(laserDbPath + "/Laser/LASERDB/" + files[i].replace("_", "."));
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                }
            } catch (Exception e) {
                Log.e("tag", e.getMessage());

            }
        }
    }

    /**
     * Copy file.
     *dfadsfasdfasdf
     * @param in  input stream.
     * @param out output stream.
     */
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }

    }

    public void checkPermmision() {
        if (Build.VERSION.SDK_INT >= 23) {

            int permissionResult = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permissionResult == PackageManager.PERMISSION_DENIED) {


                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setTitle("권한이 필요합니다.")
                            .setMessage("이 기능을 사용하기 위해서는 단말기의 \"전화걸기\" 권한이 필요합니다. 계속하시겠습니까?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1000);
                                    }

                                }
                            })
                            .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(MainActivity.this, "기능을 취소했습니다.", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .create()
                            .show();
                }

                else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1000);
                }

            }


        }
                /* 사용자의 OS 버전이 마시멜로우 이하일 떄 */
        else {
            // nothing...
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1000) {

            /* 요청한 권한을 사용자가 "허용"했다면 인텐트를 띄워라
                내가 요청한 게 하나밖에 없기 때문에. 원래 같으면 for문을 돈다.*/
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:010-1111-2222"));
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    startActivity(intent);
//                }
            }
            else {
                Toast.makeText(MainActivity.this, "권한 요청을 거부했습니다.", Toast.LENGTH_SHORT).show();
            }

        }
    }



    /**
     * Initialize ui controls.
     */
    private void initUiControls() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        progressDialog = new SimpleProgressDialog(MainActivity.this);

    /* Add store list item. */
        ListView mainListView = (ListView) findViewById(R.id.main_list_view);
        storeListAdapter = new StoreListAdapter(this);
        mainListView.setAdapter(storeListAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "i > " + i);
                final ImageView imageView = (ImageView) view.findViewById(R.id.list_item_iv);
                final BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();

                // Save to selected chatbot icon.
                String selectedChatbotIconPath = FileUtil.getSelectedChatbotIconFilePath(MainActivity.this);
                FileUtil.writeFileWithBitmapData(selectedChatbotIconPath, bitmapDrawable.getBitmap());

                final StoreListItem storeListItem = storeListItems.get(i);
                Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_CHAT_VIEW_MODE, WebViewActivity.CHAT_VIEW_CHAT_MODE);
                intent.putExtra(WebViewActivity.EXTRA_SVC_GROUP_INFO, storeListItem);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //2017.02.08 Modify : 요청에 의해 SignOut 시 Login page 로 이동하도록 한다.
      /*Intent intent = new Intent();
      setResult(RESULT_OK, intent);
      finish();
      */
            // Back-ground 로 가도록 처리함.
            moveTaskToBack(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_SUBACTIVITY_CLOSED:
                if (RESULT_OK == resultCode) {
                    //2017.02.08 Modify : 요청에 의해 SignOut 시 Login page 로 이동하도록 한다.
          /*Intent intent = new Intent();
          setResult(RESULT_OK, intent);*/
                    finish();
                }
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String svcGroupName = "";

        if (id == R.id.nav_setting) {
            // Setting
//            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
//            startActivityForResult(intent, REQUEST_SUBACTIVITY_CLOSED);

            showNetSettingDialog(true);
        } else {
            if (id == R.id.about_us_mindslab) {
                svcGroupName = "chorong";
            } else if (id == R.id.nav_pronword) {
                svcGroupName = "word_pronunciation";
            } else if (id == R.id.nav_pronsentence) {
                svcGroupName = "sentence_pron";
            } else if (id == R.id.nav_syntax) {
                svcGroupName = "grammar";
            } else if (id == R.id.nav_loud) {
                svcGroupName = "read_aloud";
            } else if (id == R.id.nav_pic) {
                svcGroupName = "picture";
            } else if (id == R.id.nav_explain) {
                svcGroupName = "english_writing";
            } else if (id == R.id.nav_han) {
                svcGroupName = "english_writing";
            } else if (id == R.id.nav_pattern) {
                svcGroupName = "pattern_drills";
            } else if (id == R.id.nav_conversation) {
                svcGroupName = "role_play";
            }
            openLearnActivity(svcGroupName, "");
        }

        return true;
    }

    /**
     * 챗봇 구매 내역 확인.
     */
//    private void requestPurchasesFindChatbot() {
//        // Show progressbar
//        progressDialog.show();
//
//        // Get Access-Token
//        String accessToken = PreferenceHelper.getStringKeyValue(getBaseContext(),
//                PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);
//
//        packet.reqPurchasesFindChatbot("resPurchasesFindChatbot", accessToken);
//    }

    /**
     * Get service group list.
     */
    private void getServiceGroupList(final List<HashMap> list) {
        // Create chat client session.
        serviceGroupFinderClient = new ServiceGroupFinderClient(getBaseContext(), new IMetadataGetter() {
            @Override
            // Meta data receiver
            public void onMetaData(Metadata md) {
                Log.d(TAG, md.toString());
            }
        });

        // Show progressbar
        progressDialog.show();

        // Get ServiceGroup
        serviceGroupFinderClient.getSenderAllServiceGroupList(false, list, new IGetServiceGroupCallback() {
            @Override
            public void onGetServiceGropuList(final java.util.List<elsa.facade.Dialog.ServiceGroup> list) {
                Log.i(TAG, list.toString());
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Iterator<Dialog.ServiceGroup> iterator = list.iterator();
                        while (iterator.hasNext()) {
                            Dialog.ServiceGroup svcGrpItem = iterator.next();

                            // Get svcGroup detail.
                            Dialog.ServiceGroupDetail svcDetail = svcGrpItem.getDetail();
                            Map<String, String> icons = svcDetail.getIconsMap();
                            String imageUrl = "";
                            if (icons.containsKey("default")) {
                                String imageName = icons.get("default").toString();

                                // Ex) String imageUrlPrefix = "http://cdn.mindslab.ai/icons/128/mindslab.png";
                                imageUrl = String.format("%s/%s", CHATBOT_ICON_URL_PREFIX_128, imageName);
                            }

                            StoreListItem storeListItem = new StoreListItem(imageUrl,
                                    svcGrpItem.getName(),
                                    svcGrpItem.getDescription(),
                                    svcGrpItem.getName());
                            storeListItem.setVersion(svcGrpItem.getVersion());

                            storeListItems.add(storeListItem);
                        }

                        storeListAdapter.setItems(storeListItems);

                        // Dismiss progressbar
                        progressDialog.dismiss();
                    }
                });
            }

            @Override
            public void onError(String messsage) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String errorMessage = context.getString(R.string.error_grpc_get_svcgroup_error);
                        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                        // Dismiss progressbar
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }

    /**
     * Http 전송 결과 값 Res 리스너
     */
    private MarketPacket.IPacketListener onPacket = new MarketPacket.IPacketListener() {
        @Override
        public void onApiResult(String resMethod, int resCode, Response resObj) {
            Log.d(TAG, "resMethod : " + resMethod);
            Log.d(TAG, "resCode : " + resCode);
            Log.d(TAG, "resObj : " + resObj);

            try {
                if(resCode == HttpURLConnection.HTTP_OK){
                    Method method = this.getClass().getDeclaredMethod(resMethod, Response.class);
                    method.invoke(this, resObj);
                } else {
                    if (resCode == HttpURLConnection.HTTP_NOT_FOUND
                            && "resFindOne".equals(resMethod) == true) {
                        Log.d(TAG, "404, resFindOne() No data.");
                    } else {
                        HashMap<String, Object> errorMap = (HashMap<String, Object>) resObj.get("error");
                        String errorMessage = errorMap.get("message").toString();
                        Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Dismiss progressbar
                progressDialog.dismiss();
            }
        }

        @Override
        public void onApiError(int resCode, String errorMessage) {
            // Dismiss progressbar
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_LONG).show();
        }

    };


    /**
     * Network setting.
     * @param isChangeMode true is previous data set.
     */
    private void showNetSettingDialog(boolean isChangeMode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View popupView = inflater.inflate(R.layout.popup_network_setting_dialog, null);
        final RadioButton httpsOption = (RadioButton) popupView.findViewById(R.id.radio_https_option);
        final RadioButton httpOption = (RadioButton) popupView.findViewById(R.id.radio_http_option);
        final EditText restUrlEt = (EditText) popupView.findViewById(R.id.rest_url_et);
        final EditText restPortEt = (EditText) popupView.findViewById(R.id.rest_port_et);
        final EditText grpcUrlEt = (EditText) popupView.findViewById(R.id.grpc_url_et);
        final EditText grpcPortEt = (EditText) popupView.findViewById(R.id.grpc_port_et);


        httpsOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                httpsOption.setChecked(true);
            }
        });

        httpOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                httpOption.setChecked(true);
            }
        });

        if (isChangeMode == true) {
            if ("https".equals(NetConfigInfo.PROTOCOL.toLowerCase())) {
                httpsOption.setChecked(true);
            } else {
                httpOption.setChecked(true);
            }

            restUrlEt.setText(NetConfigInfo.DEV_API_URL);
            restPortEt.setText("" + NetConfigInfo.REST_PORT);
            grpcUrlEt.setText(NetConfigInfo.DEV_GRPC_HOST_ADDRESS);
            grpcPortEt.setText("" + NetConfigInfo.GRPC_PORT);
        }

        builder.setView(popupView);
        builder.setMessage("네트워크 설정");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Save protocol type.
                if (httpOption.isChecked()) {
                    NetConfigInfo.setHttpProtocolType(context, true);
                } else {
                    NetConfigInfo.setHttpProtocolType(context, false);
                }

                // Save rest network info.
                if (restPortEt.getText().length() == 0) {
                    restPortEt.setText("0");
                }

                Integer restPort = Integer.parseInt(restPortEt.getText().toString());
                NetConfigInfo.setApiUrlPort(context, restUrlEt.getText().toString(), restPort);

                // Save grpc network info.
                if (grpcPortEt.getText().length() == 0) {
                    grpcPortEt.setText("0");
                }
                Integer grpcPort = Integer.parseInt(grpcPortEt.getText().toString());
                NetConfigInfo.setGrpcUrlPort(context, grpcUrlEt.getText().toString(), grpcPort);

                // App Restart.
                Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        });

        builder.create();
        builder.show();
    }


    public void hideIntroView() {
        ImageView loadingIntroImage = (ImageView) findViewById(R.id.imageLoading1);
        loadingIntroImage.setVisibility(View.INVISIBLE);
    }

    protected void openLearnActivity(String svcGroupName, String subName) {
        Intent intent = null;
        if (svcGroupName.equals("chorong")) {
            intent = new Intent(MainActivity.this, WebViewActivity.class);
        } else if (svcGroupName.equals("word_pronunciation")) {
            intent = new Intent(MainActivity.this, PronWordActivity.class);
        } else if (svcGroupName.equals("sentence_pron")) {
            intent = new Intent(MainActivity.this, PronSentenceActivity.class);
        } else if (svcGroupName.equals("grammar")) {
            intent = new Intent(MainActivity.this, SyntaxActivity.class);
        } else if (svcGroupName.equals("read_aloud")) {
            intent = new Intent(MainActivity.this, LoudActivity.class);
        } else if (svcGroupName.equals("picture")) {
            intent = new Intent(MainActivity.this, PicActivity.class);
        } else if (svcGroupName.equals("listen_and_choose")) {
            intent = new Intent(MainActivity.this, ExplainActivity.class);
        } else if (svcGroupName.equals("english_writing")) {
            intent = new Intent(MainActivity.this, HanActivity.class);
        } else if (svcGroupName.equals("pattern_drills")) {
            intent = new Intent(MainActivity.this, PatternActivity.class);
        } else if (svcGroupName.equals("role_play")) {
            if(subName.equals("2")) {
                intent = new Intent(MainActivity.this, Conversation2Activity.class);
            } else {
                intent = new Intent(MainActivity.this, ConversationActivity.class);
            }

        }
        StoreListItem storeListItem = new StoreListItem("", svcGroupName, "", svcGroupName);
        intent.putExtra(WebViewActivity.EXTRA_CHAT_VIEW_MODE, WebViewActivity.CHAT_VIEW_CHAT_MODE);
        intent.putExtra(WebViewActivity.EXTRA_SVC_GROUP_INFO, storeListItem);

        startActivity(intent);
    }


    private class JavaScriptInterface {

        @JavascriptInterface
        public void openLearn(String svcGroupName, String subName) {
            openLearnActivity(svcGroupName, subName);
        }

        @JavascriptInterface
        public void hideIntro() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideIntroView();
                }
            });
        }

        @JavascriptInterface
        public void openBrowser(String url) {
            if (AdvancedWebView.Browsers.hasAlternative(MainActivity.this)) {
                AdvancedWebView.Browsers.openUrl(MainActivity.this, url);
            }
        }

        @JavascriptInterface
        public void moveUrl(final String url) {

            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl(url);
                }
            });
        }

    }
}
