package ai.mindslab.eduapp.network;

import android.content.Context;
import android.util.Log;

/**
 * Classes for MarketPacket
 */
public class MarketPacket extends RestApiURI {
  /* For log tag */
  private final String TAG = MarketPacket.class.getSimpleName();
  /* Resthttp intstance */
  private NetRestHttp restHttp;
  /* 통신 설정 정보 정의 */
  private String restApi;

  /**
   * Constructor
   * @param context
   */
  public MarketPacket(Context context) {
    this.context = context;

    restApi = NetConfigInfo.getApiUrl();
    Log.d(TAG, restApi);

    restHttp = new NetRestHttp();
    restHttp.setListener(onHttpResListener);
  }

  /**
   * 챗봇 구매 내역 확인.
   * @param method
   * @param accessToken
   */
  public void reqPurchasesFindChatbot(String method, String accessToken) {
    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCPURCHASES_FIND_CHATBOTNAMES + accessToken,
        null);
  }

  /**
   * 챗봇 구매.
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqPurchasesChatbot(String method, String accessToken, Request reqObj) {
    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCPURCHASES_PURCHASES_CHATBOT + accessToken,
        reqObj.mapToJson());

  }

  /**
   * 구매한 사용자 download count
   * @param method
   * @param accessToken
   * @param svcGroup
   */
  public void reqPurchasesGetDownloadCount(String method, String accessToken, String svcGroup) {
    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCPURCHASES_GETDOWNLOADCOUNT + "?access_token=" + accessToken
            + "&name=" + svcGroup,
        null);
  }

  /**
   * 구매한 chatbot check
   * @param method
   * @param accessToken
   * @param svcGroup
   */
  public void reqPurchasesCheckPurchased(String method, String accessToken, String svcGroup) {
    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCPURCHASES_CHECK_PURCHASED_CHATBOT + "?access_token=" + accessToken
            + "&chatbotName=" + svcGroup,
        null);
  }

  /**
   * 리뷰 조회.
   * @param method
   * @param accessToken
   * @param userId
   * @param svcGroup
   */
  public void reqReviewList(String method,
                            String accessToken,
                            String userId,
                            String svcGroup) {
    // Make params.
    String params = makeReviewListParam(accessToken, userId, svcGroup);

    // Send http post.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCREVIEWS_FILTER + params,
        null);
  }

  /**
   * Make reqReviewList params.
   * @param accessToken
   * @param userId
   * @param svcGroup
   * @return result params string.
   */
  private String makeReviewListParam(String accessToken, String userId, String svcGroup) {
    String params;

    // Ex:{"where":{"chatbotName":"wiki"}}
    params = String.format("?filter={\"where\":{\"chatbotName\":\"%s\"}}&access_token=%s",
        svcGroup, accessToken);

    return params;
  }

  /**
   * 리뷰 저장
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqReviewSave(String method, String accessToken, Request reqObj) {
    // Send http post.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCREVIEWS_SAVE + accessToken,
        reqObj.mapToJson());
  }


  /**
   * 개인 리뷰 조회
   * @param method
   * @param accessToken
   * @param svcGroup
   * @param svcGroupVersion
   */
  public void reqFindReview(String method,
                            String accessToken,
                            String svcGroup,
                            String svcGroupVersion) {
    // Make params.
    String params = makeFindReviewParam(accessToken, svcGroup, svcGroupVersion);

    // Send http post.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCREVIEWS_FIND_REVIEW + params,
        null);
  }

  /**
   * FindView Params
   * @param accessToken
   * @param svcGroup
   * @param svcGroupVersion
   * @return result params string.
   */
  private String makeFindReviewParam(String accessToken, String svcGroup, String svcGroupVersion) {
    String params;

    // Ex:url :/Reviews/findReview?chatbotName=meari&chatbotVersion=v0.1&access_token=fFDNlxxx"
    params = String.format("?chatbotName=%s&chatbotVersion=%s&access_token=%s",
        svcGroup, svcGroupVersion, accessToken);

    return params;
  }

  /**
   * 마켓리스트에서 모든 챗봇에 대한 평점과 review count수
   * @param method
   * @param accessToken
   */
  public void reqReviewFindTotalGrades(String method, String accessToken) {
    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCREVIEWS_FIND_TOTAL_GRADES,
        null);
  }

  /**
   * detail 챗봇 페이지에서 각 grade count 수
   * @param method
   * @param accessToken
   * @param svcGroup
   */
  public void reqReviewFindDetailGrades(String method, String accessToken, String svcGroup) {
    // Make params.
    String params = makeReviewFindDetailGradesParam(accessToken, svcGroup);

    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCREVIEWS_FIND_DETAIL_GRADES + params,
        null);
  }

  /**
   * Make ReviewFindDetailGrades params.
   * @param svcGroup
   * @param accessToken
   * @return Params string.
   */
  private String makeReviewFindDetailGradesParam(String accessToken, String svcGroup) {
    String params;

    params = String.format("?chatbotName=%s&access_token=%s", svcGroup, accessToken);

    return params;
  }
}
