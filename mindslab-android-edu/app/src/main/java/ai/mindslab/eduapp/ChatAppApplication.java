package ai.mindslab.eduapp;

import android.app.Application;
import android.content.res.Configuration;
import android.os.Environment;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;

/**
 * Classes for ChatAppApplication
 */
public class ChatAppApplication extends Application {
  // Image cache path.
  public static String imageCachePath = "/Android/data/ai.mindslab.eduapp/cache";

  @Override
  public void onCreate() {
    super.onCreate();

    // Image loader config
    // ImageLoader Initialize
    DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(false).resetViewBeforeLoading(true).cacheOnDisk(true)
        .imageScaleType(ImageScaleType.EXACTLY).build();

    File cacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + imageCachePath);

    if (cacheDir.exists() != true) {
      cacheDir.mkdirs();
    }

    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).threadPriority(Thread.MIN_PRIORITY)
        .defaultDisplayImageOptions(defaultOptions).threadPoolSize(3).memoryCacheSize(10 * 1024 * 1024).memoryCache(new WeakMemoryCache())
        .diskCache(new UnlimitedDiscCache(cacheDir)).denyCacheImageMultipleSizesInMemory().diskCacheFileNameGenerator(new Md5FileNameGenerator())
        .diskCacheSize(100 * 1024 * 1024).tasksProcessingOrder(QueueProcessingType.FIFO).build();

    ImageLoader.getInstance().init(config);
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
  }
}
