package ai.mindslab.eduapp.model;

/**
 * Created by hybell on 2017. 2. 20..
 */

public class PropertiesBooleanItem extends PropertiesItemBase {
  /* Boolean type property value */
  private Boolean booleanValue = false;

  /**
   * Constructor
   * @param type
   * @param keyValue
   * @param attrTitle
   */
  public PropertiesBooleanItem(int type, String keyValue, String attrTitle) {
    super(type, keyValue, attrTitle);
  }

  /**
   * Boolean value getter.
   * @return boolean value.
   */
  public Boolean getBooleanValue() {
    return booleanValue;
  }

  /**
   * Boolean value setter.
   * @param booleanValue boolean value.
   */
  public void setBooleanValue(Boolean booleanValue) {
    this.booleanValue = booleanValue;
  }
}
