package ai.mindslab.eduapp.chatclient;

import static android.R.id.empty;
import static android.R.id.list;

import com.google.common.util.concurrent.SettableFuture;
import com.google.protobuf.Empty;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.preference.PreferenceHelper;
import elsa.facade.Dialog;
import elsa.facade.Dialog.AccessFrom;
import elsa.facade.Dialog.AudioUtter;
import elsa.facade.Dialog.Caller;
import elsa.facade.Dialog.Caller.Builder;
import elsa.facade.Dialog.ServiceGroup;
import elsa.facade.Dialog.ServiceGroupList;
import elsa.facade.Dialog.Session;
import elsa.facade.Dialog.SessionKey;
import elsa.facade.Dialog.SessionStat;
import elsa.facade.Dialog.TextUtter;
import elsa.facade.ServiceGroupFinderGrpc;
import elsa.facade.ServiceGroupFinderGrpc.ServiceGroupFinderStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

/**
 * Classes for ServiceGroupFinderClient
 */
public class ServiceGroupFinderClient {
  /* For log tag */
  private final String TAG = ServiceGroupFinderClient.class.getSimpleName();
  /* Context */
  private Context context;
  /* ServiceGroup stub */
  private ServiceGroupFinderStub serviceGroupFinderStub;
  /* Stub channel */
  private ManagedChannel originalChannel;
  /* Service group */
  private String serviceGroup = "meari";
  /* Language type */
  public enum ChatLanguage {
    kor,
    eng
  }
  /* chat language type save */
  private ChatLanguage chatLanguage = ChatLanguage.eng;

  /**
   * Constructor
   */
  public ServiceGroupFinderClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = OkHttpChannelBuilder.forAddress(NetConfigInfo.getGrpcUrl(),
            NetConfigInfo.GRPC_PORT).usePlaintext(true).build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    serviceGroupFinderStub = ServiceGroupFinderGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * Get purchaes chatbot list.
   * @param isAll
   * @param chatbotList
   * @param svcGrpCallback
   */
  public void getSenderAllServiceGroupList(boolean isAll, final List<HashMap> chatbotList, final IGetServiceGroupCallback svcGrpCallback) {
    Dialog.ServiceGroupParams.Builder builder = Dialog.ServiceGroupParams.newBuilder();
    builder.setAll(isAll);
    if (isAll == false) {
      ArrayList<String> addedList = new ArrayList<String>();
      for (int i = 0; i < chatbotList.size(); i++) {
        HashMap info = (HashMap) chatbotList.get(i);

        if (info.containsKey("name") == true) {
          String chatbotName = info.get("name").toString();
          if (addedList.contains(chatbotName) == false) {
            Log.d(TAG, "getSenderAllServiceGroupList():" + chatbotName);
            addedList.add(chatbotName);
            builder.addNames(chatbotName);
          }
        }
      }
    }
    Dialog.ServiceGroupParams request = builder.build();

    serviceGroupFinderStub.getAllServiceGroups(request, new StreamObserver<ServiceGroupList>() {
      @Override
      public void onNext(ServiceGroupList responseData) {
        Log.d(TAG, "getSenderAllServiceGroupList:"+ responseData.getSvcGroupsList());
        svcGrpCallback.onGetServiceGropuList(responseData.getSvcGroupsList());
      }

      @Override
      public void onError(Throwable err) {
        Log.d(TAG, "getSenderAllServiceGroupList error:" + err.toString());
      }

      @Override
      public void onCompleted() {
        Log.d(TAG, "getSenderAllServiceGroupList completed");
      }
    });
  }

  /**
   * Get Service Group
   */
  public void getSenderServiceGroupList(final IGetServiceGroupCallback svcGrpCallback) {
    Empty.Builder builder = Empty.newBuilder();
    Empty empty = builder.build();

    serviceGroupFinderStub.getServiceGroups(empty ,new StreamObserver<ServiceGroupList>() {
      @Override
      public void onNext(ServiceGroupList responseData) {
        System.out.println("getServiceGroups:"+ responseData.getSvcGroupsList());
        svcGrpCallback.onGetServiceGropuList(responseData.getSvcGroupsList());
      }

      @Override
      public void onError(Throwable err) {
        System.out.println("getServiceGroups error:" + err.toString());
        svcGrpCallback.onError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("getServiceGroups completed");
      }
    });
  }

  /**
   * Get Service
   */
  public void getSenderServiceGroup(final String svcGroupName,
                                    final IGetService getServiceCallback) {
    // Get Access-Token
    String accessToken = PreferenceHelper.getStringKeyValue(context,
            PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);

    /* Set meta data */
    Metadata.Key<String> authToken =
            Metadata.Key.of("x-elsa-authentication-token", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> provider =
            Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

    Metadata header = new Metadata();
    header.put(authToken, accessToken);
    header.put(provider, "mindslab");
    serviceGroupFinderStub = MetadataUtils.attachHeaders(serviceGroupFinderStub, header);
    Log.d(TAG, "[GRPC Header]" + header.toString());

    /* Make Request Param */
    Dialog.ServiceGroupKey.Builder builder = Dialog.ServiceGroupKey.newBuilder();
    builder.setName(svcGroupName);
    Dialog.ServiceGroupKey sgKey = builder.build();

    /* Request getServiceGroup */
    serviceGroupFinderStub.getServiceGroup(sgKey ,new StreamObserver<ServiceGroup>() {
      @Override
      public void onNext(ServiceGroup sgDetail) {
        System.out.println("onNext getSenderServiceGroup:"+ sgDetail.toString());
        getServiceCallback.onGetServiceGropuDetail(sgDetail);
      }

      @Override
      public void onError(Throwable err) {
        System.out.println("onError getSenderServiceGroup:"+ err.toString());
        getServiceCallback.onError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("onCompleted getSenderServiceGroup");
      }
    });
  }

  /**
   * Get Service
   */
  public void getSenderUserAttributes(final String svcGroupName,
                                      final IUserAttributesCallback userAttrCallback) {
    // Get Access-Token
    String accessToken = PreferenceHelper.getStringKeyValue(context,
            PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);

    /* Set meta data */
    Metadata.Key<String> authToken =
            Metadata.Key.of("x-elsa-authentication-token", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> provider =
            Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

    Metadata header = new Metadata();
    header.put(authToken, accessToken);
    header.put(provider, "mindslab");
    serviceGroupFinderStub = MetadataUtils.attachHeaders(serviceGroupFinderStub, header);
    Log.d(TAG, "[GRPC Header]" + header.toString());

    /* Make Request Param */
    Dialog.ServiceGroupKey.Builder builder = Dialog.ServiceGroupKey.newBuilder();
    builder.setName(svcGroupName);
    Dialog.ServiceGroupKey sgKey = builder.build();

    /* Request getServiceGroup */
    serviceGroupFinderStub.getUserAttributes(sgKey, new StreamObserver<Dialog.ServiceGroupUserAttributes>() {
      @Override
      public void onNext(Dialog.ServiceGroupUserAttributes value) {
        System.out.println("onNext getDomainUserAttrsCount:"+ value.getDomainUserAttrsCount());
        System.out.println("onNext getDomainUserAttrsList:"+ value.getDomainUserAttrsList());
        System.out.println("onNext getDomainUserAttrsList:"+ value.toString());
        System.out.println("onNext getSvcGroup:"+ value.getSvcGroup());
        userAttrCallback.onGetUserAttributes(value);
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("onError userAttributes:"+ t.toString());
        userAttrCallback.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("onCompleted userAttributes");
      }
    });
  }
}

