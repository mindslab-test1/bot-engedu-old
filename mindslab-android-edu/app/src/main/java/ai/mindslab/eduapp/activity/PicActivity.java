package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class PicActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#pic");
        super.onCreate(savedInstanceState);

        setTitleBarText("그림보고 답하기");

        loadUrl();
    }

}
