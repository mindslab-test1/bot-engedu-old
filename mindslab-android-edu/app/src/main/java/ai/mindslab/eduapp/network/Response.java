package ai.mindslab.eduapp.network;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 응답 데이터
 */
public class Response extends HashMap<String, Object> {

  /**
   * 전송 데이터 String --> Map으로 변환
   * @param contents
   * @return map data.
   * @throws Exception
   */
  public Response strToMap(String contents) throws Exception {
    return jsonToMap(strToJson(contents));
  }

  /**
   * 전송 데이터 String --> JSONObject로 변환
   * @param contents
   * @return jsonobject.
   * @throws ParseException
   */
  public JSONObject strToJson(String contents) throws ParseException {
    return (JSONObject) new JSONParser().parse(contents);
  }

  /**
   * JSONObject를 Response 객체로 변환
   * @param jsonObj
   * @return response object.
   */
  public Response jsonToMap(JSONObject jsonObj) {
    Response param = new Response();
    if (jsonObj == null) return param;

    Set keySet = jsonObj.keySet();
    Iterator itr = keySet.iterator();
    while (itr.hasNext()) {
      String key = (String) itr.next();

      if (jsonObj.get(key) instanceof JSONArray) {
        List<HashMap> list = new LinkedList();
        JSONArray jsonArr = (JSONArray) jsonObj.get(key);
        for (Object obj : jsonArr) {
          JSONObject jsonObj2 = (JSONObject) obj;
          list.add(jsonToMap(jsonObj2)); //재귀 처리
        }
        param.put(key, list);
      } else {
        param.put(key, jsonObj.get(key));
      }
    }
    return param;
  }

}
