package ai.mindslab.eduapp.view;

/*
 * Created by hybell on 2016. 12. 27..
 */

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.model.ChatListItem;

/**
 * Classes for ChatMetaObject
 */
public abstract class ChatMetaObject {
  private static final String TAG = ChatMetaObject.class.getSimpleName();
  // Session id.
  protected long sessionId;
  // AccessFrom value.
  private int accessFromValue;
  // Message create time.
  protected String messageTime = null;
  // Utter 응답 메시지.
  protected String utterMessage = null;
  // STT 응답 메시지.in.text field message.
  protected String intextMessage = null;
  // Metaobject type.
  public ChatListItem.ChatItemType chatItemType;
  // Metabobject long click linstener.
  protected View.OnLongClickListener onLongClickListener = null;
  // SearchMode On.
  protected boolean searchModeOn = false;
  // Setting search keyword.
  protected String searchWord = "";

  /**
   * Long click listener setter.
   * @param onLongClickListener
   */
  public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
    this.onLongClickListener = onLongClickListener;
  }

  /**
   * This object 를 리턴한다.
   * @return this object.
   */
  protected ChatMetaObject getMe() {
    return this;
  }

  /**
   * AccessFromValue getter.
   * @return accessFrom value
   */
  public int getAccessFromValue() {
    return accessFromValue;
  }

  /**
   * AccessFromValue setter.
   * @param accessFromValue accessFrom value
   */
  public void setAccessFromValue(int accessFromValue) {
    this.accessFromValue = accessFromValue;
  }

  /**
   * SessionId getter.
   * @return session id.
   */
  public long getSessionId() {
    return sessionId;
  }

  /**
   * SessionId setter.
   * @param sessionId session id.
   */
  public void setSessionId(long sessionId) {
    this.sessionId = sessionId;
  }

  /**
   * Message time getter.
   * @return Message time string.
   */
  public String getMessageTime() {
    return messageTime;
  }

  /**
   * Message time setter.
   * @param messageTime Message time string.
   */
  public void setMessageTime(String messageTime) {
    this.messageTime = messageTime;
  }

  /**
   * Utter message string getter.
   * @return Utter message string
   */
  public String getUtterMessage() {
    return utterMessage;
  }

  /**
   * Utter message string Setter.
   * @param utterMessage Utter message string
   */
  public void setUtterMessage(String utterMessage) {
    this.utterMessage = utterMessage;
  }

  /**
   * Intext message getter.
   * @return Intext message string.
   */
  public String getIntextMessage() {
    return intextMessage;
  }

  /**
   * Intext message setter.
   * @param intextMessage Intext message string.
   */
  public void setIntextMessage(String intextMessage) {
    this.intextMessage = intextMessage;
  }

  /**
   * Search mode on getter.
   * @return true is search mode on.Else is off.
   */
  public boolean getSearchModeOn() {
    return searchModeOn;
  }

  /**
   * Search mode on setter.
   * @param searchModeOn true or false.
   */
  public void setSearchModeOn(boolean searchModeOn) {
    // Clear the search mode.
    this.searchModeOn = searchModeOn;
  }

  /**
   * Search word getter.
   * @return Search word string.
   */
  public String getSearchWord() {
    return searchWord;
  }

  /**
   * Search word setter.
   * @param searchWord Search word string.
   */
  public void setSearchWord(String searchWord) {
    if (searchWord == null || searchWord.trim().length() == 0) {
      return;
    }

    this.searchWord = searchWord;
  }

  /**
   * 검색 모드일 경우 Text Highlight 표기 수정함.
   * @param context application context.
   * @param message message string.
   * @param keyword search keyword.
   * @return SpannableStringBuilder instance.
   */
  protected SpannableStringBuilder getSpannableString(Context context, String message, String keyword) {
    SpannableStringBuilder sps = new SpannableStringBuilder();

    Log.i(TAG, "getSpannableString():" + message);
    int nextIndx = 0;
    int keywordLength = keyword.length();
    int totalCount = message.length();
    for (int i = 0; i < totalCount; i++) {
      // Spannable instance.
      SpannableString ss;

      // Find match index.
      int searchIndex = message.indexOf(keyword, nextIndx);
      Log.i(TAG, "nextIndx:" + nextIndx + ", matchIndex:" + searchIndex);
      if (searchIndex == -1) {
        ss = new SpannableString(message.substring(nextIndx));
        sps.append(ss);
        break;
      }

      // Normal String.
      String normal = message.substring(nextIndx, searchIndex);
      ss = new SpannableString(normal);
      sps.append(ss);
      Log.i(TAG, "normal[" + normal.length() + "]:" + normal);

      // Append keyword string.
      int colorValue = context.getResources().getColor(R.color.chatlist_bg);
      ss = new SpannableString(keyword);
      ss.setSpan(new BackgroundColorSpan(colorValue),
          0, keywordLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      sps.append(ss);
      // increase the next search offset.
      nextIndx = searchIndex + keywordLength;
    }

    return sps;
  }

  /**
   * Chatitem content 별로 text title 을 리턴한다.
   */
  public abstract String getChatTitle();

  /**
   * Search keyword.
   * text message only.
   * @param keyword
   */
  protected abstract void searchKeyword(String keyword) ;

  /**
   * Chat meta object 를 상위 View 에 Add 한다.
   * @param parent Parent view to add the view.
   * @param hasAlready True is the target view has already.
   */
  public abstract void addToParent(final LinearLayout parent, final boolean hasAlready);

}
