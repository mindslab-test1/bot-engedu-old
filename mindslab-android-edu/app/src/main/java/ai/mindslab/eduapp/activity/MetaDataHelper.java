package ai.mindslab.eduapp.activity;

import android.util.Base64;
import android.util.Log;

import java.util.Map;

import ai.mindslab.eduapp.model.DecodeMetaData;
import io.grpc.Metadata;

/*
 * Classes for helping meta data
 */
public class MetaDataHelper {
  /**
   * 수신 받은 메타 데이터를 디코딩한다.
   * @param receiveMetaData Metadata type 의 데이터.
   * @return DecodeMetaData object return.
   */
  static DecodeMetaData decodeReceiveMetaData(Metadata receiveMetaData) {
    String keyName;

    // In Data decode start.
    keyName = "in.sessionid";
    String inSessionid =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.lang";
    String inLang =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.samplerate";
    String inSamplerate =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.user.location.latitude";
    String inUserLocationLatitude =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.user.location.longitude";
    String inUserLocationLongitude =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.text";
    String inText =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "in.talk.origin";
    String inTalkOrigin =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));

    // Out Data decode start.
    keyName = "out.talk.proofread";
    String outTalkProofread =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.talk.domain";
    String outTalkDomain =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.talk.intention";
    String outTalkIntention =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.text";
    String outText =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.type";
    String outEmbedType =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.data.body";
    String outEmbedDataBody =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.data.url";
    String outEmbedDataUrl =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.data.style";
    String outEmbedDataStyle =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.data.mime";
    String outEmbedDataMime =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));
    keyName = "out.embed.data.samplerate";
    String outEmbedDataSamplerate =
        receiveMetaData.get(Metadata.Key.of(keyName, Metadata.ASCII_STRING_MARSHALLER));

    String decodedInSessionid = getDecodeBase64ToString(inSessionid);
    String decodedInLang = getDecodeBase64ToString(inLang);
    String decodedInSamplerate = getDecodeBase64ToString(inSamplerate);
    String decodedInUserLocationLatitude = getDecodeBase64ToString(inUserLocationLatitude);
    String decodedInUserLocationLongitude = getDecodeBase64ToString(inUserLocationLongitude);
    String decodedInText = getDecodeBase64ToString(inText);
    String decodedInTalkOrigin = getDecodeBase64ToString(inTalkOrigin);

    String decodedOutTalkProofread = getDecodeBase64ToString(outTalkProofread);
    String decodedOutTalkDomain = getDecodeBase64ToString(outTalkDomain);
    String decodedOutTalkIntention = getDecodeBase64ToString(outTalkIntention);
    String decodedOutText = getDecodeBase64ToString(outText);
    String decodedOutEmbedType = getDecodeBase64ToString(outEmbedType);
    String decodedOutEmbedDataBody = getDecodeBase64ToString(outEmbedDataBody);
    String decodedOutEmbedDataUrl = getDecodeBase64ToString(outEmbedDataUrl);
    String decodedOutEmbedDataStyle = getDecodeBase64ToString(outEmbedDataStyle);
    String decodedOutEmbedDataMime = getDecodeBase64ToString(outEmbedDataMime);
    String decodedOutEmbedDataSamplerate = getDecodeBase64ToString(outEmbedDataSamplerate);

    // For Test Print code.
    String text = "decoded"
        + "\nInSessionid :" + decodedInSessionid
        + "\nInLang :" + decodedInLang
        + "\nInSamplerate :" + decodedInSamplerate
        + "\nInUserLocationLatitude :" + decodedInUserLocationLatitude
        + "\nInUserLocationLongitude :" + decodedInUserLocationLongitude
        + "\nInText :" + decodedInText
        + "\nInTalkOrigin :" + decodedInTalkOrigin
        + "\nOutTalkProofread :" + decodedOutTalkProofread
        + "\nOutTalkDomain:" + decodedOutTalkDomain
        + "\nOutTalkIntention :" + decodedOutTalkIntention
        + "\nOutText:" + decodedOutText
        + "\nOutEmbedType :" + decodedOutEmbedType
        + "\nOutEmbedDataBody :" + decodedOutEmbedDataBody
        + "\nOutEmbedDataUrl :" + decodedOutEmbedDataUrl
        + "\nOutEmbedDataStyle :" + decodedOutEmbedDataStyle
        + "\nOutEmbedDataMime :" + decodedOutEmbedDataMime
        + "\nOutEmbedDataSamplerate :" + decodedOutEmbedDataSamplerate;
    Log.i("Decode Meta Data :", text);

    // Setting decoded meta data.
    DecodeMetaData resultDecodeMetaData = new DecodeMetaData();
    resultDecodeMetaData.setInSessionid(decodedInSessionid);
    resultDecodeMetaData.setInLang(decodedInLang);
    resultDecodeMetaData.setInSamplerate(decodedInSamplerate);
    resultDecodeMetaData.setInUserLocationLatitude(decodedInUserLocationLatitude);
    resultDecodeMetaData.setInUserLocationLongitude(decodedInUserLocationLongitude);
    resultDecodeMetaData.setInText(decodedInText);
    resultDecodeMetaData.setInTalkOrigin(decodedInTalkOrigin);

    resultDecodeMetaData.setOutTalkProofread(decodedOutTalkProofread);
    resultDecodeMetaData.setOutTalkDomain(decodedOutTalkDomain);
    resultDecodeMetaData.setOutTalkIntention(decodedOutTalkIntention);
    resultDecodeMetaData.setOutText(decodedOutText);
    resultDecodeMetaData.setOutEmbedType(decodedOutEmbedType);
    resultDecodeMetaData.setOutEmbedDataBody(decodedOutEmbedDataBody);
    resultDecodeMetaData.setOutEmbedDataUrl(decodedOutEmbedDataUrl);
    resultDecodeMetaData.setOutEmbedDataStyle(decodedOutEmbedDataStyle);
    resultDecodeMetaData.setOutEmbedDataMime(decodedOutEmbedDataMime);
    resultDecodeMetaData.setOutEmbedDataSamplerate(decodedOutEmbedDataSamplerate);

    return resultDecodeMetaData;
  }

  /**
   * Return the base64 decode string.
   * @param encBase64Data
   * @return Base64 decoded string.
   */
  static String getDecodeBase64ToString(String encBase64Data) {
    if (encBase64Data == null) return encBase64Data;
    return new String(Base64.decode(encBase64Data, Base64.DEFAULT));
  }


  /**
   * Map 형태의 metadata parsing.
   * @param receiveMetaData Map 형태의 metadata.
   * @return DecodeMetaData object return.
   */
  static DecodeMetaData decodeReceiveMetaDataWithMap(Map<String, String> receiveMetaData) {
    String keyName;

    // In Data decode start.
    keyName = "in.sessionid";
    String inSessionid = getValueForKey(receiveMetaData, keyName);
    keyName = "in.lang";
    String inLang = getValueForKey(receiveMetaData, keyName);
    keyName = "in.samplerate";
    String inSamplerate = getValueForKey(receiveMetaData, keyName);
    keyName = "in.user.location.latitude";
    String inUserLocationLatitude = getValueForKey(receiveMetaData, keyName);
    keyName = "in.user.location.longitude";
    String inUserLocationLongitude = getValueForKey(receiveMetaData, keyName);
    keyName = "in.text";
    String inText = getValueForKey(receiveMetaData, keyName);
    keyName = "in.talk.origin";
    String inTalkOrigin = getValueForKey(receiveMetaData, keyName);

    // Out Data decode start.
    keyName = "out.talk.proofread";
    String outTalkProofread = getValueForKey(receiveMetaData, keyName);
    keyName = "out.talk.domain";
    String outTalkDomain = getValueForKey(receiveMetaData, keyName);
    keyName = "out.talk.intention";
    String outTalkIntention = getValueForKey(receiveMetaData, keyName);
    keyName = "out.text";
    String outText = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.type";
    String outEmbedType = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.data.body";
    String outEmbedDataBody = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.data.url";
    String outEmbedDataUrl = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.data.style";
    String outEmbedDataStyle = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.data.mime";
    String outEmbedDataMime = getValueForKey(receiveMetaData, keyName);
    keyName = "out.embed.data.samplerate";
    String outEmbedDataSamplerate = getValueForKey(receiveMetaData, keyName);

    String decodedInSessionid = inSessionid;
    String decodedInLang = inLang;
    String decodedInSamplerate = inSamplerate;
    String decodedInUserLocationLatitude = inUserLocationLatitude;
    String decodedInUserLocationLongitude = inUserLocationLongitude;
    String decodedInText = inText;
    String decodedInTalkOrigin = inTalkOrigin;

    String decodedOutTalkProofread = outTalkProofread;
    String decodedOutTalkDomain = outTalkDomain;
    String decodedOutTalkIntention = outTalkIntention;
    String decodedOutText = outText;
    String decodedOutEmbedType = outEmbedType;
    String decodedOutEmbedDataBody = outEmbedDataBody;
    String decodedOutEmbedDataUrl = outEmbedDataUrl;
    String decodedOutEmbedDataStyle = outEmbedDataStyle;
    String decodedOutEmbedDataMime = outEmbedDataMime;
    String decodedOutEmbedDataSamplerate = outEmbedDataSamplerate;

    // For Test Print code.
    String text = "map meta decoded"
        + "\nInSessionid :" + decodedInSessionid
        + "\nInLang :" + decodedInLang
        + "\nInSamplerate :" + decodedInSamplerate
        + "\nInUserLocationLatitude :" + decodedInUserLocationLatitude
        + "\nInUserLocationLongitude :" + decodedInUserLocationLongitude
        + "\nInText :" + decodedInText
        + "\nInTalkOrigin :" + decodedInTalkOrigin
        + "\nOutTalkProofread :" + decodedOutTalkProofread
        + "\nOutTalkDomain:" + decodedOutTalkDomain
        + "\nOutTalkIntention :" + decodedOutTalkIntention
        + "\nOutText:" + decodedOutText
        + "\nOutEmbedType :" + decodedOutEmbedType
        + "\nOutEmbedDataBody :" + decodedOutEmbedDataBody
        + "\nOutEmbedDataUrl :" + decodedOutEmbedDataUrl
        + "\nOutEmbedDataStyle :" + decodedOutEmbedDataStyle
        + "\nOutEmbedDataMime :" + decodedOutEmbedDataMime
        + "\nOutEmbedDataSamplerate :" + decodedOutEmbedDataSamplerate;
    Log.i("Decode map meta :", text);

    // Setting decoded meta data.
    DecodeMetaData resultDecodeMetaData = new DecodeMetaData();
    resultDecodeMetaData.setInSessionid(decodedInSessionid);
    resultDecodeMetaData.setInLang(decodedInLang);
    resultDecodeMetaData.setInSamplerate(decodedInSamplerate);
    resultDecodeMetaData.setInUserLocationLatitude(decodedInUserLocationLatitude);
    resultDecodeMetaData.setInUserLocationLongitude(decodedInUserLocationLongitude);
    resultDecodeMetaData.setInText(decodedInText);
    resultDecodeMetaData.setInTalkOrigin(decodedInTalkOrigin);

    resultDecodeMetaData.setOutTalkProofread(decodedOutTalkProofread);
    resultDecodeMetaData.setOutTalkDomain(decodedOutTalkDomain);
    resultDecodeMetaData.setOutTalkIntention(decodedOutTalkIntention);
    resultDecodeMetaData.setOutText(decodedOutText);
    resultDecodeMetaData.setOutEmbedType(decodedOutEmbedType);
    resultDecodeMetaData.setOutEmbedDataBody(decodedOutEmbedDataBody);
    resultDecodeMetaData.setOutEmbedDataUrl(decodedOutEmbedDataUrl);
    resultDecodeMetaData.setOutEmbedDataStyle(decodedOutEmbedDataStyle);
    resultDecodeMetaData.setOutEmbedDataMime(decodedOutEmbedDataMime);
    resultDecodeMetaData.setOutEmbedDataSamplerate(decodedOutEmbedDataSamplerate);

    return resultDecodeMetaData;
  }

  /**
   * Map 형태의 meta data value를 반환한다.
   * @param receiveMetaData meta map data.
   * @param keyName key name
   * @return Value for key
   */
  static String getValueForKey(Map<String, String> receiveMetaData, String keyName) {
    String value = null;
    if (receiveMetaData.containsKey(keyName)) {
      value = receiveMetaData.get(keyName);
    }

    return value;
  }
}
