package ai.mindslab.eduapp.network;

import java.util.LinkedHashMap;

/**
 * 마인즈랩 HTTP HEADER 설정
 */
public class NetHttpHeader extends LinkedHashMap<String,String> {

  /**
   * Constructior
   * @param accessToken
   */
  public NetHttpHeader(String accessToken){
    this.put("Content-type", "application/json; charset=" + NetConfigInfo.CHARSET);
    this.put("Accept", "application/json");
    this.put("x-elsa-authentication-provider", "mindslab");
    this.put("x-elsa-authentication-token", accessToken);
  }

}
