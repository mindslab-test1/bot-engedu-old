package ai.mindslab.eduapp.model;

import android.graphics.Bitmap;

import ai.mindslab.eduapp.view.ChatMetaObject;

/**
 * Classes for ChatListItem
 */
public class ChatListItem {
  /* Chat item type Define */
  public enum ChatItemType{
    CHAT_NONE_MESSAGE_TYPE,
    CHAT_SESSION_SEPARATOR_TYPE,
    CHAT_SEND_MESSAGE_TEXT_TYPE,
    CHAT_SEND_MESSAGE_IMAGE_TYPE,
    CHAT_RCV_MESSAGE_TEXT_TYPE,
    CHAT_RCV_MESSAGE_HTML_TYPE,
    CHAT_RCV_MESSAGE_AUDIO_TYPE,
    CHAT_RCV_MESSAGE_LINK_TYPE
  };

  /* Session Filter Define */
  public static final int SESSION_FILTER_WEB_TYPE = 0;
//  public static final int SESSION_FILTER_TEXT_MESSANGER_TYPE = 1;
  public static final int SESSION_FILTER_IPHONE_TYPE = 1;
  public static final int SESSION_FILTER_SPEAKER_TYPE = 2;
//  public static final int SESSION_FILTER_TELEPHONE_TYPE = 4;

  private long sessionId;
  private int accessFromValue;
  private ChatItemType type;
  private Bitmap userImage;
  private ChatMetaObject chatMetaObject;

  /**
   * Constuctor
   * @param sessionId session id.
   * @param type chatting message type.
   * @param userImage chatbot icon image url.
   * @param chatMetaObject ChatMetaObject type object.
   */
  public ChatListItem(long sessionId,
                      int accessFromValue,
                      ChatItemType type,
                      Bitmap userImage,
                      ChatMetaObject chatMetaObject) {
    this.type = type;
    this.userImage = userImage;
    this.sessionId = sessionId;
    this.accessFromValue = accessFromValue;
    chatMetaObject.setSessionId(sessionId);
    chatMetaObject.setAccessFromValue(accessFromValue);
    this.chatMetaObject = chatMetaObject;


  }

  /**
   * AccessFromValue getter.
   * @return accessFrom value
   */
  public int getAccessFromValue() {
    return accessFromValue;
  }

  /**
   * AccessFromValue setter.
   * @param accessFromValue accessFrom value
   */
  public void setAccessFromValue(int accessFromValue) {
    this.accessFromValue = accessFromValue;
  }

  /**
   * SessionId getter.
   * @return session id.
   */
  public long getSessionId() {
    return sessionId;
  }

  /**
   * SessionId setter.
   * @param sessionId session id.
   */
  public void setSessionId(long sessionId) {
    this.sessionId = sessionId;
  }

  /**
   * chatting message type getter.
   * @return chatting message integer type.
   */
  public ChatItemType getType() {
    return type;
  }

  /**
   * chatting message type setter.
   * @param type chatting message integer type.
   */
  public void setType(ChatItemType type) {
    this.type = type;
  }

  /**
   * chatbot icon image url getter.
   * @return image url string.
   */
  public Bitmap getUserImage() {
    return userImage;
  }

  /**
   * chatbot icon image url setter.
   * @param userImage image url string.
   */
  public void setUserImage(Bitmap userImage) {
    this.userImage = userImage;
  }

  /**
   * chat message string getter.
   * @return ChatMetaObject type object.
   */
  public ChatMetaObject getChatMetaObject() {
    return chatMetaObject;
  }

  /**
   * chat message string setter.
   * @param chatMetaObject ChatMetaObject type object.
   */
  public void setChatMetaObject(ChatMetaObject chatMetaObject) {
    this.chatMetaObject = chatMetaObject;
  }
}
