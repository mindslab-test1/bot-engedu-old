package ai.mindslab.eduapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.model.NavigationListItem;

/**
 * Classes for NavigationListItemView
 */
public class NavigationListItemView extends RelativeLayout {
  /* Context */
  private Context context;
  private TextView titleTv;
  private ImageView menuIcon;

  /**
   * Constructor
   * @param context base context.
   */
  public NavigationListItemView(Context context) {
    this(context, null);
  }

  public NavigationListItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public NavigationListItemView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    LayoutInflater.from(context).inflate(R.layout.navigation_list_item_view, this, true);
    initView(context);
  }

  public NavigationListItemView inflate(ViewGroup parent) {
    NavigationListItemView view =
        (NavigationListItemView) LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_list_item_row, parent, false);
    return view;
  }

  /**
   * Setup view holder.
   */
  private void setupViewHolder() {
    titleTv = (TextView) findViewById(R.id.title);
    menuIcon = (ImageView) findViewById(R.id.navi_menu_icon);
  }

  /**
   * Set layout data.
   * @param data menu item object.
   */
  public void setData(Object data) {
    NavigationListItem listItem = (NavigationListItem) data;

    NavigationListItem.MenuItemType menuType = listItem.getType();
    titleTv.setText(listItem.getText());
    if (menuType == NavigationListItem.MenuItemType.TEXT_ONLY_TYPE) {
      titleTv.setTextColor(getResources().getColor(R.color.goldenYellow));
      titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
      menuIcon.setVisibility(GONE);
    } else {
      titleTv.setTextColor(getResources().getColor(R.color.navigation_text));
      titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
      menuIcon.setImageResource(listItem.getImageResourceId());
    }
  }

  /**
   * User custom function.
   * @param context base context.
   */
  private void initView(Context context) {
    this.context = context;
    setupViewHolder();
  }
}
