package ai.mindslab.eduapp.model;

/**
 * Classes for PropertiesStringItem
 */
public class PropertiesStringItem extends PropertiesItemBase {
  /* String type property value */
  private String stringValue = "";

  /**
   * Constructor
   * @param type
   * @param keyValue
   * @param attrTitle
   */
  public PropertiesStringItem(int type, String keyValue, String attrTitle) {
    super(type, keyValue, attrTitle);
  }

  /**
   * String value getter.
   * @return string value.
   */
  public String getStringValue() {
    return stringValue;
  }

  /**
   * String value setter.
   * @param stringValue string value.
   */
  public void setStringValue(String stringValue) {
    this.stringValue = stringValue;
  }
}
