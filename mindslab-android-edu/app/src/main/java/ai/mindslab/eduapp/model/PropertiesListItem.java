package ai.mindslab.eduapp.model;

import java.util.ArrayList;

/**
 * Classes for PropertiesListItem.
 */
public class PropertiesListItem extends PropertiesItemBase {
  /* IntegerList type property value */
  private ArrayList<Object> arrayList = new ArrayList<Object>();

  /**
   * Constructor
   * @param type
   * @param keyValue
   * @param attrTitle
   */
  public PropertiesListItem(int type, String keyValue, String attrTitle) {
    super(type, keyValue, attrTitle);
  }

  /**
   * Array list getter.
   * @return arraylist
   */
  public ArrayList<Object> getArrayList() {
    return arrayList;
  }
}
