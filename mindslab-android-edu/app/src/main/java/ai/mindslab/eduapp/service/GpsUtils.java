package ai.mindslab.eduapp.service;

import android.content.Context;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

/**
 * Gps utils
 */
public class GpsUtils {
  private Context context = null;

  public GpsUtils(Context context) {
    this.context = context;
  }

  /**
   * Gps enable check.
   * @return false is disabled.
   */
  public Boolean isSettingsLocationSourceEnabled() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
      String providers = Settings.Secure.getString(context.getContentResolver(),
          Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
      if (TextUtils.isEmpty(providers)) {
        return false;
      }
      return providers.contains(LocationManager.GPS_PROVIDER);
    } else {
      final int locationMode;
      try {
        locationMode = Settings.Secure.getInt(context.getContentResolver(),
            Settings.Secure.LOCATION_MODE);
      } catch (Settings.SettingNotFoundException e) {
        e.printStackTrace();
        return false;
      }
      switch (locationMode) {

        case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
        case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
          return true;
        case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
        case Settings.Secure.LOCATION_MODE_OFF:
        default:
          return false;
      }
    }
  }
}
