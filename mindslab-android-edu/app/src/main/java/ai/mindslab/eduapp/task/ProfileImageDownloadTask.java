package ai.mindslab.eduapp.task;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import java.io.File;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.media.MediaUtils;
import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.preference.PreferenceHelper;

/**
 * Classes for ProfileImageDownloadTask
 */
public class ProfileImageDownloadTask extends AsyncTask<String, Void, Boolean> {
  private static final String TAG = ProfileImageDownloadTask.class.getSimpleName();

  /* Context */
  private Context context;
  /* Listener */
  private OnCompleteListener onCompleteListener;
  /* Target imageview */
  private ImageView targetImageView;
  /* Target bitmap */
  private Bitmap convertBitmap;

  /**
   * Constructor
   * @param context
   * @param targetImageView
   * @param convertBitmap
   */
  public ProfileImageDownloadTask(Context context,
                                  ImageView targetImageView, Bitmap convertBitmap) {
    this.context = context;
    this.targetImageView = targetImageView;
    this.convertBitmap = convertBitmap;
  }

  @Override
  protected Boolean doInBackground(String... urls) {
    try {
      loadProfileImage();
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Load profile image.
   */
  private void loadProfileImage() {
    // Get avatarImgSrc
    String avatarImgSrc = PreferenceHelper.getStringKeyValue(context,
        PreferenceHelper.KEY_LOGIN_USER_AVATAR_IMG_SRC);

    Log.d(TAG, "avatarImgSrc:" + avatarImgSrc);

    Bitmap origBitmap = null;
    Bitmap roundedBitmap = null;

    if (avatarImgSrc != null && avatarImgSrc.length() > 0) {
      // Profile image getting.
      String imageUrl = NetConfigInfo.getProfileImageUrl();
      imageUrl +=  "/" + avatarImgSrc;
      Log.d(TAG, "Profile image url:" + imageUrl);

      ImageLoader imageLoader = ImageLoader.getInstance();
      File imageFile = imageLoader.getDiscCache().get(imageUrl);
      if (imageFile.exists()) {
        imageFile.delete();
      }
      MemoryCacheUtils.removeFromCache(imageUrl, imageLoader.getMemoryCache());
      origBitmap = ImageLoader.getInstance().loadImageSync(imageUrl);
      roundedBitmap = MediaUtils.getRoundedBitmap(origBitmap);
    } else {
      // get bitmap of the image
      origBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.img_profile_light);
      roundedBitmap = MediaUtils.getRoundedBitmap(origBitmap);
    }

    // Set targetBitmap
    convertBitmap = roundedBitmap;
    if (onCompleteListener != null) {
      onCompleteListener.onComplete(roundedBitmap);
    }

    Log.d(TAG, "convertBitmap setting");

    // Set targetimageView.
    if (targetImageView != null) {
      targetImageView.setImageBitmap(roundedBitmap);
      Log.d(TAG, "targetImageView setting");
    }
  }

  /**
   * Complete listener
   * @param onCompleteListener
   */
  public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
    this.onCompleteListener = onCompleteListener;
  }

  /**
   * 수행 결과를 받는  정보를 UI 리스너에 전달 하기 위함이다.
   */
  public interface OnCompleteListener {
    void onComplete(Bitmap roundBitmap);
    void onError(String errorMessage);
  }
}
