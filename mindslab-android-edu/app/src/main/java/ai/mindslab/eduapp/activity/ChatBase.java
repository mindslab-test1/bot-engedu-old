package ai.mindslab.eduapp.activity;

/*
 * Created by hybell on 2016. 12. 25..
 */

import static android.R.attr.data;
import static android.R.attr.duration;
import static android.R.attr.type;
import static android.R.id.message;

import com.google.protobuf.ByteString;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaPlayer.TrackInfo;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import android.os.Handler;

import com.etri.voicecmd.LASER;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.chatclient.AsyncClient;
import ai.mindslab.eduapp.chatclient.IAudioCallback;
import ai.mindslab.eduapp.chatclient.IAudioTextCallback;
import ai.mindslab.eduapp.chatclient.IMetadataGetter;
import ai.mindslab.eduapp.media.G711;
import ai.mindslab.eduapp.media.MusicPlayer;
import ai.mindslab.eduapp.model.DecodeMetaData;
import ai.mindslab.eduapp.view.SimpleProgressDialog;
import elsa.facade.Dialog;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

/*
 * Classes for chat base activity.
 */
public abstract class ChatBase extends AppCompatActivity {
    private static final String TAG = ChatBase.class.getSimpleName();

    // Audio parameters
    private static final int RECORDER_SAMPLERATE = 16000;
    private static final int RECORDER_CHANNEL_CONFIG = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final int RECORDER_BPP = 16;

    private int minBuffSizeInByte;
    private int minBuffSizeInShort;
    private int channels;
    private AudioRecord mAudioRecorder = null;
    private AudioTrack mAudioTrack = null;
    private MediaPlayer mMediaPlayer = null;
    private Thread recordingThread = null;
    private Thread playingThread = null;
    protected boolean isPlayingThreadEndCompleted = true;
    private boolean isRecording = false;
    private boolean isPlaying = false;

    // Policies
    private static final int SESSION_TIMEOUT = 600000;
    private static final int LISTEN_TIMEOUT = 10000;
    private static final int SILENCE_TIMEOUT = 2600;
    int sessionTimeout = 0;
    int listenTimeout = 0;
    int silenceTimeout = 0;
    int stateTimeout = 0;

    // states
    final int SLEEPING = 1;
    final int AWAKEN = 2;
    final int LISTENING = 3;
    final int QUERYING = 4;
    final int TALKING = 5;
    final int PLAYING = 6;
    final int LISTENING_PLAYING = 7;
    protected int chatbotState = SLEEPING;

    // Other members
    short readData_[] = null;
    short writeData_[] = null;
    int writePosition_ = 0;
    protected AsyncClient chatClient;
    StreamObserver<Dialog.AudioUtter> sender_;
    StreamObserver<Dialog.TextUtter> senderTextToAudio;
    boolean sending_;
    ArrayList<short[]> replies_;
    long prevTick_ = 0;
    String musicUrl_ = null;
    int prevHeadPosition_ = 0;
    private static LASER laser;

    // Music Player Control View.
    protected MusicPlayer musicPlayer = null;
    /* Progress Dialog */
    protected SimpleProgressDialog progressDialog;
    /* Context */
    protected Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 초기 음성 인식 Library 를 로드한다.
        checkLaserDatabase();
        initLaser();

        // 음성 수신 데이터 버퍼를 초기화 한다.
        replies_ = new ArrayList<short[]>();

        // Set musicplayer control.
        musicPlayer = new MusicPlayer(getBaseContext());
        musicPlayer.setCustomEventListener(musicPlayerEventListener);
    }

    /**
     * Check laser database.
     */
    protected void checkLaserDatabase() {
        String laserDbPath = Environment.getExternalStorageDirectory().toString();
        if (laserDbPath == null || laserDbPath.length() == 0) {
            laserDbPath = getFilesDir().getAbsolutePath().toString();
        }
        Log.d(TAG, "copyAssetsToSdcard()..path:" + laserDbPath);

        Runtime runtime = Runtime.getRuntime();
        Process process;
        String res = "-0-";
        try {
            String cmd = "ls " + laserDbPath + "/Laser/LASERDB";
            process = runtime.exec(cmd);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                Log.i(">>>", line);
            }
            // 명령문이 종료될 때까지 기다린다
            process.waitFor();

        } catch (Exception e) {
            e.fillInStackTrace();
            Log.e(">>>", "Unable to execute top command");
        }
    }

    /**
     * Initialize laser instance.
     */
    protected void initLaser() {

        String laserDbPath = Environment.getExternalStorageDirectory().toString();
        if (laserDbPath == null || laserDbPath.length() == 0) {
            laserDbPath = getFilesDir().getAbsolutePath().toString();
        }
        Log.d(TAG, "copyAssetsToSdcard()..path:" + laserDbPath);

        laser = new LASER();
        Boolean a = new File(laserDbPath + "/Laser/LASERDB").isDirectory();
        Log.i("file : ", a.toString());
        File[] b = new File(laserDbPath + "/Laser/LASERDB").listFiles();
        Log.i("list", Integer.toString(b.length));
        Log.i("LASER", laser.toString());
        int ret = laser.CreateExt(laserDbPath + "/Laser/LASERDB", 5);
        Log.i("ret", Integer.toString(ret));
        if (ret != 0)
            Log.i("LASER", "ERROR WHILE CREATELASER  " + ret);
        else
            Log.i("LASER", "INIT LASER DONE");
        ret = laser.SetOption("LASER_LOGFILENAME", laserDbPath + "/Laser/log.txt");
        if (ret == 0)
            Log.i("LASER", "SET LOGFILE DONE");
        else
            Log.i("LASER", "ERROR WHILE SET LOGFILE");

        Log.i("LASER", "INIT LASER ALL DONE");
        ret = laser.Prepare("kws", 5);
        if (ret != 0)
            Log.i("LASER", "ERROR WHILE PREPARE LASER FOR KWS");
        else
            Log.i("LASER", "PREPARE LASER FOR KWS DONE");

        Log.i("LASER", "RESET");
        laser.SetOption("LASER_OPTION_DOPOSTUV", "1");
        laser.SetOption("LASER_NBEST", "1");
        laser.SetOption("LASER_OPTION_KWSMODE", "1");
        laser.SetFrontEndOption("FRONTEND_OPTION_DOEPD", "0");
        laser.SetRecMode(5);
        laser.SetFrontEndOption("EPD_MAXLEN_INPUT", "500");
        Log.i("LASER", "RESET finish");
        laser.Reset();
    }

    /**
     * Setup chat client.
     */
    protected void setupChatClient(String serviceGroup) {
        Log.d(TAG, " ========== setupChatClient");
        if (chatClient != null) {
            Log.d(TAG, "========== setupChatClient  is not null");
            chatClient.close();
        }
        chatClient = new AsyncClient(getBaseContext(), new IMetadataGetter() {
            @Override
            // Meta data receiver
            public void onMetaData(Metadata md) {
                Log.i(TAG, "Metadata arrived ->" + md);
                // Decode meta data.
                final DecodeMetaData decodeMetaData = MetaDataHelper.decodeReceiveMetaData(md);
                if (decodeMetaData == null) {
                    return;
                }

                // Set ui data.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean result = addChatData(decodeMetaData);
                    }
                });
            }
        });

        // Set Service Group.
        chatClient.setServiceGroup(serviceGroup);

        // Connect client
        chatClient.open();
    }

    // Music Player Control View.
    private MusicPlayer.OnCustomEventListener musicPlayerEventListener = new MusicPlayer.OnCustomEventListener() {
        @Override
        public void onStartPlayer(String nextMusic) {
            Log.i(TAG, "onStartPlayer() nextMusic:" + nextMusic);
            Toast.makeText(getBaseContext(), nextMusic, Toast.LENGTH_LONG).show();

            startMediaPlayer(nextMusic);
        }

        @Override
        public void onStopPlayer() {
            Log.i(TAG, "onStopPlayer()");

            stopMediaPlayer();
        }
    };

    private double timeElapsed = 0;
    private double finalTime = 0;
    private Handler durationHandler = new Handler();
    /**
     * handler to change seekBarTime of MusicPlayer.
     */
    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {

            if (mMediaPlayer == null || mMediaPlayer.isPlaying() == false) {
                Log.d(TAG, "Update seekbar thread exit.");
                return;
            }

            // get current position
            timeElapsed = mMediaPlayer.getCurrentPosition();
            //set seekbar progress
            Log.i(TAG, "timeElapsed:" + timeElapsed);
            musicPlayer.updateSeekBar((int) timeElapsed);

            // set time remaing
            double timeRemaining = finalTime - timeElapsed;
            long remainMinute = TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining);
            long remainMiliToSeconds = TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining);
            long remainMiliToMinutes = TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining);
            long reminuteSeconds = remainMiliToSeconds - TimeUnit.MINUTES.toSeconds(remainMiliToMinutes);
            String remainTime = String.format("%02d:%02d", remainMinute, reminuteSeconds);
            Log.d(TAG, "Remain time:" + remainTime);
            musicPlayer.updatePlayTime(remainTime);

            //repeat yourself that again in 100 miliseconds
            durationHandler.postDelayed(this, 100);
        }

    };

    /**
     * Start Media Player
     */
    protected void startMediaPlayer(String url) {
        //if(mAudioTrack!=null) {
        //	stopPlaying();
        //}

        if (url == null) {
            Log.d(TAG, "There is no next song.");
            return;
        }

        Log.d("o", "media player start");
        try {
            // Player control state 변경.
            if (mMediaPlayer != null) {
                //stopMediaPlayer();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Player control 을 정지시킨다.
                        musicPlayer.playAndPause(false);
                    }
                });
            }
            if (mMediaPlayer == null) {
                mMediaPlayer = new MediaPlayer();
            }
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepare(); // might take long! (for buffering, etc)
            mMediaPlayer.start();

            // TODO:Duration 정보를 가져오지 못하는 경우가 있음.
            // - 추후 MetaData 에서 해당 정보들을 가져와야 된다.
            try {
                finalTime = mMediaPlayer.getDuration();
            } catch (Exception ex) {
                finalTime = 600000;
            }

            // Player seekbar max value setting.
            musicPlayer.setPlaySeekBarMax((int) finalTime);
            // Player current position.
            timeElapsed = mMediaPlayer.getCurrentPosition();
            // seekbar.setProgress((int) timeElapsed);
            durationHandler.postDelayed(updateSeekBarTime, 100);

        } catch (Exception ex) {
            ex.printStackTrace();
            // TODO : 재생 불가 콘텐츠 발생시 예외 처리.
            //Toast.makeText(getBaseContext(), "재생할 수 없는 컨텐츠입니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Stop media player
     */
    protected void stopMediaPlayer() {
        if (mMediaPlayer != null) {
            Log.d("x", "media player stop/*");
            mMediaPlayer.stop();
            while (mMediaPlayer.isPlaying()) sleep(100);
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    /**
     * Begin sending stream
     */
    void beginAudioStream() {

        // audio to text
        if (isSpeakerOn() == false) {
            Log.d(TAG, "============= begintextStream" + chatClient.getSessionId().toString());
            sender_ = chatClient.getSenderAudioTextTalk(new IAudioTextCallback() {
                @Override
                public void onText(final String text) {
                    Log.d(TAG, "TEXT >>>" + text);
                    chatbotState = SLEEPING; // directly transit to sleeping
                    runOnUiThread(new Runnable() {
                        public void run() {
                            // Show progress.
                            //progressDialog.dismiss();
                            // Display Error Message.
                            // Listening dialog hide.
                            voiceListeningView(false);
                            // Resut Audio Stream Data.
                            boolean result = addAudioToTextTalkData(text);
                        }
                    });
                }

                @Override
                public void onError(String message) {
                    Log.d(TAG, "audio onAudioError" + message);
                    if (message.contains("CANCELLED")) {
                        return;
                    }

                    // Show progress.
                    //progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            // Display Error Message.
                            String errorMessage = getBaseContext().getString(R.string.error_grpc_connection_fail);
                            Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else {
            Log.d(TAG, "============= beginAudioStream" + chatClient.getSessionId().toString());
            if (chatClient != null) chatClient.open();
            // audio to audio
            sender_ = chatClient.getSenderAudioTalk(new IAudioCallback() {

                short[] ab = new short[minBuffSizeInShort];

                /** Audio incoming */
                @Override
                public void onAudio(ByteString utter) {
                    Log.d(TAG, "audio onAudio");
                    byte[] bytes = utter.toByteArray();
//          System.out.printf(">>>>>>>>>>>>>>>>>>>>> %d byte reached\n", bytes.length);
                    short data[] = new short[bytes.length];
                    for (int i = 0; i < bytes.length; i++) {
                        data[i] = (short) G711.ulaw2linear(bytes[i]);
                        //writeData_[writePosition_++] = data[i];
                        ab[writePosition_++] = data[i];
                        if (writePosition_ == minBuffSizeInShort) {
                            writePosition_ = 0;
                            replies_.add(ab);
                            ab = new short[minBuffSizeInShort];
                        }
                    }

                    //Add the length = 0 : 다음 wakeup 인식을 위해 state 변경을 한다.
                    if (bytes.length == 0) {
                        chatbotState = SLEEPING;
                        return;
                    }
                }

                /** Incoming stream end */
                @Override
                public void onAudioEnd() {
                    Log.d(TAG, "audio onAudioEnd");
                    if (writePosition_ > 0) {
                        replies_.add(ab);
                        ab = new short[minBuffSizeInShort];
                    }
                    runOnUiThread(new Runnable() {
                        public void run() {
                            // Show progress.
                            //progressDialog.dismiss();
                            // Listening dialog hide.
                            voiceListeningView(false);
                            // Add the audio meta data.
                            boolean result = addAudioTalkData();
                        }
                    });
                }


                public void onError(Throwable err) {
                    Log.d(TAG, " ============================== 111111111111111111111111111111111111111");
                    io.grpc.StatusRuntimeException e = (io.grpc.StatusRuntimeException) err;
                    Log.d(TAG, " ======= error         >>>>>>>>>>>>." + e.getStatus().toString() + ">>" + Status.NOT_FOUND
                    );
                    if (e.getStatus().getCode() == Status.NOT_FOUND.getCode()) {

                        Log.d(TAG, "~~~~~~~~~~~~~~~~~~~");
                        chatClient.close();
                        Log.d(TAG, "~~~~~~~~~~~~~~~~~~~" + chatClient.getSessionId().toString());
                        chatClient.open(AsyncClient.ChatLanguage.eng);
                        Log.d(TAG, "~~~~~~~~~~~~~~~~~~~" + chatClient.getSessionId().toString());

                    }
                }

                @Override
                public void onAudioError(String message) {
                    Log.d(TAG, " =============================22222222222222222222222");
                    Log.d(TAG, "audio onAudioError" + message);
                    if (message.contains("CANCELLED")) {
                        return;
                    }

                    // Show progress.
                    //progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            // Display Error Message.
                            Log.d(TAG, "=================================== ppp");
                            String errorMessage = getBaseContext().getString(R.string.error_grpc_connection_fail);
                            Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }

        sending_ = true;
    }

    protected void sendTextToAudioMessage(String sendMessage) {
        senderTextToAudio = chatClient.getSenderTextToAudioTalk(new IAudioCallback() {

            short[] ab = new short[minBuffSizeInShort];

            /** Audio incoming */
            @Override
            public void onAudio(ByteString utter) {
                Log.d(TAG, "audio onAudio");
                byte[] bytes = utter.toByteArray();
                System.out.printf(">>>>>>>>>>>>>>>>>>>>> %d byte reached\n", bytes.length);

                short data[] = new short[bytes.length];
                for (int i = 0; i < bytes.length; i++) {
                    data[i] = (short) G711.ulaw2linear(bytes[i]);
                    //writeData_[writePosition_++] = data[i];
                    ab[writePosition_++] = data[i];
                    if (writePosition_ == minBuffSizeInShort) {
                        writePosition_ = 0;
                        replies_.add(ab);
                        ab = new short[minBuffSizeInShort];
                    }
                }

                //Add the length = 0 : 다음 wakeup 인식을 위해 state 변경을 한다.
                if (bytes.length == 0) {
                    chatbotState = SLEEPING;
                    return;
                }
            }

            /** Incoming stream end */
            @Override
            public void onAudioEnd() {
                Log.d(TAG, "audio onAudioEnd");
                if (writePosition_ > 0) {
                    replies_.add(ab);
                    ab = new short[minBuffSizeInShort];
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        // Show progress.
                        //progressDialog.dismiss();
                        // Listening dialog hide.
                        voiceListeningView(false);
                        // Add the audio meta data.
                        boolean result = addAudioTalkData();
                    }
                });
            }

            @Override
            public void onAudioError(String message) {
                Log.d(TAG, "audio onAudioError" + message);
                if (message.contains("CANCELLED")) {
                    return;
                }

                // Show progress.
                //progressDialog.dismiss();
                runOnUiThread(new Runnable() {
                    public void run() {
                        // Display Error Message.
                        String errorMessage = getBaseContext().getString(R.string.error_grpc_connection_fail);
                        Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        Dialog.TextUtter.Builder builder = Dialog.TextUtter.newBuilder();
        builder.setUtter(sendMessage);
        Dialog.TextUtter utter = builder.build();
        senderTextToAudio.onNext(utter);
        senderTextToAudio.onCompleted();
    }

    /**
     * Finish sending stream
     */
    void finishAudioStream() {
        try {
            sending_ = false;
            //System.out.printf("finishAudioStream()..IN..sender completed\n");
            sender_.onCompleted();
        } catch (Exception e) {

        } finally {
            runOnUiThread(new Runnable() {
                public void run() {
                    // Listening dialog hide.
                    voiceListeningView(false);
                    // Show progress.
//        progressDialog.show();
                }
            });
        }
    }

    /**
     * start RECORD thread
     */
    protected void startRecording() {
        // 이미 실행중인 경우 두번 초기화를 방지한다.
        if (isRecording == true || recordingThread != null) {
            Log.d(TAG, "Already voice recording started!!");
            return;
        }

        minBuffSizeInByte = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNEL_CONFIG,
                RECORDER_AUDIO_ENCODING);
        if (minBuffSizeInByte < 8000) minBuffSizeInByte = 8000;
        minBuffSizeInShort = minBuffSizeInByte / 2;
        mAudioRecorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, RECORDER_SAMPLERATE,
                RECORDER_CHANNEL_CONFIG, RECORDER_AUDIO_ENCODING, minBuffSizeInByte * 4);
        channels = mAudioRecorder.getChannelCount();
        mAudioRecorder.startRecording();

        Log.d("XXXX", "min buf size:" + minBuffSizeInShort);

        readData_ = new short[minBuffSizeInShort];
        isRecording = true;

        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    recordLoop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "AudioRecord Thread");
        recordingThread.start();
    }

    /**
     * Record thread loop.
     *
     * @throws Exception
     */
    private void recordLoop() throws Exception {
        int read = 0;
        //FileOutputStream fos = new FileOutputStream("/mnt/sdcard/LASERDB/result.pcm");
        short max1 = 0;
        Log.d("o", "recorder thread start");
        while (isRecording) {
            // 음악 재생중일 경우 보이스 레코딩을 Skip 하는 예외 처리를 함.
            // 검색 모드일 경우.
            if (musicPlayer.getMusicPlayerState() == MusicPlayer.PLAYING_STATE
                    || getSearchMode() == true) {
                sleep(100);
                continue;
            }

            short max2 = 0;
            // should read short data when we use PCM 16
            read = mAudioRecorder.read(readData_, 0, minBuffSizeInShort);
      /*System.out.printf("%04X %04X %04X %04X %04X %04X %04X %04X \n",
       readData_[0], readData_[1], readData_[2], readData_[3],
       readData_[4], readData_[5], readData_[6], readData_[7]);*/
            if (AudioRecord.ERROR_INVALID_OPERATION == read) break;
            // recognizer
            ByteBuffer bb = ByteBuffer.allocate(minBuffSizeInByte);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            for (int i = 0; i < minBuffSizeInShort; i++) {
                bb.putShort(readData_[i]);
                if (max1 < readData_[i]) max1 = readData_[i];
                if (max2 < readData_[i]) max2 = readData_[i];
            }
            byte[] b = bb.array();
            for (int i = 0; i < 25; i++) {
                //System.arraycopy(b, i*320, bp_, 0, 320);
                //byte[] r = recognize(bp_, 160);
                short[] s = new short[160];
                System.arraycopy(readData_, i * 160, s, 0, 160);

                int r = laser.Recognize(160, s);
                //int r = recognize(bp_, 160);
                String keyword = null;
                if (r != 0) { // keyword detected
                    //Log.d(TAG, "If......................................r != 0");
                    //Modify :4->6 '오케이마인즈' keyword 인식하기 위함.
                    keyword = new String(laser.GetResult(1), "MS949").substring(0, 6).trim();
                    Log.d("keyword : ", keyword + "  " + new String(laser.GetResult(1), "MS949"));
                    laser.Reset();
                    Log.i(TAG, "result :" + r);
                    Log.d(TAG, minBuffSizeInShort + ":" + keyword + ":" + keyword.length() + ":" + sessionTimeout);
                    if (sessionTimeout < 0) {
                        Log.d(">>", "RECONNECT SILENT");
                        reconnectSilent();
                    }
                    sessionTimeout = SESSION_TIMEOUT;
                    //stopMediaPlayer();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Player control 을 정지시킨다.
                            musicPlayer.playAndPause(false);
                        }
                    });
                    processKey(keyword, max2);
                } else {
                    //Log.d(TAG, "Else......................................r == 0");
                    processKey(keyword, max2);
                }

            }

            // Send audio stream
            if (sending_) {
                Dialog.AudioUtter.Builder builder = Dialog.AudioUtter.newBuilder();
                ByteString bstring = ByteString.copyFrom(b);
                builder.setUtter(bstring);
                Dialog.AudioUtter utter = builder.build();
                sender_.onNext(utter);
            }
        }
        Log.d("x", "recorder thread stop");

        mAudioRecorder.stop();
        mAudioRecorder.release();
        mAudioRecorder = null;

        // Voice Listening Dialog Hide.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                voiceListeningView(false);
            }
        });
    }

    /**
     * Stop recording.
     */
    protected void stopRecording() {
        if (null != mAudioRecorder) {
            isRecording = false;
            recordingThread = null;
        }
    }

    /**
     * start PLAY thread
     */
    protected void startPlaying() {
        //stopMediaPlayer();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Player control 을 정지시킨다.
                musicPlayer.playAndPause(false);
            }
        });

        Log.d("play >> ", "Player sound");
        if (mAudioTrack != null) return;

        minBuffSizeInByte = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO,
                RECORDER_AUDIO_ENCODING);
        if (minBuffSizeInByte < 8000) minBuffSizeInByte = 8000;
        minBuffSizeInShort = minBuffSizeInByte / 2;
        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO,
                RECORDER_AUDIO_ENCODING, minBuffSizeInByte * 4, AudioTrack.MODE_STREAM);
        channels = mAudioRecorder.getChannelCount();

        mAudioTrack.play();

        writeData_ = new short[minBuffSizeInShort];
        isPlaying = true;

        playingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                playLoop();
            }
        }, "AudioPlay Thread");
        playingThread.start();
    }

    /**
     * Audio play thread loop.
     */
    private void playLoop() {
        Log.d("o", "player thread start");
        boolean waiting = true;
        isPlayingThreadEndCompleted = false;
        // Activity 종류 후에 Looping 방지를 위해 playingThread 동작중일 경우만 체크한다.
        while (playingThread != null && replies_.size() == 0) {
            sleep(10);
            //Log.d(TAG, "playLoop()--->>while Loop");
        }

        while (isPlaying) {
            //System.out.printf("Head : %d\n", mAudioTrack.getPlaybackHeadPosition());
            if ((replies_.size() > 0 && !waiting) || replies_.size() > 1) {
//        Log.d(TAG, "playLoop()--->>isPlaying:--if:" + waiting + ":" + replies_.size());
                waiting = false;
                //Log.d(">>", "audio list size :"+replies_.size());
                short[] data = replies_.get(0);
                mAudioTrack.write(data, 0, data.length);
                replies_.remove(0);
            } else {
                sleep(200);
//        Log.d(TAG, "playLoop()--->>isPlaying:--else:" + replies_.size());
                if (replies_.size() == 0) { // Audio list is empty
                    waiting = true;
                    Log.d(">>", "head:" + mAudioTrack.getPlaybackHeadPosition());
                    if (prevHeadPosition_ == mAudioTrack.getPlaybackHeadPosition()) {
                        break;
                    }
                    prevHeadPosition_ = mAudioTrack.getPlaybackHeadPosition();
                }
            }
        }
        Log.d("x", "player thread stop");

        mAudioTrack.stop();
        mAudioTrack.release();
        mAudioTrack = null;
        replies_.clear();
        sleep(100);
        Log.d(">>", "state to Sleeping");
        chatbotState = SLEEPING;
        Log.d(">>", "music url :" + musicUrl_);
        if (musicUrl_ != null) {
            startMediaPlayer(musicUrl_);
            musicUrl_ = null;
        }

        isPlayingThreadEndCompleted = true;
    }

    /**
     * Stop playing
     */
    protected void stopPlaying() {
        if (null != mAudioTrack) {

            isPlaying = false;
            playingThread = null;
        }
    }

    /**
     * Process by chatbot state
     *
     * @param key keyword string.
     * @param vol volume level.
     */
    void processKey(String key, short vol) {
        long tick = System.currentTimeMillis();
        if (prevTick_ == 0) prevTick_ = tick;
        long passed = tick - prevTick_;
        prevTick_ = tick;

        sessionTimeout -= passed;
        if (key != null) Log.d(TAG, "====== state:" + chatbotState);
        switch (chatbotState) {
            case SLEEPING:
                procSleep(key, vol, passed);
                break;
            case AWAKEN:
                procAwaken(key, vol, passed);
                break;
            case LISTENING:
                procListening(key, vol, passed);
                break;
            case QUERYING:
                procQuerying(key, vol, passed);
                break;
            case TALKING:
                procTalking(key, vol, passed);
                break;
            case PLAYING:
                procPlaying(key, vol, passed);
                break;
            case LISTENING_PLAYING:
                procListening(key, vol, passed);
                break;
        }
    }

    /**
     * Sleep - waiting for keyword spotting
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procSleep(String key, short vol, long passed) {

        if ("얘초롱아".equals(key)) {

            if (chatClient.getChatLanguage() != AsyncClient.ChatLanguage.kor) {
                try {
//          chatClient.close();
                    chatClient.open(AsyncClient.ChatLanguage.kor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            awake();
        }
        if ("오케이마인즈".equals(key)) {
            if (chatClient.getChatLanguage() != AsyncClient.ChatLanguage.eng) {
                try {
//          chatClient.close();
                    chatClient.open(AsyncClient.ChatLanguage.eng);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            awake();
        }
        if ("이제그만".equals(key) || "플리즈스탑".equals(key)) {
            //stopMediaPlayer();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Player control 을 정지시킨다.
                    musicPlayer.playAndPause(false);
                }
            });
        }
        if ("그만멈춰".equals(key)) {
            reconnect();
        }
    }

    /**
     * Awake chatbot.
     */
    void awake() {
        stateTimeout = 700;
        startPlaying();

        //2017.02.17 Modify : 요청에 의해 wakeup 시 삐~~ 소리 제거함.
    /*
    short[] d = new short[minBuffSizeInShort * 4];
    for (int i = 0; i < minBuffSizeInShort * 2; i++) {
      d[i] = (short) (Math.sin(Math.PI * i / 15) * 8000);
    }
    mAudioTrack.write(d, 0, d.length);
    */
        chatbotState = AWAKEN;
        Log.d(">>", "State to AWAKEN (" + chatClient.getChatLanguage().name() + ")");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Voice Listening Dialog show.
                voiceListeningView(true);
            }
        });
    }

    /**
     * Awaken - awake and ack to user
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procAwaken(String key, short vol, long passed) {
        stateTimeout -= passed;

        if (stateTimeout < 0) {
            stateTimeout = 0;
            chatbotState = LISTENING;
            listenTimeout = LISTEN_TIMEOUT;
            silenceTimeout = SILENCE_TIMEOUT;
            beginAudioStream();
            Log.d(">>", "State to LISTENING");
        }
    }

    /**
     * Listening chatbot.
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procListening(String key, short vol, long passed) {
        listenTimeout -= passed;
        silenceTimeout -= passed;
//    System.out.printf("vol:%d,  s:%d, l:%d\n",vol,silenceTimeout,listenTimeout);
//    Log.d(TAG, "procListening >>" +  "silenceTimeout:" + silenceTimeout +  ", listenTimeout:" + listenTimeout);
        if (vol > 3000) {
            silenceTimeout = SILENCE_TIMEOUT / 2;
        }
        if (listenTimeout < 0 || silenceTimeout < 0) {
            chatbotState = TALKING;
            finishAudioStream();
            Log.d(">>", "State to TALKING");
        }
    }

    /**
     * Voice Talk Client Cancel.
     */
    protected void voiceTalkStop() {
        Log.d(TAG, "voiceTalkStop()..IN");
        listenTimeout = 0;
        procListening("", (short) 0, 500);
        Log.d(TAG, "voiceTalkStop()..OUT");
    }

    /**
     * Querying chatbot.
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procQuerying(String key, short vol, long passed) {
    }

    /**
     * Talking chatbot.
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procTalking(String key, short vol, long passed) {
    }

    /**
     * Playing chatbot.
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procPlaying(String key, short vol, long passed) {
    }

    /**
     * Playing & Listening chatbot.
     *
     * @param key    keyword string.
     * @param vol    volume level.
     * @param passed passed tick time.
     */
    void procPlayingListening(String key, short vol, long passed) {
    }

    /**
     * custom sleep.
     *
     * @param interval sleep time.
     */
    void sleep(long interval) {
        try {
            Thread.sleep(interval);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Reconnect chat client for chorong answer play.
     */
    void reconnect() {
        Log.d(TAG, "reconnect()..IN");
        chatClient.close();
        try {
            chatClient.open();
            mMediaPlayer = MediaPlayer.create(this, R.raw.chorong_answer);
            mMediaPlayer.start();
            while (mMediaPlayer.isPlaying()) {
                sleep(100);
            }
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d(TAG, "reconnect()..OUT");
    }

    /**
     * Reconnect chat client.
     */
    void reconnectSilent() {
//    Log.d(TAG, "========== reconnectSilent");
//    chatClient.close();
        try {
            chatClient.open();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Check audio to text callback talk enabled.
     *
     * @return true is the audio to text callback talk.
     */
    protected abstract boolean isSpeakerOn();

    /**
     * Add the recevied meta data.
     *
     * @return true is the audio to text callback talk.
     */
    protected abstract boolean addAudioToTextTalkData(String text);

    /**
     * Add the recevied audio toalk meta data.
     *
     * @return true is success.
     */
    protected abstract boolean addAudioTalkData();

    /**
     * Add the recevied meta data.
     *
     * @return true is success.
     */
    protected abstract boolean addChatData(final DecodeMetaData decodeMetaData);

    /**
     * Get serarch mode on.
     *
     * @return true is search mode on.
     */
    protected abstract boolean getSearchMode();

    /**
     * Voice Listening status show.
     *
     * @param enable
     */
    protected abstract void voiceListeningView(boolean enable);
}