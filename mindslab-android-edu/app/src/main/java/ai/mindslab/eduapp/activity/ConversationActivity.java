package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class ConversationActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#conversation");
        super.onCreate(savedInstanceState);

        setTitleBarText("기초 회화 A");

        loadUrl();
    }

}
