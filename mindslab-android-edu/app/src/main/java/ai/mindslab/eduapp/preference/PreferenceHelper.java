package ai.mindslab.eduapp.preference;

import android.content.Context;
import android.content.SharedPreferences;

/*
 * Created by yjhwang on 2016-12-27.
 */
public class PreferenceHelper {
  // 환경설정 저장 파일
  public final static String AI_CLOUD_CHATAPP_PREFERENCES = "ai_cloud_chatapp_preferences";
  // Signin remeber me checked
  public final static String KEY_SIGNIN_REMEMBER_ME_CHECKED = "signin_rembember_me_checked";
  // Prev Signin method
  public final static String KEY_SIGNIN_REMEMBER_ME_LASTEST_METHOD = "signin_last_success_method";
  // Signin input field id
  public final static String KEY_SIGNIN_REMEMBER_ME_ID = "signin_rembember_me_id";
  // Signin input field password
  public final static String KEY_SIGNIN_REMEMBER_ME_PASSWORD = "signin_rembember_me_password";
  // GPS enable next question disable checked.
  public final static String KEY_GPS_NEXT_QUESTION_DISABLE_CHECKED = "gps_next_question_disable_checked";
  // Access Token
  public final static String KEY_NETWORK_ACCESS_TOKEN = "netowrk_access_token";
  // Login User name
  public final static String KEY_LOGIN_USER_NAME = "username";
  // Login User email
  public final static String KEY_LOGIN_USER_EMAIL = "email";
  // Login User id
  public final static String KEY_LOGIN_USER_ID = "userid";
  // Login name
  public final static String KEY_LOGIN_NAME = "name";
  // Profile Bio
  public final static String KEY_LOGIN_USER_BIO = "bio";
  // Profile Bio
  public final static String KEY_LOGIN_USER_DESCRIPTION = "description";
  // Profile Avatar image src
  public final static String KEY_LOGIN_USER_AVATAR_IMG_SRC = "avatarImgSrc";
  // Profile Avatar image file id
  public final static String KEY_LOGIN_USER_AVATAR_FILEID = "avatarFileId";
  // GPS 위도값
  public final static String KEY_LAST_LOCATION_LATITUDE = "last_latitude";
  // GPS 경도값
  public final static String KEY_LAST_LOCATION_LONGITUDE = "last_longitude";
  // HTTP Protocol type.
  public final static String KEY_NET_REST_IS_HTTPS = "net_rest_is_https";
  // REST API Url.
  public final static String KEY_NET_REST_URL = "net_rest_url";
  // REST Port.
  public final static String KEY_NET_REST_PORT = "net_rest_port";
  // GRPC Url.
  public final static String KEY_NET_GRPC_URL = "net_grpc_host";
  // GRPC Port.
  public final static String KEY_NET_GRPC_PORT = "net_grpc_port";
  /**
   * Integer value getter.
   * @param context context
   * @param key key
   * @return integer value
   */
  public static int getIntKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getInt(key, 0);
  }

  /**
   * String value getter.
   * @param context context
   * @param key key
   * @return string value
   */
  public static String getStringKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getString(key, "");
  }

  /**
   * boolean value getter.
   * @param context context
   * @param key key
   * @return boolean value
   */
  public static boolean getBooleanKeyValue(Context context, String key) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    return sp.getBoolean(key, false);
  }

  /**
   * Integer value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setIntKeyValue(Context context, String key, int value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putInt(key, value);
    editor.commit();
  }

  /**
   * String value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setStringKeyValue(Context context, String key, String value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putString(key, value);
    editor.commit();
  }

  /**
   * boolean value setter.
   * @param context context
   * @param key key
   * @param value value
   */
  public static void setBooleanKeyValue(Context context, String key, boolean value) {
    SharedPreferences sp = context.getSharedPreferences(AI_CLOUD_CHATAPP_PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();
    editor.putBoolean(key, value);
    editor.commit();
  }
}
