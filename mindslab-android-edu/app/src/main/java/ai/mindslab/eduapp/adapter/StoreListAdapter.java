package ai.mindslab.eduapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ai.mindslab.eduapp.model.StoreListItem;
import ai.mindslab.eduapp.view.StoreListItemView;

/**
 * Classes for StoreListAdapter
 */
public class StoreListAdapter extends BaseAdapter {
  /* Context */
  private Context context;
  /* List items */
  private ArrayList<StoreListItem> items;

  /**
   * Constructor
   * @param context base context.
   */
  public StoreListAdapter(Context context) {
    this.context = context;
    items = new ArrayList<StoreListItem>();
  }

  @Override
  public int getCount() {
    if (this.items != null) {
      return this.items.size();
    }
    return 0;
  }

  @Override
  public StoreListItem getItem(int i) {
    return this.items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return 0;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    StoreListItemView itemView;
    if (view != null && view instanceof StoreListItemView) {
      itemView = (StoreListItemView) view;
    } else {
      itemView = new StoreListItemView(context).inflate(viewGroup);
    }
    itemView.setData(getItem(i));
    return itemView;
  }

  /**
   * User custom function.
   * @param items StoreListItem type ArrayList.
   */
  public void setItems(ArrayList<StoreListItem> items) {
    this.items = items;
    notifyDataSetChanged();
  }
}
