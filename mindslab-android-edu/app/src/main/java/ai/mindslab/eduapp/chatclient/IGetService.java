package ai.mindslab.eduapp.chatclient;

import elsa.facade.Dialog;

/**
 * Service interface
 */
public interface IGetService {
  /**
   *
   * @param sgDetail service group detail info.
   */
  void onGetServiceGropuDetail(Dialog.ServiceGroup sgDetail);

  /**
   * Error
   * @param messsage error message.
   */
  void onError(String messsage);
}
