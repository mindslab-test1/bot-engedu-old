package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class ExplainActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#explain");
        super.onCreate(savedInstanceState);

        setTitleBarText("설명 듣고 답하기");

        loadUrl();
    }

}
