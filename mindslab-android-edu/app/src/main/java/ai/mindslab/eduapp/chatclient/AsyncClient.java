package ai.mindslab.eduapp.chatclient;

import com.google.common.util.concurrent.SettableFuture;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.activity.MetaDataHelper;
import ai.mindslab.eduapp.model.DecodeMetaData;
import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.preference.PreferenceHelper;
import elsa.facade.Dialog;
import elsa.facade.Dialog.AccessFrom;
import elsa.facade.Dialog.AudioUtter;
import elsa.facade.Dialog.Caller;
import elsa.facade.Dialog.Session;
import elsa.facade.Dialog.SessionKey;
import elsa.facade.Dialog.SessionStat;
import elsa.facade.Dialog.TextUtter;
import elsa.facade.DialogServiceGrpc;
import elsa.facade.DialogServiceGrpc.DialogServiceStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.okhttp.OkHttpChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

/**
 * Classes for AsyncClient
 */
public class AsyncClient {
  /* For log tag */
  private final String TAG = AsyncClient.class.getSimpleName();
  /* Context */
  private Context context;
  /* Dialog stub */
  private DialogServiceStub asyncStub;
  /* Stub channel */
  private ManagedChannel originalChannel;
  /* Session id */
  private Long sessionId = null;
  /* Audio samplerate*/
  private static final Integer audioSampleRate = 16000;
  /* Service group */
  private String serviceGroup = "";

  /* Language type */
  public enum ChatLanguage {
    kor,
    eng
  }

  /* chat language type save */
  private ChatLanguage chatLanguage = ChatLanguage.eng;

  /**
   * Constructor
   */
  public AsyncClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = OkHttpChannelBuilder.forAddress(NetConfigInfo.getGrpcUrl(),
            NetConfigInfo.GRPC_PORT).usePlaintext(true).build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    asyncStub = DialogServiceGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /**
   * Grpc open with servicegroup.
   */
  public void open() {
    open(ChatLanguage.eng, serviceGroup); //default
  }

  /**
   * Grpc open with language.
   */
  public void open(ChatLanguage lang) {
    open(lang, serviceGroup);
  }

  /**
   * Grpc open
   */
  public void open(final ChatLanguage lang, String svcGroup) {
    final SettableFuture<Void> finishFuture = SettableFuture.create();

    Log.d(TAG, " ========= open : " + sessionId);
    //blockingStub_.open(accessCaller);
    if (sessionId == null) {
      // Get Login Name.
      String username = PreferenceHelper.getStringKeyValue(context,
              PreferenceHelper.KEY_LOGIN_USER_NAME);

      // Get Access-Token
      String accessToken = PreferenceHelper.getStringKeyValue(context,
              PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);
      // Get Location.
      String latitudeValue = PreferenceHelper.getStringKeyValue(context,
              PreferenceHelper.KEY_LAST_LOCATION_LATITUDE);
      String longitudeValue = PreferenceHelper.getStringKeyValue(context,
              PreferenceHelper.KEY_LAST_LOCATION_LONGITUDE);

      Log.d(TAG, "username:" + username);
      Log.d(TAG, "AccessToken:" + accessToken);

      Metadata.Key<String> audioHz =
              Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> language =
              Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> latitude =
              Metadata.Key.of("in.user.location.latitude", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> longitude =
              Metadata.Key.of("in.user.location.longitude", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> authToken =
              Metadata.Key.of("x-elsa-authentication-token", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> provider =
              Metadata.Key.of("x-elsa-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

      Metadata header = new Metadata();
      header.put(audioHz, "" + audioSampleRate);
      header.put(language, lang.name());
      header.put(authToken, accessToken);
      header.put(provider, "mindslab");
      header.put(latitude, latitudeValue);
      header.put(longitude, longitudeValue);

      // Meta header 를 붙여 오픈하여야 한다.
      asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
      Log.d(TAG, "[GRPC Header]" + header.toString());

      Caller.Builder builder = Caller.newBuilder();
      builder.setName(username);
      builder.setAccessFrom(AccessFrom.MOBILE_ANDROID);
      builder.setSvcGroup(svcGroup);
      Caller accessCaller = builder.build();

      asyncStub.open(accessCaller, new StreamObserver<Session>() {
        @Override
        public void onNext(Session session) {
          Log.d(TAG, "========================= before" + sessionId);
          sessionId = session.getId();
          Log.d(TAG, "========================= after" + sessionId);
          Metadata header = new Metadata();
          Metadata.Key<String> sessionKey =
                  Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
          header.put(sessionKey, "" + sessionId);

          asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
          chatLanguage = lang;
          Log.d("asd", "async client on next lang:::::"+lang.name());
          Log.d(TAG, "[GRPC Header]" + header.toString());
        }

        @Override
        public void onError(Throwable err) {
          err.printStackTrace();
          finishFuture.setException(err);
        }

        @Override
        public void onCompleted() {
          System.out.println("stub open completed");
          finishFuture.set(null);
        }
      });
    } else {
      Log.d(TAG, "========== use old sessionId" + sessionId);

      Dialog.SessionKey key = Dialog.SessionKey.newBuilder()
              .setSessionId(sessionId.longValue()).build();

      asyncStub.updateSession(key, new StreamObserver<Session>() {
        @Override
        public void onNext(Session session) {
          Log.d(TAG, "====== update sessionID" + session.toString());
        }

        @Override
        public void onError(Throwable err) {
          Log.d(TAG, "======================== err " + err.toString() + ">>" + err.getMessage());
          io.grpc.StatusRuntimeException e = (io.grpc.StatusRuntimeException) err;
          Log.d(TAG, " ======= error" + e.getStatus().toString() + ">>" + Status.NOT_FOUND
          );
          if (e.getStatus().getCode() == Status.NOT_FOUND.getCode()) {

            Log.d(TAG, "~~~~~~~~~~~~~~~~~~~" + sessionId);
            close();
          }

//          err.printStackTrace();
//          finishFuture.setException(err);
        }

        @Override
        public void onCompleted() {
          System.out.println("stub open completed");
          finishFuture.set(null);
        }
      });

    }
//    try {
//      finishFuture.get(10, TimeUnit.SECONDS);
//    } catch (Exception e) {
//      String errorMessage = this.context.getString(R.string.error_grpc_connection_fail);
//      Toast.makeText(this.context, errorMessage, Toast.LENGTH_SHORT).show();
//      e.printStackTrace();
//    }
  }

  /**
   * AudioTalk
   *
   * @param ac callback
   * @return AudioUtter data.
   */
  public StreamObserver<AudioUtter> getSenderAudioTalk(final IAudioCallback ac) {
    return asyncStub.audioTalk(new StreamObserver<AudioUtter>() {
      @Override
      public void onNext(AudioUtter utter) {
        ac.onAudio(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        ac.onAudioError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("audio talk completed");
        ac.onAudioEnd();
      }
    });
  }

  /**
   * AudioTextTalk
   *
   * @param atc callback
   * @return AudioUtter data.
   */
  public StreamObserver<AudioUtter> getSenderAudioTextTalk(final IAudioTextCallback atc) {
    return asyncStub.audioToTextTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
        //System.out.println("utter:"+utter.getUtter());
        atc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        atc.onError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("audio-text talk completed");
      }
    });
  }

  /**
   * TextTalk
   *
   * @param tc callback
   * @return TextUtter data.
   */
  public StreamObserver<TextUtter> getSenderTextTalk(final ITextCallback tc) {
    return asyncStub.textTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
        //System.out.println("utter:"+utter.getUtter());
        tc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        tc.onError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("text talk completed");
      }
    });
  }

  /**
   * Text to Audio talk.
   *
   * @param ac callback
   * @return TextUtter data.
   */
  public StreamObserver<TextUtter> getSenderTextToAudioTalk(final IAudioCallback ac) {
    return asyncStub.textToAudioTalk(new StreamObserver<AudioUtter>() {
      @Override
      public void onNext(AudioUtter utter) {
        System.out.println("getSenderTextToAudioTalk onNext");
        ac.onAudio(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        System.out.println("getSenderTextToAudioTalk onError");
        ac.onAudioError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("getSenderTextToAudioTalk completed");
        ac.onAudioEnd();
      }
    });
  }


  /**
   * imageToTextTalk
   *
   * @param tc callback
   */
  public StreamObserver<Dialog.Image> getSenderImageTalk(final ITextCallback tc) {
    return asyncStub.imageToTextTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter value) {
        System.out.println("ImageTalk:" + value.getUtter());
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("ImageTalk talk error");
        tc.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("ImageTalk completed");
      }
    });
  }

  /**
   * Get past session list.
   *
   * @param pastSessionQuery Dialog.PastSessionQuery
   */
  public void getPastSessions(Dialog.PastSessionQuery pastSessionQuery, final IPastSessionsCallback psc) {
    asyncStub.getPastSessions(pastSessionQuery, new StreamObserver<Dialog.PastSessionList>() {
      @Override
      public void onNext(Dialog.PastSessionList value) {
        /*System.out.println("PastSessionList:" + value.getSessionsList());
        System.out.println("PastSessionList session Count:" + value.getSessionsCount());*/
        psc.onGetPastSessionsList(value);
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("getPastSessions error" + t.toString());
        psc.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("getPastSessions completed");
        psc.onCompleted();
      }
    });
  }

  /**
   * Get past talk list.
   *
   * @param pastTalkQuery Dialog.PastTalkQuery
   */
  public void getPastTalk(final Dialog.PastTalkQuery pastTalkQuery, final IPastTalkCallback ptc) {
    asyncStub.getPastTalks(pastTalkQuery, new StreamObserver<Dialog.PastTalkList>() {
      @Override
      public void onNext(Dialog.PastTalkList value) {
        ptc.onGetPastTalkList(value);
      }

      @Override
      public void onError(Throwable t) {
//        System.out.println("getPastTalk error" + t.toString());
        ptc.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("getPastTalk completed");
      }
    });
  }

  /**
   * Close asyncClient.
   */
  public void close() {
    Log.d(TAG, "======== close called");
    if (asyncStub == null || sessionId == null) {
      return;
    }

    SessionKey.Builder builder = SessionKey.newBuilder();
    builder.setSessionId(sessionId);
    final SessionKey key = builder.build();
    Log.d(TAG, "======== stub called" + key.getSessionId());
    sessionId = null;
    System.out.println("stub close SessionKey : " + key.getSessionId());
    Log.d(TAG, "stub close SessionKey : " + key.getSessionId());
    //SessionStat stat = blockingStub_.close(key);
    asyncStub.close(key, new StreamObserver<SessionStat>() {
      @Override
      public void onNext(SessionStat stat) {
        // 여기서 대답을 얻는다.
        System.out.println("stub close code : " + key.getSessionId() + "]" + stat.getResultCode());
        System.out.println("stub close code : " + stat.getFullMessage());

        Log.d(TAG, "stub close code : " + key.getSessionId() + "]" + stat.getResultCode());
        Log.d(TAG, "stub close code : " + stat.getFullMessage());
      }

      @Override
      public void onError(Throwable err) {
        Log.d(TAG, "===============================err");
        err.printStackTrace();
      }

      @Override
      public void onCompleted() {
        open(ChatLanguage.eng);
      }
    });
  }

  /**
   * 서비스 그룹을 셋팅한다.
   *
   * @param serviceGroup
   */
  public void setServiceGroup(String serviceGroup) {
    this.serviceGroup = serviceGroup;
  }

  /**
   * Get chatLanguage.
   */
  public ChatLanguage getChatLanguage() {
    return chatLanguage;
  }

  /**
   * Set chatLanguage.
   */
  public void setChatLanguage(ChatLanguage chatLanguage) {
    this.chatLanguage = chatLanguage;
  }

  /**
   * SessionId getter.
   *
   * @return session id.
   */
  public Long getSessionId() {
    if (sessionId == null) {
      return null;
    }

    return sessionId;
  }
}

