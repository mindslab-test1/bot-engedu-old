package ai.mindslab.eduapp.network;

import android.content.Context;
import android.util.Log;

public class PropertiesPacket extends RestApiURI {

  private final String TAG = PropertiesPacket.class.getSimpleName();
  private NetRestHttp restHttp;

  //통신 설정 정보 정의
  private String restApi;

  public PropertiesPacket(Context context) {
    this.context = context;

    restApi = NetConfigInfo.getApiUrl();
    Log.d(TAG, restApi);

    restHttp = new NetRestHttp();
    restHttp.setListener(onHttpResListener);
  }

  public void stop(String method) {
    restHttp.stop(method);
  }

  public void stopAll() {
    restHttp.stopAll();
  }


  /**
   * 사용자 정의 속성 조회.
   * @param method
   * @param accessToken
   * @param svcGroup
   */
  public void reqProfertiesFindOne(String method,
                                   String accessToken, String userId, String svcGroup) {
    // Make params.
    String params = makeFindOneParam(accessToken, userId, svcGroup);

    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCPROPERTIES_FINDONE + params,
        null);
  }

  /**
   * Make findone params.
   * @param accessToken
   * @param userId
   * @param svcGroup
   * @return result params string.
   */
  private String makeFindOneParam(String accessToken, String userId, String svcGroup) {
    String params;
    String param1 = String.format("\"userId\":\"%s\"", userId);
    String param2 = String.format("\"groupName\":\"%s\"", svcGroup);
    String param3 = String.format("\"type\":\"properties\"");

    params = String.format("?filter={\"where\":{%s,%s,%s}}&access_token=%s&id=%s",
        param1, param2, param3, accessToken, accessToken);

    return params;
  }

  /**
   * 사용자 정의 그룹 속성 조회
   * @param method
   * @param accessToken
   * @param userId
   * @param svcGroup
   * @param domain
   */
  public void reqProfertiesFindGroupAndDomain(String method,
                                              String accessToken,
                                              String userId,
                                              String svcGroup,
                                              String domain) {
    // Make params.
    String params = makeFindGroupAndDomain(accessToken, svcGroup, domain);

    // Send http get.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        restApi + "" + API_SVCPROPERTIES_FINDGROUP_AND_DOMAIN + params,
        null);
  }

  /**
   * Make findGroupAndDomain params.
   * @param accessToken
   * @param svcGroup
   * @param domain
   * @return result params string.
   */
  private String makeFindGroupAndDomain(String accessToken, String svcGroup, String domain) {
    String params;

    params = String.format("group=%s&access_token=%s", svcGroup, accessToken);

    //Optional check.
    if (domain != null && domain.length() > 0) {
      params += String.format("&domain=%s", domain);
    }

    return params;
  }

  /**
   * 사용자 정의 속성 추가 및 업데이트(New API)
   * @param isAdd true is add with http post method.Else is update with http put method.
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqPropertiesAddAndUpdate(boolean isAdd,
                                        String method,
                                        String accessToken,
                                        Request reqObj) {
    // HTTP Method select.
    String httpMethod = NetConfigInfo.HTTP_METHOD_POST;
    if (isAdd == false) {
      httpMethod = NetConfigInfo.HTTP_METHOD_PUT;
    }
    Log.d(TAG, "reqPropertiesAddNUpdate() http method:" + httpMethod);

    // Send http post.
    restHttp.send(method,
        httpMethod,
        accessToken,
        restApi + "" + API_SVCPROPERTIES_ADD_N_UPDATE + accessToken,
        reqObj.mapToJson());
  }


  /**
   * 사용자 정의 속성 저장.
   * @param method
   * @param accessToken
   * @param userId
   * @param svcGroup
   * @param jsonObject
   */
  public void reqPropertiesSave(String method,
                                String accessToken,
                                String userId,
                                String svcGroup,
                                org.json.simple.JSONObject jsonObject) {
    // Make params.
    String params = makePropertiesSaveParam(accessToken, userId, svcGroup);

    Log.d(TAG, params);
    // Send http post.
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCPROPERTIES_UPSERT + params,
        jsonObject);
  }

  /**
   * Make properties save params.
   * @param accessToken
   * @param userId
   * @param svcGroup
   * @return result params string.
   */
  private String makePropertiesSaveParam(String accessToken,
                                         String userId, String svcGroup) {
    String params;
    String param1 = String.format("\"userId\":\"%s\"", userId);
    String param2 = String.format("\"groupName\":\"%s\"", svcGroup);
    String param3 = String.format("\"type\":\"properties\"");

    params = String.format("?where={%s,%s,%s}&access_token=%s",
        param1, param2, param3, accessToken);

    return params;
  }
}
