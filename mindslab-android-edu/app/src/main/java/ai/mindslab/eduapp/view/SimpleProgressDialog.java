package ai.mindslab.eduapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import ai.mindslab.eduapp.R;
/**
 * Classes for SimpleProgressDialog
 */
public class SimpleProgressDialog extends ProgressDialog {
  /**
   * Constructor
   * @param context
   */
  public SimpleProgressDialog(Context context) {
    super(context);
    super.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    super.setCancelable(false);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.simple_progress_bar);
  }

  /**
   * Set cancelable setting.
   * @param flag true is dialog cancelable.
   */
  public void setCancelable(boolean flag) {
    super.setCancelable(flag);
  }
}
