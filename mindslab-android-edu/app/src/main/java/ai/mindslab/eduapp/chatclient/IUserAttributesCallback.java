package ai.mindslab.eduapp.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

import elsa.facade.Dialog;

/**
 * Userattributes callback interface
 */
public interface IUserAttributesCallback {
  /**
   * Get user attributes
   * @param value user attributes value.
   */
  void onGetUserAttributes(Dialog.ServiceGroupUserAttributes value);

  /**
   * Error.
   * @param messsage
   */
  void onError(String messsage);
}
