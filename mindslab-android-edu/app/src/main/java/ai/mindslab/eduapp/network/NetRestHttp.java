package ai.mindslab.eduapp.network;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Rest 방식의 Http 프로토콜 Connector
 */
public class NetRestHttp {
  /* For log tag */
  private final String TAG = NetRestHttp.class.getSimpleName();
  /* Timeout */
  private int timeOut;
  /* Charset */
  private String charSet;
  /* Message handler */
  private MsgHandler msgHandler = null;
  /* Request Queue */
  private RequestQueue netQueue = null;
  /* Connect listener */
  private INetConnectorListener netListener = null;
  /* Queue list */
  private ArrayList<Queue> queueList = null;

  /**
   * HTTP Method type
   */
  private enum HttpMethodType {
    NOVALUE, PUT, GET, POST, PATCH, DELETE;

    public static HttpMethodType compare(String str) {
      try {
        return valueOf(str.toUpperCase());
      } catch (Exception e) {
        return NOVALUE;
      }
    }
  }

  /**
   * Constructor
   */
  public NetRestHttp() {
    timeOut = NetConfigInfo.TIMEOUT;
    charSet = NetConfigInfo.CHARSET;
    msgHandler = new MsgHandler();
    queueList = new ArrayList<Queue>();
  }

  /**
   * Queue class
   */
  private class Queue {
    private String          METHOD;
    private HttpMethodType  TYPE;
    private NetHttpHeader   HEADERS;
    private String          URL;
    private JSONObject      BODY;

    public Queue(String method, HttpMethodType type, NetHttpHeader headers, String url, JSONObject body) {
      this.METHOD   = method;
      this.TYPE     = type;
      this.HEADERS  = headers;
      this.URL      = url;
      this.BODY     = body;
    }
  }

  /**
   * Classes for MsgHandler
   */
  private class MsgHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      try {
        if (netListener != null) {
          if(msg.what == NetConfigInfo.RESCODE_EXCEPTION){
            netListener.onError(msg.what, (String) msg.obj);
          }else{
            netListener.onResponse(netQueue.queue.METHOD, msg.what, (String) msg.obj);
          }
        }

        synchronized (queueList) {
          queueList.remove(0);
          netQueue = null;
        }

        post(onCheckAnotherJob);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Classes for RequestQueue
   */
  private class RequestQueue extends Thread {
    private Queue queue;

    public RequestQueue(Queue queue) {
      this.queue = queue;
    }

    @Override
    public void run() {

      HttpURLConnection conn = null;

      int resCode = NetConfigInfo.RESCODE_EXCEPTION; //200 정상, 그외 Network Error Code, 999 Network Exception
      StringBuffer res = new StringBuffer();
      try {
        String strUrl = queue.URL;
        URL url = new URL(strUrl);
        Log.d(TAG, "url : " + url + "");

        if (NetConfigInfo.PROTOCOL == "HTTPS") {

          // Create a trust manager that does not validate certificate chains
          TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
              return new X509Certificate[]{};
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
          }};

          SSLContext sc = SSLContext.getInstance("TLS");
          sc.init(null, trustAllCerts, new SecureRandom());
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

          HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
          httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
              return true;
            }
          });

          conn = httpsURLConnection;

        } else {

          conn = (HttpURLConnection) url.openConnection();

        }

        conn.setConnectTimeout(timeOut * 1000);
        conn.setReadTimeout(timeOut * 1000);

        switch (queue.TYPE) {
          case PUT:
            conn.setRequestMethod(NetConfigInfo.HTTP_METHOD_PUT);
            break;
          case GET:
            conn.setRequestMethod(NetConfigInfo.HTTP_METHOD_GET);
            break;
          case POST:
            conn.setRequestMethod(NetConfigInfo.HTTP_METHOD_POST);
            break;
          case PATCH:
            conn.setRequestMethod(NetConfigInfo.HTTP_METHOD_PATCH);
            break;
          case DELETE:
            conn.setRequestMethod(NetConfigInfo.HTTP_METHOD_DELETE);
            break;
        }

        if (queue.HEADERS != null) {
          Set<String> setKeys = queue.HEADERS.keySet();
          Iterator itor = setKeys.iterator();
          while (itor.hasNext()) {
            String key = itor.next().toString();
            //Log.d(TAG, key + ":" + queue.HEADERS.get(key));
            conn.setRequestProperty(key, queue.HEADERS.get(key));
          }
        }
        conn.setDoInput(true);

        if (isInterrupted()) return;
        if (HttpMethodType.POST == queue.TYPE
            || HttpMethodType.PUT == queue.TYPE
            || HttpMethodType.PATCH == queue.TYPE) {

          conn.setDoOutput(true);
          OutputStream output = conn.getOutputStream();
          Log.d(TAG, "queue.BODY : " + (queue.BODY));

          if (queue.BODY != null) {
            output.write(queue.BODY.toString().getBytes(charSet));
          }
          output.flush();
          output.close();
        }

        resCode = conn.getResponseCode();
        Log.d(TAG, "resCode : " + resCode);

        InputStreamReader isr = null;
        if (resCode == HttpURLConnection.HTTP_OK) {
          isr = new InputStreamReader(conn.getInputStream(), charSet);
        } else {
          isr = new InputStreamReader(conn.getErrorStream(), charSet);
        }

        BufferedReader br = new BufferedReader(isr);

        String line = "";
        while (isInterrupted() == false) {
          line = br.readLine();
          if (line == null) break;
          res.append(line + "\n");
        }

        isr.close();
        br.close();

        if (isInterrupted()) return;

        Log.d(TAG, "res : " + res.toString());

      }catch (Exception e) {
        Log.e(TAG, "Exception : " + e);
        JSONObject jobj = new JSONObject();
        jobj.put("error",e.toString());
        res.append(jobj.toJSONString());
      } finally {
        Message msg = Message.obtain(msgHandler, resCode, res.toString());
        msgHandler.sendMessage(msg);
        if (conn != null) conn.disconnect();
      }
    }

  }

  /**
   * Runnable job
   */
  private Runnable onCheckAnotherJob = new Runnable() {
    @Override
    public void run() {
      if (netQueue != null) return;

      synchronized (queueList) {
        if (queueList.size() == 0) return;

        Queue queue = queueList.get(0);

        netQueue = new RequestQueue(queue);
        netQueue.start();
      }
    }
  };

  /**
   * Http 전송
   *
   * @param method 응답 받을 method, 리스너에서 이 값을 가지고 어떤 작업을 수행했는가를 판단하도록 한다.
   * @param type   | Http Method
   * @param uri    | API URI
   * @param body   | 전송 PARAM
   */
  public void send(String method, String type, String accessToken, String uri, JSONObject body) {

    Queue queue = new Queue(method, HttpMethodType.compare(type), new NetHttpHeader(accessToken), uri, body);
    synchronized (queueList) {
      queueList.add(queue);
      msgHandler.post(onCheckAnotherJob);
    }

  }

  /**
   * send에 대한 수행 결과를 받는 리스너를 등록한다.
   *
   * @param l ICLHttpListener 리스너
   */
  public void setListener(INetConnectorListener l) {
    netListener = l;
  }

  /**
   * 접속 시간 제한을 지정한다. (기본값 = 30초)
   *
   * @param nSec 시간 제한 (초단위)
   */
  public void setTimeOut(int nSec) {
    timeOut = nSec;
  }

  /**
   * 받아오는 문자열 유형을 지정한다. (기본값 : UTF-8)
   *
   * @param sSet 문자열 유형
   */
  public void setCharSet(String sSet) {
    charSet = sSet;
  }

  /**
   * getHttp 요청들에 대한 작업을 모두 중단한다.
   */
  public void stopAll() {
    if (netQueue != null) {
      netQueue.interrupt();
      netQueue = null;
    }

    synchronized (queueList) {
      queueList.clear();
    }
  }

  /**
   * 특정 요청 id에 대한 작업을 중단한다.
   *
   * @param method 응답 method
   */
  public void stop(String method) {
    if (netQueue != null && netQueue.queue.METHOD == method) {
      netQueue.interrupt();

      synchronized (queueList) {
        queueList.remove(0);
        netQueue = null;
      }
      msgHandler.post(onCheckAnotherJob);
      return;
    }

    synchronized (queueList) {
      for (Queue queue : queueList) {
        if (queue.METHOD != method) continue;
        queueList.remove(queue);
        break;
      }
    }
  }

  /**
   * send에 대한 수행 결과를 넘겨주는 리스너이다.
   */
  public interface INetConnectorListener {
    /**
     * send 작업이 완료되면 알려준다.
     *
     * @param method   응답줄 method 명
     * @param resCode  결과 코드 (HTTP_OK, HTTP_ERROR, ...)
     * @param sContent 받은 내용
     */
    void onResponse(String method, int resCode, String sContent) throws Exception;

    /**
     * Network 오류 처리 리스너
     * @param resCode
     * @param sContent
     * @throws Exception
     */
    void onError(int resCode, String sContent) throws Exception;

  }
}
