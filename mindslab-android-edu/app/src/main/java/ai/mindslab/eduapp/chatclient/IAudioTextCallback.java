package ai.mindslab.eduapp.chatclient;

/**
 * AudioTextTalk interface.
 */
public interface IAudioTextCallback {
  /**
   * Text result
   * @param text
   */
  void onText(String text);

  /**
   * Audiotalk error
   * @param message error message.
   */
  void onError(String message);
}
