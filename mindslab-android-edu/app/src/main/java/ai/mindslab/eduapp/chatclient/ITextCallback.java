package ai.mindslab.eduapp.chatclient;

/**
 * Texttalk callback interface
 */
public interface ITextCallback {
  /**
   * Get text talk result.
   * @param text text string.
   */
  void onText(String text);

  /**
   * Error
   * @param message error message.
   */
  void onError(String message);
}
