package ai.mindslab.eduapp.activity;

import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_NONE_MESSAGE_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_AUDIO_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_HTML_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_LINK_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_TEXT_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_SEND_MESSAGE_TEXT_TYPE;
import static ai.mindslab.eduapp.model.ChatListItem.ChatItemType.CHAT_SESSION_SEPARATOR_TYPE;

import com.google.protobuf.ByteString;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TimeZone;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.chatclient.IPastSessionsCallback;
import ai.mindslab.eduapp.chatclient.IPastTalkCallback;
import ai.mindslab.eduapp.chatclient.ITextCallback;
import ai.mindslab.eduapp.media.MediaUtils;
import ai.mindslab.eduapp.model.ChatListItem;
import ai.mindslab.eduapp.model.ChatListItem.ChatItemType;
import ai.mindslab.eduapp.model.DecodeMetaData;
import ai.mindslab.eduapp.model.StoreListItem;
import ai.mindslab.eduapp.network.MarketPacket;
import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.network.Request;
import ai.mindslab.eduapp.network.Response;
import ai.mindslab.eduapp.preference.PreferenceHelper;
import ai.mindslab.eduapp.utils.FileUtil;
import ai.mindslab.eduapp.view.ChatAudioContent;
import ai.mindslab.eduapp.view.ChatHtmlContent;
import ai.mindslab.eduapp.view.ChatLinkContent;
import ai.mindslab.eduapp.view.ChatMetaObject;
import ai.mindslab.eduapp.view.ChatTextContent;
import ai.mindslab.eduapp.view.SimpleProgressDialog;
import elsa.facade.Dialog;
import im.delight.android.webview.AdvancedWebView;
import io.grpc.stub.StreamObserver;

/*
 * Classes for chatting activity.
 */
public class WebViewActivity extends ChatBase implements AdvancedWebView.Listener {
    /* For log tag */
    protected static final String TAG = WebViewActivity.class.getSimpleName();
    /* Subactivty close result for gallery */
    protected static final int REQUEST_GALLERY_SELECT_COMPLETE = 10;
    /* Subactivty close result for take picture */
    protected static final int REQUEST_TAKE_PICTURE_COMPLETE = 11;
    /* Subactivty close result for take picture */
    public static final int CHAT_VIEW_CHAT_MODE = 1;
    /* Subactivty close result for take picture */
    public static final int CHAT_VIEW_HISTORY_MODE = 2;
    /* Intent for activity service group info parameter */
    public static final String EXTRA_CHAT_VIEW_MODE = "ai.mindslab.eduapp.WebViewActivity.ExtraChatViewMode";
    /* Intent for activity service group info parameter */
    public static final String EXTRA_SVC_GROUP_INFO = "ai.mindslab.eduapp.WebViewActivity.ExtraSvcGroupInfo";
    /* Intent for activity service group info parameter */
    public static final String EXTRA_PASTTALK_HISTORYITEM = "ai.mindslab.eduapp.WebViewActivity.ExtraPastTalkHistoryItem";

    /* None serch mode title bar control */
    Toolbar toolbar;
    protected LinearLayout titleBarNonesearchHeader;
    protected ImageButton backButton;
    protected ImageButton speakerButton;
    protected ImageButton searchButton;

    /* Serch mode title bar control */
    protected LinearLayout titleBarSearchHeader;
    protected ImageButton searchCloseButton;
    protected EditText searchWordEditText;
    protected ImageButton searchDownButton;
    protected ImageButton searchUpButton;
    protected SwipeRefreshLayout swipeRefreshLayout;

    /* Other control */
    protected ListView chatListView;
    protected LinearLayout bottomMenuBar;
    protected LinearLayout bottomMoreMenuBar;
    protected LinearLayout bottomInputStatusLayout;
    protected LinearLayout bottomVoiceStatusLayout;
    protected ImageButton addButton;
    protected EditText messageEditText;
    protected ImageButton voiceButton;
    protected Button sendMessageButton;
    protected ImageButton moreGalleryButton;
    protected ImageButton moreTakePictureButton;
    protected ImageView voiceListeningImageView;
    protected ImageButton voiceListeningCancelButton;

    protected AdvancedWebView mWebView;
    protected Boolean first_page_loaded = false;
    protected String mUrl = "";
    protected String mCurrentHost = "";


    /* Rest packet */
    protected MarketPacket packet;
    /* Init intent View mode */
    protected int initChatViewMode = CHAT_VIEW_CHAT_MODE;
    /* Service group info */
    protected StoreListItem servieGroupInfo;
    /* Default AccessFrom */
    protected int defaultAccessFromValue = Dialog.AccessFrom.MOBILE_ANDROID_VALUE;
    /* Chat list items array */
    protected ArrayList<ChatListItem> chatListItems = new ArrayList<ChatListItem>();
    /* Past talk query result items array */
    protected ArrayList<ChatListItem> pastTalkSearchResultItems = new ArrayList<ChatListItem>();
    /* Past session search index to get the past talk detail info */
    protected int lastPastSessionIndex;
    /* Past session list */
    protected Dialog.PastSessionList pastSessionList;
    /* Chat meta recevied data reference instance */
    protected ReceiveMetaData receiveDecodeMetaData = new ReceiveMetaData();
    /* Message create time format */
    protected SimpleDateFormat messageDateFormat;
    /* Chat separator time format */
    protected SimpleDateFormat chatSeparatorDateForamt;
    /* 음성 요청으로 Text 응답을 받을지 여부를 설정한다. */
    protected boolean speakerOn = true;
    // Chatbot icon image.
    protected Bitmap chatbotIcon = null;
    // User icon image.
    protected Bitmap profileIcon = null;
    // Profile Image Url
    protected String profileImageUrl = "";
    /* Search list index */
    protected int searchIndex = -1;
    /* Current search word */
    protected String currentSearchWord;
    /* Search Mode on */
    protected boolean isSearchViewMode = false;
    /* Session Filter selected items */
    protected ArrayList sessonFilterSelectedItems = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        // Set Main Layout xml.
        setContentView(R.layout.activity_chat);

        // Init context.
        context = this;

        // Param 으로 넘어온 ServiceGroupKey 를 셋팅한다.
        Intent intent = getIntent();
        servieGroupInfo = (StoreListItem) intent.getSerializableExtra(EXTRA_SVC_GROUP_INFO);

        // Chatview mode setting.
        initChatViewMode = (int) intent.getExtras().get(EXTRA_CHAT_VIEW_MODE);
        Log.d(TAG, "initChatViewMode:" + initChatViewMode);

        // Init ui controls
        initUiControls();

        // Initialize client and set meta data receive handler
        setupChatClient(servieGroupInfo.getBigName());

        if (initChatViewMode == CHAT_VIEW_HISTORY_MODE) {
            swipeRefreshLayout.setEnabled(false);
            swipeRefreshLayout.setOnRefreshListener(null);
            bottomMenuBar.setVisibility(View.GONE);
            bottomMoreMenuBar.setVisibility(View.GONE);

        } else {
            // Init send handler.
            packet = new MarketPacket(context);
            packet.setListener(onPacket);
        }

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(this, this);
        mWebView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");

        // 웹 프로그램이 모바일 웹뷰인지 인식할 수 있게 하기위해
        String agentModified = mWebView.getSettings().getUserAgentString().concat(" " + getResources().getString(R.string.user_agent_string));
        mWebView.getSettings().setUserAgentString(agentModified);

        //  웹에서 음성  mp3  바로 재생 가능하도록 하기 위해
        mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

        // gin_java_bridge_dispatcher_host.cc .. WebView: Unknown object: .. 어쩌구 에러 나면서 링크 안눌러지는 오류 방지 위해
        mWebView.getSettings().setCacheMode(mWebView.getSettings().LOAD_NO_CACHE);

        try {
            URI currentUri = new URI(mUrl);
            mCurrentHost = currentUri.getHost();
        } catch(URISyntaxException e) {

        }

        mWebView.setVisibility(View.VISIBLE);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mWebView.reload(); // refreshes the WebView
            }
        });

        // 웹뷰의 div scroll 감지할 수 없어서 일단 사용 안함
        swipeRefreshLayout.setEnabled(false);
//        swipeRefreshLayout.setEnabled(true);

        //  API 서버의  AudioToText 가 동작하지 않는 관계로(TTS 서버 문제) 기본값을 Text로만 통신하게(스피커 끄기) 토글 시킨다.
        toggleSpeakerOn();
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public void loadUrl() {
        this.loadUrl(mUrl);
    }

    public void loadUrl(String url) {
        mWebView.loadUrl(url);

    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        if(progressDialog != null) {
            progressDialog.show();
        }


        // JSInterface  세팅하는걸  onPageFinished 에 놓았더니
        // 페이지가 다 로딩되기 전에 클릭하면  JSInterface 를 인식 못하는 것 같아서  onPageStarted 에서  JSInterface 를 세팅하게 함.
        try {
            URI currentUri = new URI(url);
            String currentHost = currentUri.getHost();
            if(currentHost==null) currentHost = "";

            mWebView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");
            mCurrentHost = currentHost;
        } catch(URISyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPageFinished(String url) {
        if(progressDialog != null) {
            progressDialog.hide();
        }
        progressDialog.hide();


        if(first_page_loaded==false) {
            TranslateAnimation animate = new TranslateAnimation(0,0,0,0);
            animate.setDuration(1000);
            animate.setFillAfter(true);
            //show webview
            findViewById(R.id.webview).setVisibility(View.VISIBLE);

            first_page_loaded = true;
        }

        if(swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }


    @Override
    public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) { }
//    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {}

    @Override
    public void onExternalPageRequest(String url) {}



    protected void setTitleBarText(String titleBarText) {
        TextView chatbotName = (TextView) findViewById(R.id.chatbotName);
        chatbotName.setText(titleBarText);
    }

    /**
     * Initialize ui controls.
     */
    private void initUiControls() {
        // Initialize view & control.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set chatbot name title.
        TextView chatbotName = (TextView) findViewById(R.id.chatbotName);
        chatbotName.setText(servieGroupInfo.getBigName().toUpperCase());


        // Initialize other view & control
        titleBarNonesearchHeader = (LinearLayout) findViewById(R.id.chat_title_bar_none_search_ll);
        backButton = (ImageButton) findViewById(R.id.back_btn);
        speakerButton = (ImageButton) findViewById(R.id.speaker_btn);
        speakerButton.setTag(true);
        searchButton = (ImageButton) findViewById(R.id.search_btn);

        // swipe_web_layout...
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_web_layout);

        titleBarSearchHeader = (LinearLayout) findViewById(R.id.chat_title_bar_search_ll);
        searchCloseButton = (ImageButton) findViewById(R.id.search_close_btn);
        searchWordEditText = (EditText) findViewById(R.id.seach_word_edittext);
        searchDownButton = (ImageButton) findViewById(R.id.search_down_btn);
        searchUpButton = (ImageButton) findViewById(R.id.search_up_btn);

        bottomMenuBar = (LinearLayout) findViewById(R.id.chat_bottom_menu_ll);
        bottomMoreMenuBar = (LinearLayout) findViewById(R.id.more_icon_ll);
        bottomInputStatusLayout = (LinearLayout) findViewById(R.id.chat_bottom_textinput_status_ll);
        bottomVoiceStatusLayout = (LinearLayout) findViewById(R.id.chat_bottom_voicelistening_status_ll);
        addButton = (ImageButton) findViewById(R.id.chat_add_btn);
        messageEditText = (EditText) findViewById(R.id.chat_text_et);
        voiceButton = (ImageButton) findViewById(R.id.chat_voice_btn);
        sendMessageButton = (Button) findViewById(R.id.chat_send_btn);
        moreGalleryButton = (ImageButton) findViewById(R.id.chat_more_gallery_btn);
        moreTakePictureButton = (ImageButton) findViewById(R.id.chat_more_camera_btn);
        voiceListeningImageView = (ImageView) findViewById(R.id.chat_voice_listening);
        voiceListeningCancelButton = (ImageButton) findViewById(R.id.chat_voice_listen_cancel_btn);
        progressDialog = new SimpleProgressDialog(context);
        //GRPC 등 서버 미응답 상태일때를 대비해 취소할 수 있도록 한다.
        //    progressDialog.setCancelable(true);

        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        backButton.setOnClickListener(onClickListener);
        speakerButton.setOnClickListener(onClickListener);
        searchButton.setOnClickListener(onClickListener);
        searchCloseButton.setOnClickListener(onClickListener);
        searchDownButton.setOnClickListener(onClickListener);
        searchUpButton.setOnClickListener(onClickListener);

        // 요청에 의해 막음 처리.
        // addButton.setOnClickListener(onClickListener);
        voiceButton.setOnClickListener(onClickListener);
        voiceListeningCancelButton.setOnClickListener(onClickListener);
        sendMessageButton.setOnClickListener(onClickListener);
        moreGalleryButton.setOnClickListener(onClickListener);
        moreTakePictureButton.setOnClickListener(onClickListener);

//        searchWordEditText.setOnEditorActionListener(OnSearchEditActionListener);
        messageEditText.addTextChangedListener(messageEditTextWatcher);

        // Message date format.
        messageDateFormat = new SimpleDateFormat("hh:mm aa");
        // Chat separtor data format.
        chatSeparatorDateForamt =  new SimpleDateFormat("yyyy-MM-dd hh:mm aa");

        String[] stringArray = getResources().getStringArray(R.array.chatting_session_filter);
        // Initialize session filter default.
        for(int i = 0; i < stringArray.length; i++) {
            sessonFilterSelectedItems.add(i, true);
        }

        // added by kimho
        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {

        mWebView.loadUrl("javascript:backPressed();");
        return;
    }

    // Cleanup
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Stop voice recording.
        stopRecording();
        // Stop audio track playing.
        stopPlaying();
        musicUrl_ = null;
        //stopMediaPlayer();
        // Player control 을 정지시킨다.
        musicPlayer.playAndPause(false);

        // Close chat client.
        try {
            chatClient.close();
            chatClient.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //cleanup();

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Stop voice recording.
        stopRecording();
        musicUrl_ = null;

        //stopMediaPlayer();
        // Player control 을 정지시킨다.
        musicPlayer.playAndPause(false);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (initChatViewMode == CHAT_VIEW_CHAT_MODE) {
            // Start recording.
            startRecording();
            // Start playing.
            startPlaying();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    protected boolean addAudioToTextTalkData(String text) {
        Log.i(TAG, "TextUtter Callback:" + text);

        // Check chat session.
        if (chatClient.getSessionId() == null) {
            Log.e(TAG, "addAudioToTextTalkData()..chatClient.getSessionId() is invalid");
            return false;
        }

        Calendar calendar = Calendar.getInstance();
        final String messageTime = messageDateFormat.format(calendar.getTime());
        ChatItemType chatItemType;


        // 메타 데이터 존재여부 체크한다.
        if (receiveDecodeMetaData.hasDecodeMetaData() == true) {
            ChatMetaObject metaObject = null;
            DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
            // Intext message field check.
            String sendMessage = decodeMetaData.getInText();

            mWebView.loadUrl("javascript:runClientSpeech('"+jsParamText(sendMessage)+"');");

            // '얘 초롱아' 등 Wakeup keyword 에 의한 Response 메시지가 없는 경우. 빈 대화 메시지 방지 체크.
            if (sendMessage != null && sendMessage.trim().length() > 0) {
            }

            // OutText Message check.
            //String receiveMessage = decodeMetaData.getOutText();
            String receiveMessage = text;
            if (decodeMetaData.getOutEmbedType() != null) {
                // Embed type 존재시 outtext message 를 1개의 채팅아이템에 결합한다.
                metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
                metaObject.setMessageTime(messageTime);
                if (receiveMessage != null) {
                    metaObject.setUtterMessage(receiveMessage);
                }

                mWebView.loadUrl("javascript:receiveResultFromServer('"+jsParamText(receiveMessage)+"');");

                metaObject.setOnLongClickListener(onLongClickListener);
            } else {
            }

            mWebView.loadUrl("javascript:receiveMessageFromServer('"+jsParamText(receiveMessage)+"');");

            // Clear for next received meta data container.
            receiveDecodeMetaData.clear();
        }

        return true;
    }

    @Override
    protected boolean addAudioTalkData() {
        // Check chat session.
        if (chatClient.getSessionId() == null) {
            Log.e(TAG, "addAudioTalkData()..chatClient.getSessionId() is invalid");
            return false;
        }

        Calendar calendar = Calendar.getInstance();
        final String messageTime = messageDateFormat.format(calendar.getTime());
        ChatItemType chatItemType;

        // 메타 데이터 존재여부 체크한다.
        if (receiveDecodeMetaData.hasDecodeMetaData() == true) {
            ChatMetaObject metaObject = null;
            DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
            // Intext message field check.
            String sendMessage = decodeMetaData.getInText();

            mWebView.loadUrl("javascript:runClientSpeech('"+jsParamText(sendMessage)+"');");

            // '얘 초롱아' 등 Wakeup keyword 에 의한 Response 메시지가 없는 경우. 빈 대화 메시지 방지 체크.
            if (sendMessage != null && sendMessage.trim().length() > 0) {
            }

            // Check OutEmbedType.
            String receiveMessage = decodeMetaData.getOutText();
            if (decodeMetaData.getOutEmbedType() != null) {
                // Embed type 존재시 outtext message 를 1개의 채팅아이템에 결합한다.
                metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
                metaObject.setMessageTime(messageTime);
                if (receiveMessage != null) {
                    metaObject.setUtterMessage(receiveMessage);
                }

                mWebView.loadUrl("javascript:receiveResultFromServer('"+jsParamText(receiveMessage)+"');");

                metaObject.setOnLongClickListener(onLongClickListener);
            } else {
            }

            mWebView.loadUrl("javascript:receiveMessageFromServer('"+jsParamText(receiveMessage)+"');");

            // Clear for next received meta data container.
            receiveDecodeMetaData.clear();

        }

        return true;
    }

    @Override
    protected boolean addChatData(final DecodeMetaData decodeMetaData) {
        receiveDecodeMetaData.setDecodedMetatdata(decodeMetaData);

        return true;
    }

    @Override
    protected boolean isSpeakerOn() {
        return speakerOn;
    }

    @Override
    protected boolean getSearchMode() {
        return isSearchViewMode;
    }

    @Override
    protected void voiceListeningView(boolean enable) {
        if (enable) {
            bottomInputStatusLayout.setVisibility(View.INVISIBLE);
            bottomVoiceStatusLayout.setVisibility(View.VISIBLE);

            if(android.os.Build.VERSION.SDK_INT >= 21){
                voiceListeningImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_voice_listening, context.getTheme()));
            } else {
                voiceListeningImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_voice_listening));
            }
            AnimationDrawable frameAnimation = (AnimationDrawable) voiceListeningImageView.getDrawable();
            frameAnimation.start();
        } else {
            bottomInputStatusLayout.setVisibility(View.VISIBLE);
            bottomVoiceStatusLayout.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Toggle Speaker On/Off button.
     */
    public void toggleSpeakerOn() {
        // Value change.
        speakerOn = (Boolean)speakerButton.getTag();
        Log.d(TAG, "BEFOE Speaker Status:" + speakerOn);
        speakerOn = !speakerOn;
        Log.d(TAG, "AFTER Speaker Status:" + speakerOn);
        speakerButton.setTag(speakerOn);

        // Change buttong image.
        Drawable drawableId;
        if (speakerOn == true) {
            if(android.os.Build.VERSION.SDK_INT >= 21){
                drawableId = getResources().getDrawable(R.drawable.ic_speaker, context.getTheme());
            } else {
                drawableId = getResources().getDrawable(R.drawable.ic_speaker);
            }
        } else {
            if(android.os.Build.VERSION.SDK_INT >= 21){
                drawableId = getResources().getDrawable(R.drawable.ic_speaker_mute, context.getTheme());
            } else {
                drawableId = getResources().getDrawable(R.drawable.ic_speaker_mute);
            }
        }

        speakerButton.setImageDrawable(drawableId);
    }

    /**
     * Chat message OnLongClick listener.
     */
    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            Log.d(TAG, "Show context menu");
            ChatMetaObject chatMetaObject = (ChatMetaObject)v.getTag();

            showContextMenuDialog(chatMetaObject);
            return true;
        }
    };


    /**
     * Clipboard 에 string 을 저장하다.
     * @param text 저장하고자 하는 텍스트.
     */
    private void saveToClipboard(String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard =
                    (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard =
                    (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }

        String message = getString(R.string.chatting_save_to_cliboard);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Chatbot context menu dialog 를 보여준다.
     */
    private void showContextMenuDialog(final ChatMetaObject chatMetaObject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(R.array.chatbot_context_menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String sessionSting;

                // The 'which' argument contains the index position
                // of the selected item
                switch (which) {
                    case 0://Share.
                        String subject = "MINDsLab";
                        String text = chatMetaObject.getChatTitle();

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                        intent.putExtra(Intent.EXTRA_TEXT, text);
                        Intent chooser = Intent.createChooser(intent, "타이틀");
                        startActivity(chooser);
                        break;
                    case 1://Copy.
                        // Get all session string.
                        sessionSting = chatMetaObject.getChatTitle();
                        // Save to clipboard.
                        saveToClipboard(sessionSting);
                        break;
                    case 2://Copy this session.
                        // Get all session string.
                        sessionSting = getThisSessionString(chatMetaObject.getSessionId());
                        // Save to clipboard.
                        saveToClipboard(sessionSting);
                        break;
                }
            }
        });

        // create alert dialog
        AlertDialog alertDialog = builder.create();
        // show it
        alertDialog.show();
    }


    /**
     * Message edit text watcher.
     */
    private TextWatcher messageEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String inputString = s.toString();
            if (inputString != null && inputString.trim().length() > 0) {
                voiceButton.setVisibility(View.INVISIBLE);
                sendMessageButton.setVisibility(View.VISIBLE);
            } else {
                voiceButton.setVisibility(View.VISIBLE);
                sendMessageButton.setVisibility(View.INVISIBLE);
            }
        }
    };


    /**
     * Activity onRefresh listener.
     */
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener =
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    // 새로고침 완료
                    swipeRefreshLayout.setRefreshing(false);
                }
            };

    /**
     * Activity onClick listener.
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;

            switch (v.getId()) {
                case R.id.back_btn:
                    // Chatting 화면을 종료한다.
                    finish();
                    break;
                case R.id.speaker_btn:
                    // Speaker On/Off.
                    toggleSpeakerOn();
                    break;
                case R.id.chat_add_btn:
                    if (findViewById(R.id.more_icon_ll).getVisibility() == View.VISIBLE) {
                        findViewById(R.id.more_icon_ll).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.more_icon_ll).setVisibility(View.VISIBLE);
                    }
                    break;
                case R.id.chat_voice_btn:
                    // Stop audio track playing.
                    stopPlaying();
                    // Next Voice Request Action.
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int waitCountMax = 0;
                            while(isPlayingThreadEndCompleted == false && waitCountMax++ < 20) {
                                Log.d(TAG, "[Chat voice request..audioTrack stop waiting!!!! counting :" + waitCountMax);
                                sleep(100);
                            }
                            Log.d(TAG, "[Chat voice request..audioTrack stopped!!!!]:" + waitCountMax);
                            // Voice Listening Dialog show.
                            voiceListeningView(true);
                            // start recording
                            startRecording();
                            // '얘 초롱아' 를 다시 보낸다.
                            chatbotState = SLEEPING;
//                            processKey("얘초롱아", (short)1000); //  한글로 오픈
                            processKey("오케이마인즈", (short)1000); //  영어로 오픈.  edited by kimho
                        }
                    }, 100);

                    break;
                case R.id.chat_voice_listen_cancel_btn:
                    // 음성 입력 호출을 중단한다.
                    if (sender_ != null) {
                        voiceTalkStop();
                    }
                    break;
                case R.id.chat_send_btn:
                    if (speakerOn == true) {
                        startPlaying();
                        sendTextMessageToAudio(null);
                    } else {
                        sendTextMessage(null);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    /**
     * Text message send and response data add to chat list with audio response.
     */
    private void sendTextMessageToAudio(String message) {
        // Check chat session.
        if (chatClient.getSessionId() == null) {
            Log.e(TAG, "sendTextMessageToAudio()..chatClient.getSessionId() is invalid");
            return;
        }

        Calendar calendar = Calendar.getInstance();
        final String messageTime = messageDateFormat.format(calendar.getTime());
//        final String sendMessage = messageEditText.getText().toString();
        String sendMessage = null;
        if(message==null) {
            sendMessage = messageEditText.getText().toString();
        } else {
            sendMessage = message;
        }
        if (!sendMessage.trim().isEmpty()) {
            // GRPC Send message.
            sendTextToAudioMessage(sendMessage);
            // Clear input text box.
            messageEditText.setText("");
        }
    }

    /**
     * Text message send and response data add to chat list.
     */
    private void sendTextMessage(String message) {
        // Check chat session.
        if (chatClient.getSessionId() == null) {
            Log.e(TAG, "sendTextMessage()..chatClient.getSessionId() is invalid");
            return;
        }

        Calendar calendar = Calendar.getInstance();
        final String messageTime = messageDateFormat.format(calendar.getTime());
        String sendMessage = null;
        if(message==null) {
            sendMessage = messageEditText.getText().toString();
        } else {
            sendMessage = message;
        }

        if (!sendMessage.trim().isEmpty()) {
            StreamObserver<Dialog.TextUtter> sender = chatClient.getSenderTextTalk(new ITextCallback() {
                @Override
                public void onText(final String receiveMessage) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(TAG, "TextUtter Callback:::" + receiveMessage);
                            // Check chat session.
                            if (chatClient.getSessionId() == null) {
                                Log.e(TAG, "getSenderTextTalk()..onText()..chatClient.getSessionId() is invalid");
                                return;
                            }

                            DecodeMetaData decodeMetaData = receiveDecodeMetaData.getDecodedMetatdata();
                            // 메타 데이터 존재여부 체크한다.
                            if (receiveDecodeMetaData.hasDecodeMetaData() == true
                                    && decodeMetaData.getOutEmbedType() != null) {
                                // Embed type 존재시 response message 를 1개의 채팅아이템에 결합한다.
                                ChatMetaObject metaObject = receiveDecodeMetaData.getEmbedMetaDataObject();
                                metaObject.setMessageTime(messageTime);
                                if (receiveMessage != null) {
                                    metaObject.setUtterMessage(receiveMessage);
                                }
                                mWebView.loadUrl("javascript:receiveResultFromServer('"+jsParamText(receiveMessage)+"');");
                                metaObject.setOnLongClickListener(onLongClickListener);
                            } else {

                                mWebView.loadUrl("javascript:receiveMessageFromServer('"+jsParamText(receiveMessage)+"');");
                            }

                        }
                    });
                }

                @Override
                public void onError(final String messsage) {
                    // Show error message.
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String errorMessage = context.getString(R.string.error_grpc_connection_fail);
                            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            Log.d(TAG, messsage);
                        }
                    });
                }
            });
            Dialog.TextUtter.Builder builder = Dialog.TextUtter.newBuilder();
            builder.setUtter(sendMessage);
            Dialog.TextUtter utter = builder.build();
            sender.onNext(utter);
            sender.onCompleted();

            messageEditText.setText("");
        }
    }


    /**
     * Get this session message text.
     * @return session string.
     */
    private String getThisSessionString(long sessionId) {
        String sessionString = "";

        for (int i = 0; i < chatListItems.size(); i++) {
            ChatListItem chatListItem = chatListItems.get(i);

            // Check session id.
            if (sessionId != chatListItem.getSessionId()) {
                continue;
            }

            sessionString += "\n" + chatListItem.getChatMetaObject().getChatTitle();
        }

        return sessionString;
    }

    /**
     * Show keyboard forced.
     */
    private void showKeyboard(EditText editText) {
        if (editText == null) {
            Log.d(TAG, "showKeyboard() editText is null");
            return;
        }

        // Keyboard show.
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Hide keyboard forced.
     * @param editText
     */
    private void hideKeyboard(EditText editText) {
        if (editText == null) {
            Log.d(TAG, "hideKeyboard() editText is null");
            return;
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    /**
     * Http 전송 결과 값 Res 리스너
     */
    private MarketPacket.IPacketListener onPacket = new MarketPacket.IPacketListener() {
        @Override
        public void onApiResult(String resMethod, int resCode, Response resObj) {
            Log.d(TAG, "resMethod : " + resMethod);
            Log.d(TAG, "resCode : " + resCode);
            Log.d(TAG, "resObj : " + resObj);

            try {
                if(resCode == HttpURLConnection.HTTP_OK){
                    Method method = this.getClass().getDeclaredMethod(resMethod, Response.class);
                    method.invoke(this, resObj);
                } else {
                    if (resCode == HttpURLConnection.HTTP_NOT_FOUND
                            && "resFindOne".equals(resMethod) == true) {
                        Log.d(TAG, "404, resFindOne() No data.");
                    } else {
                        HashMap<String, Object> errorMap = (HashMap<String, Object>) resObj.get("error");
                        String errorMessage = errorMap.get("message").toString();
                        Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // Dismiss progressbar
                progressDialog.dismiss();
            }
        }

        @Override
        public void onApiError(int resCode, String errorMessage) {
            Toast.makeText(getBaseContext(), errorMessage, Toast.LENGTH_LONG).show();
            // Dismiss progressbar
            progressDialog.dismiss();
        }


    };

    /**
     * 수신된 Meta data object container.
     */
    private class ReceiveMetaData {
        private boolean isSet = false;
        private DecodeMetaData decodedMetatdata = null;

        /**
         * Has received meta data object set.
         * @return True is success.Else is failure.
         */
        public boolean hasDecodeMetaData() {
            return this.isSet;
        }

        /**
         * Clear meta data object.
         */
        public void clear() {
            this.isSet = false;
        }

        /**
         * Decode metadata getter.
         * @return Decode metadata.
         */
        public DecodeMetaData getDecodedMetatdata() {
            return decodedMetatdata;
        }

        /**
         * Decode metadata setter.
         * @param decodedMetatdata Decode meta data.
         */
        public void setDecodedMetatdata(DecodeMetaData decodedMetatdata) {
            if (decodedMetatdata == null)
                return;

            this.decodedMetatdata = decodedMetatdata;
            isSet = true;
        }

        /**
         * Get metaobject from decoded meta data.
         * @return Parsed meta object.
         */
        public ChatMetaObject getEmbedMetaDataObject() {
            ChatItemType chatItemType = CHAT_NONE_MESSAGE_TYPE;
            ChatMetaObject metaObject = null;
            DecodeMetaData decodeMetaData = this.decodedMetatdata;

            // Check decode meta data.
            if (decodeMetaData == null) {
                return null;
            }

            // Add talk data to chat list.
            String outEmbedType = decodeMetaData.getOutEmbedType();
            if ("html5".equals(outEmbedType)) {
                String dataStyle = decodeMetaData.getOutEmbedDataStyle();
                String dataBody = decodeMetaData.getOutEmbedDataBody();
                metaObject = new ChatHtmlContent(context, dataStyle, dataBody);
                metaObject.setOnLongClickListener(onLongClickListener);
                metaObject.chatItemType = CHAT_RCV_MESSAGE_HTML_TYPE;
            } else if ("audio".equals(outEmbedType)) {;
                String dataUrl = decodeMetaData.getOutEmbedDataUrl();
                String mimeType = decodeMetaData.getOutEmbedDataMime();
                String sampleRate = decodeMetaData.getOutEmbedDataSamplerate();

                ArrayList<String> playList = new ArrayList<String>();
                playList.add(dataUrl);
                ChatAudioContent chatAudioContent =
                        new ChatAudioContent(context, playList, mimeType, sampleRate);
                chatAudioContent.setMusicPlayer(musicPlayer);
                chatAudioContent.setOnLongClickListener(onLongClickListener);
                chatAudioContent.chatItemType = CHAT_RCV_MESSAGE_AUDIO_TYPE;
                metaObject = chatAudioContent;
            } else if ("link".equals(outEmbedType)) {
                metaObject = new ChatLinkContent(context, decodeMetaData.getOutEmbedDataUrl());
                metaObject.setOnLongClickListener(onLongClickListener);
                metaObject.chatItemType = CHAT_RCV_MESSAGE_LINK_TYPE;
            }

            // Intext message field check.
            if (decodeMetaData.getInText() != null) {
                if (metaObject == null) {
                    // Outembed type 미생성시 text chat object 로 생성한다.
                    // Audio Talk 대화 시만 존재하여, Send Message Type 으로 지정한다.
                    metaObject = new ChatTextContent(context, decodeMetaData.getInText());
                    metaObject.chatItemType = CHAT_SEND_MESSAGE_TEXT_TYPE;
                } else {
                    metaObject.setIntextMessage(decodeMetaData.getInText());
                }
            }

            return metaObject;
        }
    }


    public void showChatBottomInput() {
        bottomInputStatusLayout.setVisibility(View.VISIBLE);
    }

    private void callAppBackPress() {
        super.onBackPressed();
    }

    private class JavaScriptInterface {

        @JavascriptInterface
        public void sendMessage(String message) {
            final String msg = message;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sendTextMessage(msg);
                }
            });
        }

        @JavascriptInterface
        public void sendMessageResAudio(String message) {
            final String msg = message;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sendTextMessageToAudio(msg);
                }
            });
        }

        @JavascriptInterface
        public void appBackPressed() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callAppBackPress();
                }
            });
        }

        @JavascriptInterface
        public void showChatInputBox() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showChatBottomInput();
                }
            });
        }

        @JavascriptInterface
        public void hideKeyboard() {
            View view = WebViewActivity.this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

        @JavascriptInterface
        public void openBrowser(String url) {
            if (AdvancedWebView.Browsers.hasAlternative(WebViewActivity.this)) {
                AdvancedWebView.Browsers.openUrl(WebViewActivity.this, url);
            }
        }

        @JavascriptInterface
        public void moveUrl(final String url) {

            WebViewActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl(url);
                }
            });
        }

    }

    protected String jsParamText(String text) {
        text = text.replaceAll("'", "\\\\'"); //  작은 따옴표  escape
        text = text.replaceAll("[\u0000-\u001f]", ""); //  안보이는 문자들 제거
        return text;
    }
}
