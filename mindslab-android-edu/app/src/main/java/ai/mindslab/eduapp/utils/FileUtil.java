package ai.mindslab.eduapp.utils;

import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Classes for FileUtil
 */
public class FileUtil {
  /* Download profile image local path */
  public static final String PROFILE_ICON_FILE_NAME = "local_profile.png";
  /* Download selected chatbot image local path */
  public static final String SELECTED_CHATBOT_ICON_FILE_NAME = "local_chatbot.png";
  /* Download selected chatbot image local path */
  public static final String SELECTED_CHATBOT_ICON_FILE_NAME_INMARKET = "local_chatbot_market.png";

  /**
   * Get profile image local path
   * @param context context.
   * @return string path.
   */
  public static String getProfileIconFilePath(Context context) {
    String profileImageSavePath = context.getFilesDir().getAbsolutePath().toString();
    profileImageSavePath += "/" + FileUtil.PROFILE_ICON_FILE_NAME;

    return profileImageSavePath;
  }

  /**
   * Get select chatbot icon image local path
   * @param context context.
   * @return string path.
   */
  public static String getSelectedChatbotIconFilePath(Context context) {
    String chatbotIconPath = context.getFilesDir().getAbsolutePath().toString();
    chatbotIconPath += "/" + FileUtil.SELECTED_CHATBOT_ICON_FILE_NAME;

    return chatbotIconPath;
  }

  /**
   * Get select chatbot icon image local path in market
   * - Not used.
   * @param context context.
   * @return string path.
   */
  /*public static String getSelectedChatbotIconFilePathInMarket(Context context) {
    String chatbotIconPath = context.getFilesDir().getAbsolutePath().toString();
    chatbotIconPath += "/" + FileUtil.SELECTED_CHATBOT_ICON_FILE_NAME_INMARKET;

    return chatbotIconPath;
  }*/

  /**
   * Write to file with bitmap data.
   * @param path string path.
   * @param bitmap bitmap data.
   * @return 0 is successs. Else is failure.
   */
  public static int writeFileWithBitmapData(String path, Bitmap bitmap) {
    int result = -1;
    if (path == null || path.trim().length() == 0 || bitmap == null) {
      Log.d(TAG, "writeFileWithBitmapData() has null param check.!!");
      return result;
    }

    File fileCacheItem = new File(path);
    OutputStream out = null;

    try {
      fileCacheItem.createNewFile();
      out = new FileOutputStream(fileCacheItem);

      bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

      // Set success code is 0.
      result = 0;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        out.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return result;
  }
}
