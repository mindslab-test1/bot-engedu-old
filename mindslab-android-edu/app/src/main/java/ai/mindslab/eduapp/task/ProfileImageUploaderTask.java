package ai.mindslab.eduapp.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.network.NetConfigInfo;
import ai.mindslab.eduapp.preference.PreferenceHelper;
import ai.mindslab.eduapp.view.SimpleProgressDialog;

/**
 * Classes for ProfileImageUploaderTask
 */
public class ProfileImageUploaderTask extends AsyncTask<String, Integer, String> {
  private static final String TAG = ProfileImageUploaderTask.class.getSimpleName();

  /* Context */
  private Context context;
  /* Progress Dialog */
  private SimpleProgressDialog progressDialogForTask;
  /* Listener */
  private OnCompleteListener onCompleteListener;

  /**
   * Constructor
   * @param context
   */
  public ProfileImageUploaderTask(Context context) {
    this.context = context;
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    progressDialogForTask = new SimpleProgressDialog(context);
    progressDialogForTask.show();
  }

  @Override
  protected String doInBackground(String... str) {

    String res = null;
    try {
      String ImagePath = str[0];
      File sourceFile = new File(ImagePath);
      Log.d(TAG, "File...::::" + sourceFile + " : " + sourceFile.exists());

      final MediaType mimeType = MediaType.parse("image/jpeg");
      String filename = ImagePath.substring(ImagePath.lastIndexOf("/") + 1);

      // Get Access-Token
      String accessToken = PreferenceHelper.getStringKeyValue(context,
          PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);
      // Get username
      String username = PreferenceHelper.getStringKeyValue(context,
          PreferenceHelper.KEY_LOGIN_USER_NAME);
      // Get userId
      String userId = PreferenceHelper.getStringKeyValue(context,
          PreferenceHelper.KEY_LOGIN_USER_ID);
      // Get upload url
      String urlString = NetConfigInfo.getApiUrl() + NetConfigInfo.API_COMMON_URI + "/Files/upload";
      Log.e(TAG, "Start image send:" + urlString);

      // Make header & body
      RequestBody requestBody = new MultipartBuilder()
          .type(MultipartBuilder.FORM)
          .addFormDataPart("id", userId)
          .addFormDataPart("x-elsa-authentication-provider", "mindslab")
          .addFormDataPart("x-elsa-authentication-token", accessToken)
          .addFormDataPart("file", "avatar.jpg", RequestBody.create(mimeType, sourceFile))
          .build();

      // HTTP Post request.
      Request request = new Request.Builder()
          .url(urlString)
          .post(requestBody)
          .build();

      OkHttpClient client = new OkHttpClient();
      // Create a trust manager that does not validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
          return new X509Certificate[]{};
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
      }};

      SSLContext sc = SSLContext.getInstance("TLS");
      sc.init(null, trustAllCerts, new SecureRandom());
      client.setSslSocketFactory(sc.getSocketFactory());

      Response response = client.newCall(request).execute();
      res = response.body().string();
      return res;

    } catch (UnknownHostException | UnsupportedEncodingException e) {
      Log.e(TAG, "Error: " + e.getLocalizedMessage());
      String errorMessage = context.getString(R.string.error_rest_image_unknown_host);
      Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    } catch (Exception e) {
      Log.e(TAG, "Other Error: " + e.getLocalizedMessage());
      String errorMessage = context.getString(R.string.error_rest_image_cannot_find_image);
      Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    return res;
  }

  @Override
  protected void onPostExecute(String response) {
    super.onPostExecute(response);
    if (progressDialogForTask != null) {
      progressDialogForTask.dismiss();
    }
    Log.d(TAG, "onPostExecute:" + response);
    // Success
    if (response != null) {
      StringBuilder stringBuilder = new StringBuilder(response);
      int index = 0;
      String specialChar = stringBuilder.substring(index, 1);
      if ("[".equals(specialChar) == true) {
        stringBuilder.replace(index, 1, "");
      }

      index = stringBuilder.length() - 1;
      specialChar = stringBuilder.substring(index, stringBuilder.length());
      if ("]".equals(specialChar) == true) {
        stringBuilder.replace(index, stringBuilder.length(), "");
      }

      if (onCompleteListener != null) {
        onCompleteListener.onComplete(stringBuilder.toString());
      }
    } else {
      if (onCompleteListener != null) {
        String errorMessage = context.getString(R.string.error_rest_image_upload_error);
        onCompleteListener.onError(errorMessage);
      }
    }
  }

  /**
   * Complete listener
   * @param onCompleteListener
   */
  public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
    this.onCompleteListener = onCompleteListener;
  }

  /**
   * 수행 결과를 받는  정보를 UI 리스너에 전달 하기 위함이다.
   */
  public interface OnCompleteListener {
    void onComplete(String response);
    void onError(String errorMessage);
  }
}

