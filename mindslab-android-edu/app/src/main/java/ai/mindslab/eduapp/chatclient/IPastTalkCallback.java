package ai.mindslab.eduapp.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

import elsa.facade.Dialog;

/**
 * PastTalk callback interface
 */
public interface IPastTalkCallback {
  /**
   * Get past talk list
   * @param list
   */
  void onGetPastTalkList(Dialog.PastTalkList list);

  /**
   * Error
   * @param message error message.
   */
  void onError(String message);

  /**
   * Complete
   */
  void onCompleted();
}
