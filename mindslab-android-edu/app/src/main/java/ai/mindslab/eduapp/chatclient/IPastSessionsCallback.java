package ai.mindslab.eduapp.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

import elsa.facade.Dialog;

/**
 * PastSession callback interface
 */
public interface IPastSessionsCallback {
  /**
   * Get past session list.
   * @param list past session list.
   */
  void onGetPastSessionsList(Dialog.PastSessionList list);

  /**
   * Error
   * @param message error message.
   */
  void onError(String message);

  /**
   * Complete
   */
  void onCompleted();
}
