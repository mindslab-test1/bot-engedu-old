package ai.mindslab.eduapp.view;

/*
 * Created by hybell on 2016. 12. 27..
 */

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.mindslab.eduapp.R;
/**
 * Classes for ChatTextContent
 */
public class ChatTextContent extends ChatMetaObject {
  /* Context */
  private Context context;
  /* Text Info */
  private String textData;
  /* UI Controls */
  private TextView messageTitleTextView;

  /**
   * Constructor
   * @param context base context.
   */
  public ChatTextContent(Context context, String textData) {
    this.context = context;
    this.textData = textData;
  }

  @Override
  public void addToParent(final LinearLayout parent, final boolean hasAlready) {
    View view = LayoutInflater.from(context).inflate(R.layout.chat_list_item_text_content, null);
    messageTitleTextView = (TextView) view.findViewById(R.id.chat_msg_content_title);

    // 검색 모드일 경우 Keyword 가 있는지 확인한다.
    if (searchModeOn == true) {
      searchKeyword(searchWord);
    } else {
      messageTitleTextView.setText(this.textData);
    }

    // Previous child view remove.
    if (hasAlready == true) {
      parent.removeAllViews();
    }
    // Long click listener 등록.
    if (onLongClickListener != null) {
      view.setOnLongClickListener(onLongClickListener);
      view.setTag(getMe());
    }
    // Add to parent.
    parent.addView(view);
  }

  @Override
  public String getChatTitle() {
    if (utterMessage != null) {
      return utterMessage;
    } else if (textData != null) {
      return textData;
    }
    return null;
  }

  @Override
  protected void searchKeyword(String keyword) {
    // Check keyword length.
    if (keyword == null || keyword.trim().length() == 0) {
      return;
    }

    String message;
    // Meta data 의 경우 uttermessage 로 체크한다.
    if (utterMessage != null) {
      message = utterMessage;
    } else {
      // 일반 텍스트 메시지의 경우 TextMessage 로 판단한다.
      message = textData;
    }

    SpannableStringBuilder sps = getSpannableString(context, message, keyword);
    messageTitleTextView.setText(sps);
  }

  /**
   * Text string getter.
   * @return Test string.
   */
  public String getTextData() {
    return textData;
  }

  /**
   * Text string setter.
   * @param textData Text string.
   */
  public void setTextData(String textData) {
    this.textData = textData;
  }
}
