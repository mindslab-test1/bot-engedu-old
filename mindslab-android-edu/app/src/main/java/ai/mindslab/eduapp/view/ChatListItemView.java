package ai.mindslab.eduapp.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ai.mindslab.eduapp.R;
import ai.mindslab.eduapp.model.ChatListItem;
/**
 * Classes for ChatListItemView
 */
public class ChatListItemView extends RelativeLayout {
  /* Context */
  private Context context;
  /* UI Controls */
  private View rcvMessageView;
  /* UI Controls */
  private LinearLayout rcvMessageViewContent;
  /* UI Controls */
  private TextView rcvMessageTime;
  /* UI Controls */
  private View sendMessageView;
  /* UI Controls */
  private LinearLayout sendMessageViewContent;
  /* UI Controls */
  private TextView sendMessageTime;
  /* UI Controls */
  private ImageView receiveUserIcon;
  /* UI Controls */
  private ImageView sendUserIcon;
  /* UI Controls */
  private View separatorView;
  /* UI Controls */
  private LinearLayout separatorViewContent;
  /* UI Controls */
  private TextView separatorMessage;

  /**
   * Constructor
   * @param context base context.
   */
  public ChatListItemView(Context context) {
    this(context, null);
  }

  public ChatListItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public ChatListItemView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    LayoutInflater.from(context).inflate(R.layout.chat_list_item_view, this, true);
    initView(context);
  }

  /**
   * User custom function.
   * @param context base context.
   */
  private void initView(Context context) {
    this.context = context;
    setupViewHolder();
  }

  public ChatListItemView inflate(ViewGroup parent) {
    ChatListItemView view =
        (ChatListItemView) LayoutInflater
            .from(parent.getContext()).inflate(R.layout.chat_list_item_row, parent, false);
    return view;
  }

  /**
   * Setup view holder.
   */
  private void setupViewHolder() {
    rcvMessageView = findViewById(R.id.list_rcv_msg_type_ll);
    rcvMessageViewContent = (LinearLayout) findViewById(R.id.rcv_msg_content_ll);
    rcvMessageTime = (TextView) findViewById(R.id.rcv_msg_time);
    receiveUserIcon = (ImageView) findViewById(R.id.rcv_user_icon);

    sendMessageView = findViewById(R.id.list_send_msg_type_ll);
    sendMessageViewContent = (LinearLayout) findViewById(R.id.send_msg_content_ll);
    sendMessageTime = (TextView) findViewById(R.id.send_msg_time);
    sendUserIcon = (ImageView) findViewById(R.id.send_user_icon);

    separatorView = findViewById(R.id.list_separator_msg_type_ll);
    separatorViewContent = (LinearLayout) findViewById(R.id.separator_content_ll);
    separatorMessage = (TextView) findViewById(R.id.separator_msg);
  }

  /**
   * Set layout data.
   * @param data menu item object.
   * @param hasAlready True is the target view has already.
   */
  public void setData(Object data, boolean hasAlready, boolean searchModeOn, String searchWord,
                      Bitmap profileImage) {
    if (data != null) {
      ChatListItem item = (ChatListItem) data;
      ChatMetaObject chatMetaObject = item.getChatMetaObject();
      LinearLayout targetContent;

      // Set meta object by message type.
      if (item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_TEXT_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_HTML_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_AUDIO_TYPE
          || item.getType() == ChatListItem.ChatItemType.CHAT_RCV_MESSAGE_LINK_TYPE) {
        // Set chatbot icon
        if (item.getUserImage() != null) {
          receiveUserIcon.setImageBitmap(item.getUserImage());
        }

        rcvMessageView.setVisibility(View.VISIBLE);
        sendMessageView.setVisibility(View.GONE);
        separatorView.setVisibility(View.GONE);

        rcvMessageTime.setText(chatMetaObject.getMessageTime());
        targetContent = rcvMessageViewContent;
      } else if (item.getType() == ChatListItem.ChatItemType.CHAT_SEND_MESSAGE_TEXT_TYPE) {
        rcvMessageView.setVisibility(View.GONE);
        sendMessageView.setVisibility(View.VISIBLE);
        separatorView.setVisibility(View.GONE);
        // Set user icon
        if (profileImage != null) {
          sendUserIcon.setImageBitmap(profileImage);
        } else {
          if(android.os.Build.VERSION.SDK_INT >= 21){
            sendUserIcon.setImageDrawable(getResources().getDrawable(R.drawable.img_profile_dark, context.getTheme()));
          } else {
            sendUserIcon.setImageDrawable(getResources().getDrawable(R.drawable.img_profile_dark));
          }
        }

        sendMessageTime.setText(chatMetaObject.getMessageTime());
        targetContent = sendMessageViewContent;
      } else if (item.getType() == ChatListItem.ChatItemType.CHAT_SESSION_SEPARATOR_TYPE) {
        rcvMessageView.setVisibility(View.GONE);
        sendMessageView.setVisibility(View.GONE);
        separatorView.setVisibility(View.VISIBLE);
        separatorMessage.setText(chatMetaObject.getChatTitle());
        return;
      } else {
        // Error!!Not Supported type.
        return;
      }

      // Check search mode.
      chatMetaObject.setSearchModeOn(searchModeOn);
      if (searchModeOn) {
        chatMetaObject.setSearchWord(searchWord);
      }

      // Add the meta object to parent view.
      chatMetaObject.addToParent(targetContent, hasAlready);
    }
  }
}
