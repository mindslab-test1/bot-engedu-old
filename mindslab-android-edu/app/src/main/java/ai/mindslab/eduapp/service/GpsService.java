package ai.mindslab.eduapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import ai.mindslab.eduapp.preference.PreferenceHelper;

/**
 * Classes for GpsService
 */
public class GpsService extends Service implements LocationListener {
  private static final String TAG = GpsService.class.getSimpleName();

  // GPS Handling
  public static final String GPS_SERVICE = "ai.mindslab.eduapp.service.GpsService.SERVICE";

  private LocationManager locationManager = null;
  private Location lastLocation = null;
  private int updateRate = -1;

  private String bestProvider = null;

  @Override
  public void onCreate() {
    super.onCreate();
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    serviceStart();
    return super.onStartCommand(intent, flags, startId);
  }

  @Override
  public void onStart(Intent intent, int startId) {
    serviceStart();
  }

  private void serviceStart() {
    //Time 설정
    updateRate = 1000;// 1초 주기로  함

    //LocationManager 설정
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    Criteria criteria = new Criteria();
    criteria.setAccuracy(Criteria.NO_REQUIREMENT);
    criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
    criteria.setSpeedRequired(true);
    criteria.setBearingRequired(true);

    bestProvider = locationManager.getBestProvider(criteria, false);
    if(bestProvider == null){
      bestProvider = locationManager.GPS_PROVIDER;
    }

    Log.d(TAG, "bestProvider:" + bestProvider);
    locationManager.requestLocationUpdates(bestProvider, 1000, 0, this);//1초마다 갱신
  }

  @Override
  public void onDestroy() {
    if (locationManager != null) {
      locationManager.removeUpdates(this);
      locationManager = null;
    }
    super.onDestroy();
  }


  @Override
  public IBinder onBind(Intent intent) {
    // TODO: Return the communication channel to the service.
    throw new UnsupportedOperationException("Not yet implemented");
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
    Log.d(TAG, "Status Changed : " + status);
  }

  //GPS 상태 변경 값.
  public void onLocationChanged(Location location) {
    String locInfo = null;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    Date curTime = new Date();
    String currentDateandTime = sdf.format(curTime);

    if(location != null) {
      locInfo = String.format("onLocationChanged: %s : %f, %f, (s)%f, (d)%f",
          currentDateandTime,
          location.getLatitude(),
          location.getLongitude(),
          location.getSpeed(),
          location.getBearing());

      // Save to device preference.
      String latitude = String.format("%2.6f", location.getLatitude());
      String longitude = String.format("%3.6f", location.getLongitude());
          PreferenceHelper.setStringKeyValue(getApplicationContext(),
          PreferenceHelper.KEY_LAST_LOCATION_LATITUDE, latitude);
      PreferenceHelper.setStringKeyValue(getApplicationContext(),
          PreferenceHelper.KEY_LAST_LOCATION_LONGITUDE, longitude);
    } else {
      locInfo = String.format("onLocationChanged: %s : %s", currentDateandTime, "location Invalid");
    }

    lastLocation = location;

    Log.d(TAG, locInfo);
  }

  public void onProviderDisabled(String provider) {
    Log.d(TAG,  "Provider disabled : "+ provider);

    // 초기값 셋팅하여 준다.
    lastLocation = null;
  }

  public void onProviderEnabled(String provider) {
    Log.d(TAG,  "Provider enabled : "+ provider + ", status is:" + locationManager.getGpsStatus(null));

    // 초기값 셋팅하여 준다.
    lastLocation = null;
  }
}
