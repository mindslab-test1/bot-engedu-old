package ai.mindslab.eduapp.model;

import static android.provider.Telephony.BaseMmsColumns.TEXT_ONLY;

/*
 * Created by pmomac2015 on 2016. 12. 16..
 */
public class NavigationListItem {
  public enum MenuItemType{
    TEXT_ONLY_TYPE,
    ICON_TEXT_TYPE
  };

  private MenuItemType type;
  private int imageResourceId;
  private String text;

  /**
   * Constructor
   * @param type menu type of MenuItemType
   * @param resourceId drawable resource id.
   * @param text menu title.
   */
  public NavigationListItem(MenuItemType type, int resourceId, String text) {
    this.type = type;
    this.imageResourceId = resourceId;
    this.text = text;
  }

  /**
   * Navigation menu type getter.
   * @return MenuItemType
   */
  public MenuItemType getType() {
    return type;
  }

  /**
   * Navigation menu type setter.
   * @param type MenuItemType
   */
  public void setType(MenuItemType type) {
    this.type = type;
  }

  /**
   * Icon image resource id getter.
   * @return image resource id.
   */
  public int getImageResourceId() {
    return imageResourceId;
  }

  /**
   * Icon image resource id setter.
   * @param imageResourceId darwable resource id.
   */
  public void setImageResourceId(int imageResourceId) {
    this.imageResourceId = imageResourceId;
  }

  /**
   * Menu title getter.
   * @return menu title string.
   */
  public String getText() {
    return text;
  }

  /**
   * Menu title setter.
   * @param text menu title string.
   */
  public void setText(String text) {
    this.text = text;
  }
}
