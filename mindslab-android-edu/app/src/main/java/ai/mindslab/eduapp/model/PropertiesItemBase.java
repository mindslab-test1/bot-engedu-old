package ai.mindslab.eduapp.model;

/**
 * Created by hybell on 2017. 2. 20..
 */

/**
 * Classes base for properties item.
 */
public class PropertiesItemBase {
  /* 신규 추가인지 업데이트인지 구분한다 */
  private boolean isPrevDataExist = false;
  protected int type;
  protected String keyValue;
  protected String attrTitle;

  /**
   * Constructor
   * @param type
   * @param keyValue
   * @param attrTitle
   */
  public PropertiesItemBase(int type, String keyValue, String attrTitle) {
    this.type = type;
    this.keyValue = keyValue;
    this.attrTitle = attrTitle;
  }

  /**
   * Type getter.
   * @return user atrribute type.
   */
  public int getType() {
    return type;
  }

  /**
   * Type setter.
   * @param type user atrribute type.
   */
  public void setType(int type) {
    this.type = type;
  }

  /**
   * User attribute key value getter.
   * @return user attribute key string.
   */
  public String getKeyValue() {
    return keyValue;
  }

  /**
   * User attribute key value setter.
   * @param keyValue user attribute key string.
   */
  public void setKeyValue(String keyValue) {
    this.keyValue = keyValue;
  }

  /**
   * User attribute title getter.
   * @return user attribtue title string.
   */
  public String getAttrTitle() {
    return attrTitle;
  }

  /**
   * User attribute title setter.
   * @param attrTitle user attribtue title string.
   */
  public void setAttrTitle(String attrTitle) {
    this.attrTitle = attrTitle;
  }

  /**
   * 신규 추가인지 업데이트인지 구분하는 getter.
   * @return true is previous data exist.
   */
  public boolean isPrevDataExist() {
    return isPrevDataExist;
  }

  /**
   * 신규 추가인지 업데이트인지 구분하는 setter.
   * @param prevDataExist
   */
  public void setPrevDataExist(boolean prevDataExist) {
    isPrevDataExist = prevDataExist;
  }
}
