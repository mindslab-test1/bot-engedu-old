package ai.mindslab.eduapp.network;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import ai.mindslab.eduapp.R;

/**
 * Rest Api를 정의하고, URI을 제공한다.
 */
public abstract class RestApiURI {
  /* Context */
  protected Context context;
  /* Rest response lisnter */
  protected IPacketListener restListener = null;

  /**
   * Rest Uri String formating을 진행 (변수 1개)
   * @param uri
   * @param first
   * @return uri string.
   */
  public static String createUri(String uri, String first) {
    return String.format(uri, first);
  }

  /**
   * Rest Uri String formating을 진행 (변수 2개)
   * @param uri
   * @param first
   * @param second
   * @return uri string.
   */
  public static String createUri(String uri, String first, String second) {
    return String.format(uri, first, second);
  }

  public static String createUri(String uri, String first, String second, String third) {
    return String.format(uri, first, second, third);
  }

  /***********************************************************************************************
   * API URI
   ***********************************************************************************************/
  /* 회원 가입 */
  public final String API_SVCUSERS = NetConfigInfo.API_COMMON_URI + "/SvcUsers";
  /* 로그인 */
  public final String API_SVCUSERS_LOGIN = NetConfigInfo.API_COMMON_URI + "/SvcUsers/login?include=user";
  /* CheckAccess */
  public final String API_SVCUSERS_CHECKACCESS = NetConfigInfo.API_COMMON_URI + "/SvcUsers/check-access?token=%s";
  /* 비밀번호 RESET API ( 이메일로 비밀번호를 초기화하는 방법) */
  public final String API_SVCUSERS_RESET = NetConfigInfo.API_COMMON_URI + "/SvcUsers/reset";
  /* 비밀번호 변경 api/v1/SvcUsers/:id/updatePassword */
  public final String API_SVCUSERS_UPDATEPASSWORD = NetConfigInfo.API_COMMON_URI + "/SvcUsers/%s/updatePassword?access_token=%s";
  /* 개인정보 설정 update */
  public final String API_SVCUSERS_PID = NetConfigInfo.API_COMMON_URI + "/SvcUsers/%s?access_token=%s";
  /* 로그아웃 */
  public final String API_SVCUSERS_SIGNOUT = NetConfigInfo.API_COMMON_URI + "/SvcUsers/logout?access_token=";
  /* 로그인 후 장치 등록 */
  public final String API_DEVICES = NetConfigInfo.API_COMMON_URI + "/Devices";
  /* Profile Image Upload */
  public final String API_PROFILE_FILE_UPLOAD = NetConfigInfo.API_COMMON_URI + "/Files/upload";
  /* 사용자 정의 속성 조회 */
  public final String API_SVCPROPERTIES_FINDONE = NetConfigInfo.API_COMMON_URI + "/SvcProperties/findOne";
  /* 사용자 정의 그룹 속성 조회 */
  public final String API_SVCPROPERTIES_FINDGROUP_AND_DOMAIN = NetConfigInfo.API_COMMON_URI + "/SvcProperties/findByGroupAndDomain?";
  /* 사용자 정의 속성 저장 */
  public final String API_SVCPROPERTIES_UPSERT = NetConfigInfo.API_COMMON_URI + "/SvcProperties/upsertWithWhere";
  /* 사용자 정의 속성 추가 & 업데이트 통합 (HTTP Post is Add. HTTP PUT is Update) - API_SVCPROPERTIES_UPSERT 대체 */
  public final String API_SVCPROPERTIES_ADD_N_UPDATE = NetConfigInfo.API_COMMON_URI + "/SvcProperties?access_token=";
  /* 챗봇 구매 내역 확인 */
  public final String API_SVCPURCHASES_FIND_CHATBOTNAMES = NetConfigInfo.API_COMMON_URI + "/SvcPurchases/find-chatbotNames?access_token=";
  /* 챗봇 구매 */
  public final String API_SVCPURCHASES_PURCHASES_CHATBOT = NetConfigInfo.API_COMMON_URI + "/SvcPurchases/purchaseChatbot?access_token=";
  /* 구매한 사용자 download count */
  public final String API_SVCPURCHASES_GETDOWNLOADCOUNT = NetConfigInfo.API_COMMON_URI + "/SvcPurchases/getDownloadCount";
  /* 구매한 챗봇 확인 */
  public final String API_SVCPURCHASES_CHECK_PURCHASED_CHATBOT = NetConfigInfo.API_COMMON_URI + "/SvcPurchases/checkPurchasedChatbot";
  /* 리뷰 전체 조회 */
  public final String API_SVCREVIEWS_FILTER = NetConfigInfo.API_COMMON_URI + "/Reviews";
  /* 개인 리뷰 조회 */
  public final String API_SVCREVIEWS_FIND_REVIEW = NetConfigInfo.API_COMMON_URI + "/Reviews/findReview";
  /* 리뷰 저장 */
  public final String API_SVCREVIEWS_SAVE = NetConfigInfo.API_COMMON_URI + "/Reviews/syncReviewAndSvcChatbot?access_token=";
  /* 마켓리스트에서 모든 챗봇에 대한 평점과 review count수 */
  public final String API_SVCREVIEWS_FIND_TOTAL_GRADES= NetConfigInfo.API_COMMON_URI + "/Reviews/findTotalGrades";
  /* detail 챗봇 페이지에서 각 grade count 수 */
  public final String API_SVCREVIEWS_FIND_DETAIL_GRADES = NetConfigInfo.API_COMMON_URI + "/Reviews/findDetailGrades";


  /***********************************************************************************************
   * 응답 Listener 등록 및 응답 처리
   ***********************************************************************************************/
  protected NetRestHttp.INetConnectorListener onHttpResListener = new NetRestHttp.INetConnectorListener() {
    @Override
    public void onResponse(String method, int resCode, String sContent) throws Exception {
      try {
        if (restListener != null) {
          Object json = new JSONParser().parse(sContent);
          if (json instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray)json;
            JSONObject jsonObject = new JSONObject();
            // Server 응답시 JSONArray 만 내려올 경우 임의로 JSONObject 화 한다.
            jsonObject.put("makeClientJsonArrayKey", jsonArray);
            restListener.onApiResult(method, resCode, new Response().jsonToMap(jsonObject));
          } else if (json instanceof JSONObject) {
            // JSONObject type.
            restListener.onApiResult(method, resCode, new Response().strToMap(sContent));
          } else {
            // Special type.
            JSONObject jsonObject = new JSONObject();
            // Server 응답시 JSON type 아닐 경우 임의로 JSONObject 화 한다.
            jsonObject.put("makeClientJsonObjectKey", sContent);
            restListener.onApiResult(method, resCode, new Response().jsonToMap(jsonObject));
          }
        }
      } catch(Exception ex) {
        //Through pass.
        Log.d(TAG, ex.toString());
        String errorMessage = context.getString(R.string.error_rest_wrong_response);
        if (restListener != null) {
          restListener.onApiError(resCode, errorMessage);
        }
      }
    }

    @Override
    public void onError(int resCode, String sContent) throws Exception {
      String errorMessage;

      if(sContent.contains("SocketTimeoutException")){
        errorMessage = context.getString(R.string.error_rest_connection_fail);
      } else {
        errorMessage = context.getString(R.string.error_client_network_error);
      }

      // Send to linstener.
      if (restListener != null) {
        restListener.onApiError(resCode, errorMessage);
      }
    }
  };

  /**
   * Rest response listener.
   * @param listener
   */
  public void setListener(IPacketListener listener) {
    restListener = listener;
  }

  /**
   * 수행 결과를 받는  정보를 UI 리스너에 전달 하기 위함이다.
   */
  public interface IPacketListener {
    void onApiResult(String method, int resCode, Response resObj) throws Exception ;
    void onApiError(int resCode, String errorMessage);
  }
}
