package ai.mindslab.eduapp.network;

import android.content.Context;
import android.util.Log;

/**
 * Classes for AccountPacket
 */
public class AccountPacket extends RestApiURI {
  /* For log tag */
  private final String TAG = AccountPacket.class.getSimpleName();
  /* Resthttp intstance */
  private NetRestHttp restHttp;
  /* 통신 설정 정보 정의 */
  private String restApi;

  /**
   * Constructor
   * @param context
   */
  public AccountPacket(Context context) {
    this.context = context;

    restApi = NetConfigInfo.getApiUrl();
    Log.d(TAG, restApi);

    restHttp = new NetRestHttp();
    restHttp.setListener(onHttpResListener);
  }

  /**
   * 회원 가입
   *
   * @param method 응답 받을 메소드
   * @param reqObj 요청 파라메터
   */
  public void reqAccountJoin(String method, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST, accessToken,
        restApi + "" + API_SVCUSERS,
        reqObj.mapToJson());
  }

  /**
   * 로그인
   *
   * @param method 응답 받을 메소드
   * @param reqObj 요청 파라메터
   */
  public void reqLogin(String method, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCUSERS_LOGIN,
        reqObj.mapToJson());
  }

  /**
   * 해당 계정 정보 확인
   * @param method
   * @param accessToken
   */
  public void reqCheckAccess(String method, String accessToken) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_GET,
        accessToken,
        createUri(restApi + "" + API_SVCUSERS_CHECKACCESS, accessToken),
        null);
  }

  /**
   * 로그아웃
   *
   * @param method 응답 받을 메소드
   * @param reqObj 요청 파라메터
   */
  public void reqSignOut(String method, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCUSERS_SIGNOUT + accessToken,
        reqObj.mapToJson());
  }

  /**
   * 비밀번호 ReSet Api ( 이메일로 비밀번호를 초기화하는 방법 )
   *
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqResetPassword(String method, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_SVCUSERS_RESET,
        reqObj.mapToJson());
  }

  /**
   * 로그인 후 비밀번호 변경 API - 비밀번호 변경
   *
   * @param method
   * @param accessToken
   * @param userId
   * @param reqObj
   */
  public void reqUpdatePassword(String method, String accessToken, String userId, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        createUri(restApi + "" + API_SVCUSERS_UPDATEPASSWORD, userId, accessToken),
        reqObj.mapToJson());
  }

  /**
   * 개인정보 설정 API
   *
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqUpdateProfile(String method, String userId, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_PUT,
        accessToken,
        createUri(restApi + "" + API_SVCUSERS_PID, userId, accessToken),
        reqObj.mapToJson());
  }

  /**
   * 장비등록
   *
   * @param method
   * @param accessToken
   * @param reqObj
   */
  public void reqDevices(String method, String accessToken, Request reqObj) {
    restHttp.send(method,
        NetConfigInfo.HTTP_METHOD_POST,
        accessToken,
        restApi + "" + API_DEVICES,
        reqObj.mapToJson());
  }
}
