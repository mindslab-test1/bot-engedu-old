package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class LoudActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#loud");
        super.onCreate(savedInstanceState);

        setTitleBarText("큰 소리로 읽기");

        loadUrl();
    }

}
