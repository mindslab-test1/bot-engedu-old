package ai.mindslab.eduapp.activity;

import android.os.Bundle;

import ai.mindslab.eduapp.R;

public class SyntaxActivity extends WebViewActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setUrl(getResources().getString(R.string.service_http_host) + "/web#syntax");
        super.onCreate(savedInstanceState);

        setTitleBarText("문장 따라하기");

        loadUrl();
    }

}
