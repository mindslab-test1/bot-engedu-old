var android_version;

var updateQueryStringParameter = function(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
};


$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

var _alert = function(msg, type) {
	$.alert({
	    title: '',
      type: type,
	    content: msg,
	    confirmButton: '확인',
	    backgroundDismiss: false
	});

  speechText(msg, 'kor');
};


var _alertConfirm = function(msg, callback) {
	$.alert({
	    title: '',
	    content: msg,
	    confirmButton: '확인',
	    confirm: function(){
	        callback();
	    },
	    backgroundDismiss: false
	});
};

var confirmObj = 	{
					    title: ''
					    ,keyboardEnabled: true
				    	,confirmButtonClass: 'btn-info'
				    	,cancelButtonClass: 'btn-default'
					    // ,cancel: function(){
					    //     alert('Canceled!')
					    // }
					    ,backgroundDismiss: false
					};

var _confirm = function(msg, confirmMsg, cancelMsg, options, callback) {
  var oriConformObj = confirmObj;
	var tmpObj = confirmObj; // 다른 confirm에 영향을 주지 않기 위해 tmpObj에 넣어서 사용
	tmpObj.content = msg;
	tmpObj.confirmButton = confirmMsg;
	tmpObj.cancelButton = cancelMsg;
	tmpObj.confirm = function(){
				        callback();
				    };
	if(options) {
		_.extend(tmpObj, options);
	}
	$.confirm(tmpObj);
  confirmObj = oriConformObj;
};

var updateQueryStringParameter = function(uri, key, value) {
  if( !uri) {
    uri = '/';
  }
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
};


var navIcon = function(icon) {
	$('.menu-icon').hide();
	$('.left-arrow-icon').hide();

	if(icon != '') {
    // if( !JSInterface && icon=='menu-icon') { // 앱이면 menu-icon 숨김
      $('.'+icon).show();
    // }
		
	}
};


var afterLoad = function(delay, div, minusH) {
  setTimeout(function() {
        endLoading();
    },delay);

    // 왼쪽 메뉴 수동으로 늘이기 위해 (scrolltop으로 값을 가져올 수 있기 위해)
    $('.main, .jpanel-nav').css('height', $(document).height());

    $(document).scrollTop(0);

    if(div) {
      // 세로모드에서 스크롤되게 하기 위해
      $(div).height($(document).height() - minusH);
    }

};


var timer;
NProgress.configure({ showSpinner: false });
var startLoading = function() {

	NProgress.start();
	// $('.loading-out').hide();
	// $('.load').show();

	// timer && clearTimeout(timer);
	// timer = setTimeout(function() {
	// 	$('.loading-out').fadeIn('fast');
	// },7000); // 7초이상 걸리면 홈으로 표시
};

var endLoading = function() {
	NProgress.done();
	// clearTimeout(timer);
	// // $('#indicator').fadeOut();
	// // $('.loading').fadeOut('fast');
	// $('.load').hide();
	// $('.loading-out').hide();
};

Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if(a == b) // Or === depending on your needs
        return opts.fn(this);
    else
        return opts.inverse(this);
});
