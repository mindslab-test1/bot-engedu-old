
// IE11에서 startsWith를 지원하지 않기 때문에
if (!String.prototype.startsWith) {
  String.prototype.startsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };
}

// app에서만 사용하는 함수
var backPressed = function() {
  if(jc && jc.isOpen()) {
    jc.close();
  } else {
    // 뒤로가게 하지 않을 것이므로 주석처리
    // startLoading();
    // history.back();

    if(JSInterface) {
      JSInterface.appBackPressed();
    }
  }
};

var speechText = function(text, lang) {
  if(text != '') {
    $('#speech-frame').attr('src', currentHttpHost + '/tts?lang='+lang+'&message='+encodeURIComponent(text));
  }
}

var resizeMain = function() {
  // $('.main').height($(document).height());
  var commonNavBarHeight = $('.common-navbar').height();
  if(JSInterface) {
    commonNavBarHeight = 0;
  }
  $('.content-main').height($(document).height() - commonNavBarHeight);
};

var setContentBoxDiv = function() {
  var commonNavBarHeight = $('.common-navbar').height();
  var paginationBoxHeight = $('.pagination-box').height();
  var microphoneDivHeight = $('.microphone-div').height();
  var titleQuestionHeight = $('.title-question').height() + 1;
  if(JSInterface) {
    commonNavBarHeight = 0;
    microphoneDivHeight = 0;
  }
  $('.content-box-div').height( $(window).height() - ( commonNavBarHeight + paginationBoxHeight + microphoneDivHeight + titleQuestionHeight ) );
};

var jc;
var guideAlert = function(text, callback) {
  speechText(text.replace(/<br\/>/gm,''), 'kor');
  setTimeout(function() {
    jc = $.confirm({
        animateFromElement: false,
        title: '<i class="fa fa-flag" aria-hidden="true"></i> 가이드',
        content: text,
        // container: '.content-main',
        openAnimation: 'zoom',
        animationSpeed: 600,
        alignMiddle: false,
        offsetTop: 30,
        closeIcon: false,
        theme: 'modern',
        buttons: {
            confirm: {
              text: '시작',
              btnClass: 'btn-primary',
              keys: ['enter'],
              action: function() {
                callback();
              }
            },
            cancel : {
              isHidden: true,
              isDisabled : true
            }
        }
    });
  }, 500);
  
};

var startUnit = function(unitNumber, callback) {
  console.log('unit '+unitNumber +' start');
  $('.pagination li').removeClass('active');
  $('.pagination .page-'+unitNumber).addClass('active');
  loadUnitContent(unitNumber);
  showMicrophone();
  callback();
};

var showMicrophone = function() {
  if(JSInterface) {
    JSInterface.showChatInputBox();
  } else {
    $('.microphone-div').show();
  }
};

var sendMessage = function(message) {
  if(JSInterface) {
    JSInterface.sendMessage(message);
  } else {

  }
}

var sendMessageResAudio = function(message) {
  if(JSInterface) {
    JSInterface.sendMessage(message);
  } else {

  }
};

var runClientSpeech = function(text) {
  console.log('client message:'+text);
  operateClientMessage(text);
};

var receiveMessageFromServer = function(message) {
    console.log('message receive : '+message);
    // "RESULT:"로 체크하는건 임시다.
    if(message.startsWith('RESULT:')) {
      receiveResultFromServer(message);
    } else {
      operateMessage(message);
    }
    
};

var receiveResultFromServer = function(result) {
    console.log('result receive : '+result);
    operateResult(result);
};


var showResultWithBootstrap = function(resultData, chartTypes, callback) {

  $('.result-modal').modal('show');

  $.ajax({
    type: 'get',
    cache:false,
    url: '/result',
    data: {result_data: resultData, chart_types:chartTypes},
    dataType: 'json',
    success: function(res, textStatus, xhr) {
      $('.result-modal .modal-body').html(res.result.html);
    },
    error: function(xhr, e) {
    }
  });
};

var showResult = function(title, resultData, chartTypes, callback) {
  // $('#question-result-modal').modal('show');
  var self;
  jc = $.confirm({
      title: '평가 결과',
      theme: 'supervan',
      // theme: 'dark',
      animateFromElement: false,
      animation: 'opacity',
      closeAnimation: 'scale',
      closeIcon : true,
      type: 'green',
      columnClass: 'col-md-12', // 크게
      content: function(){
          self = this;
          // self.setContent('Checking callback flow');
          return $.ajax({
              url: '/result',
              dataType: 'json',
              method: 'get',
              data: {result_data: resultData, chart_types:chartTypes}
          }).done(function (res) {
              self.setContentAppend(res.result.html);
          }).fail(function(){
              self.setContentAppend('<div>결과 데이터를 가져오는 도중 오류가 발생하였습니다.</div>');
          }).always(function(){
              // self.setContentAppend('<div>Always!</div>');
          });
      },
      contentLoaded: function(data, status, xhr){
          // self.setContentAppend('<div>Content loaded!</div>');
      },
      onContentReady: function(){
          // this.setContentAppend('<div>Content ready!</div>');
      },
      buttons: {
        confirm: {
            text: '다시하기',
            btnClass: 'btn-danger',
            keys: ['esc'],
            action: function(){
              callback('re');
            }
        },
        cancel: {
            text: '다음으로',
            btnClass: 'btn-danger',
            keys: ['enter'],
            action: function(){
              callback('next');
            }
        }
        // heyThere: {
        //     text: 'Hey there!', // text for button
        //     btnClass: 'btn-blue', // class for the button
        //     keys: ['enter', 'a'], // keyboard event for button
        //     isHidden: false, // initially not hidden
        //     isDisabled: false, // initially not disabled
        //     action: function(heyThereButton){
        //         // longhand method to define a button
        //         // provides more features
        //     }
        // }
      }
  });
};

var getFormattedDate = function(date) {
  var year = date.getFullYear();

  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;

  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  
  return year + '-' + month + '-' + day;
}


var getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

var copyToClipboardFF = function(text) {
  window.prompt ("Copy to clipboard: Ctrl C, Enter", text);
};

var copyToClipboard = function(text, callback) {
  var success   = true,
      range     = document.createRange(),
      selection;

  // For IE.
  if (window.clipboardData) {
    window.clipboardData.setData("Text", text);        
  } else {
    // Create a temporary element off screen.
    var tmpElem = $('<div>');
    tmpElem.css({
      position: "absolute",
      left:     "-1000px",
      top:      "-1000px",
    });
    // Add the input value to the temp element.
    tmpElem.text(text);
    $("body").append(tmpElem);
    // Select temp element.
    range.selectNodeContents(tmpElem.get(0));
    selection = window.getSelection ();
    selection.removeAllRanges ();
    selection.addRange (range);
    // Lets copy.
    try { 
      success = document.execCommand ("copy", false, null);
    }
    catch (e) {
      copyToClipboardFF(text);
    }
    if (success) {
      // remove temp element.
      tmpElem.remove();
      callback('success');
    } else {
      callback('fail')
    }
  }
}


var loginRequiredConfirm = function() {

};

var closeMenu = function() {
  if(jPanelMenu) {
    jPanelMenu.close();
  }
};

var reviewAlert = function(callback) {
  $.ajax({
      type: 'get',
      cache:false,
      url: '/review',
      data: {},
      dataType: 'json',
      success: function(res, textStatus, xhr) {
        callback(res);
      },
      error: function(xhr, e) {
      }
    });
};

var cmpVersion = function(a, b) {
    var i, cmp, len, re = /(\.0)+[^\.]*$/;
    a = (a + '').replace(re, '').split('.');
    b = (b + '').replace(re, '').split('.');
    len = Math.min(a.length, b.length);
    for( i = 0; i < len; i++ ) {
        cmp = parseInt(a[i], 10) - parseInt(b[i], 10);
        if( cmp !== 0 ) {
            return cmp;
        }
    }
    return a.length - b.length;
};

var gteVersion = function(a, b) {
    return cmpVersion(a, b) > 0;
};

var androidVersion = function(installed_version) {
  if(JSInterface) {
    if(navigator.userAgent.match(/android/i)) {
      // if(installed_version!='1.0') {
      //     _alertConfirm("앱이 이전하였습니다. 새 앱으로 설치해주시기 바랍니다.", function() {
      //       JSInterface.openBrowser(playstore_url);
      //     });
      // }
      
      // if(android_version != '' && gteVersion(android_version, installed_version)) {
      //   $('.installed-version').text(installed_version);
      //   $('.last-version').text(android_version);
      //   $('.app-update-guide').show();
      // } else {
      //   $('.app-update-guide').hide();
      // }
    } 
  }
};


var appRouter;
var fromMenuCaches = [];
var exceptedFromMenus = ['setting?from_menu=Y'];
$(function() {
	
	var AppRouter = Backbone.Router.extend({
		routes: {
			":route": "display",
			":route/:page": "display",
			"manage/:route": "displayManage",
			"manage/:route/:page": "displayManage"
		},

		displayManage: function(route, page) {
			appRouter.display('manage/'+route, page);
		},
		
		display: function(route, page) {
			var url = route;
			var cls = route;
			if(page) {
				url = route + '/' + page;
				cls = route + '-' + page;
			}

			$('.jpanel-nav ul li').removeClass('active');
			$('.jpanel-nav ul li[class='+cls.split("?")[0]+']').addClass('active');

      // if(route.indexOf("?from_menu=Y") > 0 && typeof fromMenuCaches[route] !== 'undefined' && $.inArray(route, exceptedFromMenus) == -1) {
      //   $('.nav-div').html(fromMenuCaches[route].nav);
      //   $('.content-main').html(fromMenuCaches[route].main);
      // } else {
        startLoading();

        $.ajax({
          type: 'get',
          url: url,
          data: {},
          dataType: 'json',
              error: function(xhr, e){
                  console.log(xhr);
              },
              success: function(res, textStatus, xhr) {
                if(res.code==403) {
                  act403(res);
                } else if(res.code==200) {
                  $('.nav-div').html(res.result.nav);

                  $('.content-main').hide();
                  $('.content-main').html(res.result.html);
                  $(".content-main").fadeIn('fast');

                  // fromMenuCaches[route] = {nav:res.result.nav, main:res.result.html};

                } else {
                  if(res.code==8001) {
                    _alert(res.message);
                    history.back();
                  } else if(res.code==4001) {
                    document.location.href = 'web#filter';
                  } else {
                    _alert(res.message);
                  }
                }
            
          },
          complete: function(xhr, textStatus) {
            // endLoading();
          }
        });
      // }
		},

	});

	appRouter = new AppRouter();
  Backbone.history.start();
	
	// route가 변경될 때마다
	appRouter.bind("all",function(route, router) {
		// clearInterval(monitorTimerId);  
	});
});


$(document).ready(function () {

  $('.back-btn').click(function() {
    history.back();
  });


	$('.loading-out-btn').click(function() {
		document.location.href = "/";
	});

});


