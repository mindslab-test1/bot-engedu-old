var jPanelMenu = {};

(function($){
	$(function() {
		$('pre').each(function(i, e) {hljs.highlightBlock(e)});
		
		var closeOnContentClick = false;
		if($(window).width() <= 1024) {
			closeOnContentClick = true;
		}
		var firstClose = false;
		jPanelMenu = $.jPanelMenu({
			menu: '.jpanel-nav',
			closeOnContentClick : closeOnContentClick,
			panel: '.content',
			beforeOpen : function() {  // for trasform
				$('.menu-icon').css('margin-left', '253px'); // 3px 약간 띄어준다.
				$('.left-arrow-icon').css('margin-left', '253px');
				$('#search-input').focus();
			},
			beforeClose : function() {
				$('.menu-icon').css('margin-left', '0');
				$('.left-arrow-icon').css('margin-left', '0');
			},
			afterOpen : function() {
				if($(window).width() > 1024) {
					$('.jPanelMenu-panel').width($('body').width() - 250);
				}
			},
			afterClose : function() {
				if(firstClose==true) {
					firstClose = false;
				} else {
					if($(window).width() > 1024) {
						$('.jPanelMenu-panel').width($('.jPanelMenu-panel').width() + 250);
					}
				}
			}
		});
		jPanelMenu.on();
		// jPanelMenu.open();
		

		$(document).on('click',jPanelMenu.menu + ' li a',function(e){
			// if ( jPanelMenu.isOpen() && $(e.target).attr('href').substring(0,1) == '#' ) { 
				if($(window).width() <= 1024) {
					jPanelMenu.close(); 
				}
			// }
		});

		$(document).on('click','#trigger-off',function(e){
			jPanelMenu.off();
			$('html').css('padding-top','40px');
			$('#trigger-on').remove();
			$('body').append('<a href="" title="Re-Enable jPanelMenu" id="trigger-on">Re-Enable jPanelMenu</a>');
			e.preventDefault();
		});

		$(document).on('click','#trigger-on',function(e){
			jPanelMenu.on();
			$('html').css('padding-top',0);
			$('#trigger-on').remove();
			e.preventDefault();
		});

		var firstHidden = function() {
			firstClose = true;
			jPanelMenu.close();
		}

		if($(window).width() <= 1024) {
			firstHidden();
		} else {
			// 메뉴 생기면 주석 풀기
			// jPanelMenu.open();

			// 메뉴 생기면 주석처리하기 시작
			$('.jPanelMenu-panel').css('margin', '0 auto');
			$('.nav-bottom').css('max-width', '810px');
			$('.nav-bottom').addClass('center-block');
			$('.left-arrow-icon').css('margin-left', ($(document).width() - $('.jPanelMenu-panel').width()) / 2);
			$(window).resize(function() {
			  	$('.left-arrow-icon').css('margin-left', ($(document).width() - $('.jPanelMenu-panel').width()) / 2);
			});
			// 메뉴 생기면 주석처리하기 끝

		}
		$(window).resize(function() {
		  	if($(window).width() <= 1024) {
		  		firstHidden();
		  	}
		});

		if($('.content').height() < $(document).height()) {
			$('.content').height($(document).height());
		}
	});
})(jQuery);
