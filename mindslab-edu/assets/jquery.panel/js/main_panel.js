var jPanelMenu = {};

(function($){
	$(function() {
		$('pre').each(function(i, e) {hljs.highlightBlock(e)});
		
		var closeOnContentClick = false;
		if($(window).width() <= 1024) {
			closeOnContentClick = true;
		}
		var firstClose = false;
		jPanelMenu = $.jPanelMenu({
			menu: '.jpanel-nav',
			closeOnContentClick : closeOnContentClick,
			panel: '.main',
			beforeOpen : function() {  // for trasform
				// $('.menu-icon').css('margin-left', '253px'); // 3px 약간 띄어준다.
				// $('.left-arrow-icon').css('margin-left', '253px');

				// $('.menu-icon').css('margin-left', '3px'); // 3px 약간 띄어준다.
				// $('.left-arrow-icon').css('margin-left', '3px');
			},
			beforeClose : function() {
				// $('.menu-icon').css('margin-left', '3px'); // 3px 약간 띄어준다.
				// $('.left-arrow-icon').css('margin-left', '3px');
			},
			afterOpen : function() {
				if($(window).width() > 1024) {
					$('.jPanelMenu-panel').width($('body').width() - 250);
				}
			},
			afterClose : function() {
				if(firstClose==true) {
					firstClose = false;
				} else {
					if($(window).width() > 1024) {
						$('.jPanelMenu-panel').width($('.jPanelMenu-panel').width() + 250);
					}
				}
			}
		});
		jPanelMenu.on();
		// jPanelMenu.open();

		$(document).on('click',jPanelMenu.menu + ' li a',function(e){
			// if ( jPanelMenu.isOpen() && $(e.target).attr('href').substring(0,1) == '#' ) { 
				if($(window).width() <= 1024) {
					jPanelMenu.close(); 
				}
			// }
		});

		$(document).on('click','#trigger-off',function(e){
			jPanelMenu.off();
			$('html').css('padding-top','40px');
			$('#trigger-on').remove();
			$('body').append('<a href="" title="Re-Enable jPanelMenu" id="trigger-on">Re-Enable jPanelMenu</a>');
			e.preventDefault();
		});

		$(document).on('click','#trigger-on',function(e){
			jPanelMenu.on();
			$('html').css('padding-top',0);
			$('#trigger-on').remove();
			e.preventDefault();
		});

		var firstHidden = function() {
			firstClose = true;
			jPanelMenu.close();
		}

		if($(window).width() <= 1024) {
			firstHidden();
		} else {
			jPanelMenu.open();
		}
		$(window).resize(function() {
		  	if($(window).width() <= 1024) {
		  		firstHidden();
		  	} else {
		  		jPanelMenu.open();
		  	}
		});

		// if($('.main').height() < $(document).height()) {
		// 	$('.main').height($(document).height());
		// }
	});
})(jQuery);
