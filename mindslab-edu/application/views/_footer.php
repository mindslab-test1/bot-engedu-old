    </div><!-- content-main -->

  </div><!-- main -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.1/highcharts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.1/highcharts-more.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.2/js/modules/solid-gauge.js"></script>

  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/js/lib/modernizr-2.6.1.min.js"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/js/lib/respond.js"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/js/lib/highlight.min.js"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/jquery.jpanelmenu.js"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/js/lib/plugins.js"></script>

  <?/* 왼쪽 메뉴 없애니까 주석처리 
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery.panel/js/main_panel.min.js"></script>
  */?>

  <script src="<?=CDN_HTTP_HOST?>/assets/lazyload/jquery.lazyload.min.js"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/p2r/jquery.p2r.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

  <script src="<?=CDN_HTTP_HOST?>/assets/js/underscore-min.js" type="text/javascript"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/js/backbone-min.js" type="text/javascript"></script>
  
  <script src="<?=CDN_HTTP_HOST?>/assets/js/common.min.js?u=<?=time()?>" type="text/javascript"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/js/web.min.js?u=<?=time()?>" type="text/javascript"></script>

  <script>
    // setDeviceUser();
  </script>

  <?php if($redirect_after) : ?>
    <script>
      document.location.href = '<?=$redirect_after?>';
    </script>
  <?php endif; ?>

</body>
</html>
