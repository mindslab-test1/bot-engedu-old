<style>
.talk-box {
	border-radius: 5px;
	height: 100%;
}
</style>


<div class="pattern-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="content-box-div">

		<div class="talk-box"  style="display:none">
			<?=$this->load->view('_talk_bubble',$this->data, true)?>
		</div>
	</div>

</div>

<?php $this->load->view('_microphone',$this->data); ?>

<script>
	endLoading();
	resizeMain();
	
	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
		$('.talks').html('');
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			addLine(message, 'left');
			speechText(message, 'eng');
		}
	};

	var operateResult = function(result) {
	};

	var operateClientMessage = function(message) {
		addLine(message, 'right');
	}

	guideAlert('주어지는 패턴과 단어를 사용하여<br/> 문장을 완성해보세요.', function() {
		$('.talk-box').fadeIn();

		setContentBoxDiv();

		initChatUI();

		startUnit(unitNum, function() {
			sendMessage('<?=$svcgroup?>'+unitNum);
		});

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['Let\'s practice new expressions! If you are ready, write "start"!', 'Start.'];
			var i = 0;
			var timer =setInterval(function() {
				if(i==msgs.length) {
					clearInterval(timer);
					return;
				}

				if(i%2==0) {
					receiveMessageFromServer(msgs[i]);
				} else {
					runClientSpeech(msgs[i]);
				}

				i++;

			}, 1000);
		<?php endif; ?>

	});
</script>



