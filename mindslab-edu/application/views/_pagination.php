<style>
.pagination {
	margin: 0 auto;
}
/*.pagination li.active > a {
	background-color: #f68e1e;
	border-color: #ffc14e;
}*/
.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
/*	background-color: #fdbf4e;
	border-color: #f5bf5c;*/
	background-color: #788bbd;
	border-color: #8f9ab9;
}
.pagination li > a {
	border-radius: 50% !important;
	margin: 5px 10px;
	padding: 10px 15px;
}
.pagination>li>a
, .pagination>li>span {
	color: #333;
	width: 45px;
	height: 45px;
}
</style>

<ul class="pagination" data-svcgroup="<?=$svcgroup?>">
	<li class="page-num page-1 <?=$unit_num==1?'active':''?>" data-unit-num="<?=$unit_num?>"><a class="cursor-pointer"><?=$page_nums[0]?$page_nums[0]:1?></a></li>
	<li class="page-num page-2 <?=$unit_num==2?'active':''?>" data-unit-num="<?=$unit_num+1?>"><a class="cursor-pointer"><?=$page_nums[1]?$page_nums[1]:2?></a></li>
	<li class="page-num page-3 <?=$unit_num==3?'active':''?>" data-unit-num="<?=$unit_num+2?>"><a class="cursor-pointer"><?=$page_nums[2]?$page_nums[2]:3?></a></li>
	<li class="page-num page-4 <?=$unit_num==4?'active':''?>" data-unit-num="<?=$unit_num+3?>"><a class="cursor-pointer"><?=$page_nums[3]?$page_nums[3]:4?></a></li>
	<li class="page-num page-5 <?=$unit_num==5?'active':''?>" data-unit-num="<?=$unit_num+4?>"><a class="cursor-pointer"><?=$page_nums[4]?$page_nums[4]:5?></a></li>
</ul>

<script>
	$('.page-num').click(function() {
		var svcgroup = $(this).closest('.pagination').data('svcgroup');
		var pageUnitNum = $(this).data('unit-num');
		var self = $(this);
		startUnit(pageUnitNum, function() {
			unitNum = pageUnitNum;
			$('.page-num').removeClass('active');
			self.addClass('active');
			sendMessage(svcgroup + pageUnitNum);
		});
	});
</script>