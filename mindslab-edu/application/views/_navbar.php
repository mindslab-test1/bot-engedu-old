<div class="nav-left">
	<a href="#menu" class="menu-trigger menu-icon" style="display:none">
	  <i class="fa fa-bars"></i>
	</a>
	<span class="left-arrow-icon" style="display:none">
	  <i class="fa fa-arrow-left"></i>
	</span>
	<a href="/" class="home-icon">
	  <i class="fa fa-home"></i>
	</a>
</div>

<div class="navbar navbar-gray common-navbar text-center">
    <h4><?=$title?></h4>
</div>

<div>
	<?=$nav_tool?>
</div>

<script>
	if(JSInterface) {
		$('.menu-icon').hide();
	}
	$('.left-arrow-icon').click(function() {
	    if(from_share=='Y') {
	      document.location.href = 'web#filter';
	    } else {
	      history.back();
	    }
	});

</script>