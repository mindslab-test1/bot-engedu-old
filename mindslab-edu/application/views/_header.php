<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>마인즈 잉글리시</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="apple-mobile-web-app-title" content="<?=$service['abbre']?> 필터야">
  <meta name="viewport" content="initial-scale = 1.0, user-scalable = no">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  <meta name="apple-touch-fullscreen" content="yes" />

  <meta property="og:image" data-page-subject="true" content="<?=$og['image']?>"/>
  <meta property="og:image:width" content="450"/>
  <meta property="og:image:height" content="298"/>
  <meta property="og:title" data-page-subject="true" content="<?=str_replace('"',"'",$og['title'])?>"/>
  <?php /* 페이스북에서 redirect하여 정보를 못 가져가므로 주석
  <meta property="og:url" data-page-subject="true" content="<?=$og['url']?>"/>
  */?>
  <meta property="og:site_name" data-page-subject="true" content="<?=$og['site_name']?>"/>
  <meta property="og:type" data-page-subject="true" content="<?=$og['type']?>"/>
  <meta property="og:description" data-page-subject="true" content="<?=str_replace('"',"'",$og['description'])?>"/>

  <meta http-equiv="Content-Script-Type" content="text/javascript">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/css/jquery-ui.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/css/loader.min.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/css/hamburger.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/jquery-toast/jquery.toast.min.css">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/sweetalert/sweetalert.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">

  <script>
    var JSInterface;
    var request_app_id = '<?=get_request_app_id()?>';
    var currentHttpHost = '<?=$this->config->item('base_url')?>';
    var proTimer;
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
  <script src="<?=CDN_HTTP_HOST?>/assets/jquery-toast/jquery.toast.min.js" type="text/javascript"></script>
  <!-- <script src="<?=CDN_HTTP_HOST?>/assets/js/jquery-ui-1.11.4.min.js" type="text/javascript"></script> -->

  <script src="<?=CDN_HTTP_HOST?>/assets/js/handlebars-v4.0.5.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js" type="text/javascript"></script>

  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/css/common.min.css?u=<?=time()?>">
  <link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/css/web.min.css?u=<?=time()?>">
  <style>
    <?php if(strlen($service['name']) < 10) : ?>
    .left-brand .left-brand-name {
      font-size: 28px;
    }
    <?php endif; ?>

    
    .left-nav .left-brand .left-brand-name {
        <?php if(strpos($service['name'], ' ') > 0) : ?>
          line-height: 30px;
        <?php else : ?>
        line-height: 57px;
        <?php endif; ?>
    }

    <?php/* if(get_request_app_id()==ANDROID_ID) : */?>
    <?php if(is_from_app()) : ?>
      .jpanel-nav {
        display: none;
      }
      .menu-icon 
      ,.home-icon {
        display: none;
      }
      .nav-div
      ,.common-navbar {
        display: none;
      }
    <?php else : ?>
      .menu-icon {
        display: inherit;
      }
    <?php endif; ?>
  </style>

</head>
<body>
  <iframe id="speech-frame" style="display: none"></iframe>

  <div class="modal fade result-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


  <?php if($from_share == 'Y') : ?>
    <?=$leadto_app_view?>
  <?php endif; ?>

  <div class="load" style="display:none">
    <div class="loading">
      Loading&#8230;
    </div>
    <span class="loading-out" style="display:none">
      <button class="btn btn-sm btn-warning loading-out-btn">홈으로 이동 >></button>
    </span>
  </div>

  <?php $this->load->view('_left'); ?>

  <div class="main">
    <div class="nav-div">
    </div>

    <div class="content-main">
