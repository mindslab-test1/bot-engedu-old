
navIcon('menu-icon');

$('.go-service-btn').click(function() {
	goService($(this).data('service-id'));
});

hideBanner();



$('.slick-item-one').css('width',$('.content-main').width()+'px');
$('.slick-item-one').css('height',($('.content-main').width() * 0.3) + 'px');

$('.slick-item-one').click(function() {
	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
	var link = $(this).data('link');
	document.location.href = link;
});

$('.app-icon-one').click(function() {
	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
	var link = $(this).data('link');
	document.location.href = link;
});



$(document).ready(function() {
	$('.apps-div').height($(document).height() - (50+50));

    setTimeout(function() {
        endLoading();
        $('.single-item').slick({
        	dots: true,
			infinite: true,
			speed: 100,
			slidesToShow: 1,
			autoplay: true,
  			autoplaySpeed: 3000
			// centerMode: true
        });
    },10);
});
