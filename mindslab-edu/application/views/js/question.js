
navIcon('');
hideBanner();
$('.nav-div').html('');

if(only_one=='Y' || question_count==1) {
  $('.carousel-control').hide();
}

var set_modal_content_height = $(document).height() - 10 - (10*2) - 28; // progressbar와 margin과 questions-header 뺌
var set_modal_content_width = 0;
if($(document).width() >= 768) { // bootstrap이 768부터 modal-dialog 넓이를 지정한다.
  set_modal_content_width = $('.modal-dialog').width() - 20;
} else {
  set_modal_content_width = $(document).width() - (20 + 20);
}

var orderLayout = function() {
  $('.item.active').find('.modal-content').animate({
    height: set_modal_content_height
  }, 'fast');

  $('.item.active').find('.modal-header span').fadeIn('slow', function() {
    var modal_header_inner_height = $('.item.active').find('.modal-header').innerHeight();
    var modal_body_height = set_modal_content_height - modal_header_inner_height; // innerHeight = padding 포함
    var modal_body_width = set_modal_content_width;
    $('.item.active').find('.modal-body').innerHeight(modal_body_height);
    $('.item.active').find('.modal-body').width(modal_body_width);
    var answer_count = parseInt($('.item.active').data('answer-count'), 10);
    var answer_type = $('.item.active').data('answer-type');

    if(answer_type=='image') {
        $('.item.active label.btn').removeClass('btn-block');
        $('.item.active .answer-div').addClass('col-xs-6 col-md-6 col-lg-6 image-answer');

        var label_height = (modal_body_height - (15 * 2) - 2) / 2;
        var label_width = (modal_body_width - (10*2)) / 2;
        var img_height = label_height - (18*2);
        var img_width = label_width - (10*2);
        $('.item.active label.btn').width(label_width);
        $('.item.active label.btn img').height(img_height);
        $('.item.active label.btn img').width(img_width);
        
    } else {
        $('.item.active label.btn').addClass('btn-block');
        $('.item.active .answer-div').removeClass('col-xs-6 col-md-6 col-lg-6 image-answer');
        $('.item.active label.btn').css('width', 'auto');

        var total_padding = modal_body_height - (15 * 2) - (8 * 3) - (22 * answer_count);
        var padding = total_padding / (answer_count * 2);
        $('.item.active').find('label.btn').css('padding', padding + 'px 10px');
    }
    

    $('.item.active').find('.modal-body .quiz').fadeIn('slow');
  });

  // $('.item.active').find('.modal-header').height($('.item.active').find('.modal-header').height()); // 문제 세로로 가운데 정렬위해 임의로 높이 지정/
};

var progressBar = function() {
  var $bar = $('.questions-bar');
  $bar.width($bar.width() + bar_step);
  var percent = ((bar_step * solved_count) / progress_width) * 100;
  var round_percent = Math.round(percent);
  if(round_percent > 100) round_percent = 100;
  $bar.text(round_percent + "%");
};

$( ".question-title-img" ).click(function() {
    $(this).closest('.question-title').find('.question-title-zoom-img').show();
});

$( ".question-title-zoom-img" ).click(function() {
    $('.question-title-zoom-img').hide();
});



$('.carousel').on('slide.bs.carousel', function () {
  // $('.item').removeClass('active');
});

$('.carousel').on('slid.bs.carousel', function () {
  // console.log$('.item.active').$(this).find('.active').index());
  orderLayout();
  $('.question-title-zoom-img').hide();
});

$('.question-left-btn').click(function() {
  if(from_share=='Y' || from_sign=='Y') {
    document.location.href = 'web#quiz';
  } else {
    history.back();
  }
});

$('.item-user-btn').click(function() {
  var user = {};
  user.no = $(this).data('user-no');
  user.name = $(this).data('user-name');
  user.html = $(this).closest('.item-user').find('.item-user-info').html();
  showUserInfo(user);
});

$('.question-like-btn').click(function() {
  var question_no = $(this).data('question-no');
  var act = $(this).data('act');
  var obj = $(this);
  likeQuestion(question_no, act, function(success_res) {
    var like_count = 0;
    if(obj.find('.like-count:first').text()=='') {
      like_count = 0;
    } else {
      like_count = parseInt(obj.find('.like-count:first').text(), 10);
    }
    if(act=='like') {
      obj.data('act','unlike');
      obj.find('i').removeClass('fa-thumbs-o-up');
      obj.find('i').addClass('fa-thumbs-up');
      obj.find('.like-count:first').text(like_count + 1);
      obj.find('.like-count:first').digits();
    } else if(act=='unlike') {
      obj.data('act','like');
      obj.find('i').removeClass('fa-thumbs-up');
      obj.find('i').addClass('fa-thumbs-o-up');
      if(like_count - 1 == 0) {
        obj.find('.like-count:first').text('');
      } else {
        obj.find('.like-count:first').text(like_count - 1);
      }
      
      obj.find('.like-count:first').digits();
    }
  });
});


///////////
// 댓글
///////////
var comment_question_no = 0;
var question_comment_page_no = 1;
var loadCommentList = function() {
  var row_count = 10;
  var param = {page_no: question_comment_page_no, question_no:comment_question_no, row_count:row_count};
  if(question_comment_page_no==1) {
    $('.now-loading').show();
  }
  $('.more-comment .fa-refresh').addClass('fa-spin');
  nextCommentList(param, function(res) {
    $('.now-loading').hide();
    if(res.result.count != 0 && res.result.count <= row_count) {
      $('.more-comment').show();
    } else {
      $('.more-comment').hide();
    }
    $('#question-comment-modal .comment-list').append(res.result.html);
    $('.more-comment .fa-refresh').removeClass('fa-spin');
    question_comment_page_no++;

    // $('.question-comment-like-btn').off('click');
    // $('.question-comment-hate-btn').off('click');
    // $('.question-comment-subcomment-btn').off('click');

    // $('.question-comment-like-btn').on('click', function () {
    // }); 
    activateCommentBtn();
    
  });
};

var activateCommentBtn = function() {
  $('.question-comment-delete-btn').off('click');
    $('.question-comment-delete-btn').on('click', function () {
      var obj = $(this);
      var question_comment_no = $(this).data('question-comment-no');
      _confirm('이 댓글을 삭제하시겠습니까?', '네, 삭제하겠습니다', '아니요', {confirmButtonClass: 'btn-danger'}, function() {
        var param = {question_comment_no:question_comment_no};
        deleteQuestionComment(param, function(res) {
          $.toast({
              heading: '알림',
              text: '댓글을 삭제하였습니다.',
              position: 'top-center',
              stack: false,
              icon: 'info',
              bgColor: 'rgba(79, 115, 134, 0.8',
              hideAfter: 600
          });

          obj.closest('.comment-item').slideUp();

          changeCommentCount($('.item.active').find('.item-tools .question-comment-btn'), 'minus');

        });
      });
    }); 
};

var changeCommentCount = function(btnObj, status) {
  var comment_count = 0;
  if(btnObj.find('.comment-count:first').text()=='') {
    comment_count = 0;
  } else {
    comment_count = parseInt(btnObj.find('.comment-count:first').text(), 10);
  }
  if(status=='plus') {
    btnObj.find('.comment-count:first').text(comment_count + 1);
  } else if(status=='minus') {
    btnObj.find('.comment-count:first').text(comment_count - 1);
  }
  btnObj.find('.comment-count:first').digits();
};

$('.more-comment').click(function() {
  loadCommentList();
});

$('.question-comment-btn').off('click');
$('.question-comment-btn').click(function() {
  comment_question_no = $(this).data('question-no');
});
// $('#question-comment-modal').off('show.bs.modal');
// $('#question-comment-modal').on('show.bs.modal', function (e) {
//   loadCommentList();
// });

var writeComment = function() {
  var obj = $('.item.active').find('.item-tools');
  var comment = $('#comment').val();
  var param = {};
  param.question_no = comment_question_no;
  param.comment = comment;
  sendQuestionComment(param, function(res) {
    if(res.code==200) {
      $.toast({
          heading: '알림',
          text: '댓글을 올렸습니다.',
          position: 'top-center',
          stack: false,
          icon: 'info',
          bgColor: 'rgba(79, 115, 134, 0.8',
          hideAfter: 600
      });
      $('#comment').val('');
      $('.comment-list').prepend(res.result.html);
      $('.empty-list').hide();

      activateCommentBtn();

      changeCommentCount($('.item.active').find('.item-tools .question-comment-btn'), 'plus');

    } else {
      _alert(res.message);
    }
  });
};

$('#question-comment-modal').off('shown.bs.modal');
$('#question-comment-modal').on('shown.bs.modal', function (e) {
  loadCommentList();
  
  var minusH = $('.comment-input-box').height() + 10;
  $('.comment-box').height($(document).height() - minusH);

  $('.comment-write-btn').off('click');
  $('.comment-write-btn').on('click', function() {
    writeComment();
  });

  $('#comment').off('keypress');
  $('#comment').on('keypress', function(e) {
      if(e.which == 13) {
        writeComment();
      }
  });
});

$('#question-comment-modal').off('hidden.bs.modal');
$('#question-comment-modal').on('hidden.bs.modal', function (e) {
  comment_question_no = 0;
  question_comment_page_no = 1;
  $('#question-comment-modal .comment-list').html('');
  $('.more-comment').hide();
  $('.empty-list').remove();
  $('.now-loading').hide();
});


///////////
// 신고하기
///////////
var report_question_no = 0;
$('.question-report-btn').off('click');
$('.question-report-send-btn').off('click');
$('.question-report-btn').click(function() {
  report_question_no = $(this).data('question-no');
});
$('.question-report-send-btn').click(function() {
  var msg = $(this).data('msg');
  var obj = $('.item.active .item-tools .question-report-btn');
  sendQuestionReport(report_question_no, msg, function(res) {
    if(res.code==200) {
      _alert("신고하였습니다.");
      var report_count = 0;
      if(obj.find('.report-count:first').text()=='') {
        report_count = 0;
      } else {
        report_count = parseInt(obj.find('.report-count:first').text(), 10);
      }
      obj.find('i').removeClass('fa-exclamation');
      obj.find('i').addClass('fa-exclamation-circle');
      obj.find('.report-count:first').text(report_count + 1);
      obj.find('.report-count:first').digits();
    } else if(res.code==201) {
      _alert("이미 신고한 문제입니다.");
    }
    $('#question-report-modal').modal('hide');
  });
});
$('#question-report-modal').on('hidden.bs.modal', function (e) {
  report_question_no = 0;
});

///////////
// 공유하기
///////////
var share_service_id = '<?=SERVICE_ID?>';
var share_question_no = 0;
var share_txt = '';
var share_url = '';
$('.question-share-btn').click(function() {
  share_question_no = $(this).data('question-no');
  // share_txt = $(this).data('share-txt');
  share_url = 'http://'+share_service_id+'.filterya.appyouwant.com/'+share_question_no;
  share_txt = $(this).closest('.modal-header').find('.question-title').text().trim();
  share_txt = share_txt + "\n\n" + $(this).closest('.modal-header').find('.answer-string').text();
});
$('.share-btn').click(function() {
  var sns = $(this).data('sns');
  openShare(sns, share_service_id, share_url, share_txt);
  $('#question-share-modal').modal('hide');
});
$('.copy-share-url-btn').click(function () {
  copyToClipboard(share_url, function(res) {
    if(res=='success') {
      $.toast({
          heading: '알림',
          text: '링크를 복사하였습니다.',
          position: 'top-center',
          stack: false,
          icon: 'info',
          bgColor: 'rgba(79, 115, 134, 0.8',
          hideAfter: 2000
      });
    } else {
      $.toast({
          heading: '오류',
          text: '링크를 복사할 수 없습니다.',
          position: 'top-center',
          stack: false,
          icon: 'danger',
          bgColor: 'rgba(79, 115, 134, 0.8',
          hideAfter: 2000
      });
    }
    $('#question-share-modal').modal('hide');
  });
});
// $('.copy-share-url-btn').click(function () {
//    document.execCommand('copy', false, null);
// });
// $(document).bind('copy', function (e) {
//   e.preventDefault();
//   if (e.clipboardData) {
//       e.clipboardData.setData('text/plain', share_url);
//   } else if (window.clipboardData) {
//       window.clipboardData.setData('Text', share_url);
//   }
//   $.toast({
//       heading: '알림',
//       text: '링크를 복사하였습니다.',
//       position: 'top-center',
//       stack: false,
//       icon: 'info',
//       bgColor: 'rgba(79, 115, 134, 0.8',
//       hideAfter: 2000
//   });
// });

// $('#question-share-modal').on('hidden.bs.modal', function (e) {
//   share_question_no = 0;
//   share_txt = '';
//   share_url = '';
// });

//////////////////
// 저장,삭제
//////////////////
var option_question_no = 0;
$('.question-option-btn').click(function() {
  option_question_no = $(this).data('question-no');
});
$('.question-option-save-btn').click(function() {
    // saveQuestion(option_question_no, function(res) {
    //   if(res.code==200) {
    //     _alert("저장하였습니다.");
    //   } else {
    //     _alert(res.message);
    //   }
    //   $('#question-option-modal').modal('hide');
    // });
});
$('#question-option-modal').on('hidden.bs.modal', function (e) {
    option_question_no = 0;
    if(quiz_type=='custom' && $(this).data('is-my-question')=='Y') {
      $('.modal-delete-btn').show();
    } else {
      $('.modal-delete-btn').hide();
    }
});

$('.modal-delete-btn').click(function() {
    _alert("삭제는 [프로필] > [출제수] > [내가 출제한 문제 목록] 에서 삭제하실 수 있습니다.");
});


$('.answer-btn').click(function() {
  solved_count++;
  progressBar();
  var obj = $(this);

  var question_no = $(this).data('question-no');
  var answer_no = $(this).data('answer-no');
  sendAnswer(question_no, answer_no, function(res) {
    if(res.code==4011) {
      _alertConfirm(res.message, function() {
        // 삭제된 문제여도 프로그래스바는 계속 진행이 되어야한다.
        $('.carousel').carousel('next');
      });
    } else {
      if(res.result.is_correct=='Y') {
        flashIcon('success');
        obj.switchClass('btn-gray', 'btn-green');
        obj.closest('.item').switchClass('bg-danger', 'bg-success');
        obj.closest('.item').switchClass('bg-', 'bg-success');
      } else {
        flashIcon('error');
        obj.switchClass('btn-gray', 'btn-red');
        obj.closest('.item').switchClass('bg-success', 'bg-danger');
        obj.closest('.item').switchClass('bg-', 'bg-danger');
      }

      // console.log(res.result);
      // if(res.result.abs_point > 0) {
      //   $('.get-point').show();
      //   $('.get-point').fadeOut();
      // }

      var labels = obj.closest('.quiz').find('label');
      labels.each(function() {
        if(res.result.canswer_no==$(this).data('answer-no')) {
          $(this).switchClass('btn-gray', 'btn-green');
        }
        if( ! $(this).hasClass('btn-green') && ! $(this).hasClass('btn-red')) {
          $(this).addClass('btn-hide');
        }
        $(this).addClass('btn-disabled');
      });

      setTimeout(function(){
        if(only_one=='Y') {
          isLoggedIn(function(is_logged_in) {
            if(is_logged_in=='Y') {
              if(from_share=='Y' || from_main=='Y') {
                document.location.href = 'web#quiz';
              } else {
                history.back();
              }
            } else {
              _alertConfirm("<b>잘 하시네요!!</b> <br/><br/>로그인하시면 랭킹 점수에도 반영되고 재밌는 다양한 기능들을 쓰실 수 있습니다.^^", function() {
                document.location.href = "web#sign";
              });
            }
          });
          
        } else {
          if(solved_count == question_count) {
            isLoggedIn(function(is_logged_in) {
              if(is_logged_in=='Y') {
                getQuizResult(quiz_no, list_type, function(res) {
                  obj.closest('.content-main').html(res.result.html);
                  obj.closest('.content-main').hide();
                  obj.closest('.content-main').fadeIn('slow');
                });
              } else {
                _alertConfirm("<b>잘 하시네요!!</b> <br/><br/>로그인하시면 랭킹 점수에도 반영되고 재밌는 다양한 기능들을 쓰실 수 있습니다.^^", function() {
                  document.location.href = "web#sign";
                });
              }
            });
            
          } else {
            $('.carousel').carousel('next');
          }
        }
        
      }, 1000);
    }
  });
});

$('.carousel').carousel('pause');

// $("#question-comment-modal").pullToRefresh({
//         // refresh: 100,
//         threshold: 1000,
//         resetSpeed: '700ms',
//         scroll: $('#question-comment-modal')
//       })
//       .on("start.pulltorefresh", function (evt, y){
//         // $state.html('Start!! ' + evt + ', '+y)
//       })
//       .on("move.pulltorefresh", function (evt, percentage){
//         // $state.html('Move.. ' + evt + ', '+percentage)
//       })
//       .on("end.pulltorefresh", function (evt){
//         // $state.html('End.. ' + evt)
//         // $(this).html("Pull-me please");
//       })
//       .on("refresh.pulltorefresh", function (evt, y){
//         // $(this).html('Refresh.. ' + evt + ', '+y)
//         // document.location.href = updateQueryStringParameter(window.location.href, 'u', $.now());
//       });

$(document).ready(function() {
    orderLayout(); // 반드시 document.ready 안에

    afterLoad(10);
});