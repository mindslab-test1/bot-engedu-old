navIcon('menu-icon');
showBanner();

var current_quiz_page_no = 1;
var end_of_quiz_load = false;
var loadQuizList = function() {
  if(end_of_quiz_load==true) {
    return;
  }
  var row_count = 20;
  var param = {page_no: current_quiz_page_no, type:'<?=$type?>', row_count: row_count};
  nextQuizList(param, function(res) {
    $('.quizzes .media-list').append(res.result.html);
    current_quiz_page_no++;

    $('.quiz-item').off('click');

    $('.quiz-item').on('click', function () {
      var quiz_no = $(this).data('quiz-no');
      var type = '<?=$type?>';
      document.location.href = 'web#question?quiz_no=' + quiz_no + '&type=' + type;
    }); 

    if(res.result.count < row_count) {
      end_of_quiz_load = true;
    }
  });

  $('.hover-div').bind('touchstart touchend', function(e) {
      // $(this).toggleClass('hover_effect');
  });
};

loadQuizList();

$('.main-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.main-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.main-div')[0].scrollHeight , 10);
    var height = parseInt($('.main-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadQuizList();
    }
}); 

$('.quiz-btn').click(function(){
  var type = $(this).data('type');
  document.location.href = "web#quiz?type="+type;
});

$('.nav-make-btn, .make-sm-btn').click(function() {
  document.location.href = 'web#make/add';
});

// webview에서 이벤트가 안먹길래
$('.make-sm-btn').on({ 
  'touchend' : function(){ 
    document.location.href = 'web#make/add'; } 
});

// // for scroll
// var banner_height = 0;
// if(is_banner_showed) {
//   banner_height = 100;
// }
// $('.quizzes').height($(document).height() - (48+34+20 + banner_height)); // 34+20 = tabgroup
// $(window).resize(function() {
//   banner_height = 0;
//   if(is_banner_showed) {
//     banner_height = 100;
//   }
//   $('.quizzes').height($(document).height()-(48+34+20 + banner_height));
// });

$(document).ready(function() {
    // for scroll
    var banner_height = 0;
    if(typeof JSInterface === "undefined") {
      banner_height = 50;
    } else if(is_banner_showed) {
      banner_height = 50;
    }
    afterLoad(10, '.main-div, .quizzes, .list-group', (48+34+20 + banner_height)); // 34+20 = tabgroup
  
    setP2r('.main-div');

    // 페북 로긴 후 이동시 loading 안없어지는 문제가 있어서 한번 더 호출
    setTimeout(function() {
        endLoading();
    },1500);
});