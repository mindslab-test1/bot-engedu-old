// navIcon('menu-icon');

if(JSInterface) {
	$('.push-list').show();
} else {
	$('.install-alert').show();
}

$('.install-btn').click(function() {
	return;
	window.open(playstore_url, '_blank');
});

$('.feedback-btn').click(function() {
	startLoading();
	_alertConfirm('만약 페이스북 메신저가 없으신 분은<br/>메신저 앱을 설치후 문의 주시면 됩니다.', function() {
		endLoading();
		var url = 'http://m.me/filterya';
		if(JSInterface) {
          JSInterface.openModal(url);
        } else {
        	document.location.href = url;
        }
	});
	
});

$('.setting-save-btn').click(function() {
	saveSetting(function(res) {
		_alert('저장하였습니다.');
		history.back();
	});
});

$('.setting-signout-btn').click(function() {
	signout();
});

$(document).ready(function() {
    // afterLoad(10, '.main-div', 48); 
    afterLoad(10); 

    // setP2r('.main-div');
    if( ! JSInterface) {
      setP2r('.main');
    }
});


