
$(document).on('change', '.btn-service-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  
  var input_file = input.get(0).files[0];

  // if (input_file && input_file.type.match('image.*')) {
    input.trigger('fileselect', [numFiles, label]);

    var reader = new FileReader();

    reader.onload = function (e) {
        $('.service-img-preview').attr('src', e.target.result);
        $('.service-img-preview').attr('height', "50");
    };

    reader.readAsDataURL(input.get(0).files[0]);
  // } else {
  //   _alert('이미지 파일이 아닙니다. 이미지 파일을 선택해주세요.');
  // }

    // $('#service-image-upload-type').val('file');
    // $('.service-image-select-cancel-btn').show();
});

var createChannel = function(act_name, formObj, nextUrl) {
  startLoading();

  var data = new FormData();

  var $files = $('.service-form input[type="file"]');
  $files.each(function() {
    data.append(this.name, $('input[name="'+this.name+'"]')[0].files[0]);
  });

  var $inputs = $('.service-form input[type="text"], .service-form input[type="hidden"]');
  $inputs.each(function() {
    data.append(this.name, $(this).val());
  });

  var $textareas = $('.service-form textarea');
  $textareas.each(function() {
    data.append(this.name, $(this).val());
  });

  var $checkboxes = $('.service-form input[type="checkbox"]');
  $checkboxes.each(function() {
    if(this.checked) {
      data.append(this.name, $(this).val());
    } else {
      data.append(this.name, '');
    }
  });


  $.ajax({
      type: 'post',
      cache:false,
      processData: false, // Don't process the files
      contentType: false, // Set content type to false as jQuery 
      url: 'channel/act_create',
      data: data,
      dataType: 'json',
      success: function(res, textStatus, xhr) {
        endLoading();
        if (res.code == 200) {
          _alert(act_name + '하였습니다.');
          document.location.href = nextUrl;
        } else {
            _alert(res.message);
        }
      },
      error: function(xhr, e) {
        console.log(xhr.responseText);
        endLoading();
      }
    });
    endLoading(); // 한번 더 호출
};

$('.add-channel-btn').click(function() {
	createChannel('생성', $('.service-form'), 'web#channel/my?from_menu=Y'); 
});

$(document).ready(function() {

	var main_tab_height = 0;
	if(current_service_id=='') {
      main_tab_height = 52;
    } 
    afterLoad(10, '.main-div', 48 + main_tab_height);
});