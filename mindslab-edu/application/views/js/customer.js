// navIcon('menu-icon');
showBanner();

var current_customer_page_no = 1;
var end_of_customer_load = false;
var loadCustomerList = function() {
  if(end_of_customer_load==true) {
    return;
  }
  var row_count = 1000;
  var param = {page_no: current_customer_page_no, type:'<?=$type?>', row_count: row_count, order_col: order_col, keyword: keyword};
  nextCustomerList(param, function(res) {
    $('.items .media-list').append(res.result.html);
    current_customer_page_no++;

    $('.customer-item-btn').off('click');
    $('.customer-item-btn').on('click', function () {
      var customer_no = $(this).data('customer-no');
      document.location.href = 'web#customer/edit?customer_no=' + customer_no;
    }); 

    $('.del-customer-btn').off('click');
    $('.del-customer-btn').on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var customer_no = $(this).data('customer-no');
      var btn = $(this);
      _confirm('이 고객을 삭제하시겠습니까?', '네, 삭제하겠습니다', '취소', {confirmButtonClass: 'btn-danger',cancelButtonClass: 'btn-default'}, function() {
            delCustomer(customer_no, function() {
              _alertConfirm('삭제하였습니다.', function() {
                btn.closest('.customer-item').fadeOut();
              });
            });
      });
      
    }); 

    if(res.result.count < row_count) {
      end_of_customer_load = true;
    }
  });

  $('.hover-div').bind('touchstart touchend', function(e) {
      // $(this).toggleClass('hover_effect');
  });
};

loadCustomerList();

$('.main-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.main-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.main-div')[0].scrollHeight , 10);
    var height = parseInt($('.main-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadCustomerList();
    }
}); 

$('.customer-btn').click(function(){
  // 글로벌 변수에 세팅
  order_col = $(this).data('order-col');
  var keyword = $('.search-keyword').val();
  document.location.href = "web#customer?order_col="+order_col+'&keyword='+keyword;
});

$('.customer-add-btn').click(function() {
  document.location.href = 'web#customer/add';
});

// webview에서 이벤트가 안먹길래
$('.make-sm-btn').on({ 
  'touchend' : function(){ 
    document.location.href = 'web#make/add'; } 
});


// // for scroll
// var banner_height = 0;
// if(is_banner_showed) {
//   banner_height = 100;
// }
// $('.items').height($(document).height() - (48+34+20 + banner_height)); // 34+20 = tabgroup
// $(window).resize(function() {
//   banner_height = 0;
//   if(is_banner_showed) {
//     banner_height = 100;
//   }
//   $('.items').height($(document).height()-(48+34+20 + banner_height));
// });

$(document).ready(function() {
    // for scroll
    var banner_height = 0;
    if(typeof JSInterface === "undefined") {
      banner_height = 50;
    } else if(is_banner_showed) {
      banner_height = 50;
    }
    banner_height = 0;
    // afterLoad(10, '.main-div, .items', (48+34+20 + banner_height)); // 34+20 = tabgroup
    afterLoad(10);
  
    // setP2r('.main-div');
    if( ! JSInterface) {
      setP2r('.main');
    }

    // 페북 로긴 후 이동시 loading 안없어지는 문제가 있어서 한번 더 호출
    setTimeout(function() {
        endLoading();
    },1500);
});