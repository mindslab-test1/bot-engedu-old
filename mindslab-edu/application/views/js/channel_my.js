
navIcon('menu-icon');

$('.go-service-btn').click(function() {
	goService($(this).data('service-id'));
});

hideBanner();

$('.create-channel-btn').show();
$('.create-channel-btn').off('click');
$('.create-channel-btn').click(function() {
	startLoading();
	document.location.href = 'web#channel/create';
});

$(document).ready(function() {

	var main_tab_height = 0;
	if(current_service_id=='') {
      main_tab_height = 52;
    } 
    afterLoad(10, '.main-div', 48 + main_tab_height);
});
