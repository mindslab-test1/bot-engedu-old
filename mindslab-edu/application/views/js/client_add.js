

hideBanner();

var source   = $("#answer-box-text-tpl").html();
var template = Handlebars.compile(source);
var aidx = 0;
var search_image_btn;

$('.question-image-select-cancel-btn').hide();


  var answerKeypress = function() {
      $('#answer_1').keypress(function(e) {
          if(e.which == 13) {
             $('#answer_2').focus();
          }
      });
      $('#answer_2').keypress(function(e) {
          if(e.which == 13) {
             $('#answer_3').focus();
          }
      });
      $('#answer_3').keypress(function(e) {
          if(e.which == 13) {
             $('#answer_4').focus();
          }
      });
      $('#answer_4').keypress(function(e) {
          if(e.which == 13) {
             $('.add-question-btn').click();
          }
      });
  };

  $('#q-title').focus(function() {
    $('.make-info').hide();
  });

  $('.step-btn').click(function() {
    $('.step1').hide();
    $('.step2').hide();
    $('.step3').hide();
    $('.make-info').hide();

    if($(this).hasClass('to-step1')) {
      $('.step1').show();
      $('.make-info').show('fast', function() {

      });
    } else if($(this).hasClass('to-step2')) {
      if($('#q-title').val()=='') {
        _alert("문제를 입력해주세요.");
        $('.step1').show('fast', function() {
        
        });
      } else {
        $('.step2').show('fast', function() {
        
        });
      }
    } else if($(this).hasClass('to-step3')) {
      $('.current-question-title').text($('#q-title').val());
      $('.step3').show('fast', function() {
        answerKeypress();
      });
    }
  });

  $(document).on('change', '.btn-question-file :file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      
      var input_file = input.get(0).files[0];

      // if (input_file && input_file.type.match('image.*')) {
        input.trigger('fileselect', [numFiles, label]);

        var reader = new FileReader();

        reader.onload = function (e) {
            $('.question-img-preview').attr('src', e.target.result);
            $('.question-img-preview').attr('height', "50");
        };

        reader.readAsDataURL(input.get(0).files[0]);
      // } else {
      //   _alert('이미지 파일이 아닙니다. 이미지 파일을 선택해주세요.');
      // }

        $('#question-image-upload-type').val('file');
        $('.question-image-select-cancel-btn').show();
  });

  $(document).on('change', '.btn-answer-file :file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      
      var input_file = input.get(0).files[0];

      // if (input_file && input_file.type.match('image.*')) {
        input.trigger('fileselect', [numFiles, label]);

        var reader = new FileReader();
        var preview_tag = input.closest('.answer-box, .answer-image-box').find('.answer-img-preview');

        reader.onload = function (e) {
            preview_tag.attr('src', e.target.result);
            preview_tag.attr('height', "50");
        };

        reader.readAsDataURL(input.get(0).files[0]);

        $(this).closest('.search-image-box-div').find('.search-image-upload-type').val('file');

      // } else {
      //   _alert('이미지 파일이 아닙니다. 이미지 파일을 선택해주세요.');
      // }
  });

  $('.set-answer-type-btn').click(function() {
    var answer_type = $(this).data('answer-type');
    $('#answer_type').val(answer_type);
    source   = $('#answer-box-'+answer_type+'-tpl').html();
    template = Handlebars.compile(source);
    setAnswers();
  });

  var setAnswers = function() {
    $('.answer-boxes').html('');
    for(i=0; i < 4; i++) {
      var html = template({n:i+1, i:i, t:i+2});
      $('.answer-boxes').append(html);
    }

    $('.search-image-btn').off('click');
    $('.search-image-btn').on('click', function() {
      search_image_btn = $(this);
    });
  };

  var delAnswer = function(obj) {
    obj.closest('.answer-box').remove();
  };

  $('.back-btn').click(function() {
    history.back();
  });

  var addAnswerTpl = function() {
    var html = template( {i:aidx, n:aidx+1, t:aidx+2} );
    $('.answer-boxes').append(html);
    $('.del-answer-btn').on("click", function(){ delAnswer($(this)); return false; });
    aidx++;
  };

  $('.add-answer-btn').click(function() {
    addAnswerTpl();
  });

  $('.del-answer-btn').click(function() {
    delAnswer($(this));
  });

  // $('input[type=file]').on('change', prepareUpload);

  // // Grab the files and set them to our variable
  // function prepareUpload(event)
  // {
  //   files = event.target.files;
  // }

  $('.add-question-btn').click(function() {
    actWithFile('make/act_add', '제출', $('.question-form'), 'web#make/add?from_menu=Y&u='+$.now()); // u is for reload.
  });

  $('#image-search-modal').on('show.bs.modal', function (e) {
    $('#image-search-cb').prop('checked', true);
    $('#image-search-cb').attr('checked', true);
    $('#image-search-input').val('');
    $('.image-search-result').html('');
    $('.isr-msg').show();

    $('.set-selected-image-btn').off();
    $('.set-selected-image-btn').click(function() {
      var sel = $('.sr-img-box').find('.img-selected');
      if(sel.length==0) {
        _alert('이미지를 선택해주세요.');
        return;
      }

      var l = sel.data('l');
      var l_w = sel.data('l-w');
      var l_h = sel.data('l-h');
      var t_l = sel.data('t-l');
      var t_l_w = sel.data('t-l-w');
      var t_l_h = sel.data('t-l-h');
      // var link = t_l;
      // if(t_l_h < 100) {
      //   link = l;
      // }

      search_image_btn.closest('.search-image-box-div').find('.search-image-upload-type').val('link');
      search_image_btn.closest('.search-image-box-div').find('.search-image-url').val(l);
      search_image_btn.closest('.search-image-box-div').find('.search-image-thumb-url').val(t_l);
      search_image_btn.closest('.search-image-box-div').find('.search-image-preview').attr('src', t_l);
      search_image_btn.closest('.search-image-box-div').find('.search-image-preview').attr('height', "50");

      $('#image-search-modal').modal('hide');
      $('.question-image-select-cancel-btn').show();
    });
  });

  $('#image-search-input').keypress(function(e) {
      if(e.which == 13) {
          $('.image-search-btn').click();
      }
  });

  $('.image-search-btn').click(function() {
      var q = $('#image-search-input').val();
      var c = 'Y';
      if( $('#image-search-cb').is(":checked")==false ) {
        c = 'N';
      }
      var param = {};
      param.q = q;
      param.c = c;
      searchImage(param, function(res){
        $('.image-search-result').html(res.result.html);
      });
  });

  $('.question-image-select-cancel-btn').click(function() {
    $('#question-image-upload-type').val('');
    $('#question-image-url').val('');
    $('#question-image-thumb-url').val('');
    $('.question-img-preview').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
    $('.question-img-preview').attr('height', "1");
    $('.question-image-select-cancel-btn').hide();
  });

  /* for android scroll when keyboard showing */
  $(window).resize(function() {
    $('.make-div').height($(document).height()-(48 + 50));
  });

  var spreadForm = function() {
    $('.to-step2').hide();
    $('.step2').hide();
    $('.step2-for-fullsize').show();
    $('.step3').show();

    $('.fullsize-set-answer-type-btn').click(function() {
      var answer_type = $(this).data('answer-type');
      $('#answer_type').val(answer_type);
      source   = $('#answer-box-'+answer_type+'-tpl').html();
      template = Handlebars.compile(source);
      setAnswers();

      $('.step2-for-fullsize li').removeClass('active');
      $(this).parent().addClass('active');
    });
  };

  $(document).ready(function() {

      // for(i=0; i<4; i++) {
      //   addAnswerTpl();
      // }
      setAnswers();

      answerKeypress();

      if($(document).height() > 800) { // 넉넉히 잡아야 함. 폼 모양이 달라지기 때문
        spreadForm();
      }

      afterLoad(10, '.make-div', 48);
  });