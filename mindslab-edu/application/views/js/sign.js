
// navIcon('menu-icon');
hideBanner();

$('.fb-login-btn').click(function() {
  startLoading();
  document.location.href="<?=get_facebook_url()?>";
});

$('.gl-login-btn').click(function() {
  startLoading();
  document.location.href="<?=get_google_url()?>";
});

$('.sign-btn').click(function() {
  // signin('local');
  signCheck(function(res) {
    if(res.code==200) {
      signin('local');
    } else if(res.code==201) {
      _confirm('이 신규 아이디로 가입과 동시에 로그인하시겠습니까?.<br/> 비밀번호를 잘 기억해주세요.', '네, 가입하겠습니다', '아니요', {}, function() {
        signin('local');
      });
    } else {
      _alert(res.message);
    }
  });
});

$('.device-login-btn').click(function() {
  if(JSInterface) {
    JSInterface.callDeviceIdSignin();
  }
});

$('.user-id-input').keypress(function(e) {
    if(e.which == 13) {
        $('.user-pass-input').focus();
    }
});

$('.user-pass-input').keypress(function(e) {
    if(e.which == 13) {
        $('.sign-btn').click();
    }
});

/* for android scroll when keyboard showing */
$(window).resize(function() {
  $('.sign-div').height($(document).height()-(48 + 50));
});

$(document).ready(function () {



  afterLoad(10, '.sign-div', 48);
});