navIcon('left-arrow-icon');

var goAppSearch = function() {
	var q = $('#q').val();
    var param = {q:q};
    nextSearch(param, function(res) {
      if(res.code==200) {
  		$('.search-list').html(res.result.html);

  		$('.app-icon-one').off('click');
  		$('.app-icon-one').on('click', function() {
			startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
			var link = $(this).data('link');
			document.location.href = link;
		});

      } else {
        _alert(res.message);
      }
    });
};

$('.search-input').off('keypress');
$('.search-input').on('keypress', function(e) {
  if(e.which == 13) {
    goAppSearch();
  }
});

$('.app-search-btn').off('click');
$('.app-search-btn').on('click', function() {
	goAppSearch();
});

$(document).ready(function() {
    afterLoad(10, '.main-div', 48);
});
