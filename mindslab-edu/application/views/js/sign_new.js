
// navIcon('menu-icon');
hideBanner();

$('.fb-login-btn').click(function() {
  startLoading();
  document.location.href="<?=get_facebook_url()?>";
});

$('.gl-login-btn').click(function() {
  startLoading();
  document.location.href="<?=get_google_url()?>";
});

$('.sign-btn').click(function() {
  // signin('local');
  signCheck(function(res) {
    if(res.code==200) {
      signin('local');
    } else if(res.code==201) {
      _confirm('이름: '+res.result.name+'<br/>휴대폰 번호: '+res.result.phone+'<br/><br/>위 정보로 가입과 동시에 로그인하시겠습니까?<br/> 비밀번호를 잘 기억해주세요.', '네, 가입하겠습니다', '아니요', {}, function() {
        signin('local');
      });
    } else {
      _alert(res.message);
    }
  });
});

$('.user-id-input').keypress(function(e) {
    if(e.which == 13) {
        $('.user-pass-input').focus();
    }
});

$('.user-pass-input').keypress(function(e) {
    if(e.which == 13) {
        $('.sign-btn').click();
    }
});

/* for android scroll when keyboard showing */
$(window).resize(function() {
  $('.sign-div').height($(document).height()-(48 + 50));
});

$(document).ready(function () {

  // '#'' 때문에 URL 인코딩 해줘야
  // $('#redirect-url').val(encodeURIComponent($('#redirect-url').val()));

  // $(".user-phone-input").keyup(function (e) {
  //    //if the letter is not digit then display error and don't type anything
  //    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
  //       var textVal = $('.user-phone-input').val();
  //       $('.user-phone-input').val(textVal.substring(0,textVal.length - 1));
  //   }
  //  });

  afterLoad(10, '.sign-div', 48);
});