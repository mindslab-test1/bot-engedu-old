navIcon('menu-icon');

var current_rank_page_no = 1;
var rank_order_col = 'ranking';
var rank_order_type = 'asc';
var end_of_load = false;
var rank_loading = false;
var loadRankList = function() {
  if(rank_loading==false) {
    rank_loading = true;
    var param = {page_no:current_rank_page_no};
    param.order_col = rank_order_col;
    param.order_type = rank_order_type;
    param.row_count = 50;
    if(end_of_load==true) {
      return;
    }
    nextRankList(param, function(res) {
      $('.rank .list-group').append(res.result.html);
      current_rank_page_no++;

      if(res.result.rank_count > 0) {
        $('.item-user-btn').off("click"); // 중복 클릭 방지
        $('.item-user-btn').on('click', function() {
          var user = {};
          user.no = $(this).data('user-no');
          user.name = $(this).data('user-name');
          user.html = $(this).closest('.item-rank').find('.item-user-info').html();
          showUserInfo(user);
        });
      }

      if(res.result.rank_count < param.row_count) {
        end_of_load = true;
      }
      rank_loading = false;
    });
  }
  
};

loadRankList();

$('.main-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.main-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.main-div')[0].scrollHeight , 10);
    var height = parseInt($('.main-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadRankList();
    }
}); 

$('.rank-btn').click(function() {
  $('.main-div').scrollTop(0);
  $('.rank .list-group').html('');
  rank_order_col = $(this).data('order-col');
  rank_order_type = $(this).data('order-type');
  current_rank_page_no = 1;
  end_of_load = false;
  rank_loading = false;
  $('.rank-btn').removeClass('active');
  $(this).addClass('active');
  loadRankList();
  
});

$(document).ready(function() {
    // for scroll
    var banner_height = 0;
    var main_tab_height = 0;
    
    if(current_service_id=='') {
      main_tab_height = 52;
    } else {
      if(typeof JSInterface === "undefined") {
        banner_height = 50;
      } else if(is_banner_showed) {
        banner_height = 50;
      }
    }
    var minusH = (48+34+20 + banner_height + main_tab_height); // 34+20 = tabgroup
    afterLoad(10, '.main-div, .list-group .content-main', minusH); 

    setP2r('.main-div');

});