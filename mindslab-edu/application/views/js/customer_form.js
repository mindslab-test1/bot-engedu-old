navIcon('left-arrow-icon');
hideBanner();

var actCustomer = function(act) {
	startLoading();
	var url = '/customer/act_add';
	if(act=='edit') {
		url = '/customer/act_edit';
	}
	$.ajax({
	    type: 'post',
	    url: url,
	    data: $('.customer-form').serialize(),
	    dataType: 'json',
	        error: function(xhr, e){
	            console.log(xhr.responseText);
	        },
	        success: function(res, textStatus, xhr) {
	          endLoading();
	          if(res.code==200) {
	             
	             if(act=='edit') {
	             	_alertConfirm("저장하였습니다.", function() {
	             		document.location.reload();
	             	});
	             } else {
	             	_alert("저장하였습니다.");
	             	document.location.href = '/web#customer';
	             }
	          } else {
	            _alert(res.message);
	          }
	      },
	      complete: function(xhr, textStatus) {
	        endLoading();
	      }
	});
}

/* for android scroll when keyboard showing */
$(window).resize(function() {
	var adHeight = 0;
	// var adHeight = 50;
  	$('.customer-div').height($(document).height()-(48 + adHeight));
});

$('.model-input').autocomplete({
    serviceUrl: '/lr/models',
    onSelect: function (suggestion) {
    	console.log(suggestion);
        // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});


$(document).ready(function() {
	$('.act-customer-btn').click(function() {
		var act = $(this).data('act');
		actCustomer(act);
	});

	$('.activate-edit-btn').click(function() {
		var selector = $(this).data("selector");
		if($(this).hasClass('activated')) {
			setAllFieldReadOnly(selector);
			$(this).removeClass('activated');
			$(this).text('편집');
		} else {
		   	setAllFieldWritable(selector);
		   	$(selector+' input.first-input').focus();
		  	$(this).addClass('activated');
		  	$(this).text('취소');
		}
	});

    afterLoad(50, '.customer-div', 48);
});