
navIcon('left-arrow-icon');

$(document).ready(function() {
	afterLoad(10, '.profile-setting-div', 48);

    $('.btn-profile-file :file').on('fileselect', function(event, numFiles, label) {
        $('.profile-file-label').text(label);
    });

    $('.btn-background-file :file').on('fileselect', function(event, numFiles, label) {
        $('.background-file-label').text(label);
    });
});

$(document).on('change', '.btn-profile-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

    var input_file = input.get(0).files[0];

    // if (input_file && input_file.type.match('image.*')) {
    	input.trigger('fileselect', [numFiles, label]);

    	var reader = new FileReader();

	    reader.onload = function (e) {
	        $('.profile-img')
	            .attr('src', e.target.result)
	            .width(50)
	            .height(50);
	    };

	    reader.readAsDataURL(input_file);
    // } else {
    // 	_alert('이미지 파일이 아닙니다. 이미지 파일을 선택해주세요.');
    // }
});

$(document).on('change', '.btn-background-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    
    var input_file = input.get(0).files[0];

    // if (input_file && input_file.type.match('image.*')) {
    	input.trigger('fileselect', [numFiles, label]);
    	
    	var reader = new FileReader();

	    reader.onload = function (e) {
	        // $('.background-img')
	        //     .attr('src', e.target.result)
	        //     .width(100)
	        //     .height(50);
	        $('.background-img-div')
	            .css('background-image', 'url('+e.target.result+')');

	        $('.background-img-div')
	            .css('border', 'none');
	    };

	    reader.readAsDataURL(input.get(0).files[0]);
    // } else {
    // 	_alert('이미지 파일이 아닙니다. 이미지 파일을 선택해주세요.');
    // }
});

$('.profile-save-btn').click(function() {
	saveProfile(function(res) {
		_alert('저장하였습니다.');
		history.back();
	});
});

$('.email-verify-btn').click(function() {
	verifyEmail(function(res) {
		_alert($('#email').val()+'로 인증 메일을 전송하였습니다.');
	});
});