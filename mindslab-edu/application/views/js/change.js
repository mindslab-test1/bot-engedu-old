
// navIcon('menu-icon');
hideBanner();

$('.change-badge').hide();

var current_change_page_no = 1;
var loadChangeList = function() {
  var param = {page_no:current_change_page_no};
  nextChangeList(param, function(res) {
    $('.change-list-div .timeline').append(res.result.html);
    if(current_change_page_no==1 && res.result.change_count==0) {
      $('.change-list-div ul').toggleClass('line-hide');
    }
    current_change_page_no++;

    // $('.change-link-btn').off();
    // $('.change-link-btn').on('click', function() {
    //   var link = $(this).data('change-link');
    //   document.location.href = link;
    // });

    $('.del-change-btn').off('click');
    $('.del-change-btn').on('click', function (e) {
      var user_filter_change_no = $(this).data('user-filter-change-no');
      var btn = $(this);
      _confirm('이  내역을 삭제하시겠습니까?', '네, 삭제하겠습니다', '취소', {confirmButtonClass: 'btn-danger',cancelButtonClass: 'btn-default'}, function() {
            delChange(user_filter_change_no, function() {
              _alertConfirm('삭제하였습니다.', function() {
                btn.closest('.change-row').fadeOut();
              });
            });
      });
      
    }); 

  });
};

loadChangeList();

$('.change-list-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.change-list-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.change-list-div')[0].scrollHeight , 10);
    var height = parseInt($('.change-list-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadChangeList();
    }
}); 


$(document).ready(function() {
    var main_tab_height = 0;
    if(''=='<?=SERVICE_ID?>') {
      main_tab_height = 52;
    }
    // afterLoad(10, '.change-list-div', 48 + main_tab_height);
    afterLoad(10);
    
    // setP2r('.main-div');
    if( ! JSInterface) {
      setP2r('.main');
    }
});