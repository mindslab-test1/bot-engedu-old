
navIcon('menu-icon');

$('.go-service-btn').click(function() {
	goService($(this).data('service-id'));
});

hideBanner();

$('.go-random-btn').show();
$('.go-random-btn').off('click');
$('.go-random-btn').click(function() {
	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
	document.location.href = '/random';
});

$('.go-search-btn').show();
$('.go-search-btn').off('click');
$('.go-search-btn').click(function() {
	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
	document.location.href = 'web#search';
});

// $('.create-channel-btn').show();
// $('.create-channel-btn').off('click');
// $('.create-channel-btn').click(function() {
// 	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
// 	document.location.href = 'web#channel/create';
// });


$('.app-icon-one').click(function() {
	startLoading(); // host를 이동하므로 무조건 로딩 바로 보여주기
	var link = $(this).data('link');
	document.location.href = link;
});



$(document).ready(function() {

	var main_tab_height = 0;
	if(current_service_id=='') {
      main_tab_height = 52;
    } 
    afterLoad(10, '.main-div', 48 + main_tab_height);
});
