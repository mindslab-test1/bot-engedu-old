navIcon('left-arrow-icon');
hideBanner();

var actCar = function(act) {
	startLoading();
	var url = '/car/act_add';
	if(act=='edit') {
		url = '/car/act_edit';
	}
	$.ajax({
	    type: 'post',
	    url: url,
	    data: $('.car-form').serialize(),
	    dataType: 'json',
	        error: function(xhr, e){
	            console.log(xhr.responseText);
	        },
	        success: function(res, textStatus, xhr) {
	          endLoading();
	          if(res.code==200) {
	             
	             if(act=='edit') {
	             	_alertConfirm("저장하였습니다.", function() {
	             		document.location.reload();
	             	});
	             } else {
	             	_alert("저장하였습니다.");
	             	document.location.href = '/web#car';
	             }
	          } else {
	            _alert(res.message);
	          }
	      },
	      complete: function(xhr, textStatus) {
	        endLoading();
	      }
	});
}

/* for android scroll when keyboard showing */
$(window).resize(function() {
	var adHeight = 0;
	// var adHeight = 50;
  	$('.car-div').height($(document).height()-(48 + adHeight));
});

$('.model-input').autocomplete({
    serviceUrl: '/lr/models',
    onSelect: function (suggestion) {
    	console.log(suggestion);
        // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});

// var firstCompleteItem;
// var search_items = 'model';
// var model_search_cache = {};
// $( ".model-input" ).autocomplete({
// 	minLength: 1,
// 	autoFocus: true,
// 	select: function(event, ui) {
// 	  // goSearchResult(ui.item);
// 	},
// 	source: function( request, response ) {
// 	  var term = request.term;

// 	  // 화면 넓이에 따라 search_items가 달라지므로 cache를 주석처리
// 	  // if ( term in model_search_cache ) {
// 	  //   response( model_search_cache[ term ] );
// 	  //   return;
// 	  // }

// 	  var keyword = $('.model-input').val();

// 	  $.ajax({
// 	    type: 'get',
// 	    url: '/lr/models',
// 	    data: {keyword:keyword, search_items:search_items},
// 	    dataType: 'json',
// 	      error: function(xhr, e){
// 	          console.log(xhr);
// 	      },
// 	      success: function(res, textStatus, xhr) {
// 	        model_search_cache[ term ] = res.result.models;
// 	        firstCompleteItem = res.result.models[0];
// 	        response( res.result.models );
// 	      },
// 	      complete: function(xhr, textStatus) {
// 	        // 검색 히스토리에 저장 (검색 후에 저장하기 위해 여기서 저장)
// 	        // searchLog(keyword);
// 	        endLoading();
// 	      }
// 	  });

// 	  // $.getJSON( "/r/get_json", request, function( data, status, xhr ) {
// 	  //   model_search_cache[ term ] = data;
// 	  //   response( data );
// 	  // });
// 	}
// 	}).autocomplete( "instance" )._renderItem = function( ul, model ) {
// 	var cls = 'search-model-region-li';
// 	var badge = '';
// 	// var badge = '<span class="label label-primary pull-right">지역</span>';
// 	if(model.label=='name') {
// 	  cls = 'search-model-name-li';
// 	  badge = '<span class="label label-warning pull-right">업체</span>';
// 	} else if(model.label=="subway") {
// 	  cls = 'search-model-name-li';
// 	  badge = '<span class="label label-gray pull-right" style="background-color:'+model.extra_color+'">'+model.extra+'</span>';
// 	} else {
// 	  cls = 'search-region-name-li';
// 	  badge = '<span class="label label-default pull-right">지역</span>';
// 	}
// 	return $( '<li class="'+cls+'">' )
// 	  .append(model.value + badge)
// 	  .appendTo( ul );
// };


$(document).ready(function() {
	$('.act-car-btn').click(function() {
		var act = $(this).data('act');
		actCar(act);
	});

	$('.activate-edit-btn').click(function() {
		var selector = $(this).data("selector");
		if($(this).hasClass('activated')) {
			setAllFieldReadOnly(selector);
			$(this).removeClass('activated');
			$(this).text('편집');
		} else {
		   	setAllFieldWritable(selector);
		   	$(selector+' input.first-input').focus();
		  	$(this).addClass('activated');
		  	$(this).text('취소');
		}
	});

    afterLoad(50, '.car-div', 48);
});