
$('.profile-edit-btn').click(function() {
	document.location.href = 'web#profile/setting';
});

$('.profile-desc-edit-btn').click(function() {
	document.location.href = 'web#profile/description';
});

$('.cardsummary .make-list-btn').click(function() {
	var user_no = $(this).data('user-no');
	document.location.href = 'web#make?user_no='+user_no;
});

$('.service-profile-btn').click(function() {
	startLoading();
	var service_id = $(this).data('service-id');
	document.location.href = 'http://'+service_id+'.filterya.appyouwant.com/web#profile';
});

$('.go-rank-btn').click(function() {
	document.location.href = 'web#rank';
});

$('.nav-logout-btn').show();
$('.nav-logout-btn').click(function() {
	signout();
});


$(document).ready(function() {

	// $('.cardheader').height( $('.cardheader').width() * 0.4 );

	var main_tab_height = 0;
	if(current_service_id=='') {
      main_tab_height = 52;
    } 
    afterLoad(10, '.main-div', 48 + main_tab_height);

    setP2r('.main-div');

    // for scroll
    $('.nav-profile-select ul').height($(document).height() - 100);
});
