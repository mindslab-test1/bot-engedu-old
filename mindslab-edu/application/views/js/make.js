
var make_user_no = '<?=$user_no?>';
navIcon('left-arrow-icon');
hideBanner();

var current_make_page_no = 1;
var loadMakeList = function() {
  var param = {page_no:current_make_page_no, order_col:'<?=$order_col?>', order_type:'<?=$order_type?>', user_no: make_user_no};
  nextMakeList(param, function(res) {
    $('.make-list-div .list-group').append(res.result.html);
    current_make_page_no++;

    // 중복 클릭 방지
    $('.question-item').off('click');
    $('.question-delete-btn').off('click');

    $('.question-item').on('click', function () {
        var question_no = $(this).data('question-no');
        var go_url = '';
        // 메인 페이지인 경우 전체 URL로 이동함
        if(current_service_id=="") {
          var service_id = $(this).data('service-id');
          go_url = 'http://'+service_id+'.filterya.appyouwant.com/';
        }
        document.location.href = go_url + "web#question?question_no="+question_no;
    });
    $('.question-delete-btn').on('click', function () {
        event.stopPropagation(); // 문제로 이동하는 것 막기
        var question_no = $(this).data('question-no');
        var itemObj = $(this).closest('.question-item');
        _confirm('이 문제를 삭제하시겠습니까?', '네, 삭제하겠습니다', '아니요', {confirmButtonClass: 'btn-danger'}, function() {
          deleteQuestion(question_no, function(success_res) {
            _alertConfirm("삭제하였습니다.", function() {
              itemObj.slideUp();
            });
          });
        });
    }); 
  });
};

loadMakeList();

$('.make-list-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.make-list-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.make-list-div')[0].scrollHeight , 10);
    var height = parseInt($('.make-list-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadMakeList();
    }
}); 


$('.make-current-list-btn').click(function() {
  document.location.href = 'web#make?user_no='+make_user_no;
});

$('.make-like-list-btn').click(function() {
  document.location.href = 'web#make?order_col=like_count&order_type=desc&user_no='+make_user_no;
});

$(document).ready(function() {
    var main_tab_height = 0;
    if(current_service_id=='') {
      main_tab_height = 52;
    } 
      // for scroll
    afterLoad(10, '.make-list-div', (48+34+20+main_tab_height)); // 34+20 = tabgroup
});