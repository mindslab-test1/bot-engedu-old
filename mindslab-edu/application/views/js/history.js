
// navIcon('menu-icon');
hideBanner();

$('.history-badge').hide();

var current_history_page_no = 1;
var loadHistoryList = function() {
  var param = {page_no:current_history_page_no};
  nextHistoryList(param, function(res) {
    $('.history-list-div .timeline').append(res.result.html);
    if(current_history_page_no==1 && res.result.history_count==0) {
      $('.history-list-div ul').toggleClass('line-hide');
    }
    current_history_page_no++;

    $('.history-link-btn').off();
    $('.history-link-btn').on('click', function() {
      var link = $(this).data('history-link');
      document.location.href = link;
    });
  });
};

loadHistoryList();

$('.history-list-div').scroll(function(){
    // 소수점 제거
    var scrollTop = parseInt($('.history-list-div').scrollTop(), 10);
    var scrollHeight = parseInt($('.history-list-div')[0].scrollHeight , 10);
    var height = parseInt($('.history-list-div').height() , 10);
    if(scrollTop == scrollHeight - height){
      loadHistoryList();
    }
}); 


$(document).ready(function() {
    var main_tab_height = 0;
    if(''=='<?=SERVICE_ID?>') {
      main_tab_height = 52;
    }
    // afterLoad(10, '.history-list-div', 48 + main_tab_height);
    afterLoad(10);
    
    // setP2r('.main-div');
    if( ! JSInterface) {
      setP2r('.main');
    }
});