<style>
.pic-div {
	padding: 0 10px;
}
.pic-box {
	margin-bottom: 10px;
	height: 40%;
	/*background-image: url(/assets/images/pics/picture1.jpg);*/
	background-size: contain;
	background-position: center center;
	background-repeat: no-repeat;
	background-color: #222;
}
.talk-box {
	border-radius: 5px;
	height: 55%;
}
</style>

<div class="pic-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="content-box-div">
		<div class="pic-box" style="display:none">
		</div>

		<div class="talk-box"  style="display:none">
			<?=$this->load->view('_talk_bubble',$this->data, true)?>
		</div>
	</div>

</div>

<?php $this->load->view('_microphone',$this->data); ?>

<script>
	endLoading();
	resizeMain();

	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
		$('.pic-box').css('background-image', 'url(/assets/images/pics/picture'+unitNum+'.jpg?u=<?=time()?>)');
		$('.talks').html('');
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			addLine(message, 'left');
			speechText(message,'eng');
		}
	};

	var operateResult = function(result) {
		_alert('축하합니다.<br/>레벨이 1등급 승급하셨습니다.', 'green');
	};

	var operateClientMessage = function(message) {
		addLine(message, 'right');
	};

	guideAlert('그림을 참조하여 각 그림당 3개의<br/> 질문에 답하세요.', function() {
		$('.pic-box').fadeIn();
		$('.talk-box').fadeIn();

		setContentBoxDiv();

		initChatUI();

		startUnit(unitNum, function() {
			sendMessage('picture'+unitNum);
		});

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['What they doing?', 'They are playing tennis.'];
			var i = 0;
			var timer =setInterval(function() {
				if(i==msgs.length) {
					clearInterval(timer);
					return;
				}

				if(i%2==0) {
					receiveMessageFromServer(msgs[i]);
				} else {
					runClientSpeech(msgs[i]);
				}

				i++;

			}, 1000);
		<?php endif; ?>
	});
</script>



