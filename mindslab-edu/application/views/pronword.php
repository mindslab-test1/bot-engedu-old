<style>
.pronword-div {
	padding: 0px 10px;
}
.question-multi-slick > div {
	text-align: center;
}
.slick-slide {
	height: 200px;
	font-size: 28px;
	background-color: #eee;
	color: #ccc;
}
.slick-slide.slick-center {
	color: #333;
	background-color: #fff;
}
.slick-slide > span {
	/* 세로로 가운데 정렬 */
	position: relative;
	display: block;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    top: 50%;
}
.question-box {
	display: none;
}
</style>


<div class="pronword-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="content-box-div">
		<div class="question-box">
			<?/*display:none으로 숨기면 slick이 초기화를 제대로 못하는 문제가 있어서 position으로 처리 */?>
			<div class="question-multi-slick" style="position:relative; left: -10000px; overflow-x: hidden;">
				<?php for ($i=0; $i < 100; $i++) : ?>
					<div><span> </span></div>
				<?php endfor; ?>
			</div>
		</div>
	</div>

</div>

<?php $this->load->view('_microphone',$this->data); ?>

<script>
	endLoading();
	resizeMain();

	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
		initSlick();
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			$(".question-multi-slick").slick('slickNext');
			$('.slick-current span').text(message);
			speechText(message,'eng');
		}
	};

	var operateResult = function(result) {
		showResult('평가 결과', result, ['point','word'], function(act) {
			if(act=='re') {
				startUnit(unitNum, function() {
					sendMessage('word_pronunciation'+unitNum);
				});
			} else if(act=='next') {
				unitNum++;
				startUnit(unitNum, function() {
					sendMessage('word_pronunciation'+unitNum);
				});
			}
		});
	};

	var operateClientMessage = function(message) {
	}

	var curSlick;
	var initSlick = function() {
		// $(".slider").not('.slick-initialized').slick({
		$(".question-multi-slick").not('.slick-initialized').slick({
		  infinite: false,
		  arrows: true,
		  centerMode: true,
		  centerPadding: '60px',
		  slidesToShow: 3,
		  responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '40px',
		        // slidesToShow: 3
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
		  ]
		});
	}

	guideAlert('들려주는 5개의 단어를 듣고<br/> 순서대로 따라 해보세요.', function() {

		$('.question-box').fadeIn();

		$('.question-multi-slick').css('left', 'auto');

		setContentBoxDiv();

		startUnit(unitNum, function() {
			// sendMessage('word_pronunciation'+unitNum);
			sendMessage('word_pronunciation'+unitNum);
		});

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['Flower','Stair'];
			var i = 0;
			var timer =setInterval(function() {
				if(i == msgs.length) {
					clearInterval(timer);
					// receiveResultFromServer('test_result');
					setTimeout(function(){
						receiveMessageFromServer('RESULT: success');
					}, 1000);
					return;
				}
				receiveMessageFromServer(msgs[i]);
				i++;
				
			}, 2000);
		<?php endif; ?>

	});



</script>



