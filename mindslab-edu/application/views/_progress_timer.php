<style>
#progressTimer {
    width:100%;
    margin-bottom: 10px;
    position: relative;
}
#progressTimer .progress {
	margin-bottom: 0;
}
</style>

<div id="progressTimer"></div>

<script>
	var startProgressTimer = function() {
		proTimer = $("#progressTimer").progressTimer({
		    timeLimit: 15,
		    warningThreshold: 10,
		    baseStyle: 'progress-bar-warning',
		    warningStyle: 'progress-bar-danger',
		    completeStyle: 'progress-bar-info',
		    showHtmlSpan: false,
		    onFinish: function() {
		        console.log("progress done");
		        // 타이머 멈추기
		        proTimer.progressTimer('complete');
		    }
		});
	};
</script>

<?/*
<!-- ###################################################### -->

<div id="progressBar">
  <div class="bar"></div>
</div>
<style>
#progressBar {
  width: 90%;
  margin: 10px auto;
  height: 22px;
  background-color: #0A5F44;
}

#progressBar div {
  height: 100%;
  text-align: right;
  padding: 0 10px;
  line-height: 22px; 
  width: 0;
  background-color: #CBEA00;
  box-sizing: border-box;
}
</style>

<script>
	function progress(timeleft, timetotal, $element) {
	    var progressBarWidth = timeleft * $element.width() / timetotal;
	    // $element.find('div').animate({ width: progressBarWidth }, 500).html(Math.floor(timeleft/60) + ":"+ timeleft%60);
	    $element.find('div').animate({ width: progressBarWidth }, 500).html(Math.floor(timeleft));
	    if(timeleft > 0) {
	        setTimeout(function() {
	            progress(timeleft - 1, timetotal, $element);
	        }, 1000);
	    }
	};

	progress(15, 15, $('#progressBar'));	
</script>
*/?>