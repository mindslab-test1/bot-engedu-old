<nav class="jpanel-nav" style="display:none">

  <div class="left-my <?=$current_user?'':'display-none'?>">
    <span class="current-user pull-left"><?=$current_user['name']?></span>
    <span class="signout-btn left-logout pull-right">로그아웃</span>
  </div>
  <ul class="nav nav-pills nav-stacked left-nav">
    <div class="left-logo"><a href="/" style="display: inline-block;padding-right: 0"><img src="/assets/home/images/english-edu-101x101.png" width="22" height="22"></a> <a href="/web#main"  style="display: inline-block;padding-left: 5px;">마인즈 잉글리시</a></div>
    <li class="pronword"><a href="/web#pronword?from_menu=Y">발음 평가 (단어)</a></li>
    <li class="pronsentence"><a href="/web#pronsentence?from_menu=Y">발음 평가 (문장)</a></li>
    <li class="syntax"><a href="/web#syntax?from_menu=Y">문장 따라하기</a></li>
    <li class="loud"><a href="/web#loud?from_menu=Y">큰 소리로 읽기</a></li>
    <li class="pic"><a href="/web#pic?from_menu=Y">그림 보고 답하기</a></li>
    <li class="explain"><a href="/web#explain?from_menu=Y">설명 듣고 답하기</a></li>
    <li class="han"><a href="/web#han?from_menu=Y">한글 듣고 영어로 말하기</a></li>
    <li class="pattern"><a href="/web#pattern?from_menu=Y">회화 패턴 연습</a></li>
    <li class="conversation"><a href="/web#conversation?from_menu=Y">기초 회화</a></li>
  </ul>
</nav>