<link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/jquery-progressTimer/css/jquery.progresstimer.min.css">
<script src="<?=CDN_HTTP_HOST?>/assets/jquery-progressTimer/js/jquery.progresstimer.min.js" type="text/javascript"></script>

<style>
.loud-div {
	padding: 0px 15px;
}
.question-box {
	height: 42%;
}
.answer-box {
	height: 42%;
}
</style>

<div class="loud-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="content-box-div">
		<div class="question-box" style="display: none">
			<div class="question">
				<!-- How old are you? -->
			</div>
		</div>

		<?=$this->load->view('_progress_timer',$this->data, true)?>

		<div class="answer-box" style="display: none">
			<div class="answer"></div>
		</div>
	</div>

</div>

<script>
	endLoading();
	resizeMain();

	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			$('.question').html(message);
			speechText(message,'eng');
		}
	};

	var operateResult = function(result) {
		showResult('평가 결과', result, ['speed', 'explain'], function(act) {
			if(act=='re') {
				startUnit(unitNum, function() {
					sendMessage('read_aloud'+unitNum);
				});
			} else if(act=='next') {
				unitNum++;
				startUnit(unitNum, function() {
					sendMessage('read_aloud'+unitNum);
				});
			}
		});
	};

	var operateClientMessage = function(message) {
	}

	guideAlert('제시된 문장을 큰 소리로 읽어 보세요.<br/>제한된 시간내에 읽기를 완료해 주세요.', function() {

		$('.question-box').fadeIn();
		$('.answer-box').fadeIn();

		setContentBoxDiv();

		startUnit(unitNum, function() {
			// sendMessage('word_pronunciation'+unitNum);
			sendMessage('read_aloud'+unitNum);
		});

		startProgressTimer();

		// Streaming Text TEST
		// var k = 1;
		// var t = setInterval(function() {
		// 	var word = 'hello';
		// 	if(k%2==0) {
		// 		word = 'world';
		// 	}
		// 	$('.answer-box .answer').append(word + ' ');

		// 	if(k > 10) {
		// 		clearInterval(t);
		// 	}

		// 	k++;

		// }, 500);

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['It\'s time to go to bed and this is how I get ready for bed. I put on my pajamas, brush my teeth, and say good night to my mom and dad. Finally, I go to sleep.'];
			var i = 0;
			var timer =setInterval(function() {
				if(i == msgs.length) {
					clearInterval(timer);
					// receiveResultFromServer('test_result');
					setTimeout(function(){
						receiveMessageFromServer('RESULT: success');
					}, 8000);
					return;
				}
				receiveMessageFromServer(msgs[i]);
				i++;
				
			}, 1000);
		<?php endif; ?>
	});
</script>



