<style>
.microphone-div {
	position: absolute;
	bottom: 0;
	display: block;
	width: 100%;
	height: 48px;
	line-height: 48px;
	background-color: #fff;
	color: #da6013;
	text-align: center;
}
.microphone-div i {
	line-height: 48px;
}
</style>
<div class="microphone-div" style="display: none">
	<i class="fa fa-microphone fa-2x" aria-hidden="true"></i>
</div>