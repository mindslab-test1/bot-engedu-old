<div class="search-box">
  <div class="input-group">
    <input type="text" name="keyword" class="form-control search-keyword" placeholder="검색..." value="<?=$keyword?>">
    <span class="input-group-btn">
      <button class="btn btn-default search-btn" type="button"><i class="fa fa-search"></i></button>
    </span>
  </div>
</div>

<script>
$('.search-btn').click(function() {
	var keyword = $('.search-keyword').val();
	var url = updateQueryStringParameter(window.location.href, 'keyword', encodeURIComponent(keyword));
	document.location.href = url;
});

$('.search-keyword').keyup(function(e) {
	if(e.keyCode==13) {
		$('.search-btn').click();
	}
});
</script>