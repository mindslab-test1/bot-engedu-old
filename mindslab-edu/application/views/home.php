<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Minds English Website Builder v4.3.5, https://minds-english.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Minds English v4.3.5, minds-english.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="shortcut icon" href="/assets/home/images/logo4.png" type="image/x-icon"> -->
  <meta name="description" content="">
  <title>Minds English</title>
  <script src="/assets/home/web/assets/jquery/jquery.min.js"></script>
  <script>
    var JSInterface;
    var request_app_id = '<?=get_request_app_id()?>';
    var currentHttpHost = '<?=$this->config->item('base_url')?>';
    var proTimer;
  </script>
  <link rel="stylesheet" href="/assets/home/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="/assets/home/tether/tether.min.css">
  <link rel="stylesheet" href="/assets/home/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/home/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="/assets/home/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="/assets/home/socicon/css/styles.css">
  <link rel="stylesheet" href="/assets/home/theme/css/style.css">
  <link rel="stylesheet" href="/assets/home/mobirise/css/mbr-additional.css" type="text/css">
  	<style>
      .pull-left {
        float: left !important;
      }
      .pull-right {
        float: right !important;
      }

  		.display-5 {
  			letter-spacing: 1.2px;
  		}

      .service-div  {
        /*width: 33.3333%;*/
        width: 50%;
        float: left;
        padding: 32px;
        font-size: 20px;

      }
      .service-div .service-item {
        display: inline-block;
        width: 100%;
        height: 120px;
        border-radius: 3px;
      }
      .service-div .service-item.image-item:hover {
        /* hover가 모바일에서는 동작이 다르므로 scale은 주석처리 */
      /*  transform: scale(1.022,1.022);
          -webkit-transform: scale(1.022,1.022);
          -moz-transform: scale(1.022,1.022);
          -ms-transform: scale(1.022,1.022);*/
          opacity: 0.5;
      }
      .service-caption {
        color: #333;
        text-align: center;

        /* 세로로 가운데 정렬 */
        position: relative;
          -webkit-transform: translateY(-50%);
          -ms-transform: translateY(-50%);
          transform: translateY(-50%);
          top: 50%;
      }
      .empty-service-div {
        display: none;
      }
      @media (max-width: 680px) {
        .main-div {
          padding: 3px;
        }
        .service-div {
          width: 50%;
          padding: 3px;
          font-size: 16px;
        }
        .empty-service-div {
          display: block;
        }
      }

      .service-caption {
        font-size: 28px;
      }
      @media (max-width: 640px) {
        .service-caption {
          font-size: 16px;
        }
      }
	</style>
  
</head>
<body>
<section class="cid-qzfMceUuRu mbr-fullscreen" data-bg-video="https://www.youtube.com/embed/sXTGMMenxwI&amp;rel=0&amp;showinfo=0&amp;autoplay=0&amp;loop=0" id="header2-f" data-rv-view="53">

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(35, 35, 35);"></div>

    <div class="container align-center">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                    Minds English</h1>
                
                <p class="mbr-text pb-3 mbr-fonts-style display-5">언제 어디서나 인공지능으로 쉽게 영어를 공부하세요.
                </p>
                <div class="mbr-section-btn"><a class="btn btn-md btn-secondary display-4" href="/web#main">Live Demo</a></div>
            </div>
        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section style="overflow: hidden;">
  <div class="services">
    <?php $i=0; ?>
    <?php foreach($services as $service) : ?>
      <div style="overflow: hidden;">
        <div class="service-div <?=$i%2==0?'pull-left':'pull-right'?>">

          <a class="service-item image-item cursor-pointer" data-route="<?=$service['route']?>" data-svc-group-name="<?=$service['svc_group_name']?>" data-svc-group-sub-name="<?=$service['svc_group_sub_name']?>" style="text-shadow:none; background:linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(<?=$service['image_url']?>);background-size:cover;background-repeat:no-repeat;">
            <div class="service-caption">
              <!-- <div><?=nl2br($service['name'])?></div> -->
            </div>
          </a>
        
        </div>

        <div class="service-div <?=$i%2==0?'pull-right':'pull-left'?>">

          <a class="service-item cursor-pointer" data-route="<?=$service['route']?>" data-svc-group-name="<?=$service['svc_group_name']?>" data-svc-group-sub-name="<?=$service['svc_group_sub_name']?>">
            <div class="service-caption">
              <div><?=nl2br($service['name'])?></div>
            </div>
          </a>
        
        </div>
      </div>
    <?php $i++; endforeach; ?>
  </div>
</section>

<section class="cid-qzfMcgxX7c" id="footer1-g" data-rv-view="57">

  
    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="http://mindslab.ai/?page_id=5722&lang=ko" target="_blank">
                        <img src="/assets/home/images/english-edu-101x101.png" alt="Minds English" title="" media-simple="true">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Address
                </h5>
                <p class="mbr-text">
                     Dasan Tower 6F, 49<br/>Daewangpangyo-Ro,<br/>644beon-gil,<br/>BundangGu, Seongnam City,<br/>GyeonggiDo, Korea
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email: master@mindslab.ai
                    <br>Phone: +82-31-625-4340
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Links
                </h5>
                <p class="mbr-text">
                    <a class="text-primary" href="http://mindslab.ai/">Minds Lab</a>
                    <br><a class="text-primary" href="http://maum.ai/">maum.ai</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2017 Minds Lab - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://www.facebook.com/mindsinsight/?fref=ts" target="_blank">
                                <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$('.service-item').click(function() {
    // 모바일때문에
    $('.service-div .service-item').css('opacity', 1.0);

    var route = $(this).data('route');
    var svc_group_name = $(this).data('svc-group-name');
    var svc_group_sub_name = $(this).data('svc-group-sub-name');
    if(JSInterface) {
      JSInterface.openLearn(svc_group_name, svc_group_sub_name);
    } else {
      document.location.href = '/web#'+route;
    }
  });
  $(document).ready(function () {
    if(JSInterface) {
      JSInterface.hideIntro();
    }

    $('.service-item').height($('.service-div').width()*0.6);
  });
</script>


  
  <script src="/assets/home/popper/popper.min.js"></script>
  <script src="/assets/home/tether/tether.min.js"></script>
  <script src="/assets/home/bootstrap/js/bootstrap.min.js"></script>
  <script src="/assets/home/smooth-scroll/smooth-scroll.js"></script>
  <script src="/assets/home/jquery-mb-ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="/assets/home/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="/assets/home/theme/js/script.js"></script>
  
  
</body>
</html>