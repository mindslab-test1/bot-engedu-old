<style>
.talks {
	position: relative;
	background-color: #fff;
	overflow: auto;
	height: 100%;
	padding: 10px;
}
/* CSS talk bubble */
.talk-bubble {
	margin: 10px;
  display: block;
  position: relative;
	width: 60%;
	height: auto;
	background-color: #eee;
	color: #333;
	clear:both;
}
.border{
  border: 8px solid #666;
}
.round{
  border-radius: 30px;
	-webkit-border-radius: 30px;
	-moz-border-radius: 30px;
}

/* Right triangle placed top left flush. */
.tri-right.border.left-top:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: -40px;
	right: auto;
  top: -8px;
	bottom: auto;
	border: 32px solid;
	border-color: #666 transparent transparent transparent;
}
.tri-right.left-top:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: -20px;
	right: auto;
  top: 0px;
	bottom: auto;
	border: 22px solid;
	border-color: #eee transparent transparent transparent;
}

/* Right triangle, left side slightly down */
.tri-right.border.left-in:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: -40px;
	right: auto;
  top: 30px;
	bottom: auto;
	border: 20px solid;
	border-color: #666 #666 transparent transparent;
}
.tri-right.left-in:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: -10px;
	right: auto;
  top: 10px;
	bottom: auto;
	border: 12px solid;
	border-color: #eee #eee transparent transparent;
}

/*Right triangle, placed bottom left side slightly in*/
.tri-right.border.btm-left:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
	left: -8px;
  right: auto;
  top: auto;
	bottom: -40px;
	border: 32px solid;
	border-color: transparent transparent transparent #666;
}
.tri-right.btm-left:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
	left: 0px;
  right: auto;
  top: auto;
	bottom: -20px;
	border: 22px solid;
	border-color: transparent transparent transparent #eee;
}

/*Right triangle, placed bottom left side slightly in*/
.tri-right.border.btm-left-in:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
	left: 30px;
  right: auto;
  top: auto;
	bottom: -40px;
	border: 20px solid;
	border-color: #666 transparent transparent #666;
}
.tri-right.btm-left-in:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
	left: 38px;
  right: auto;
  top: auto;
	bottom: -20px;
	border: 12px solid;
	border-color: #eee transparent transparent #eee;
}

/*Right triangle, placed bottom right side slightly in*/
.tri-right.border.btm-right-in:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: 30px;
	bottom: -40px;
	border: 20px solid;
	border-color: #666 #666 transparent transparent;
}
.tri-right.btm-right-in:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: 38px;
	bottom: -20px;
	border: 12px solid;
	border-color: #eee #eee transparent transparent;
}
/*
	left: -8px;
  right: auto;
  top: auto;
	bottom: -40px;
	border: 32px solid;
	border-color: transparent transparent transparent #666;
	left: 0px;
  right: auto;
  top: auto;
	bottom: -20px;
	border: 22px solid;
	border-color: transparent transparent transparent #eee;

/*Right triangle, placed bottom right side slightly in*/
.tri-right.border.btm-right:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: -8px;
	bottom: -40px;
	border: 20px solid;
	border-color: #666 #666 transparent transparent;
}
.tri-right.btm-right:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: 0px;
	bottom: -20px;
	border: 12px solid;
	border-color: #eee #eee transparent transparent;
}

/* Right triangle, right side slightly down*/
.tri-right.border.right-in:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: -40px;
  top: 30px;
	bottom: auto;
	border: 20px solid;
	border-color: #666 transparent transparent #666;
}
.tri-right.right-in:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: -10px;
  top: 10px;
	bottom: auto;
	border: 12px solid;
	border-color: #eee transparent transparent #eee;
}

/* Right triangle placed top right flush. */
.tri-right.border.right-top:before {
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: -40px;
  top: -8px;
	bottom: auto;
	border: 32px solid;
	border-color: #666 transparent transparent transparent;
}
.tri-right.right-top:after{
	content: ' ';
	position: absolute;
	width: 0;
	height: 0;
  left: auto;
	right: -20px;
  top: 0px;
	bottom: auto;
	border: 20px solid;
	border-color: #eee transparent transparent transparent;
}
.align-right {
	float: right;
	background-color: #0076ff;
	color: #fff;
}
.align-right:after {
	border-color: #0076ff transparent transparent #0076ff !important;
}
/* talk bubble contents */
.talktext{
  padding: 1em;
	text-align: left;
  line-height: 1.5em;
}
.talktext p{
  /* remove webkit p margins */
  -webkit-margin-before: 0em;
  -webkit-margin-after: 0em;
}
</style>

<div class="talks">
<!-- 	<div class="talk-bubble tri-right left-in align-left">
	  <div class="talktext">
	    <p>This talk-bubble uses .left-in class to show a triangle on the left slightly indented. Still a blocky square.</p>
	  </div>
	</div>

	<div class="talk-bubble tri-right right-in align-right">
	  <div class="talktext">
	    <p>This talk-bubble uses .left-in class to show a triangle on the left slightly indented. Still a blocky square.</p>
	  </div>
	</div> -->
</div>

<script>
var talkScrollToBottom = function() {
  	$('.talks').animate({scrollTop: $('.talks').get(0).scrollHeight}, 1000);
};

var addLine = function(str, direction) {
	var direction = direction || 'left';

	if(str=='') {
		str = '(메시지가 없습니다)';
	}
    $('.talks').append('<div class="talk-bubble tri-right '+direction+'-in align-'+direction+'">' + 
						  '<div class="talktext">' + 
						    '<p>'+str+'</p>' + 
						  '</div>' + 
						'</div>');
    talkScrollToBottom();
};

var initChatUI = function() {
	// $('.talks').height( $(window).height() - ( $('.common-navbar').height() + $('.pagination-box').height() + $('.pic-box').height() + $('.microphone-div').height() + 25 )  );
	talkScrollToBottom();
}
</script>