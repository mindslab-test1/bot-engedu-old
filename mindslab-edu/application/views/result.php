<style>

/* ajax 로딩시 깨지는 현상 없애기 위해 정의했던것을 다시 auto로 돌려놓음*/
.jconfirm.jconfirm-supervan .jconfirm-box {
    height: auto;
    width: auto;
}

.display-none {
    display: none !important;
}
.result-div {
	width: 100%;
	height: 100%;
	overflow: hidden;
	position: relative;
}
.result-box {
	max-width: 800px;
	margin: 0 auto 10px;
	height: 500px;
}
.slick-initialized .slick-slide {
	height: 500px;
}
@media (max-width: 480px) {
	.result-box {
		height: 230px;
	}
	.slick-initialized .slick-slide {
		height: 230px;
	}
}
.result-slick {
	margin: 0 auto;
}

/* 낮은 해상도에서 slick 안되는 문제 해결 위해 */
.slick-slider { 
	display: table; 
	table-layout: fixed; 
	width: 100%; 
}
@media (max-width: 480px) {
	.slick-slider { 
		width: 320px;
	}
}
@media (max-width: 320px) {
	.slick-slider { 
		width: 280px;
	}
}


.jconfirm.jconfirm-supervan .jconfirm-bg {
	background-color: rgba(24, 31, 41, 0.95);
}
.jconfirm.jconfirm-supervan .jconfirm-box .jconfirm-buttons button {
	background-color: #5d789c;
}
#point-chart .highcharts-axis-title {
	font-size: 18px;
}

#explain {
	padding: 15px;
	background-color: #fff;
	color: #333;
}
#explain form {
	width: 80%;
	margin: 0 auto;
}
#explain .control-label {
	background-color: #eee;
}
#explain .sentence {
	line-height: 20px;
    height: 20px;
    /*padding-top: 7px;*/
    margin-bottom: 10px;
}
#explain .error {
	color: #FC361D;
}
#explain .explain-result {
	padding: 10px 0;
    border-top: 1px solid #eee;
}
#explain label {
	padding: 0px 5px;
}
.my-slick-arrow {
	/* 세로로 가운데 정렬 */
    display: block;
    position: absolute;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    top: 50%;
    color: #888;
    z-index: 999;
}
.my-slick-prev {
	left: 5px;
}
.my-slick-next {
	right: 5px;
}

@media (max-width: 680px) {
	.jconfirm .jconfirm-box {
		padding: 15px 5px 0;
	}
}

@media (max-width: 1000px) {
	/* 마우스 오버 및 터치가 안되도록 막는 div */
	.result-cover {
		position: absolute;
		width: 100%;
		height: 100%;
		z-index: 999;
	}
}
</style>

<div class="result-div">
	<!-- <div class="result-cover"></div> -->
	<div class="my-slick-arrow my-slick-prev"><i class="fa fa-lg fa-chevron-circle-left" aria-hidden="true"></i></div>
	<div class="my-slick-arrow my-slick-next"><i class="fa fa-lg fa-chevron-circle-right" aria-hidden="true"></i></div>

	<div class="result-slick">

		<?php if(in_array('point', $chart_types)) : ?>
			<!-- 점수 차트 -->
			<div class="<?=in_array('point', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="point-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('word', $chart_types)) : ?>
			<div class="<?=in_array('word', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="word-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('intonation', $chart_types)) : ?>
			<!-- 그래프 차트 -->
			<div class="<?=in_array('intonation', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="intonation-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('stress', $chart_types)) : ?>
			<?/* 권장 구간도 표시하는 차트 */?>
			<div class="<?=in_array('stress', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="stress-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('score', $chart_types)) : ?>
			<!-- 스코어 차트 -->
			<div class="<?=in_array('score', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="score-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('speed', $chart_types)) : ?>
			<!-- 스피트 게이지 -->
			<div class="<?=in_array('speed', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="speed-chart"></div>
			</div>
		<?php endif; ?>

		<?php if(in_array('explain', $chart_types)) : ?>
			<!-- 설명 -->
			<div class="<?=in_array('explain', $chart_types)?'':'display-none'?>">
				<div class="result-cover"></div>
				<div class="result-box" id="explain">
					<form class="form-horizontal">
					  <div class="form-group">
					    <label class="col-sm-2 col-xs-2 control-label">Native</label>
					    <div class="col-sm-10 col-xs-10 sentence text-left">
					      My father is good at soccer.
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 col-xs-2 control-label">You</label>
					    <div class="col-sm-10 col-xs-10 sentence text-left">
					      <!-- My father is good <span class="error">meet</span> Jane tomorrow. -->
					      My father is good soccer.
					    </div>
					  </div>
					  <div class="form-group explain-result">
					    <div class="col-sm-offset-2 col-sm-10 col-xs-offset-2 col-xs-10 text-left">
					      	<!-- <i class="fa fa-check" aria-hidden="true"></i> 동명사 meeting 오류 -->
					      	<i class="fa fa-check" aria-hidden="true"></i> 전치사 at 누락
					    </div>
					  </div>
					</form>
				</div>
			</div>
		<?php endif; ?>

	</div>

</div>

<script>
	var resultSlick = function() {
		$(".result-slick").slick({
		  infinite: false,
		  arrows: false,
		  slidesToShow: 1,
		  responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		      	infinite: false,
		        arrows: false,
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		      	infinite: false,
		        arrows: false,
		        slidesToShow: 1
		      }
		    }
		  ]
		});

		$('.my-slick-prev').click(function() {
			$(".result-slick").slick('slickPrev');
		});

		$('.my-slick-next').click(function() {
			$(".result-slick").slick('slickNext');
		});
	};

	var initCharts = function() {
		<?php if(in_array('point', $chart_types)) : ?>
			var gaugeOptions = {

			    chart: {
			        type: 'solidgauge'
			    },

			    title: {
			        text: '종합 점수'
			    },

			    pane: {
			        center: ['50%', '85%'],
			        size: '100%',
			        startAngle: -90,
			        endAngle: 90,
			        background: {
			            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			            innerRadius: '60%',
			            outerRadius: '100%',
			            shape: 'arc'
			        }
			    },

			    tooltip: {
			        enabled: false
			    },

			    // the value axis
			    yAxis: {
			        stops: [
			            [0.1, '#DF5353'], // red
			            [0.5, '#DDDF0D'], // yellow
			            [0.9, '#55BF3B'] // green
			        ],
			        lineWidth: 0,
			        minorTickInterval: null,
			        tickAmount: 2,
			        title: {
			            y: -70
			        },
			        labels: {
			            y: 16
			        }
			    },

			    plotOptions: {
			        solidgauge: {
			            dataLabels: {
			                y: 5,
			                borderWidth: 0,
			                useHTML: true
			            }
			        }
			    }
			};

			var chartSpeed = Highcharts.chart('point-chart', Highcharts.merge(gaugeOptions, {
			    yAxis: {
			        min: 0,
			        max: 100,
			        title: {
			            text: 'Good'
			        }
			    },

			    credits: {
			        enabled: false
			    },

			    series: [{
			        name: 'Speed',
			        data: [<?=rand(66, 98)?>],
			        dataLabels: {
			            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
			                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
			                   '<span style="font-size:12px;color:silver">점</span></div>'
			        },
			        tooltip: {
			            valueSuffix: ' 점'
			        }
			    }]

			}));

		<?php endif; ?>

		<?php if(in_array('word', $chart_types)) : ?>
			Highcharts.chart('word-chart', {

			    title: {
			        text: '단어별 점수'
			    },

			    subtitle: {
			        text: 'Good'
			    },

			    xAxis: {
			        categories: ['Flower', 'Clairfy', 'Maintain', 'Star', 'Use'],
			        gridLineWidth: 0
			    },
			    yAxis: {
			        title: {
			            text: '',
			        },
			        labels: {
					  enabled: false
					},
					gridLineWidth: 0
			    },
			    credits: {
			      	enabled: false
			  	},
			  	plotOptions: {
			        series: {
			            dataLabels: {
			                enabled: true
			            }
			        }
			    },

			    series: [{
			        type: 'column',
			        colorByPoint: true,
			        data: [{y:29.9, color:'#DF5353'}, {y:71.5, color:'#DDDF0D'}, {y:96.4, color:'#55BF3B'}, {y:56.2, color:'#DDDF0D'}, {y:28.0, color:'#DF5353'}],
			        showInLegend: false
			    }]

			});
		<?php endif; ?>

		<?php if(in_array('intonation', $chart_types)) : ?>
			Highcharts.chart('intonation-chart', {
				chart: {
			        type: 'spline'
			    },
			    title: {
			        text: 'INTONATION'
			    },
			    subtitle: {
			        text: 'Good - 88점'
			    },
			    xAxis : {
			    	labels: {
					  enabled: false
					},
					gridLineWidth: 0,
					// lineColor: 'transparent'
			    },
			    yAxis: {
			        title: {
			            text: '',
			        },
			        labels: {
					  enabled: false
					},
					gridLineWidth: 0
			    },
			    legend: {
			        layout: 'vertical',
			        align: 'right',
			        verticalAlign: 'middle'
			    },
			    plotOptions: {
			        series: {
			            label: {
			                connectorAllowed: false
			            },
			            pointStart: 2010,
			            marker: {
			                enabled: false
			            }
			        }
			    },
			    series: [{
			        name: 'Native',
			        data: [38121, 119931, 137133, 29742, 140434, 52503, 154175],
			        zones: [{
			            value: 8
			        }, {
			            dashStyle: 'dot'
			        }],
			        color: '#ff0000'
			    }, {
			        name: 'You',
			        data: [40434, 52503, 154175, 38121, 29742, 119931, 137133],
			        lineWidth: 3
			    }],
			    responsive: {
			        rules: [{
			            condition: {
			                maxWidth: 500
			            },
			            chartOptions: {
			                legend: {
			                    layout: 'horizontal',
			                    align: 'center',
			                    verticalAlign: 'bottom'
			                }
			            }
			        }]
			    },
			    credits: {
			      	enabled: false
			  	}

			});
		<?php endif; ?>

		<?php if(in_array('stress', $chart_types)) : ?>
			Highcharts.chart('stress-chart', {

			    chart: {
			        type: 'bubble',
			        plotBorderWidth: 1,
			        zoomType: 'xy'
			    },
			    credits: {
						      	enabled: false
						  	},

			    title: {
			        text: 'STRESS'
			    },

			    xAxis: {
			        gridLineWidth: 1,
			        categories: ['How', 'Can', 'I', 'do', 'for', 'you'],
			        gridLineWidth: 0
			    },

			    yAxis: {
			        startOnTick: false,
			        endOnTick: false,
			        title: {
						            text: ''
			                  },
			        labels: {
								  enabled: false
								},
					gridLineWidth: 0
			    },

			    series: [{
			    	name: "Native",
			        data: [
			        	[0, 1, -1],
			        	[0, 3, 50],
			            [1, 3, 50],
			            [2, 3, 10],
			            [3, 3, 90],
			            [4, 3, 50],
			            [5, 3, 10],
			            [5, 4, -1]
			        ],
			        displayNegative: false,
			        marker: {
			            fillColor: {
			                radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
			                stops: [
			                    [0, 'rgba(255,0,0,0.5)'],
			                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.5).get('rgba')]
			                ]
			            }
			        }
			    }, {
			    	name: "You",
			        data: [
			        	[0, 1, -1],
			        	[0, 2, 50],
			            [1, 2, 10],
			            [2, 2, 90],
			            [3, 2, 50],
			            [4, 2, 50],
			            [5, 2, 90],
			            [5, 4, -1]
			        ],
			        displayNegative: false,
			        marker: {
			            fillColor: {
			                radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
			                stops: [
			                    [0, 'rgba(255,255,255,0.5)'],
			                    [1, Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.5).get('rgba')]
			                ]
			            }
			        }
			    }]

			});
		<?php endif; ?>

		<?php if(in_array('score', $chart_types)) : ?>
			Highcharts.chart('score-chart', {
			    title: {
			        text: 'SCORE'
			    },

			    subtitle: {
			        text: 'Good'
			    },

			    xAxis: {
			        categories: ['How', 'Can', 'I', 'do', 'For', 'you'],
			        gridLineWidth: 0
			    },
			    yAxis: {
			        title: {
			            text: '',
			        },
			        labels: {
					  enabled: false
					},
					gridLineWidth: 0
			    },
			    credits: {
			      	enabled: false
			  	},
			  	plotOptions: {
			        series: {
			            dataLabels: {
			                enabled: true
			            }
			        }
			    },

			    series: [{
			        type: 'column',
			        colorByPoint: true,
			        data: [{y:29.9, color:'#DF5353'}, {y:71.5, color:'#DDDF0D'}, {y:96.4, color:'#55BF3B'}, {y:56.2, color:'#DDDF0D'}, {y:28.0, color:'#DF5353'}, {y:71.5, color:'#DDDF0D'}],
			        showInLegend: false
			    }]

			});
		<?php endif; ?>

		<?php if(in_array('speed', $chart_types)) : ?>
			Highcharts.chart('speed-chart', {

			    chart: {
			        type: 'gauge',
			        plotBackgroundColor: null,
			        plotBackgroundImage: null,
			        plotBorderWidth: 0,
			        plotShadow: false
			    },
			    credits: {
			      	enabled: false
			  	},

			    title: {
			        text: 'SPEED'
			    },

			    pane: {
			        startAngle: -150,
			        endAngle: 150,
			        background: [{
			            backgroundColor: {
			                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
			                stops: [
			                    [0, '#FFF'],
			                    [1, '#333']
			                ]
			            },
			            borderWidth: 0,
			            outerRadius: '109%'
			        }, {
			            backgroundColor: {
			                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
			                stops: [
			                    [0, '#333'],
			                    [1, '#FFF']
			                ]
			            },
			            borderWidth: 1,
			            outerRadius: '107%'
			        }, {
			            // default background
			        }, {
			            backgroundColor: '#DDD',
			            borderWidth: 0,
			            outerRadius: '105%',
			            innerRadius: '103%'
			        }]
			    },

			    // the value axis
			    yAxis: {
			        min: 0,
			        max: 200,

			        minorTickInterval: 'auto',
			        minorTickWidth: 1,
			        minorTickLength: 10,
			        minorTickPosition: 'inside',
			        minorTickColor: '#666',

			        tickPixelInterval: 30,
			        tickWidth: 2,
			        tickPosition: 'inside',
			        tickLength: 10,
			        tickColor: '#666',
			        labels: {
			            step: 2,
			            rotation: 'auto'
			        },
			        title: {
			            text: 'pt'
			        },
			        plotBands: [{
			            from: 0,
			            to: 120,
			            color: '#DDDF0D' // yellow
			        }, {
			            from: 120,
			            to: 160,
			            color: '#55BF3B' // green
			        }, {
			            from: 160,
			            to: 200,
			            color: '#DF5353' // red
			        }]
			        ,gridLineWidth: 0
			    },

			    series: [{
			        name: '속도',
			        data: [134],
			        tooltip: {
			            valueSuffix: ' pt'
			        }
			    }
			  	]

			},
			function (chart) {
			});
		<?php endif; ?>

		
	};

	resultSlick();

	// div 크기 잘 읽어오기 위해 timeout 줌.
	setTimeout(function() {
		initCharts();
	}, 300);
	
	
</script>