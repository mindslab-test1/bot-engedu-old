<style>
.nav-div .navbar {
	background-color: #2666a5 !important;
	color: #fff;
}
.content-main {
	overflow: auto;
}
.main-div {
	padding: 5px;
}
.service-div  {
	width: 33.3333%;
	float: left;
	padding: 5px;
	font-size: 20px;

}
.service-div .service-item {
	display: inline-block;
	width: 100%;
	height: 120px;
	border-radius: 3px;
}
.service-div .service-item:hover {
	/* hover가 모바일에서는 동작이 다르므로 scale은 주석처리 */
/*	transform: scale(1.022,1.022);
    -webkit-transform: scale(1.022,1.022);
    -moz-transform: scale(1.022,1.022);
    -ms-transform: scale(1.022,1.022);*/
    opacity: 0.5;
}
.service-caption {
	color: #fff;
	text-align: center;

	/* 세로로 가운데 정렬 */
	position: relative;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    top: 50%;
}
.empty-service-div {
	display: none;
}
@media (max-width: 680px) {
	.main-div {
		padding: 3px;
	}
	.service-div {
		width: 50%;
		padding: 3px;
		font-size: 16px;
	}
	.empty-service-div {
		display: block;
	}
}
</style>

<div class="main-div">

	<div class="services">
        <?php foreach($services as $service) : ?>
        	<div class="service-div">

	        	<a class="service-item cursor-pointer" data-route="<?=$service['route']?>" data-svc-group-name="<?=$service['svc_group_name']?>" data-svc-group-sub-name="<?=$service['svc_group_sub_name']?>" style="text-shadow:none; background:linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ), url(<?=$service['image_url']?>);background-size:cover;background-repeat:no-repeat;">
						<div class="service-caption">
							<div><?=nl2br($service['name'])?></div>
						</div>
				</a>
				
			</div>
        <?php endforeach; ?>

        <div class="service-div empty-service-div">
        	<a class="service-item"></a>
		</div>
    </div>

</div>

<script>
<?/*=$this->load->view('js/main.min.js',$this->data,true);*/?>
	endLoading();
	resizeMain();

	$('.service-item').click(function() {
		// 모바일때문에
		$('.service-div .service-item').css('opacity', 1.0);

		var route = $(this).data('route');
		var svc_group_name = $(this).data('svc-group-name');
		var svc_group_sub_name = $(this).data('svc-group-sub-name');
		if(JSInterface) {
			JSInterface.openLearn(svc_group_name, svc_group_sub_name);
		} else {
			document.location.href = '/web#'+route;
		}
	});
	$(document).ready(function () {
		if(JSInterface) {
			JSInterface.hideIntro();
		}

		var itemWidth = $(".service-item").width() / $('.service-item').parent().width() * 100;
		$('.service-item').height(itemWidth*0.6);
	});
</script>