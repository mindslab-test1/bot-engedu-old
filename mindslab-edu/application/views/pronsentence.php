<link rel="stylesheet" href="<?=CDN_HTTP_HOST?>/assets/jquery-progressTimer/css/jquery.progresstimer.min.css">
<script src="<?=CDN_HTTP_HOST?>/assets/jquery-progressTimer/js/jquery.progresstimer.min.js" type="text/javascript"></script>

<style>
.pronsentence-div {
	padding: 0px 15px;
}
.question-box {
	height: 48%;
	font-size: 16px;
}
.answer-box {
	height: 48%;
	background-color: #fff;
	font-size: 16px;
}
</style>

<div class="pronsentence-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="content-box-div">
		<div class="question-box" style="display: none">
			<div class="question">
				<!-- How can I do for you? -->
			</div>
		</div>

		<div class="answer-box" style="display: none">
			<div class="answer"></div>
		</div>
	</div>

</div>

<script>
	endLoading();
	resizeMain();

	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			$('.question').html(message);
			speechText(message,'eng');
		}
	};

	var operateResult = function(result) {
		showResult('평가 결과', result, ['point', 'intonation', 'stress', 'score', 'speed'], function(act) {
			if(act=='re') {
				startUnit(unitNum, function() {
					sendMessage('sentence_pron'+unitNum);
				});
			} else if(act=='next') {
				unitNum++;
				startUnit(unitNum, function() {
					sendMessage('sentence_pron'+unitNum);
				});
			}
		});
	};

	guideAlert('들려주는 5개의 문장을 듣고<br/> 따라 해보세요.', function() {

		$('.question-box').fadeIn();
		// $('.answer-box').fadeIn();

		setContentBoxDiv();

		/* 스트리밍 말하기
		var i = 1;
		var t = setInterval(function() {
			var word = 'hello';
			if(i%2==0) {
				word = 'world';
			}
			$('.answer-box .answer').append(word + ' ');

			if(i > 10) {
				clearInterval(t);
			}

			i++;

		}, 500);
		*/

		startUnit(unitNum, function() {
			// sendMessage('word_pronunciation'+unitNum);
			sendMessage('sentence_pron'+unitNum);
		});

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['How can I do for you?'];
			var i = 0;
			var timer =setInterval(function() {
				if(i == msgs.length) {
					clearInterval(timer);
					// receiveResultFromServer('test_result');
					setTimeout(function(){
						receiveMessageFromServer('RESULT: success');
					}, 2000);
					return;
				}
				receiveMessageFromServer(msgs[i]);
				i++;
				
			}, 1000);
		<?php endif; ?>


	});
</script>



