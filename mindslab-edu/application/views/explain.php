<style>
.title-question {
	background-color: #fff;
	text-align: center;
	font-size: 22px;
	font-weight: 900;
	margin-bottom: 5px;
	padding: 5px;
	overflow: hidden;

}
.talk-box {
	border-radius: 5px;
	height: 100%;
}

</style>


<div class="explain-div">
	<div class="pagination-box">
		<?=$this->load->view('_pagination',$this->data, true)?>
	</div>

	<div class="title-question"></div>
	<div class="content-box-div">
		
		<div class="talk-box"  style="display:none">
			<?=$this->load->view('_talk_bubble',$this->data, true)?>
		</div>
	</div>

</div>

<?php $this->load->view('_microphone',$this->data); ?>

<script>
	endLoading();
	resizeMain();

	var titleQuestionObj = <?=$title_question_json?>;
	var answersObj = <?=$answers?>;

	var unitNum = <?=$unit_num?$unit_num:1?>;

	var loadUnitContent = function(unitNum) {
		console.log('load unit content : '+unitNum);
		$('.title-question').hide();
		$('.title-question').html(titleQuestionObj[unitNum]);
		$('.title-question').fadeIn();
		$('.talks').html('');
	};

	var operateMessage = function(message) {
		if(message == "") {
			_alert('마이크를 가까이 대고 발음해주세요.', 'red');
		} else {
			var choiceHtml = '';
			var choiceText = '';
			var answerKey = message.replace(titleQuestionObj[unitNum]+' ','').trim();
			// console.log(answerKey);
			if(typeof answersObj[answerKey] !== 'undefined') {
				choiceText = answersObj[answerKey];
				choiceHtml = '<pre>' + answersObj[answerKey] + '</pre>';
				addLine(answerKey + choiceHtml, 'left');
				speechText(titleQuestionObj[unitNum] + '. ' + answerKey + ' ' + choiceText.replace(/\n/gi, '. '),'eng');
			} else {
				addLine(message);
				speechText(message,'eng');
			}
			
		}
	};

	var operateResult = function(result) {
	};

	var operateClientMessage = function(message) {
		addLine(message, 'right');
	}

	guideAlert('설명에 해당하는 단어를 보기에서<br/>골라보세요.<br/>보기 안에서 답을 찾으세요.', function() {
		$('.talk-box').fadeIn();

		setContentBoxDiv();

		initChatUI();

		startUnit(unitNum, function() {
			sendMessage('<?=$svcgroup?>'+unitNum);
		});

		<?php if(!is_from_app()) : ?>
			// TEST
			var msgs = ['Who am I? I am a person who teaches students. ', 'Teacher'];
			var i = 0;
			var timer =setInterval(function() {
				if(i==msgs.length) {
					clearInterval(timer);
					return;
				}

				if(i%2==0) {
					receiveMessageFromServer(msgs[i]);
				} else {
					runClientSpeech(msgs[i]);
				}

				i++;

			}, 1000);
		<?php endif; ?>
	});
</script>



