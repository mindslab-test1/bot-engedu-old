<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Result extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$result_data = $this->input->get_post('result_data');
		$chart_types = $this->input->get_post('chart_types')?$this->input->get_post('chart_types'):array();

		$this->data['result_data'] = $result_data;
		$this->data['chart_types'] = $chart_types;

		$result['html'] = $this->load->view('result', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















