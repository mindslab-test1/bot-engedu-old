<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Explain extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$title_question[1] = "Who am I?";
		$title_question[2] = "What is it?";
		$title_question[3] = "Where is it?";
		$title_question[4] = "When is it?";
		$title_question[5] = "What can I do?";
		$this->data['title_question_json'] = json_encode($title_question);

		$answers["I am a person who teaches students."] = 
"1. Student
2. Teacher
3. Farmer";
		$answers["I am a small animal with long ears."] = 
"1. Lion
2. Dog
3. Rabbit";
		$answers["I have a long body and no legs."] = 
		"1. Zebra
2. Bear
3. Snake";
		$answers["I am a person who helps sick people."] = 
"1. Doctor
2. Chef
3. Scientist";
		$answers["I am a person who catches thieves."] = 
"1. Firefighter
2. Police officer
3. Pianist";
		$answers["We use this to clean the room."] = 
"1. Purse
2. Vacuum
3. Shirt";
		$answers["We must drink this every day."] = 
"1. Water
2. Paint
3. Oil";
		$answers["We wear this in our finger."] = 
"1. Necklace
2. Ring
3. Bracelet";
		$answers["We use them to cut paper."] = 
"1. Wallet
2. Stapler
3. Scissors";
		$answers["We use this when it is raining."] = 
"1. Camera
2. Jacket
3. Umbrella";
		$answers["It is a place where my family and I live."] = 
"1. Hospital
2. Office
3. House";
		$answers["It is a place where we save money."] = 
"1. Court
2. Bank
3. Beauty salon";
		$answers["It is a place where we grow flowers and vegetables."] = 
"1. Garden
2. Drug store
3. Bus station";
		$answers["It is a place where we can read and borrow books."] = 
"1. Subway station 
2. Post office
3. Library";
		$answers["It is a place where students learn and teachers teach."] = 
"1. Concert hall
2. Police station
3. School";
		$answers["It is a special day only for children."] = 
"1. Parents' Day
2. Labor Day
3. Children's Day";
		$answers["It is a special day when we give chocolate each other."] = 
"1. Valentine's Day
2. Thanksgiving Day
3. New Year's Day";
		$answers["It is a holiday when we eat songpyeon."] = 
"1. Independence Day
2. National Foundation Day
3. Thanksgiving Day";
		$answers["It is the first day of the year."] = 
"1. Christmas
2. New Year's Day
3. National Liberation Day";
		$answers["It is a holiday when we celebrate the birth of Jesus Christ."] = 
"1. Constitution Day
2. Christmas
3. New Year's Day";
		$answers["Jack falls in the water."] = 
"1. Leave the area
2. Call a friend
3. Ask for help";
		$answers["My little sister is very hungry."] = 
"1. Ignore her
2. Cook for her
3. Go to school";
		$answers["There is an elderly person in the bus."] = 
"1. Play with toys
2. fall asleep
3. offer my seat";
		$answers["I cut my finger on a piece of glass."] = 
"1. Go to hospital
2. Climb a mountain
3. Play piano";
		$answers["Sally broke a mirror."] = 
"1. Clean it together
2. Sing a song
3. Buy food";

		$this->data['answers'] = json_encode($answers);

		$this->data['title'] = "설명 듣고 답하기";
		$this->data['svcgroup'] = "listen_and_choose";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('explain', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















