<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Web extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();

		// no cache for app
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header("HTTP/1.1 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}
	
	public function index($options = array())
	{	
		// $this->auth->restrict();

		$this->data['from_sign'] = $this->input->get_post['from_sign'];

		$this->data['header'] = $this->_header($options);
		$this->data['footer'] = $this->_footer($options);

		$this->load->view('web', $this->data);
	}

	public function _header($options = array())
	{
		$this->data = array_merge($this->data, $options);
		$this->data['from_share'] = $this->data['from_share']!='' ? $this->data['from_share'] : $this->input->get_post('from_share');

		return $this->load->view('_header', $this->data, true);
	}

	public function _footer($options = array())
	{
		$this->data = array_merge($this->data, $options);
		return $this->load->view('_footer', $this->data, true);
	}
}
