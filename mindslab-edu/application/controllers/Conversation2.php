<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Conversation2 extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$this->data['title'] = "기초 회화 B";
		$this->data['svcgroup'] = "role_play";

		$this->data['unit_num'] = 6;
		$page_nums[0] = '6';
		$page_nums[1] = '7';
		$page_nums[2] = '8';
		$page_nums[3] = '9';
		$page_nums[4] = '10';
		$this->data['page_nums'] = $page_nums;

		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('conversation', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















