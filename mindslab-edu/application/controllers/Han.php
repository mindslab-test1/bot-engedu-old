<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Han extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$han_array['1.'] = "그는 파란색 셔츠를 입고 있어요.";
		$han_array['2.'] = "우리는 어제 축구를 했어요.";
		$han_array['3.'] = "나는 미국에 방문할거에요.";
		$han_array['4.'] = "너는 여기서 수영할 수 없어.";
		$han_array['5.'] = "너는 몇시에 일어나니?";
		$this->data['han_json'] = json_encode($han_array);

		$this->data['title'] = "한글 듣고 영어로 말하기";
		$this->data['svcgroup'] = "english_writing";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('han', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















