<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tts extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$message = $this->input->get_post('message');
		$lang = $this->input->get_post('lang');

		$this->load->library('TextToSpeech');

		$this->texttospeech->setMessage( $message );

		if($lang=='kor') {
			$this->texttospeech->setLanguage( "ko-KR" );
		} else {
			$this->texttospeech->setLanguage( "en-US" );
		}

		if($this->texttospeech->createMessageFile( "dummy")) {
			$this->texttospeech->getAudioFile();

			print $this->texttospeech->getEmbedAudio(true);
		} else {
			$error = "Create TTS File Error";
			elog($error);
		}

		
	}

}
