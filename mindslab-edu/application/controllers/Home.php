<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("abstractor.php");

class Home extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$services = $this->_get_services();

		$this->data['services'] = $services;
		$this->load->view('home', $this->data);
	}
}
