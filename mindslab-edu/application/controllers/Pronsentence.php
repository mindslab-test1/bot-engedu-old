<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Pronsentence extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$this->data['title'] = "발음평가(문장)";
		$this->data['svcgroup'] = "sentence_pron";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('pronsentence', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















