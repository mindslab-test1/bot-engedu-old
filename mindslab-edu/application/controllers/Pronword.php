<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Pronword extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$page_nums[0] = 'f';
		$page_nums[1] = 'r';
		$page_nums[2] = 'v';
		$page_nums[3] = 'th';
		$page_nums[4] = 'sh';
		$this->data['page_nums'] = $page_nums;

		$this->data['title'] = "발음평가(단어)";
		$this->data['svcgroup'] = "word_pronunciation";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('pronword', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















