<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Pic extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$this->data['title'] = "그림보고 질문에 답하기";
		$this->data['svcgroup'] = "picture";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('pic', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















