<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abstractor extends CI_Controller {
	
	public $data = array();
	public $current_user = array();
	public $send_observer;
	
	public function __construct()
	{
		parent::__construct();

		$from_menu = $this->input->get_post('from_menu');
		$unit_num = $this->input->get_post('unit_num');
		$this->data['from_menu'] = $from_menu;
		$this->data['unit_num'] = $unit_num?$unit_num:1;
	}

	public function _get_services()
	{
		$services = array();
		$i = -1;

		$n = $i++;
		$services[$n]['route'] = "pronword";
		$services[$n]['svc_group_name'] = "word_pronunciation";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "발음평가(단어)";
		$services[$n]['image_url'] = "/assets/images/pronword.jpg";

		$n = $i++;
		$services[$n]['route'] = "pronsentence";
		$services[$n]['svc_group_name'] = "sentence_pron";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "발음평가(문장)";
		$services[$n]['image_url'] = "/assets/images/pronsentence.jpg";
		
		$n = $i++;
		$services[$n]['route'] = "syntax";
		$services[$n]['svc_group_name'] = "grammar";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "문장 따라하기\n(문법체크)";
		$services[$n]['image_url'] = "/assets/images/syntax.jpg";

		/* 큰소리로 읽기 일단 숨기기 
		$n = $i++;
		$services[$n]['route'] = "loud";
		$services[$n]['svc_group_name'] = "read_aloud";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "큰 소리로 읽기";
		$services[$n]['image_url'] = "/assets/images/loud.jpg";
		*/

		$n = $i++;
		$services[$n]['route'] = "pic";
		$services[$n]['svc_group_name'] = "picture";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "그림 보고 답하기";
		$services[$n]['image_url'] = "/assets/images/pic.jpg";

		$n = $i++;
		$services[$n]['route'] = "explain";
		$services[$n]['svc_group_name'] = "listen_and_choose";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "설명 듣고 답하기";
		$services[$n]['image_url'] = "/assets/images/explain.jpg";

		$n = $i++;
		$services[$n]['route'] = "han";
		$services[$n]['svc_group_name'] = "english_writing";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "한글 듣고 영어로 말하기";
		$services[$n]['image_url'] = "/assets/images/han.jpg";

		$n = $i++;
		$services[$n]['route'] = "pattern";
		$services[$n]['svc_group_name'] = "pattern_drills";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "회화 패턴 연습";
		$services[$n]['image_url'] = "/assets/images/pattern.jpg";

		$n = $i++;
		$services[$n]['route'] = "conversation";
		$services[$n]['svc_group_name'] = "role_play";
		$services[$n]['svc_group_sub_name'] = "";
		$services[$n]['name'] = "기초 회화 A";
		$services[$n]['image_url'] = "/assets/images/conversation.jpg";

		$n = $i++;
		$services[$n]['route'] = "conversation2";
		$services[$n]['svc_group_name'] = "role_play";
		$services[$n]['svc_group_sub_name'] = "2";
		$services[$n]['name'] = "기초 회화 B";
		$services[$n]['image_url'] = "/assets/images/conversation.jpg";

		return $services;
	}


	public function _page_link($base_url = '', $total_rows = 1000, $page_row_count = 20)
	{
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $total_rows;
		$config['page_row_count'] = $page_row_count; 
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = '<span class="glyphicon glyphicon-step-backward"></span>';
		$config['last_link'] = '<span class="glyphicon glyphicon-step-forward"></span>';
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#'.$this->data['schema']['id'].'/index?page='.$this->input->get_post('page').'">';
		$config['cur_tag_close'] = '</a></li>';
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';
		$this->pagination->initialize($config); 
		return $this->pagination->create_links();
	}
}


