<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('abstractor.php');

class Main extends Abstractor {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$from_menu = $this->input->get_post('from_menu');
		$this->data['from_menu'] = $from_menu;

		$services = $this->_get_services();

		$this->data['services'] = $services;
		$this->data['title'] = "마인즈 잉글리시";
		$result['nav'] = $this->load->view('_navbar', $this->data, true);
		$result['html'] = $this->load->view('main', $this->data, true);
		send_json(200, 'Success', $result);
	}
}






















