<?php

if (!function_exists('getallheaders')) {
    function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
    }
}

// redirect_url : 로그인 후 이동할 URL
// go_url : alert 띄우고 바로 이동할 URL
function send_json($code = 200, $message = 'Success', $result = null, $redirect_url = '', $go_url = '') {
	$CI =& get_instance();

	$CI->output
		->set_content_type('application/json')
		->set_output(json_encode(array('code' => $code, 'message' => $message, 'result'=>$result, 'redirect_url'=>$redirect_url, 'go_url'=>$go_url), JSON_NUMERIC_CHECK));
	$CI->output->_display();
	exit;
}

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

function get_request_app_id() {
    return getallheaders()['X-Requested-With'];
}

function is_from_app() {
  $CI =& get_instance();
  if(strpos($_SERVER['HTTP_USER_AGENT'], $CI->config->item('app_user_agent')) !== false) {
    return true;
  } else {
    return false;
  }
}

function store_html($app_id) {
    if(!$app_id) {
        return '';
    }

    $url = "https://play.google.com/store/apps/details?id=".$app_id;
    if(get_http_response_code($url) != "200"){
        return '';
    }else{
        return file_get_contents($url);
    }
}

function current_url_with_querystring()
{
    $CI =& get_instance();

    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}


?>