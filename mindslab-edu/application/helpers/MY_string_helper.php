<?php

function now()
{
	return date('Y-m-d H:i:s');
}

function calculate_status($params)
{
	return '2000';
}

function elog($msg)
{
    if(is_array($msg)) {
        $msg = print_r($msg, true);
    }
    $bt =  debug_backtrace();
    log_message('error', $bt[0]['file'] . ' line  '. $bt[0]['line'] . ": \n" . $msg); 
}

function get_lc_id($user_name, $user_phone) {
    // 같은 id가 생성되지 않게하기위해 앞뒤로 md5를 해줌 (ex: 'aa|+bb'와 'aa|b+b'는 같아져 버릴 수 있기때문)
    return md5(trim($user_name).'us@r') . '|' . md5(trim($user_phone).'#h0ne');
}


function timeago($ptime, $word = ' 전') 
{
    $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '방금 전';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  =>  '년',
                    30 * 24 * 60 * 60       =>  '개월',
                    24 * 60 * 60            =>  '일',
                    60 * 60                 =>  '시간',
                    60                      =>  '분',
                    1                       =>  '초'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                // return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' 전';
                return $r . $str . $word;
            }
        }
}

function is_valid_email($email) {
    $is_valid=filter_var($email, FILTER_VALIDATE_EMAIL);
    return $is_valid;
}


?>