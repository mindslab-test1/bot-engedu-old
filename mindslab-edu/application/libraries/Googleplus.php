<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH .'third_party/google-api/src/Google/autoload.php';

class Googleplus {
	

	public function __construct() {
		
		$CI =& get_instance();
		$CI->config->load('google');
				
		require APPPATH .'third_party/google-api/src/Google/Client.php';
		require APPPATH .'third_party/google-api/src/Google/Service/Plus.php';
		
		$cache_path = $CI->config->item('cache_path');
		$GLOBALS['apiConfig']['ioFileCache_directory'] = ($cache_path == '') ? APPPATH .'cache/' : $cache_path;

		$this->client = new Google_Client();
		$this->client->setApplicationName($CI->config->item('application_name', 'google'));
		$this->client->setClientId($CI->config->item('client_id', 'google'));
		$this->client->setClientSecret($CI->config->item('client_secret', 'google'));
		$this->client->setRedirectUri($CI->config->item('redirect_uri', 'google'));
		$this->client->setDeveloperKey($CI->config->item('api_key', 'google'));
		
		$this->plus = new Google_Service_Plus($this->client);
	}
	
	public function __get($name) {
		
		if(isset($this->plus->$name)) {
			return $this->plus->$name;
		}
		return false;
		
	}
	
	public function __call($name, $arguments) {
		
		if(method_exists($this->plus, $name)) {
			return call_user_func(array($this->plus, $name), $arguments);
		}
		return false;
		
	}
	
}
?>