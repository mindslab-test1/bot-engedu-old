#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import logging.handlers
from logging.handlers import RotatingFileHandler
import ConfigParser
import os
#from util import Util

def Get_da_logger():
    config = ConfigParser.ConfigParser()
    config.read('logger.cfg')
    da_log = logging.getLogger('da_log')
    formatter = logging.Formatter('[%(levelname)s|%(asctime)s] %(message)s')
    dir = config.get('LOG', 'PATH')
    file = config.get('LOG', 'FILE')
    log_level = config.get('LOG', 'LOG_LEVEL')
    if log_level == 'DEBUG':
       log_level = logging.DEBUG
    elif log_level == 'INFO':
       log_level = logging.INFO
    else:
       log_level = logging.DEBUG

    da_log.setLevel(log_level)
    if not os.path.isdir(dir):
        os.mkdir(dir)
    fileHandler = RotatingFileHandler(dir+file, maxBytes=50 * 1024 * 1024, backupCount=10)
    streamHandler = logging.StreamHandler()
    fileHandler.setFormatter(formatter)
    streamHandler.setFormatter(formatter)
    da_log.addHandler(fileHandler)
    da_log.addHandler(streamHandler)
    return da_log

DA_LOGGER = Get_da_logger()

def info(msg, *args, **kwargs):
    DA_LOGGER.info(msg, *args, **kwargs)

def debug(msg, *args, **kwargs):
    DA_LOGGER.debug(msg, *args, **kwargs)

def error(msg, *args, **kwargs):
    DA_LOGGER.error(msg, *args, **kwargs)

def warning(msg, *args, **kwargs):
    DA_LOGGER.warning(msg, *args, **kwargs)

##############
# TLO_LOGGER #
##############

def Get_tlo_logger():
    config = ConfigParser.ConfigParser()
    config.read('logger.cfg')
    da_log = logging.getLogger('TLO')

    formatter = logging.Formatter('[%(levelname)s|%(filename)s|%(asctime)s] %(message)s')
    dir = config.get('TLO', 'PATH')
    file = config.get('TLO', 'FILE')
    log_level = config.get('LOG', 'LOG_LEVEL')
    if log_level == 'DEBUG':
        log_level = logging.DEBUG
    elif log_level == 'INFO':
        log_level = logging.INFO
    else:
        log_level = logging.DEBUG
    da_log.setLevel(log_level)

    if not os.path.isdir(dir):
        os.mkdir(dir)
    fileHandler = RotatingFileHandler(dir+file, maxBytes=50 * 1024 * 1024, backupCount=10)
    # fileHandler = logging.FileHandler(config.get('LOG', 'PATH'))
    streamHandler = logging.StreamHandler()
    fileHandler.setFormatter(formatter)
    streamHandler.setFormatter(formatter)
    da_log.addHandler(fileHandler)
    da_log.addHandler(streamHandler)
    return da_log

TLO_LOGGER = Get_tlo_logger()

def tlo_info(msg, *args, **kwargs):
    msg = msg+"\n"
    TLO_LOGGER.info(msg, *args, **kwargs)

def tlo_debug(seq_id='',log_time='',log_type='',sid='',transaction_id=''
              , message_id='',from_svc_name='',to_svc_name='',svc_type='',dev_type=''
              , device_token='', event_directive='', user_text='', repeat_code='', repeat_url=''
              , repeat_leader_text='', repeat_iword_position='', repeat_score='', repeat_iword_score='', repeat_feadback_code=''
              , start_quize_result='', start_quize_url='', ending_quize_result='', ending_start_quize_url=''
              ):
    #meta = tlo_init()
            meta =  'SEQ_ID=' + str(seq_id) \
                    + '|LOG_TIME=' + str(log_time)\
                    + '|LOG_TYPE=' + str(log_type)\
                    + '|SID=' + str(sid)\
                    + '|TR_ID=' + str(transaction_id)\
                    + '|MSG_ID=' + str(message_id)\
                    + '|FROM_SVC_NAME=' + str(from_svc_name)\
                    + '|TO_SVC_NAME=' + str(to_svc_name)\
                    + '|SVC_TYPE=' + str(svc_type)\
                    + '|DEV_TYPE=' + str(dev_type)\
                    + '|DEVICE_TOKEN=' + str(device_token)\
                    + '|EVENT_DIRECTIVE=' + str(event_directive)\
                    + '|USER_TEXT=' + str(user_text)\
                    + '|REPEAT_CODE=' + str(repeat_code)\
                    + '|REPEAT_URL=' + str(repeat_url)\
                    + '|REPEAT_LEADER_TEXT=' + str(repeat_leader_text)\
                    + '|REPEAT_IWORD_POSITION=' + str(repeat_iword_position)\
                    + '|REPEAT_SCORE=' + str(repeat_score)\
                    + '|REPEAT_IWORD_SCORE=' + str(repeat_iword_score)\
                    + '|REPEAT_FEADBACK_CODE=' + str(repeat_feadback_code)\
                    + '|START_QUIZE_RESULT=' + str(start_quize_result)\
                    + '|START_QUIZE_URL=' + str(start_quize_url)\
                    + '|ENDING_QUIZE_RESULT=' + str(ending_quize_result)\
                    + '|ENDING_START_QUIZE_URL=' + str(ending_start_quize_url)
                    #  + '|' + 'RESULT_CODE=' + result_code'])\
                    #  + '|' + 'REQ_TIME=' + req_time'])\
                    #  + '|' + 'RSP_TIME=' + rsp_time'])\
                    # + '|' + 'CLIENT_IP=' + client_ip'])\
                    # + '|' + 'DEV_INFO='  + dev_info'])\
                    # + '|' + 'OS_INFO=' + os_info'])\
                    # + '|' + 'NW_INFO=' + nw_info'])\
                    # + '|' + 'SVC_NAME=' + svc_name'])\
                    # + '|' + 'DEV_MODEL=' + dev_model'])\
                    # + ' + 'CARRIER_TYPE=' + carrier_type'])\
            TLO_LOGGER.info(meta)



