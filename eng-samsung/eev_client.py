#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import grpc
import os ,re
import logger
from common import Common
#from flask import json ,jsonify
#from maum.m2u.brain.eval.evaluation_pb2 import evaluation_pb2
#from maum.m2u.brain.eval.evaluation_pb2_grpc import evaluation_pb2_grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

# import evaluation_pb2
# import evaluation_pb2_grpc
import maum.brain.eev.evaluation_pb2 as evaluation_pb2
import maum.brain.eev.evaluation_pb2_grpc as evaluation_pb2_grpc


class Eev:
    _REX_NUM = re.compile('^(\d+)$')
    def __init__(self):
        pass

    @classmethod
    def Evaluation_emul(self, param):
        return 50 ,50,param['expected'],50,'fail'

    @classmethod
    def Evaluation(self, param):
        eevScore = 0
        eevKeywordScore = 0
        eevResult = 'fail'
        keyWordpos = param['keyword_pos']
        scorePos = param['score']
        evalRes = evaluation_pb2.Analysis()

        logger.debug('req.evauation param %s', param)
        # logger.debug('req.score %s', param['score'])
        # logger.debug('req.utter %s', param['utter'])
        # logger.debug('req.keyword_pos %s %d', type(keyWordPos) ,int(keyWordPos))
        # logger.debug('req.expected %s', param['expected'])

        try:
            channel = grpc.insecure_channel('localhost:9992')
            #channel = grpc.insecure_channel('10.122.64.106:9992')

            stub = evaluation_pb2_grpc.EvaluationServiceStub(channel)
            evalreq = evaluation_pb2.TextInfo()
            evalreq.type = 1
            evalreq.expected = param['expected']
            evalreq.utter = param['utter']
            evalreq.score = scorePos.replace(',', ' ')
            #eever server keyword posion 배열 -1
            logger.debug('after eev.req. evalreq.score %s', evalreq.score)

            evalreq.keyword_pos =  int(keyWordpos)-1
            evalRes = stub.TextAnalyze(evalreq)
            word_score = str(evalRes.word_score)
            word_score = word_score.replace('[', '').replace(']', '')

            logger.debug('eev.res.response %s', evalRes)
            logger.debug('eev.res.score %s', evalRes.score)
            logger.debug('eev.res.keyword_score %s', evalRes.keyword_score)
            logger.debug('eev.res.sentence %s', evalRes.sentence)
            logger.debug('eev.res.word_score %s', evalRes.word_score)
            logger.debug('eev.res.result_code %s', evalRes.result_code)

            if int(keyWordpos) != -1:
                eevResult = self.EevCalculator(evalreq.score, keyWordpos, evalRes.keyword_score)
            else:
                referenceScore = 100
                if evalRes.score >= referenceScore:
                    eevResult = 'pass'

            eevScore = self.EevScoreAdjust(evalRes.score)
        except:
            logger.error('평가 서버 연결 에러 %s ', sys.exc_info()[0])
        return eevScore,evalRes.keyword_score,evalRes.sentence,word_score,eevResult,evalRes.score

    @classmethod
    def EevScoreAdjust(self, evalResscore):
        resultScore  = 0
        if evalResscore< 0:
            resultScore = 0
        elif evalResscore > 100:
            resultScore = 100
        else:
            resultScore = evalResscore
        return resultScore

    @classmethod
    def EevCalculator(self, answerPoints, kewordPoint, eeviptScore):
        result = False
        aitp = kewordPoint
        answerPointList = answerPoints.split(' ')
        keywordPoint = answerPointList[int(aitp) - 1]
        logger.debug('answerPointList %s', answerPointList)
        logger.debug('keywordPoint %s', keywordPoint)
        logger.debug('eeviptScore %s', eeviptScore)
        logger.debug("eevkeywordPecent %.2f" % (eeviptScore / float(keywordPoint) * 100.0))
        eevkeywordPecent = "%.2f" % (eeviptScore / float(keywordPoint) * 100.0)
        if float(eevkeywordPecent) >= 80:
            result = True
        return result

    @classmethod
    def EevTest(self):
        eev = {}
        #"answerText": "I want some coke please",
        answerPoint = "10 20 10 50 10"
        answerImportantWordPosision = "3"
        eeviptScore = 8
        res = self.EevCalculator(answerPoint, answerImportantWordPosision, eeviptScore)
        print ('eevres',res )

    @classmethod
    def EvalGenerator(self):
        print ("Enter/Paste your content. Ctrl-C to exit.")
        userTalk =''
        while True:
            try:
                if not userTalk:
                    #talk = "i'm going to learn you" #오류
                    userTalk = "she is going to go shopping"
                    talk = userTalk
                   # talk = Common.Get4Word(userTalk)
                    if talk:
                        param = {'expected':'i am going to','utter':talk ,'score':'30 30 20 20' ,'keyword_pos':'-1'}
                        result = self.Evaluation(param)
                        userTalk = raw_input("")
                        logger.debug('response %s', result)
            except:
                logger.error('%s ', sys.exc_info()[0])
                break

if __name__ == '__main__':
     Eev.EvalGenerator()
     #eev.eevTest()


