#!/usr/bin/env python
# -*- coding: utf-8 -*-

def Head(transactionId='', customerId='', deviceToken=''):
	head = {
		"svrKey": "c1dadb9d1011e7bb3a024fe0504892",
		"transactionId": "aaadafadf3",
		"Content-Language": "ko-kr",
		"messageId": "test001",
		"deviceToken": "ef1328abc2e493d8ee97f90f27b46cc",
		"customerId": "ljw76@naver.com",
		"Content-Type": "application/json;charset=UTF-8",
	}
	return head

def RequestlmsPlayData(moveType='',  chapter=''):

	if chapter:
		chaptName = 'chapter'

	data = {
	"common" : {
		"serviceType" : "SVC_003",
		"deviceLanguage" : "ko_KR",
		"triggerType":"KDB",
		"logData" : {
			"clientIp" : "192.168.0.100",
			"devInfo" : "PHONE",
			"osInfo" : "android_7.0.0",
			"nwInfo" : "5G",
			"devModel" : "LE-E250",
			"svcName":"INWF",
			"svcType":"SVC_003",
			"devType" : "DEV_009",
			"carrierType":"L"
		}
	},
	"body" : {
		"skill" : "lms",
		"intent" : "lmsRepeatPlay",
		"slot" : {
			"lecture" : "samsung english",
			"moveType":moveType,
			"lvl1": {"name": chaptName, "value": chapter},
				}
	}
}
	return data


def RequestCurrentPosition(lectPosition):
     data = {
			"common": {
				"serviceType": "SVC_003",
				"deviceLanguage": "ko_KR",
				"triggerType": "KDB",
				"logData": {
					"clientIp": "192.168.0.100",
					"devInfo": "PHONE",
					"osInfo": "android_7.0.0",
					"nwInfo": "5G",
					"devModel": "LE-E250",
					"svcName": "INWF",
					"svcType": "SVC_003",
					"devType": "DEV_009",
					"carrierType": "L"
				}
			},
			"body": {
				"skill": "lms",
				"intent": "lmsRepeatPosition",
				"slot": {
					"sampleYn": "N",
					"eventType": "P",
					"currentPostion": lectPosition
				}
			}
		}

     return data
