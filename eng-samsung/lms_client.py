#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
reload(sys)
sys.setdefaultencoding('utf-8')

import json
import urllib
import urllib2
#from flask import json ,jsonify
import lms.lms_req_data as lms_req_data
import requests
import logger
from common import DAError
class LmsClient:
    #LMS Emulator
    baseurl = 'http://127.0.0.1:5000' #local
    # baseurl = 'http://172.17.22.142' #aws developer
    #baseurl = 'http://dncoif.ai.uplus.co.kr'
    header =  {
        "svrKey": "46c1dadb9d1011e7bb3a024fe0504892",
        "transactionId": "aaadafadf3",
        "Content-Language": "ko-kr",
        "messageId": "test001",
        "deviceToken": "02152a3f70d8441fbd5c4d8eb0ca49ce",
        "customerId": "4007832115",
        "Content-Type": "application/json;charset=UTF-8",
    }
    #header = lms_req_data.head()
    lsmProgramName = "100001-"
    def __init__(self,context):
        context = 'context'
    pass

    @classmethod
    def SendLmsData(self, lecture='', moveType=''):
        resultData = {'resultCode':-1}
        strchapter = ''
        chapterNum = ''
        url = self.baseurl+'/coif/lms'
        print  'sendLmsData' ,lecture ,url

        jsonData = lms_req_data.RequestlmsPlayData(moveType=moveType, chapter=lecture)
        print type(jsonData),jsonData

        try:
            response = requests.post(url, data=json.dumps(jsonData), headers=self.header)
            print "response " ,response
            print "response content " ,  type(response.content),response.content
            print "response json", type(response.json())  ,response.json()
            newLMSData = response.json()
            resultData = self.checkLmsData(newLMSData, lecture)

            if resultData['resultCode'] == 200000:
                resultData['repeatData'] = newLMSData['body']['lmsContents']['repeat'][0]
            print "resultData",resultData
        except DAError as e:
            raise "%s %s" % ('300001','LMs Server not connect')
            #print("LMS Server Error" ,response.content)
            # result['common'] = {}
            #result['common']['resultCode'] = ''
            # result['common']['message'] = response.content
        return resultData

    @classmethod
    def checkLmsData(self, newLMSData,lecture):
        result = {}

        if newLMSData['common']['resultCode'] != 'COIF20000000':
            result['utterByContents'] = newLMSData['body']['utterByContents']
            result['resultCode'] = 300003
            result['resultMsg'] = 'Lms empty Data'
        elif len(newLMSData['body']['lmsContents']['repeat'])==0:
            logger.debug("No found MenuId:%s", lecture)
            result['resultCode'] = 300004
            result['resultMsg'] = 'No found MenuID('+lecture+')'
        else:
            result['resultCode'] = 200000

        return result

    @classmethod
    def SendLmsRepeatCurrentPosition(self, lmsContents, turnType, turnNumber):
        url = self.baseurl + '/coif/lms'
        print  'sendLmsRepeatCurrentPosition' ,turnType ,url
        turnType = turnType.replace('_c','')
        print '1curposition' ,turnType
        print 'type' ,type(lmsContents['lectureId']) , type(lmsContents['chapterId']), type(turnType), type(turnNumber)

        #curposition = lmsContents['lectureId']+'-' + lmsContents['chapterId'] + "-" + turnType + "-" + str(turnNumber)
        curposition = ("{}-{}-{}-{} ".format(str(lmsContents['lectureId']), lmsContents['chapterId'], str(turnType), str(turnNumber)))
        logger.debug('position#turnType:%s',curposition)
        data = lms_req_data.RequestCurrentPosition(curposition)
        response = requests.post(url, data=json.dumps(data, ensure_ascii=False).encode('utf8'), headers=self.header)
        print(response)
        print "response " ,response.content
        print "response", type(response.content) ,response.json()
        return response.json()

if __name__ == '__main__':
    print "lmsClient"
    LmsClient.SendLmsData(lecture='1H1K1C1', moveType='')

    #lmsClient.sendLmsRepeatCurrentPosition('10001')

