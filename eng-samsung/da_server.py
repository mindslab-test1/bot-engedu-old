#! /usr/bin/env python
#-*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from concurrent import futures
import time
import argparse
import grpc
import syslog

import os
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
from common import Common
exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da.v3 import talk_pb2
from maum.brain.sds import sds_pb2
from maum.m2u.facade import front_pb2
from da_biz_process import  BizProcess
import logger
import traceback

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class Stat:
    sessionData = {}
    running = False
    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE
        pass

class Da_Server(talk_pb2_grpc.DialogAgentProviderServicer):
    sessionStat = {}
    # STATE
    state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'daServer'
    provider.description = 'daServer'
    provider.version = '0.3'
    provider.single_turn = False
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # PARAMETER
    sds_path = ''
    # SDS Stub
    sds_stub = None
    lang = 1
    def __init__(self):
     BizProcess()
     syslog.syslog('init')
     self.state = provider_pb2.DIAG_STATE_IDLE
     syslog.syslog(str(self.state))

    #
    # INIT or TERM METHODS
    #
  
    def IsReady(self, empty, context):
        logger.debug( 'V3 IsReady called')
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        logger.debug( 'V3 Init called')
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        logger.debug( 'result called')
        return result

    def Terminate(self, empty, context):
        logger.debug( 'V3 Terminate called')
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    def GetUserAttributes(self, empty, context):
        logger.debug( 'V3 GetUserAttributes called')
        result = userattr_pb2.UserAttributeList()
        attrs = []

        # UserAttribute의 name은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.

        lang = userattr_pb2.UserAttribute()
        lang.name = 'lang'
        lang.title = '기본 언어 설정'
        lang.type = userattr_pb2.DATA_TYPE_STRING
        lang.desc = '기본으로 사용할 언어를 지정해주세요.'
        attrs.append(lang)

        loc = userattr_pb2.UserAttribute()
        loc.name = 'location'
        loc.title = '기본 지역'
        loc.type = userattr_pb2.DATA_TYPE_STRING
        loc.desc = '기본으로 조회할 지역을 지정해주세요.'
        attrs.append(loc)

        device = userattr_pb2.UserAttribute()
        device.name = 'device'
        device.title = '기본 디바이스'
        device.type = userattr_pb2.DATA_TYPE_STRING
        device.desc = '기본으로 사용할 디바이스를 지정해주세요.'
        attrs.append(device)

        country = userattr_pb2.UserAttribute()
        country.name = 'time'
        country.title = '기준 국가 설정'
        country.type = userattr_pb2.DATA_TYPE_STRING
        country.desc = '기본으로 조회할 국가를 지정해주세요.'
        attrs.append(country)

        result.attrs.extend(attrs)
        return result

    def GetProviderParameter(self, empty, context):
        logger.debug( 'V3 GetProviderParameter called')
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        logger.debug( 'V3 GetRuntimeParameters called')
        result = ''
        params = []
        result = provider_pb2.RuntimeParameterList()
        logger.debug(result)

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'AI_Manager_Start'
        sds_path.required = True
        params.append(sds_path)
        result.params.extend(params)
        return result

    def parseLectureNum(self,utter):
        logger.debug( 'parseLectureNum %s', utter)

        param = {'lectureNum': '', 'userTalk': ''}
        lecNumber = Common.GetParserlectureNum(utter)
        if lecNumber:
            param["lectureNum"] = lecNumber
        else:
            param["userTalk"] = utter
        return param

    def Event(self, request, context):
        logger.debug("Talk oneLog:session_id %s",request.session_id)
        logger.debug('Event context %s', context)
        logger.debug('oneLog:request.session_id', request.session_id)
        result = talk_pb2.EventResponse()
        result.code = 200
        result.reason = 'succes'
        return result

    def Open(self, request, context):
        errorTrace = ''
        param = {}
        bizRes = {}
        lectureInfo = {}
        resMessage = 'success'
        selectMenu =''
        actionType =''
        logger.debug('Open oneLog:session_id %s userId %s device model:%s serial:%s',request.session_id,request.user.user_id,request.device.model,request.device.serial)

        logger.debug('Open request %s', request)
        logger.debug('Open context %s', context)
        daContent ={'selectMenu':'1H1K1C1' ,'actionType':'enter'}

        try:
            session_id = request.session_id
            if not session_id in self.sessionStat:
                self.sessionStat[session_id] = Stat()
            localSessionObj = self.sessionStat[session_id]

            metadata = context.invocation_metadata()
            logger.debug('metadata %s', metadata)

            if 'actionType' in request.meta:
                actionType = request.meta['actionType']
                logger.debug('actionType %s', actionType)

            if 'selectMenu' in request.meta:
                selectMenu = request.meta['selectMenu']
                logger.debug('selectMenu %s', selectMenu)

            param = {'isOpenRequest':True,'userTalk': '' ,'session_id':session_id ,'selectMenu':selectMenu,'actionType':actionType}
            (bizRes ,lectureInfo) = BizProcess().DaMain(param, localSessionObj.sessionData)

            self.sessionStat[session_id].sessionData = lectureInfo
        except Exception as e:
            logger.debug(e.__doc__)
            logger.debug(e.message)
            logger.debug(traceback.format_exc())
            bizRes = {'resultCode': '3000001', 'resultMsg':'Error'+ e.message}

        result = talk_pb2.OpenResponse()
        result.code = 200
        
        res_meta = struct.Struct()
        res_meta['response'] = bizRes
        result.meta.CopyFrom(res_meta)
        logger.debug("result %s", result)

        #session 정보 ,session data 10k
        res_context = struct.Struct()
        res_context['session_id'] = session_id
        res_context['session_data'] = '' #str(lectureInfo)
        result.context.CopyFrom(res_context)
       # logger.debug("context len %d", len(res_context))

        print '______open'
        return result
    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        bizRes = {}
        lectureInfo = {}
        localSessionObj = {}
        errorTrace =''
        daContent = {'select_menu':'','actionType':''}
        try:
            session_id = talk.session_id
            userTalk =  str(talk.query.utter)
            logger.debug('Talk oneLog:session_id:%s userId:%s device model:%s serial:%s userTalk:%s',talk.session_id,
                         talk.user.user_id,talk.device.model,talk.device.serial,userTalk)
            logger.debug('talk_scontext %s',talk.context)

            result = talk_pb2.TalkResponse()
            if session_id in self.sessionStat:
                localSessionObj = self.sessionStat[session_id]

                param = {'isOpenRequest':False,'userTalk':userTalk,'session_id': session_id, 'selectMenu': '', 'actionType': ''}
                (bizRes ,lectureInfo) = BizProcess().DaMain(param, localSessionObj.sessionData)
                self.sessionStat[session_id].sessionData = lectureInfo
                logger.debug('##damain response %s', bizRes)
                bizRes['resultMsg'] = ''
                bizRes['result'] = 'OK'
                bizRes['resultCode'] = 200
            else:
                bizRes['resultMsg'] = 'You must call the request function before(no session)'
                bizRes['result'] = 'Error'
                bizRes['resultCode'] = 2000001

            output = str(talk.query.utter)
            front_talk_answer = front_pb2.UtterResponse()
            front_talk_answer.utter = output
            result.answer.CopyFrom(front_talk_answer)
        except Exception as e:
            logger.debug(e.__doc__)
            logger.debug(e.message)
            logger.debug(traceback.format_exc())
            bizRes = {'resultCode': '3000002', 'resultMsg': e.message}

        res_meta = struct.Struct()
        res_meta['response'] = bizRes
        result.meta.CopyFrom(res_meta)
        logger.debug("result %s", result)
        # session 정보 ,session data 10k
        res_context = struct.Struct()
        res_context['session_data'] = '' #str(lectureInfo)

        result.context.CopyFrom(res_context)
        #logger.debug("context len %d", len(res_context))
        return result

    # 생성
    def Close(self, req, context):
        logger.debug( 'V3 Closing for %s %s', req.session_id, req.agent_key)
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.DialogueSessionKey()
        ses.session_key = req.session_id
        self.sds_stub.Close(ses)
        return talk_stat

    def EventT(self, empty, context):
        logger.debug( 'V3 DA Version 3 EventT called')
        # DO NOTHING
        return empty_pb2.Empty()

    def Event(self, req, context):
        logger.debug( 'V3 Event called')
        event_res = talk_pb2.EventResponse()
        event_res.code = 10
        event_res.reason = 'success'

        answer_meta = struct.Struct()
        answer_meta['meta1'] = 'meta_body_1'
        answer_meta['meta2'] = 'meta_body_2'
        event_res.meta.CopyFrom(answer_meta)

        answer_context = struct.Struct()
        answer_context['context1'] = 'context_body1'
        answer_context['context2'] = 'context_body2'
        answer_context['context3'] = 'context_body3'
        event_res.context.CopyFrom(answer_context)
        return event_res

def Server():
    parser = argparse.ArgumentParser(description='Da for English Education service')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    talk_pb2_grpc.add_DialogAgentProviderServicer_to_server(Da_Server(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    Server()
