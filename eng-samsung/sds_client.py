#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# 기본 모듈
from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
import os ,random
import logger
from common import DAError


# Path
exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.brain.sds import sds_pb2
from maum.brain.sds import sds_pb2_grpc
from maum.brain.sds import resolver_pb2
from maum.brain.sds import resolver_pb2_grpc
#from common.config import Config

class SdsAgent():
    def __init__(self):
        logger.debug('###sdsAgent __init__')
       # self.Get_sds_server()
        pass
    ####################################################################
    ### SDS 2.0
    ### lang: 한국어일 경우 0, 영어일 경우 1
    ### is_external: 외부 DB를 쓰는 경우 True, 내부 DB만 쓰는 경우 False
    ### model_list: 열고 싶은 모델들의 리스트
    #####################################################################
    lang = 1
    is_external = False
   # model_list = ['001_1']
   # sds_modelgroup_name ='english_writing1'  ## TODO: FIX ME!!
    sds_modelgroup_name ='english_education'  ## TODO: FIX ME!!

    # SDS Stub
    sds_server_addr = ''
    sds_stub = None
    turnModel = ''
    url = '0.0.0.0:9860'
    #url = '127.0.0.1:9860'
    #url = '10.122.64.10:9860'
    #url = '10.122.64.91:9906'

    @classmethod
    def Get_sds_server(self):
        logger.debug('url %s', self.url)
        sds_channel = grpc.insecure_channel(self.url)

        try:
            resolver_stub = resolver_pb2_grpc.SdsServiceResolverStub(sds_channel)
            ### For SDS 2.0
            MG = self.GetModelGroup()
            resolver_stub.CreateModelGroup(MG)
            server_status = resolver_stub.Find(MG)
            logger.debug('find server_status %s', server_status)

            for i in xrange(1,2):
             if server_status.state is resolver_pb2.SERVER_STATE_STARTING:
                 logger.debug("time.sleep %d %s", i, server_status.state)
                 time.sleep(0.5)

            # Create SpokenDialogService Stub
            logger.debug('find result server_status: %s', server_status)

            # SpokenDialogServiceInternalStub  english internal
            self.sds_stub = sds_pb2_grpc.SpokenDialogServiceInternalStub(
                grpc.insecure_channel(server_status.server_address))

            empty = empty_pb2.Empty()
            aML = self.sds_stub.GetAvailableModels(empty)
            logger.debug('aML.models len %d', len(aML.models))
            # for am in aML.models:
            #  logger.debug( 'available model list:', type(aML),aML.models
            for am in aML.models:
              #  logger.debug( am.name
                # Model.name = mn
                MP = resolver_pb2.ModelParam()
                MP.lang = self.lang
                MP.is_external = self.is_external
                MP.group_name = MG.name
                ML = resolver_pb2.ModelList()
                MP.model_name = am.name
                #Model = ML.models.add()
                resolver_stub.LinkModel(MP)

            self.sds_server_addr = server_status.server_address
            logger.debug('stub sds %s', server_status.server_address)
        except:
            logger.error('except %s ', sys.exc_info()[0])
            raise Exception('Error SDS Server not connect ')

    @classmethod
    def GetModelGroup(self):
          MG = resolver_pb2.ModelGroup()
          MG.name = self.sds_modelgroup_name  ## TODO: FIX ME!!
          MG.lang = self.lang
          MG.is_external = self.is_external
          return MG

    @classmethod
    def CreateModelGroup(self, MG):

         Model = resolver_pb2.Model()
         Model.lang = self.lang
         Model.is_external = self.is_external
         return Model
    #
    # DIALOG METHODS
    #
    @classmethod
    def CallSds(self, input_text, turnModel, session_id):
        self.turnModel = turnModel
        #######################################################
        ### SDS 2.0: model_list에 있는 모델을 1. 하나만 열거나,
        ### 2. 순차적으로 열거나,
        ### 3. 한번에 다 열수도 있다.
        ### 주어진 두 방법은 예시이며 모델을 열고 닫는 것은 충분히
        ### 조정할수 있다
        ########################################################
        empty = empty_pb2.Empty()
       # cML = self.sds_stub.GetCurrentModels(empty)
        logger.debug('current turnModel: %s', turnModel)
        dp = self.GetSDSParam(turnModel, session_id)
        logger.debug('sds session_id %s', session_id)
        OpenResult = self.sds_stub.Open(dp)
        logger.debug('OpenResult %s', OpenResult)
        sq = self.GetSdsQuery(dp, input_text)
        intent = self.sds_stub.Dialog(sq)  # internal용
        logger.debug('intent %s', intent)  ## SdsAct가 Intent로 바뀜
        entities = self.GetSdsEntities(dp)
        sds_utter = self.sds_stub.Dialog(entities) # intenal용
        logger.debug('sds_utter %s %s', sds_utter, type(sds_utter))

        self.sds_stub.Close(dp) # close 넣을지 확인
        if 'end' == intent.status :
            return True
        else:
            return False

    @classmethod
    def GetSdsEntities(self, dp):
        entities = sds_pb2.Entities()
        entities.session_key = dp.session_key
        entities.model = dp.model
        return entities

    @classmethod
    def GetSdsQuery(self, dp, inputText):
        sq = sds_pb2.SdsQuery()
        sq.model = dp.model
        sq.session_key = dp.session_key
        sq.apply_indri_score = 0
        sq.utter = inputText
        return sq

    @classmethod
    def GetSDSParam(self, tModel, s_id):
        dp = sds_pb2.DialogueParam()
        dp.model = tModel
        dp.session_key = s_id
        dp.user_initiative = True  ## user 가 먼저 발화하면 true, system발화가 먼저면 false
        logger.debug('dp %s', dp)
        return dp

    @classmethod
    def Close(self,  session_id):
        logger.debug('close session_id %s', session_id)
        ses = sds_pb2.DialogueParam()
        ses.model = self.turnModel
        ses.session_key = session_id
        self.sds_stub.Close(ses)

if __name__ == '__main__':
    sds = SdsAgent()
    sessionKey = 1111
    try:
        while True:
            modelList = ['001_1', '001_2','002_1','002_2','003_1','003_1']
           # modelList = ['001_1']
            for model in modelList:
              question = raw_input("user: ")
              response = sds.CallSds(question, model, sessionKey)
              logger.debug( 'response %s',response)
            sds.Close(sessionKey)
    except :
        logger.error('erro1 %s ', sys.exc_info()[0])
        sds.Close(sessionKey)
