#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function


import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from concurrent import futures
import argparse
import grpc
import os ,re
from google.protobuf import empty_pb2
from google.protobuf import struct_pb2 as struct
import time
import syslog

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.da.v3 import talk_pb2_grpc
from maum.m2u.da.v3 import talk_pb2
import random

class EngEduClient:
    _REX_NUM = re.compile('^(\d+)$')
    _ssession_id = random.randrange(100000,700000)
    _dev_user_id = 'user'+str(random.randrange(1,1000))+'@naver.com'
    _dev_model = 'ssh-'+str(random.randrange(1,1000))
    _dev_serial = str(random.randrange(700000,800000))

    def __init__(self):
        pass

    @classmethod
    def Talk(self, talk, port, ):
        channel = grpc.insecure_channel('localhost:'+port)
        stub = talk_pb2_grpc.DialogAgentProviderStub(channel)

        #front_pb2.descriptor_pb2 ='hello'
        # talk_pb2.descriptor_pb2 ='d'
        talkreq = talk_pb2.TalkRequest()
        talkreq.session_id = self._ssession_id
        talkreq.user.user_id = self._dev_user_id
        talkreq.device.serial = self._dev_serial
        talkreq.device.model = self._dev_model

        print('_ssession_id',self._ssession_id)
        talkreq.lang = 1
        talkreq.query.utter = talk
        # meta = struct.Struct()
        # meta['selectMenu'] = '1H1K1C1'
        # meta['actionType'] = 'enter'
        # talkreq.meta.CopyFrom(meta)

        res_context = struct.Struct()
        res_context['session_data'] = '_______sessionData__from_client_talk'
        talkreq.context.CopyFrom(res_context)
        response = stub.Talk(talkreq)
        return response

    @classmethod
    def DaOpenRequest(self, port,actionType):
        print ("client daOpenRequest ")
        channel = grpc.insecure_channel('localhost:'+port)
        stub = talk_pb2_grpc.DialogAgentProviderStub(channel)

        req = talk_pb2.OpenRequest()
        req.session_id = self._ssession_id
        req.user.user_id = self._dev_user_id
        req.device.serial = self._dev_serial
        req.device.model = self._dev_model

        req.meta['selectMenu'] = '1H4K1C1' #
        req.meta['actionType'] = actionType
        res_context = struct.Struct()
        res_context['session_data'] = '_______sessionData__from_client_open'
        req.context.CopyFrom(res_context)

        response = stub.Open(req)
        return response

    @classmethod
    def DaEventRequest(self, port):
        print ("client daEventRequest ")

        channel = grpc.insecure_channel('localhost:'+port)
        stub = talk_pb2_grpc.DialogAgentProviderStub(channel)
       # maum.m2u.facade.Devic
        req = talk_pb2.EventRequest()
        req.type ='11211'
        req.session_id = self._ssession_id
        print('_ssession_id',self._ssession_id)

        # req.device = "7ef1328abc2e493d8ee97f90f27b46cc"
        # req.user = "ljw76@naver.com"
        #req.meta  = {}
        # req.context = "context"
        response = stub.Event(req)
        return response

    @classmethod
    def UserTalk(self, port):
        print ("client userTalk ")
        contents = []
        param = {}
        _stdin_encoding = sys.stdin.encoding or 'utf-8'
        requestL = ['talk', 'next']
        param["lecture"] = ""
        param["transactionId"] = "TR_20170804083000_INWF_0123456789012345678901"
        #while True:
        try:
            lenth = 8*20
            for num in xrange(1,lenth):
                if num == 1:
                    talk ='open'
                elif (num % 2) == 0:
                    #talk ='talk'
                    talk = raw_input("").decode(_stdin_encoding)
                else:
                    talk = 'next'

                if 'open' == talk:
                    result = self.DaOpenRequest(port,'enter')
                elif 'next' == talk:
                    result = self.DaOpenRequest(port,'next')
                elif 'event' == talk:
                    result = self.DaOpenRequest(port,'')
                else:
                  result = self.Talk(talk, port)
                print('response ', result)
                print('num ', num,talk)
                   # break
            contents.append(param)
        except EOFError:
            print("error")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Da for English Education client')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()
    listen = '[::]' + ':' + str(args.port)
    EngEduClient.UserTalk(str(args.port))

