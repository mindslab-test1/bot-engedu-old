#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import logger ,copy
from common import Common
from eev_client import Eev
from lms_client import LmsClient
from sds_client import SdsAgent

class BizProcess:
    sds = None
    sds = SdsAgent()
    def __init__(self):
     pass

    @classmethod
    def CommandControl(self, eventOpen =False, uTalk='',select_menu='',actionType='',  openlecture=True):
        response = {}
        if Common.IsFindPrevious_ch(uTalk):
            response = LmsClient.SendLmsData(lecture='', moveType='PREV')
        elif Common.IsFindNxt_ch(uTalk) or actionType == 'next':
            response = LmsClient.SendLmsData(lecture=select_menu, moveType=actionType)
        elif select_menu:
            print '_lecture', select_menu
            response = LmsClient.SendLmsData(lecture=select_menu, moveType='')
        elif eventOpen:
            response = LmsClient.SendLmsData(lecture='1', moveType='')
        return response

    @classmethod
    def isNextLmsResearch(self, actionType, SessionObj):
        result = False
        if actionType == 'next':
               if 'isRepeatEnd' in SessionObj:
                   if SessionObj['isRepeatEnd'] == True:
                       result = True
        return result

    @classmethod
    def DaMain(self, param, SessionObj):
        sampleLecture = None
        isopenEvent = False
        turnType = ''
        currentInfo = {}
        resultlecinfo = {}
        userTalk = param['userTalk']
        result = {'resultCode': '', 'resultMsg': ''}
        lmsRepeatData = {}
        logger.info('daTalk %s', userTalk)

        menuId = param['selectMenu']
        actionType = param['actionType']
        isopenEvent = param['isOpenRequest']

        lectureInfo = {'turnType': 'ready'}
        #lectureInfo['actionType'] = actionType
        lectureInfo['sId'] = param['session_id']

        #open enter 이건 next 반복 인 아닌 경우 lms 조회
        if actionType == 'enter' or self.isNextLmsResearch(actionType, SessionObj):
            if actionType=='next':
                menuId = SessionObj['repeatData']['menuId']

            lectureInfo['repeatCount'] = 0
            lmsRepeatData = self.CommandControl(uTalk=userTalk, select_menu=menuId,actionType=actionType, openlecture=False)

            #network data vification
            if lmsRepeatData['resultCode'] == 200000:
                lectureInfo['repeatData'] = lmsRepeatData['repeatData']
            else:
                logger.info('LMSData null')
                result['resultCode'] = lmsRepeatData['resultCode']
                result['resultMsg'] =  lmsRepeatData['resultMsg']
                return (result, resultlecinfo)
        else:
            #이전 session에 정보가 있는 경우만 평가 서버를 준비
            if SessionObj:
                lectureInfo = SessionObj
            else:
                turnType = 'ready'

        if isopenEvent:
            lectureInfo['turnType'] = actionType
        else:
            lectureInfo['turnType'] = 'repeat'
        lectureInfo['userTalk'] = userTalk

        # data  처리 부분
        if 'ready' == turnType:
            result['state'] = turnType
            result['stat_message'] = 'please call open request'
            result['state_result'] = 'error'
            result['state_code'] = 300006
            lectureInfo = {'turnType': 'ready'}
            logger.debug('oneLog:#%s sid:%s %s %s \n',actionType,param['session_id'],menuId,result)

            return (result, resultlecinfo)
        else:
            #open ,next  case lms data receve

            (currentInfo,clientContent, eevResObj ,sdsResObj,renderCardType) = self.DoLogic(lectureInfo,SessionObj)
            result['clientContent'] = clientContent
            result['eevInfo'] = eevResObj
            result['sdsInfo'] = sdsResObj
            result['renderCardType'] = renderCardType
            result['result'] = 'success'
        return (result, currentInfo)

    # @classmethod
    # def sendfront(self, resdata):
    #     logger.debug('##send frontUrl:%s', resdata)

    @classmethod
    def CallSDS(self, input_text, turnModel, session_id):
        logger.debug('sesssion_id  %s ', session_id)

        #sds session은 one type이므로 한번만 사용하고 close
        session_id = 1
        return self.sds.CallSds(input_text, turnModel, session_id)
        # return random.choice(['success','fail']) #ses emulator

    @classmethod
    def ValidColum(self, valueObj, colume):
        if valueObj[colume]:
            res = valueObj
        else:
            res = ''
        return res

    @classmethod
    def DoLogic(self, currentInfo,sessionObj):

        sdsResObj = {}
        eevResObj = {}
        sdsResult =''
        renderCardType = ''
        mp3Url = []
        frontUrlList = {}
        EEVScore = ''
        EEVKeywordScore = ''
        EEVSentence = ''
        EEVWordscore = ''
        EEVKeywordResult = ''
        EEVResult = ''
        EEVResScore = ''
        feedBack = ''
        tts =''

        turnType = currentInfo['turnType']
      #  lecture = currentInfo['lecture']
        userTalk = currentInfo['userTalk']
        repeatCount = currentInfo['repeatCount']
        repeatData = currentInfo['repeatData']

        #lms conentsMapping
        repeatUrl = repeatData['repeatUrl']
        hint = repeatData['hint']
        menuId = repeatData['menuId']
        answerText = repeatData['answerText'].replace(',',' ').replace('.',' ').replace('!',' ')
        answerPoint = repeatData['answerPoint']
        keywordPosition = repeatData['keywordPosition']
        maxrepeatCount =  repeatData['maxRepeatCount']
        finishType  =  repeatData['finishType']

        clientContent = {}
        textList = []
        picureList = []
        teacherTextList = []
        repeatUrlList = []
        isRepeatEnd = False
        answerLoadBlankNum = int(repeatData['answerLoadBlankNum'])

        logger.debug('menuId %s before#turnType:%s  hintLoadNum:%s  maxrepeatCount %s repeatCount %s runMenuType %s',menuId,turnType,int(repeatData['hintLoadNum']),maxrepeatCount,repeatCount,repeatData['runMenuType'] )
        chepterNum, lectureNum, cswNum, cwsType, turnNum = Common.ParseRepeatType(menuId)

        #open 인경우  clinet 정보만 전송
        if 'enter' == turnType or 'next' == turnType:

            for i in xrange(1,5):
                textTemp = 'text'+str(i)
                if repeatData[textTemp]:
                    textList.append(repeatData[textTemp])

            if answerLoadBlankNum > 0 and repeatCount < answerLoadBlankNum:
                  textList[len(textList)-1] = ""

            clientContent['text'] = textList

            #마지막 앞 hint
            if repeatCount+1 == int(repeatData['hintLoadNum']):
                clientContent['hint'] = repeatData['hint']
            else:
                clientContent['hint'] = ''

            if repeatData['picture']:
               picureList.append(repeatData['picture'])
            clientContent['picture'] = picureList

            if repeatData['teacherText']:
               teacherTextList.append(repeatData['teacherText'])
            clientContent['teacherText'] = teacherTextList

            if repeatData['tts']:
               tts = repeatData['tts']
            clientContent['tts'] = tts

            if repeatData['repeatUrl']:
               repeatUrlSplit = repeatData['repeatUrl'].split(',')

               if repeatData['runMenuType']=='clue' and repeatCount == 0:
                   for cnt in xrange(1,4):
                     repeatUrlList.append(repeatUrlSplit[0])
               elif repeatData['runMenuType'] == 'sentence' and repeatCount == 0:
                   pass
               else:
                   repeatUrlList = repeatUrlSplit

                   # if repeatData['runMenuType'] == 'clue' and repeatCount == 0:
                   #     for cnt in xrange(1, 4):
                   #         repeatUrlList.append(repeatUrlSplit[0])
                   # elif repeatData['runMenuType'] == 'sentence' and repeatCount == 0:
                   #     pass
                   # elif repeatData['runMenuType'] == 'sentence' and repeatCount == 1:
                   #     repeatUrlList.append('Try_it_again.mp3')
                   # else:
                   #     repeatUrlList = repeatUrlSplit

            clientContent['mp3Url'] = repeatUrlList
            clientContent['currentMenu'] = menuId
        # 따라하기
        elif 'repeat' == turnType:
            repeatCount += 1
            tempTalk = userTalk
            if menuId == '1H4K1C3':
                find4Word,otherWord = Common.Get4Word(tempTalk.replace("\'m"," am"))
                if find4Word:
                    userTalk = find4Word

            param = {'expected': answerText, 'utter': userTalk, 'score': answerPoint, 'keyword_pos': keywordPosition}
            (EEVScore, EEVKeywordScore,EEVSentence,EEVWordscore, EEVResult,EEVResScore) = Eev.Evaluation(param) #Eev.Evaluation(param)

            if menuId == '1H4K1C3':
                answerText = answerText + otherWord
                userTalk = tempTalk

            eevResObj['rCount'] = repeatCount
            eevResObj['userText'] = userTalk
            eevResObj['expectedText'] = answerText
            eevResObj['eevScore'] = EEVScore
            eevResObj['eevKeywordScore'] = EEVKeywordScore
            eevResObj['eevWordscore'] = EEVWordscore
            #eevResObj['eevKeywordResult'] = EEVKeywordResult
            eevResObj['answerPoint'] = answerPoint
            eevResObj['result'] = EEVResult
            eevResObj['finishType'] = 'type0'
            eevResObj['currentMenu'] = menuId

            if repeatData['runMenuType'] == 'conversation':
                eevResObj['finishType'] = finishType

            # repeatCount = 0
            if EEVResult == 'pass' or repeatCount >= int(maxrepeatCount):
                isRepeatEnd = True
                if menuId == '1H4K1C3':
                   eevResObj['finishType'] = 'type2'

            logger.debug('eevResult %s', eevResObj)
            renderCardType = 'score_card'

        if 'ready' == turnType:
            print  currentInfo['turnType']

        feedbackCodeList = []
        feedbackCodeList = Common.GetFeedbackCode(mp3Url)
        if 'pf_card' in sdsResObj:
           sdsResult =  sdsResObj['pf_card']

        currentInfo['repeatCount'] = repeatCount
        currentInfo['isRepeatEnd'] = isRepeatEnd

        # logger.debug('oneLog:#turnType:%s repeatCount:%s eevScore:%s eevKeyScore:%s eevresult:%s pf_card:%s  aText:%s uTalk:%s feedCode%s menuId:%s Url:%s tts:%s finishType:%s textList:%s pic:%s\n',
        #              turnType, repeatCount, EEVScore, EEVKeywordScore, EEVResult, sdsResult, answerText, userTalk, feedbackCodeList ,menuId,repeatUrlList,tts,finishType,textList,picureList)
        logeevResObj = copy.deepcopy(eevResObj)
        if 'eevWordscore' in  logeevResObj:
            del logeevResObj['eevWordscore']

        # if turnType != 'repeat':
        #     turnType = currentInfo['actionType']

        logger.debug('oneLog:#%s sid:%s %s rCount:%s %s %s eevResScore %s',turnType,currentInfo['sId'],menuId,repeatCount,clientContent,logeevResObj,EEVResScore)
        return currentInfo,clientContent ,eevResObj ,sdsResObj ,renderCardType

if __name__ == '__main__':
    print '_main__'
    BizProcess.daTalk()
