#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import re, random ,json
from datetime import datetime


reload(sys)
sys.setdefaultencoding('utf-8')

class DAError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class Common :
        _REX_CUR_POSITION = re.compile('^(\d+)-(\d+)-(\w+)-(\d+)$')
        _REX_SAMSUNG_REPEAT_NAME = re.compile('^(\d+)H(\d+)K(\d+)([CWS])(\d+)$')
        _REX_I_AM_GOING_TO = re.compile('^(\w+\s)(\w+\s)(\w+\s)(\w+)(.+)')

        _lectureInfo = {'turnType': 'ready'}
        _QUIZ_NUM = {'startQuiz': 1, 'engdingQuiz': 2}
        _LECTURE_NUM = re.compile('^(\d+)$')
        _FEEDBACK_CODE = re.compile("1000000005\/(.+)\.mp3")

        with open('command_words.json') as data_file:
            COMMAND_WORDS = json.load(data_file)
        #print COMMAND_WORDS

        def __init__(self):
            # self.tlo_logger = TLO()
            # self.kd_logger = KD_LOG()
            # self.loggerger = logger()
            pass

        @classmethod
        def Time_check(self, type=1):
            if type == 1:
                return datetime.today().strftime("%Y%m%d%H%M%S%f")[:-3]
            elif type == 0:
                # meta의 seq_id 생성
                return datetime.today().strftime("%Y%m%d%H%M%S%f")[:-3] + str(random.random())[2:6]

        @classmethod
        def GetQuizNumtype(self, quiz):
            quiz = quiz.replace('_c','')
            if 'startQuiz' == quiz:
                return '1'
            elif 'endingQuiz' == quiz:
                return '2'
            else:
                return '0'

        @classmethod
        def ConvertTFtoSF(self, data):
            if data:
                return 'pass'
            else:
                return 'fail'

        @classmethod
        def isCommandFind(self, comand, userTalk):
            resFind = ''
           # print self.COMMAND_WORDS[comand]['regex']
            for item in self.COMMAND_WORDS[comand]['regex']:
               # print('item',item)
                reg = re.compile(item)
                findSearch = reg.search(userTalk)
                if findSearch:
                    resFind = findSearch.group()
                    break
            return resFind

        @classmethod
        def IsFindStart_ch(self, userTalk):
            return self.isCommandFind('start_ch', userTalk)

        @classmethod
        def IsFindStop_class(self, userTalk):
          return self.isCommandFind('stop_class', userTalk)

        @classmethod
        def IsFindPause_class(self, userTalk):
            return self.isCommandFind('pause_class', userTalk)

        @classmethod
        def IsFindContinue_class(self, userTalk):
             return self.isCommandFind('continue_class', userTalk)

        @classmethod
        def IsFindPrevious_ch(self, userTalk):
            return self.isCommandFind('previous_ch', userTalk)

        @classmethod
        def IsFindNxt_ch(self, userTalk):
            return self.isCommandFind('nxt_ch', userTalk)

        @classmethod
        def IsYESFind(self, userTalk):
            return self.isCommandFind('yes', userTalk)

        @classmethod
        def IsNoFind(self, userTalk):
            return self.isCommandFind('no', userTalk)

        @classmethod
        def IsSwearFind(self, userTalk):
            return self.isCommandFind('swear_word', userTalk)

        @classmethod
        def Get4Word(self, userTalk):
            talk =''
            group5 = ''
            matchObj = self._REX_I_AM_GOING_TO.search(userTalk)
            if matchObj:
                talk = matchObj.group(1) + matchObj.group(2) + matchObj.group(3) + matchObj.group(4)
                group5 = matchObj.group(5)
            return talk , group5

        @classmethod
        def GetParserlectureNum(self, userTalk):
            lectureNumber = ''
            regLec = self._LECTURE_NUM.search(userTalk)
            if regLec:
                lectureNumber = regLec.group()
                print lectureNumber
            return lectureNumber

        @classmethod
        def GetparseCurPosition(self, currentPosition):
            lecture = ''
            turnType = ''
            quiz_num = ''
            repeatTurnNum = ''
            matchObj = Common._REX_CUR_POSITION.search(currentPosition.replace(' ',''))
            if matchObj:
                lecture = matchObj.group(2)
                turnType = matchObj.group(3)
                quiz_num = Common.GetQuizNumtype(matchObj.group(3))
                repeatTurnNum = matchObj.group(4)
            return (lecture ,turnType ,quiz_num,repeatTurnNum)

        @classmethod
        def ParseRepeatType(self, repeatName):
            repeatType = ''
            matchObj = Common._REX_SAMSUNG_REPEAT_NAME.search(repeatName)
            if matchObj:
                print matchObj
                repeatType = matchObj.group(1),matchObj.group(2),matchObj.group(3),matchObj.group(4),matchObj.group(5)
            return repeatType

        @classmethod
        def GetFeedbackCode(self, urlList):
            feedbackCodeList = []
            for url in urlList:
                matchObj = Common._FEEDBACK_CODE.search(url)
                if matchObj:
                    feedbackCodeList.append(matchObj.group(1))
            return feedbackCodeList



if __name__ == '__main__':
    print '_main__'
    request = {}
    try :
         while True:
             request['userTalk']= raw_input("input Swear: ")
             #res = Common.isSwearFind(request)
             #res = Common.isCommandFind('swear_word', request)
             #res = Common.getParserlectureNum(request['userTalk'])
             #res = Common.isSwearFind(request['userTalk'])
             res = Common.IsFindStart_ch(request['userTalk'])
             print 'find',res

    except:
        print('%s ', sys.exc_info()[0])
